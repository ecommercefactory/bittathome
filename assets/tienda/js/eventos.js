$(document).ready(function(){
	// Slide toggle btn checkout agregar empresa
	$( ".btn-agregar-empresa" ).click(function() {
	  	$( ".datos-empresa" ).slideToggle( "slow" );
	});
	// Slide toggle btn checkout agregar empresa fin

	// Slide toggle btn buscador
	$( ".btn-buscador-movil" ).click(function() {
	  	$( ".buscador" ).slideToggle( "slow" );
	});
	// Slide toggle btn buscador

	// TABS checkout
	$("body").on("click",".tabbers .tab",function(e){

			  $(".tabbers .tab").removeClass("tab-active");
			  $(this).addClass("tab-active");
			  var tabTarg = $(this).data("target");

			  $(".tabs-cont .tab-c").css("display", "none");
			  $("#"+tabTarg).css("display", "block");
	});
	// TABS checkout FIN

	// Js boton ir al header
	$('a[href*=\\#]').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
			&& location.hostname == this.hostname) {

				var $target = $(this.hash);
				$target = $target.length && $target || $('[name=' + this.hash.slice(1) +']');

		if ($target.length) {
				var targetOffset = $target.offset().top;
				$('html,body').animate({scrollTop: targetOffset}, 1000);
				return false;
			}
		}
	});
	// Js boton ir al header fin

	//Js para mostrar boton top
	$(window).scroll(function(){
		if ($(window).scrollTop() > 300)
		    {
		        $("#top").addClass("add-top");
		    }
		else
		    {
		        $("#top").removeClass("add-top");
		    }
	});
	//Js para mostrar boton top fin

	// ANIMACIÓN MENU ICONO
	var text = document.getElementById("text-menu");
	function toggleClass(element, className){
	    if (!element || !className){
	        return;
	    }

	    var classString = element.className;
	    var nameIndex = classString.indexOf(className);

	   
	    if (nameIndex == -1) {
	        classString += ' ' + className;
	    } else {
	        classString = classString.substr(0, nameIndex) + classString.substr(nameIndex+className.length);
	    }
	    element.className = classString;
	}

/*
    document.getElementById('btn-menu-responsive').addEventListener('click', function() {
        toggleClass(this, 'close');
        text.innerHTML == "" ? text.innerHTML = "" : text.innerHTML = "";
    });
*/

	// ANIMACIÓN MENU ICONO FIN

	// MENU RESPONSIVE
		var accion ="dentro";
	    $("#btn-menu-responsive").click(function(){
		    if (accion == "dentro"){
		        $("#menu").addClass("add-menu");
		        accion = "fuera";
		    }
		    else{
		        $("#menu").removeClass("add-menu");
		        accion = "dentro";
		    }
	    });
	// MENU RESPONSIVE FIN

	 //ACCORDION TERMINOS
	$(function() {
	    var Accordion = function(el, multiple) {
	        this.el = el || {};
	        this.multiple = multiple || false;

	        // Variables privadas
	        var links = this.el.find('.link');
	        // Evento
	        links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
	    }

	    Accordion.prototype.dropdown = function(e) {
	        var $el = e.data.el;
	            $this = $(this),
	            $next = $this.next();

	        $next.slideToggle();
	        $this.parent().toggleClass('open');

	        if (!e.data.multiple) {
	            $el.find('.submenu-a').not($next).slideUp().parent().removeClass('open');
	        };
	    }   

	    var accordion = new Accordion($('#accordion'), false);
	});
	// ACCORDION TERMINOS FIN

	//MENU FIXED down
	$(window).scroll(function(){
		if ($(window).scrollTop() > 300)
		{
		    $(".cont-header").addClass("add-header-fixed");
		    $("#content").addClass("add-content");
		}
		else
		{
		    $(".cont-header").removeClass("add-header-fixed");
		    $("#content").removeClass("add-content");
		}
	});
	//MENU down fin
	
});


