<?php
$mysqli_user = 'solucio4_tienda15';
$mysqli_pass = 'David1974';
$mysqli_database = 'solucio4_tienda15';
$mysqli_host = 'localhost';

// DB table to use
$table = 'productos';

// Table's primary key
$primaryKey = 'id';

// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
	array( 'db' => 'id', 'dt' => 0, 'field' => 'id' ),
	array( 'db' => 'titulo',  'dt' => 1, 'field' => 'titulo' ),
	array( 'db' => 'descripcion',   'dt' => 2, 'field' => 'descripcion' ),
	array( 'db' => 'multimedia',   'dt' => 3, 'field' => 'multimedia' ),
	array( 
      'db' => 'portada',   
      'dt' => 4,  
      'field' => 'portada', 
      'formatter' => function($d, $row) {
        return '<img src="https://www.solucionaticos.com/backend/' . $d . '" width="100px">';
      }
  )
);

$sql_details = array(
	'user' => $mysqli_user,
	'pass' => $mysqli_pass,
	'db'   => $mysqli_database,
	'host' => $mysqli_host
);

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

require ("ajax/ssp.customized.class.php");

$joinQuery = "FROM productos";
$extraWhere = ""; //"`u`.`salary` >= 90000";
$groupBy = ""; //"`u`.`office`";
$having = ""; //"`u`.`salary` >= 140000";

echo json_encode(
	SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having )
);
