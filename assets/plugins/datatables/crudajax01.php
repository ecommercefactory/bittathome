<?php
$servername = "localhost";
$username = "solucio4_tienda15";
$password = "David1974";
$dbname = "solucio4_tienda15";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
if ( isset($_POST['ajax']) ) {
  
    if ( $_POST['ajax'] == 'actualizarCampo') {
        $sql = "UPDATE productos SET $_POST[campo]='$_POST[valor]' WHERE id='$_POST[id]'";
        if ($conn->query($sql) === TRUE) {
            switch ($_POST['campo']) {
                case 'titulo':
                    //echo $_POST['valor'];
                    break;
                case 'fecha':
                    //echo $_POST['valor'];
                    break;
                case 'precio':
                    //echo $_POST['valor'];
                    break;
                case 'portada':
                    //echo $_POST['valor'];
                    break;
                case 'descripcion':
                    if (strlen($_POST['valor']) > 30) {
                        $_POST['valor'] = substr($_POST['valor'],0,27) . "...";
                    }
                    break;
                case 'id_categoria':
                    $sqlCategoria = "SELECT categoria FROM categorias WHERE id='$_POST[valor]'";
                    $resultCategoria = $conn->query($sqlCategoria);
                    if ($resultCategoria->num_rows > 0) {
                        $rowCategoria = $resultCategoria->fetch_assoc();
                        $_POST['valor'] = $rowCategoria["categoria"];
                    } else {
                        $_POST['valor'] = "Error: no encontrado.";
                    }
                    break;
            }
            echo $_POST['valor'];
        } else {
            echo "Error: " . $conn->error;
        }
        $conn->close();      
    }
  
    if ( $_POST['ajax'] == 'editarCampo') {

        $tipo = '';
        $valor = '';
        $codigos = array();
        $valores = array();

        $sql = "SELECT $_POST[campo] FROM productos WHERE id = '$_POST[id]'";
        $pre = $conn->query($sql);
        if ($pre->num_rows > 0) {
            $reg = $pre->fetch_assoc();
            switch ($_POST['campo']) {
                case 'titulo':
                    $tipo = 'texto';
                    $valor = $reg[$_POST['campo']];
                    break;
                case 'fecha':
                    $tipo = 'fechaLarga';
                    $valor = $reg[$_POST['campo']];
                    break;
                case 'precio':
                    $tipo = 'numero';
                    $valor = $reg[$_POST['campo']];
                    break;
                case 'portada':
                    $tipo = 'texto';
                    $valor = $reg[$_POST['campo']];
                    break;
                case 'descripcion':
                    $tipo = 'area';
                    $valor = $reg[$_POST['campo']];
                    break;
                case 'id_categoria':
                    $tipo = 'lista';
                    $valor = $reg[$_POST['campo']];
                    $sqlCategoria = "SELECT id, categoria FROM categorias ORDER BY categoria";
                    $resultCategoria = $conn->query($sqlCategoria);
                    if ($resultCategoria->num_rows > 0) {
                        while ($rowCategoria = $resultCategoria->fetch_assoc()) {
                            $codigos[] = $rowCategoria["id"];
                            $valores[] = $rowCategoria["categoria"];
                        }
                    }
                    break;
            }
        }

        $datos = array(
            'tipo' => $tipo, 
            'valor' => $valor,
            'codigos' => $codigos,
            'valores' => $valores
        );

        echo json_encode($datos);       
    }
  
    die();
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>CRUD Ajax 01</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="Bootstrap-3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css">
  
    <script type="text/javascript" src="jQuery-1.12.4/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="Bootstrap-3.3.7/js/bootstrap.min.js"></script>
    <!-- https://www.malot.fr/bootstrap-datetimepicker/index.php -->
    <script type="text/javascript" src="bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>

    <style type="text/css">
        tbody tr td {
            white-space: nowrap;
        }
        .tdSeleccion {
            width:35px !important;
        }
        .tdBotones {
            width:35px !important;
        }
        .campoEditar:hover {color:blue;cursor: pointer;}
        .campoTexto {
            width: 200px !important;
        }
        .campoNumero {
            width: 70px !important;
        }
        .campoArea {
            width: 300px !important;
        }
        .campoLista {
            width: 160px !important;
        }
        .campoFechaLarga {
              width: 160px !important;
         }
        .glyphicon.normal-right-spinner {
            -webkit-animation: glyphicon-spin-r 2s infinite linear;
            animation: glyphicon-spin-r 2s infinite linear;
        }      
        @-webkit-keyframes glyphicon-spin-r {
            0% {
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
            }

            100% {
                -webkit-transform: rotate(359deg);
                transform: rotate(359deg);
            }
        }

        @keyframes glyphicon-spin-r {
            0% {
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
            }

            100% {
                -webkit-transform: rotate(359deg);
                transform: rotate(359deg);
            }
        } 
    </style>

</head>
<body>

    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <h3 class="text-primary">CRUD Ajax 01</h3>
            </div>
        </div>

        <div class="panel panel-primary">
            <div class="panel-heading">Lista de datos</div>
            <div class="panel-body">

                <div class="btn-groupx" style="margin-bottom: 10px;">
                    <button type="button" class="btn btn-success" id="btnNuevo"><span class="glyphicon glyphicon-plus"></span> Nuevo</button>
                    <button type="button" class="btn btn-primary" id="btnEditar"><span class="glyphicon glyphicon-pencil"></span> Editar</button>
                    <button type="button" class="btn btn-danger" id="btnEliminar"><span class="glyphicon glyphicon-trash"></span> Eliminar</button>
                </div>
              
                <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover table-condensed">
                    <thead>
                        <tr>
                            <th class="tdSeleccion" style="text-align: center;"><p class="text-center" style="text-align: center;"><input type="checkbox" id="seleccionarTodos"></p></th>
                            <th class="tdBotones"></th>
                            <th class="tdBotones"></th>
                            <th>Titulo</th><th>Descripcion</th><th>Precio</th><th>Portada</th><th>Fecha</th><th>Categoria</th>
                        </tr>
                    </thead>
<?php
$sql = "SELECT id, titulo, descripcion, precio, portada, fecha, id_categoria FROM productos LIMIT 10";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    echo '<tbody>';
    while($row = $result->fetch_assoc()) {
        echo '<tr>';

        echo '<td class="text-center">';
        echo '<input type="checkbox" id="fila_'. $row["id"] .'" class="seleccion" cod="'. $row["id"] .'">';
        echo '</td>';

        echo '<td class="text-center tdBotones">';
        echo '<button type="button" class="btn btn-primary btn-xs btnEditar" cod="'. $row["id"] .'">';
        echo '<span class="glyphicon glyphicon-pencil"></span>';
        echo '</button>';
        echo '</td>';

        echo '<td class="text-center tdBotones">';
        echo '<button type="button" class="btn btn-danger btn-xs btnEliminar" cod="'. $row["id"] .'">';
        echo '<span class="glyphicon glyphicon-trash"></span>';
        echo '</button>';
        echo '</td>';

        echo '
          <td>
            <span id="td_titulo_'. $row["id"] .'">'. $row["titulo"] .'</span>
            <span id="td_titulo_'. $row["id"] .'_accion"></span>
            <span id="td_titulo_'. $row["id"] .'_editar">&nbsp;<a href="" class="campoEditar" campo="titulo" cod="'. $row["id"] .'"><span class="glyphicon glyphicon-pencil"></span></a></span>
          </td>';
      
        if (strlen($row["descripcion"]) > 30) {
            $row["descripcion"] = substr($row["descripcion"],0,27) . "...";
        }

        echo '
          <td>
            <span id="td_descripcion_'. $row["id"] .'">'. $row["descripcion"] .'</span>
            <span id="td_descripcion_'. $row["id"] .'_accion"></span>
            <span id="td_descripcion_'. $row["id"] .'_editar">&nbsp;<a href="" class="campoEditar" campo="descripcion" cod="'. $row["id"] .'"><span class="glyphicon glyphicon-pencil"></span></a></span>
          </td>';
      
        echo '
          <td>
            <span id="td_precio_'. $row["id"] .'">'. $row["precio"] .'</span>
            <span id="td_precio_'. $row["id"] .'_accion"></span>
            <span id="td_precio_'. $row["id"] .'_editar">&nbsp;<a href="" class="campoEditar" campo="precio" cod="'. $row["id"] .'"><span class="glyphicon glyphicon-pencil"></span></a></span>
          </td>';
      
        echo '
          <td>
            <span id="td_portada_'. $row["id"] .'">'. $row["portada"] .'</span>
            <span id="td_portada_'. $row["id"] .'_accion"></span>
            <span id="td_portada_'. $row["id"] .'_editar">&nbsp;<a href="" class="campoEditar" campo="portada" cod="'. $row["id"] .'"><span class="glyphicon glyphicon-pencil"></span></a></span>
          </td>';
      
        echo '
          <td>
            <span id="td_fecha_'. $row["id"] .'">'. $row["fecha"] .'</span>
            <span id="td_fecha_'. $row["id"] .'_accion"></span>
            <span id="td_fecha_'. $row["id"] .'_editar">&nbsp;<a href="" class="campoEditar" campo="fecha" cod="'. $row["id"] .'"><span class="glyphicon glyphicon-pencil"></span></a></span>
          </td>';      

        $sqlCategoria = "SELECT categoria FROM categorias WHERE id = '$row[id_categoria]'";
        $resultCategoria = $conn->query($sqlCategoria);
        if ($resultCategoria->num_rows > 0) {
            $rowCategoria = $resultCategoria->fetch_assoc();
            $row["id_categoria"] = $rowCategoria["categoria"];
        }

        echo '
          <td>
            <span id="td_id_categoria_'. $row["id"] .'">'. $row["id_categoria"] .'</span>
            <span id="td_id_categoria_'. $row["id"] .'_accion"></span>
            <span id="td_id_categoria_'. $row["id"] .'_editar">&nbsp;<a href="" class="campoEditar" campo="id_categoria" cod="'. $row["id"] .'"><span class="glyphicon glyphicon-pencil"></span></a></span>
          </td>';      

        echo '</tr>';
    }
    echo '</tbody>';
}
$conn->close();
?>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th>Titulo</th><th>Descripcion</th><th>Precio</th><th>Portada</th><th>Fecha</th><th>Categoria</th>
                        </tr>
                    </tfoot>
                </table>
                </div>
            </div>
        </div>
    </div>

  
  <!-- Modal Nuevo - Inicio -->
  <div class="modal fade" id="modalNuevo" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">×</button>
          <h4 class="modal-title">Nuevo</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" action="/action_page.php">
              <div class="form-group">
                <label class="control-label col-sm-2" for="email">Email:</label>
                <div class="col-sm-10">
                  <input type="email" class="form-control" id="email" placeholder="Enter email">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-2" for="pwd">Password:</label>
                <div class="col-sm-10"> 
                  <input type="password" class="form-control" id="pwd" placeholder="Enter password">
                </div>
              </div>
              <div class="form-group"> 
                <div class="col-sm-offset-2 col-sm-10">
                  <div class="checkbox">
                    <label><input type="checkbox"> Remember me</label>
                  </div>
                </div>
              </div>
              <div class="form-group"> 
                <div class="col-sm-offset-2 col-sm-10">
                  <button type="submit" class="btn btn-default">Submit</button>
                </div>
              </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>  
  <!-- Modal Nuevo - Fin -->

  <!-- Modal Editar - Inicio -->
  <div class="modal fade" id="modalEditar" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">×</button>
          <h4 class="modal-title">Editar</h4>
          <input type="hidden" id="codigoEditar" value="">
        </div>
        <div class="modal-body">
            <form class="form-horizontal" action="/action_page.php">
              <div class="form-group">
                <label class="control-label col-sm-2" for="email">Email:</label>
                <div class="col-sm-10">
                  <input type="email" class="form-control" id="email" placeholder="Enter email">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-2" for="pwd">Password:</label>
                <div class="col-sm-10"> 
                  <input type="password" class="form-control" id="pwd" placeholder="Enter password">
                </div>
              </div>
              <div class="form-group"> 
                <div class="col-sm-offset-2 col-sm-10">
                  <div class="checkbox">
                    <label><input type="checkbox"> Remember me</label>
                  </div>
                </div>
              </div>
              <div class="form-group"> 
                <div class="col-sm-offset-2 col-sm-10">
                  <button type="submit" class="btn btn-default">Submit</button>
                </div>
              </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>  
  <!-- Modal Editar - Fin -->
  
  <!-- Modal Eliminar - Inicio -->
  <div class="modal fade" id="modalEliminar" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">×</button>
          <h4 class="modal-title">Confirmación de Eliminar Registro</h4>
          <input type="hidden" id="codigoEliminar" value="">
        </div>
        <div class="modal-body">
          <p class="lead text-danger">Esta seguro de eliminar el regitro? <button type="button" class="btn btn-lg btn-danger">Si, eliminar el registro</button></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>  
  <!-- Modal Eliminar - Fin -->
  
  
	<script type="text/javascript">
		$(function() {
            // $('.campoFechaLarga').datetimepicker();
      
            $("#seleccionarTodos").click(function () {
                var checkedCtr = $(this).prop("checked");
                $(".seleccion").each(function () {
                    $(this).prop("checked", checkedCtr);
                });
            });

            $(".campoEditar").click(function () {
                var campo = $(this).attr("campo");
                var id = $(this).attr("cod");
                $("#td_"+campo+"_"+id).css("display","none");
                $("#td_"+campo+"_"+id+"_editar").css("display","none");
                $("#td_"+campo+"_"+id+"_accion").html("<span class='glyphicon glyphicon-repeat normal-right-spinner'></span>");
                editarCampo(campo, id);
                return false;
            });

            function editarCampo(campo, id) {
                var valor = "";
                $.ajax({
                    url: "crudajax01.php",
                    data: {
                        id: id,
                        campo: campo,
                        ajax: "editarCampo"
                    },
                    type: "post",
                    dataType: "json",
                    success: function(datos) {
                        var proceso = true;
                        valor = "<div class='input-group'>";
                        switch(datos["tipo"]) {
                            case "texto":
                                valor += "<input type='text' class='form-control campoTexto input-sm' id='"+campo+"_"+id+"' value='"+datos["valor"]+"'>";
                                break;
                            case "numero":
                                valor += "<input type='text' class='form-control campoNumero input-sm' id='"+campo+"_"+id+"' value='"+datos["valor"]+"'>";
                                break;
                            case "fechaLarga":
                                valor += "<input type='text' class='form-control campoFechaLarga input-sm' id='"+campo+"_"+id+"' value='"+datos["valor"]+"'>";
                                break;
                            case "clave":
                                valor += "<input type='password' class='form-control input-sm' id='"+campo+"_"+id+"' value='"+datos["valor"]+"'>";
                                break;
                            case "area":
                                valor += "<textarea class='form-control campoArea input-sm' rows='5' id='"+campo+"_"+id+"'>"+datos["valor"]+"</textarea>";
                                break;
                            case "lista":
                                valor += "<select class='form-control campoLista input-sm' id='"+campo+"_"+id+"'>";
                                var i = 0;
                                var selected = "";
                                for (i=0; i<datos["valores"].length; i++) {
                                    selected = "";
                                    if (datos["codigos"][i] == datos["valor"]) {
                                        selected = " selected";
                                    }
                                    valor += "<option value='"+datos["codigos"][i]+"'"+selected+">"+datos["valores"][i]+"</option>";
                                }
                                valor += "</select>";
                                break;
                            default:
                                valor += "Error";
                                proceso = false;
                        }
                        if (proceso) {
                            valor += "<div class='input-group-btn' style='vertical-align:top;'>";
                            valor += "<button class='btn btn-default btn-sm cancelarAccion' type='button' campo='" + campo + "' cod='" + id + "'><i class='glyphicon glyphicon-remove'></i></button>";
                            valor += "<button class='btn btn-success btn-sm actualizarAccion' type='button' campo='" + campo + "' cod='" + id + "'><i class='glyphicon glyphicon-ok'></i></button>";
                            valor += "</div>";
                        }
                        valor += "</div>";
                        $("#td_"+campo+"_"+id+"_accion").html(valor);
                        if (datos["tipo"] == "fechaLarga") {
                            $('.campoFechaLarga').datetimepicker({
                              format: "yyyy-mm-dd hh:ii:ss" 
                            });    
                        }
                    }
                });
            }
      
            $(document).on("click", ".cancelarAccion", function() {
                var campo = $(this).attr("campo");
                var id = $(this).attr("cod");
                $("#td_"+campo+"_"+id+"_accion").html("");
                $("#td_"+campo+"_"+id).css("display","inline");
                $("#td_"+campo+"_"+id+"_editar").css("display","inline");                
            });
      
            $(document).on("click", ".actualizarAccion", function() {
                $("#td_"+campo+"_"+id+"_accion").html("<span class='glyphicon glyphicon-repeat normal-right-spinner'></span>");

                var campo = $(this).attr("campo");
                var id = $(this).attr("cod");
                var valor = $("#"+campo+"_"+id).val();
              
                $.ajax({
                    url: "crudajax01.php",
                    data: {
                        id: id,
                        campo: campo,
                        valor: valor,
                        ajax: "actualizarCampo"
                    },
                    type: "post",
                    success: function(datos) {
                      $("#td_"+campo+"_"+id+"_accion").html("");
                      $("#td_"+campo+"_"+id).html(datos);
                      $("#td_"+campo+"_"+id).css("display","inline");
                      $("#td_"+campo+"_"+id+"_editar").css("display","inline");                          
                    }
                });
            });

            $("#btnNuevo").click(function(){
                $("#modalNuevo").modal({backdrop: "static"});
            });
            $("#btnEditar").click(function(){
                var cod ="";
                $(".seleccion").each(function () {
                    if ($(this).prop("checked")) {
                        cod = $(this).attr("cod");
                        return false;
                    }
                });
                if (cod != "") {
                    $("#codigoEditar").val(cod);
                    $("#modalEditar").modal({backdrop: "static"});
                } else {
                    alert("Debe seleccionar un registro");
                }
            });
            $(document).on("click", ".btnEditar", function(){
                var cod ="";
                cod = $(this).attr("cod");
                $("#codigoEditar").val(cod);
                $("#modalEditar").modal({backdrop: "static"});
            });
            $("#btnEliminar").click(function(){
                var cod ="";
                $(".seleccion").each(function () {
                    if ($(this).prop("checked")) {
                        cod += $(this).attr("cod") + ";";
                    }
                });
                if (cod != "") {
                    $("#codigoEliminar").val(cod);
                    $("#modalEliminar").modal({backdrop: "static"});
                } else {
                    alert("Debe seleccionar al menos un registro");
                }
            });
            $(document).on("click", ".btnEliminar", function(){
                var cod ="";
                cod = $(this).attr("cod");
                $("#codigoEliminar").val(cod);
                $("#modalEliminar").modal({backdrop: "static"});
            });      
      
		});
	</script>

</body>
</html>