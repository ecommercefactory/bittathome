<!DOCTYPE html>
<html lang="es">
<head>
  <title>Paginacion</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h1>Paginación</h1>
  
<?php
$url = 'https://www.solucionaticos.com/';
$ruta = 'productos';
$paginas = 51;
if (isset($_GET['s'])) {
    $paginas = $_GET['s'];
}
$pagina = 1;
if (isset($_GET['p'])) {
    $pagina = $_GET['p'];
}

if($paginas > 4){

	/*=============================================
	LOS BOTONES DE LAS PRIMERAS 4 PÁGINAS Y LA ÚLTIMA PÁG
	=============================================*/

	if($pagina == 1){

		echo '<ul class="pagination">';
		
		for($i = 1; $i <= 4; $i ++){

			echo '<li id="item'.$i.'"><a href="'.$url.$ruta.'/'.$i.'">'.$i.'</a></li>';

		}

		echo ' <li class="disabled"><a>...</a></li>
			   <li id="item'.$paginas.'"><a href="'.$url.$ruta.'/'.$paginas.'">'.$paginas.'</a></li>
			   <li><a href="'.$url.$ruta.'/2"><span class="glyphicon glyphicon-chevron-right"></span></a></li>

		</ul>';

	}

	/*=============================================
	LOS BOTONES DE LA MITAD DE PÁGINAS HACIA ABAJO
	=============================================*/

	else if($pagina != $paginas && 
		    $pagina != 1 &&
		    $pagina <  ($paginas/2) &&
		    $pagina < ($paginas-3)
		    ){

			$numPagActual = $pagina;

			echo '<ul class="pagination">
				  <li><a href="'.$url.$ruta.'/'.($numPagActual-1).'"><span class="glyphicon glyphicon-chevron-left"></span></a></li> ';
		
			for($i = $numPagActual; $i <= ($numPagActual+3); $i ++){

				echo '<li id="item'.$i.'"><a href="'.$url.$ruta.'/'.$i.'">'.$i.'</a></li>';

			}

			echo ' <li class="disabled"><a>...</a></li>
				   <li id="item'.$paginas.'"><a href="'.$url.$ruta.'/'.$paginas.'">'.$paginas.'</a></li>
				   <li><a href="'.$url.$ruta.'/'.($numPagActual+1).'"><span class="glyphicon glyphicon-chevron-right"></span></a></li>

			</ul>';

	}

	/*=============================================
	LOS BOTONES DE LA MITAD DE PÁGINAS HACIA ARRIBA
	=============================================*/

	else if($pagina != $paginas && 
		    $pagina != 1 &&
		    $pagina >=  ($paginas/2) &&
		    $pagina < ($paginas-3)
		    ){

			$numPagActual = $pagina;
		
			echo '<ul class="pagination">
			   <li><a href="'.$url.$ruta.'/'.($numPagActual-1).'"><span class="glyphicon glyphicon-chevron-left"></span></a></li> 
			   <li id="item1"><a href="'.$url.$ruta.'/1">1</a></li>
			   <li class="disabled"><a>...</a></li>
			';
		
			for($i = $numPagActual; $i <= ($numPagActual+3); $i ++){

				echo '<li id="item'.$i.'"><a href="'.$url.$ruta.'/'.$i.'">'.$i.'</a></li>';

			}


			echo '  <li><a href="'.$url.$ruta.'/'.($numPagActual+1).'"><span class="glyphicon glyphicon-chevron-right"></span></a></li>
				</ul>';
	}

	/*=============================================
	LOS BOTONES DE LAS ÚLTIMAS 4 PÁGINAS Y LA PRIMERA PÁG
	=============================================*/

	else{

		$numPagActual = $pagina;

		echo '<ul class="pagination">
			   <li><a href="'.$url.$ruta.'/'.($numPagActual-1).'"><span class="glyphicon glyphicon-chevron-left"></span></a></li> 
			   <li id="item1"><a href="'.$url.$ruta.'/1">1</a></li>
			   <li class="disabled"><a>...</a></li>
			';
		
		for($i = ($paginas-3); $i <= $paginas; $i ++){

			echo '<li id="item'.$i.'"><a href="'.$url.$ruta.'/'.$i.'">'.$i.'</a></li>';

		}

		echo ' </ul>';

	}

}else{

	echo '<ul class="pagination">';
	
	for($i = 1; $i <= $paginas; $i ++){

		echo '<li id="item'.$i.'"><a href="'.$url.$ruta.'/'.$i.'">'.$i.'</a></li>';

	}

	echo '</ul>';

}

?>  

</div>

</body>
</html>