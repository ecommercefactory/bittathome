$('#lista').DataTable({
	'order': [[ 0, 'asc' ]],
	'iDisplayLength': 100
});
			$('#lista_0').DataTable({
		        initComplete: function () {
		            this.api().columns([3]).every( function () {
		                var column = this;
		                var select = $('<select><option value=\"\"></option></select>')
		                    .appendTo( $(column.footer()).empty() )
		                    .on( 'change', function () {
		                        var val = $.fn.dataTable.util.escapeRegex(
		                            $(this).val()
		                        );
		                        column
		                            .search( val ? '^'+val+'$' : '', true, false )
		                            .draw();
		                    } );
		                column.data().unique().sort().each( function ( d, j ) {
		                    select.append( '<option value=\"'+d+'\">'+d+'</option>' )
		                } );
		            } );
		        },
				'order': [[ 2, 'desc' ]],
				'columnDefs': [ {'targets': 0,'orderable': false} ],
				'language': {'url': '/datatables/language/Spanish.json'},
				'iDisplayLength': 100,
			});
			$('#lista_1').DataTable({
				'order': [[ 1, 'asc' ]],
				'columnDefs': [ {'targets': 0,'orderable': false} ],
				'language': {'url': '/datatables/language/Spanish.json'},
				'iDisplayLength': 100,
			});
  		$('#lista').DataTable({
			'order': [[ 2, 'asc' ]],
			'columnDefs': [ {'targets': 0,'orderable': false}, {'targets': 1,'orderable': false} ],
			'language': {'url': '/datatables/language/Spanish.json'},
			'paging':   false,
  		});
  		$('#lista').DataTable({
	  		
	        initComplete: function () {
	            this.api().columns([0, 1, 2, 3]).every( function () {
	                var column = this;
	                var select = $('<select><option value=\"\"></option></select>')
	                    .appendTo( $(column.footer()).empty() )
	                    .on( 'change', function () {
	                        var val = $.fn.dataTable.util.escapeRegex(
	                            $(this).val()
	                        );
	                        column
	                            .search( val ? '^'+val+'$' : '', true, false )
	                            .draw();
	                    } );
	                column.data().unique().sort().each( function ( d, j ) {
	                    select.append( '<option value=\"'+d+'\">'+d+'</option>' )
	                } );
	            } );
	        },	  		
	  		
			'order': [[ 0, 'asc' ]],
			'columnDefs': [ {'targets': 0,'orderable': false}, {'targets': 1,'orderable': false} ],
			'language': {'url': '/datatables/language/Spanish.json'},
			'paging':   false,
  		});
  		$('#lista').DataTable({
			'order': [[ 2, 'asc' ]],
			'columnDefs': [ {'targets': 0,'orderable': false}, {'targets': 1,'orderable': false} ],
			'language': {'url': '/datatables/language/Spanish.json'},
			'paging':   false,
  		});
  		$('#lista').DataTable({
	  		
	        initComplete: function () {
	            this.api().columns([0, 1, 2, 3, 4]).every( function () {
	                var column = this;
	                var select = $('<select><option value=\"\"></option></select>')
	                    .appendTo( $(column.footer()).empty() )
	                    .on( 'change', function () {
	                        var val = $.fn.dataTable.util.escapeRegex(
	                            $(this).val()
	                        );
	                        column
	                            .search( val ? '^'+val+'$' : '', true, false )
	                            .draw();
	                    } );
	                column.data().unique().sort().each( function ( d, j ) {
	                    select.append( '<option value=\"'+d+'\">'+d+'</option>' )
	                } );
	            } );
	        },	  		
	  		
	  		'order': [[ 0, 'asc' ]],
			'columnDefs': [ {'targets': 0,'orderable': false}, {'targets': 1,'orderable': false} ],
			'language': {'url': '/datatables/language/Spanish.json'},
			'paging':   false,
  		});
  		$('#lista').DataTable({
			'order': [[ 2, 'asc' ]],
			'columnDefs': [ {'targets': 0,'orderable': false}, {'targets': 1,'orderable': false} ],
			'language': {'url': '/datatables/language/Spanish.json'},
			'paging':   false,
  		});
  		$('#lista').DataTable({
	  		
	        initComplete: function () {
	            this.api().columns([1, 2, 3, 4, 5, 6]).every( function () {
	                var column = this;
	                var select = $('<select><option value=\"\"></option></select>')
	                    .appendTo( $(column.footer()).empty() )
	                    .on( 'change', function () {
	                        var val = $.fn.dataTable.util.escapeRegex(
	                            $(this).val()
	                        );
	                        column
	                            .search( val ? '^'+val+'$' : '', true, false )
	                            .draw();
	                    } );
	                column.data().unique().sort().each( function ( d, j ) {
	                    select.append( '<option value=\"'+d+'\">'+d+'</option>' )
	                } );
	            } );
	        },	  		
	  		
			'order': [[ 1, 'asc' ]],
			'columnDefs': [ {'targets': 0,'orderable': false}, {'targets': 1,'orderable': false} ],
			'language': {'url': '/datatables/language/Spanish.json'},
			'paging':   false,
  		});

		var table = $('#lista').DataTable({
			'paging':false,
	        'order': [[ 0, 'desc' ]], 
	        'footerCallback': function ( row, data, start, end, display ) {
	            var api = this.api(), data;
	            var intVal = function ( i ) {
	                return typeof i === 'string' ?
	                    i.replace(/[\$,]/g, '')*1 :
	                    typeof i === 'number' ?
	                        i : 0;
	            };
	            total3 = api
	                .column( 3, { page: 'current'} )
	                .data()
	                .reduce( function (a, b) {
	                    return intVal(a) + intVal(b);
	                }, 0 );
	            // Update footer
	            $( api.column( 3 ).footer() ).html(
	                numberFormat(total3)
	            );
	        },	        
		});


  		$('#lista').DataTable({
			'order': [[ 0, 'asc' ]],
			'language': {'url': '/datatables/language/Spanish.json'},
			'paging':   false,
			
	        scrollY:        '500px',
	        scrollX:        true,
	        scrollCollapse: true,
	        fixedColumns:   {
	            leftColumns: 1,
	            rightColumns: 1
	        }			
			
			
  		});

  		$('#lista').DataTable({
	        'paging':   false,
			'order': [[ 0, 'asc' ]],
			'columnDefs': [ 
				{'targets': 2,'orderable': false}, 
				{'targets': 3,'orderable': false}, 
				{'targets': 6,'orderable': false}, 
				{'targets': 7,'orderDataType': 'dom-text'},
				{'targets': 8,'orderDataType': 'dom-text'},
				{'targets': 9,'orderDataType': 'dom-text'},
				{'targets': 10,'orderDataType': 'dom-text'}, 
				{'targets': 12,'orderable': false}, 
				{'targets': 13,'orderable': false} 
			],
	        'language': {'url': '/datatables/language/Spanish.json'}, 		
  		});

  		$('#lista').DataTable({
  			'order': [[ 1, 'desc' ]],
			'language': {'url': '/datatables/language/Spanish.json'},
			'iDisplayLength': 100,	
	        initComplete: function () {
	            this.api().columns([0, 1]).every( function () {
	                var column = this;
	                var select = $('<select><option value=\"\"></option></select>')
	                    .appendTo( $(column.footer()).empty() )
	                    .on( 'change', function () {
	                        var val = $.fn.dataTable.util.escapeRegex(
	                            $(this).val()
	                        );
	                        column
	                            .search( val ? '^'+val+'$' : '', true, false )
	                            .draw();
	                    } );
	                column.data().unique().sort().each( function ( d, j ) {
	                    select.append( '<option value=\"'+d+'\">'+d+'</option>' )
	                } );
	            } );
	        },
	        'footerCallback': function ( row, data, start, end, display ) {
	            var api = this.api(), data;
	 
	            // Remove the formatting to get integer data for summation
	            var intVal = function ( i ) {
	                return typeof i === 'string' ?
	                    i.replace(/[\$,]/g, '')*1 :
	                    typeof i === 'number' ?
	                        i : 0;
	            };
	 
	            // Total over all pages
	            total = api
	                .column( 4 )
	                .data()
	                .reduce( function (a, b) {
	                    return intVal(a) + intVal(b);
	                }, 0 );
	 
	            // Total over this page
	            pageTotal = api
	                .column( 4, { page: 'current'} )
	                .data()
	                .reduce( function (a, b) {
	                    return intVal(a) + intVal(b);
	                }, 0 );
	 
	            // Update footer
	            $( api.column( 5 ).footer() ).html(
	                '<span class=\"text-success\">'+toTimeString(pageTotal) +'</span><br><span class=\"text-primary\">'+ toTimeString(total) +'</span>'
	            );
	        },

  		});


		function formatNumber (num) {
		    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, \"$1,\")
		}
		
		$('#lista').DataTable({
	        'footerCallback': function ( row, data, start, end, display ) {
	            var api = this.api(), data;
	 
	            // Remove the formatting to get integer data for summation
	            var intVal = function ( i ) {
	                return typeof i === 'string' ?
	                    i.replace(/[\$,]/g, '')*1 :
	                    typeof i === 'number' ?
	                        i : 0;
	            };

	            // Total over this page
	            pageTotal_1 = api
	                .column( 6, { page: 'current'} )
	                .data()
	                .reduce( function (a, b) {
	                    return intVal(a) + intVal(b);
	                }, 0 );
	
	            // Total over this page
	            pageTotal_2 = api
	                .column( 7, { page: 'current'} )
	                .data()
	                .reduce( function (a, b) {
	                    return intVal(a) + intVal(b);
	                }, 0 );
	 
	            // Update footer
				var num = pageTotal_1;
				var n = num.toFixed(2);
				var pT1 = formatNumber(n);              
	            $( api.column( 6 ).footer() ).html(
					pT1
	            );
	
	            // Update footer
				num = pageTotal_2;
				n = num.toFixed(2);
				var pT2 = formatNumber(n);  
	            $( api.column( 7 ).footer() ).html(
	                pT2
	            );

	            // Update footer
				var pT3 = 0;
				if (pageTotal_2 > 0) {
					num = ((pageTotal_2-pageTotal_1)/pageTotal_2)*100;
					n = num.toFixed(2);
					pT3 = formatNumber(n); 
				}
	            $( api.column( 8 ).footer() ).html(
	                pT3
	            );	
	
	        },
			'order': [[ 1, 'asc' ]],
			'columnDefs': [ {'targets': 0,'orderable': false}, {'targets': 4,'orderable': false}, {'targets': 5,'orderable': false} ],
			'language': {'url': '/datatables/language/Spanish.json'},
			'paging':   false
		});

	    var table = $('#lista').DataTable({
		    dom:            'Bfrtip',
			'paging':   false,
			'ordering': false,
			scrollY:        '400px',
	        scrollX:        true,
	        scrollCollapse: true,
	        paging:         false,
	        buttons:        [ 'colvis' ],
	        fixedColumns:   {
	            leftColumns: $numColsFix
	        }
	    }); 




