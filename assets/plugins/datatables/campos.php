<?php
if ( isset($_POST['ajax']) ) {
	if ( $_POST['ajax'] == 'editarCampo') {

		$tipo = '';
		$valor = '';
		$codigos = array();
		$valores = array();

		$sql = "SELECT $_POST[campo] FROM tabla WHERE id = '$_POST[id]'";
		$pre = mysqli_query($conn, $sql);
		if (mysqli_num_rows($pre)) {
			$reg = mysqli_fetch_assoc($pre);
			switch ($_POST['campo']) {
				case 'campo1':
					$tipo = 'texto';
					$valor = $reg[$_POST['campo']];
					break;
				case 'campo2':
					$tipo = 'fecha';
					$valor = $reg[$_POST['campo']];
					break;
				case 'campo3':
					$tipo = 'clave';
					$valor = $reg[$_POST['campo']];
					break;
				case 'campo4':
					$tipo = 'area';
					$valor = $reg[$_POST['campo']];
					break;
				case 'campo5':
					$tipo = 'lista';
					$valor = $reg[$_POST['campo']];
					$codigos[] = 1;
					$codigos[] = 2;
					$codigos[] = 3;
					$codigos[] = 4;
					$codigos[] = 5;
					$valores[] = 'Lunes';
					$valores[] = 'Martes';
					$valores[] = 'Miercoles';
					$valores[] = 'Jueves';
					$valores[] = 'Viernes';
					break;
			}
		}

		$datos = array(
			'tipo' => $tipo, 
			'valor' => $valor,
			'codigos' => $codigos,
			'valores' => $valores
		);

		echo json_encode($datos);		
	}
}
?>

<script type="text/javascript">

$(".campoEditar").dblclick(function () {
	var campo = $(this).attr("campo");
	var id = $(this).attr("id");
	editarCampo(campo, id);
});

function editarCampo(campo, id) {
	var campo = $("#td_"+campo+"_"+id).html();
	$.ajax({
		url: "list.phtml",
		data: {
			id: id,
			campo: campo,
			ajax: "editarCampo"
		},
		type: "post",
		dataType: "json",
		success: function(datos) {
			var proceso = true;
			campo = "<div class='input-group'>";
			switch(datos["tipo"]) {
				case "texto":
					campo += "<input type='text' class='form-control' id='"+campo+"_"+id+"' value='"+datos["valor"]+"'>";
					break;
				case "fecha":
					campo += "<input type='text' class='form-control textoFecha' id='"+campo+"_"+id+"' value='"+datos["valor"]+"'>";
					break;
				case "clave":
					campo += "<input type='password' class='form-control' id='"+campo+"_"+id+"' value='"+datos["valor"]+"'>";
					break;
				case "area":
					campo += "<textarea class='form-control' rows='5' id='"+campo+"_"+id+"'>"+datos["valor"]+"</textarea>";
					break;
				case "lista":
					campo += "<select class='form-control' id='"+campo+"_"+id+"'>";
					var i = 0;
					var selected = "";
					for (i=0; i<datos["valores"].length; i++) {
						selected = "";
						if (datos["codigos"][i] == datos["valor"]) {
							selected = " selected";
						}
						campo += "<option value='"+datos["codigos"][i]+"'"+selected+">"+datos["valores"][i]+"</option>";
					}
					campo += "</select>";
					break;
				default:
					campo += "Error";
					proceso = false;
			}
			if (proceso) {
				campo += "<div class='input-group-btn' style='vertical-align:top;'>";
				campo += "<button class='btn btn-default' type='button'><i class='glyphicon glyphicon-remove'></i></button>";
				campo += "<button class='btn btn-success' type='button'><i class='glyphicon glyphicon-ok'></i></button>";
				campo += "</div>";
			}
			campo += "</div>";
			$("#td_"+campo+"_"+id).html(campo);
		}
	});
}


</script>
