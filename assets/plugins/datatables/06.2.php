<?php
session_start();
if ( isset($_POST['ajax']) ) {
	$servername = "localhost";
	$username = "solucio4_tienda15";
	$password = "David1974";
	$dbname = "solucio4_tienda15";
	$tablas = "productos";
	$campos = "id, id_categoria, id_subcategoria, tipo, ruta, estado";
	$rs = "prd_1_";
	$paginaDefecto = 1;
	$paginacionDefecto = 25;
	$ordenDefecto = "id DESC";
	$filtroDefecto = "1=1";
	$conn = new mysqli($servername, $username, $password, $dbname);
	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	} 
    if ( $_POST['ajax'] == 'lista') {

    	if ($_POST['pagina'] == 0) {
    		if (isset($_SESSION[$rs . 'pagina'])) {
	    		$pagina = $_SESSION[$rs . 'pagina'];
    		} else {
	    		$pagina = $paginaDefecto;
    		}
    	} else {
    		$pagina = $_POST['pagina'];
    	}
		$_SESSION[$rs . 'pagina'] = $pagina;

    	if ($_POST['paginacion'] == 0) {
    		if (isset($_SESSION[$rs . 'paginacion'])) {
	    		$paginacion = $_SESSION[$rs . 'paginacion'];
    		} else {
	    		$paginacion = $paginacionDefecto;
    		}
    	} else {
    		$paginacion = $_POST['paginacion'];
    	}
		$_SESSION[$rs . 'paginacion'] = $paginacion;

    	if ($_POST['orden'] == '') {
    		if (isset($_SESSION[$rs . 'orden'])) {
		    	$orden = $_SESSION[$rs . 'orden'];
    		} else {
		    	$orden = $ordenDefecto;
    		}
    	} else {
	    	$orden = $_POST['orden'];
    	}
   		$_SESSION[$rs . 'orden'] = $orden;

    	if ($_POST['filtro'] == '') {
    		if (isset($_SESSION[$rs . 'filtro'])) {
		    	$filtro = $_SESSION[$rs . 'filtro'];
    		} else {
		    	$filtro = $filtroDefecto;
    		}
    	} else {
	    	$filtro = $_POST['filtro'];
    	}
    	$_SESSION[$rs . 'filtro'] = $filtro;

    	$registrosLista = 0;
    	$sql = "SELECT COUNT(id) AS filas FROM $tablas WHERE $filtro ORDER BY $orden";
    	$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			$row = $result->fetch_assoc();
			$registrosLista = $row['filas'];
		}

    	$paginaActual = $pagina * $paginacion;
    	$lista = array();
    	$sql = "SELECT $campos FROM $tablas WHERE $filtro ORDER BY $orden LIMIT $paginaActual, $paginacion";
    	$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
				$lista[] = $row;
			}
		}

		$conn->close();

        $datos = array(
            'sql' => $sql, 
			'registrosLista' => $registrosLista, 
            'pagina' => $pagina, 
            'paginacion' => $paginacion, 
            'orden' => $orden, 
            'lista' => $lista
        );

        echo json_encode($datos);
	}
	die();
}

?>
<!DOCTYPE html>
<html lang="es">
<head>
  <title>Paginacion</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<style type="text/css">
	.btnDef {color: #337ab7;}
	.btnSel {color: #ffffff;}
</style>

</head>
<body>

<div class="container">
  <h1>Paginación</h1>
  

	<h1>Lista</h1>
	<div>SQL: <span id="sql"></span></div>	
	<div>Informacion: <span id="informacion"></span></div>	
	<div>Pagina: <input type="text" id="pagina"></div>	
	<div id="btnPaginacion"></div>

	<div>
		Mostrar
		<select id="paginacion">
			<option value="10">10</option>
			<option value="25">25</option>
			<option value="50">50</option>
			<option value="100">100</option>
		</select>
		registros 
	</div>
	<div>
		Ordenar por 
		<select id="orden">
			<option value="id ASC">ID Ascendente</option>
			<option value="id DESC">ID Descendente</option>
			<option value="id_categoria ASC">ID Categoria Ascendente</option>
			<option value="id_categoria DESC">ID Categoria Descendente</option>
			<option value="id_subcategoria ASC">ID Subcategoria Ascendente</option>
			<option value="id_subcategoria DESC">ID Subategoria Descendente</option>
		</select>
	</div>

	<div id="listaProductos"></div>

	<script type="text/javascript">
		$(function () {
			var url = 'https://www.solucionaticos.com/';
			var ruta = 'productos';
			var i = 0;
			var j = 0;
			var numPagActual = 0;

			cargaLista();

			function cargaLista(pagina=1, paginacion=0, orden="", filtro="") {
                $.ajax({
                    url: "06.2.php",
                    data: {
                        pagina:pagina, 
                        paginacion:paginacion, 
                        orden:orden, 
                        filtro:filtro,
                        ajax: "lista"
                    },
                    type: "post",
                    dataType: "json",
                    success: function(datos) {
						//console.log(datos);
						$("#sql").html(datos.sql);
						var regInicial = 0;
						var regFinal = 0;
						regInicial = ((datos.pagina-1) * datos.paginacion) + 1;
						regFinal = datos.pagina * datos.paginacion;
						if (regFinal > datos.registrosLista) {
							regFinal = datos.registrosLista;
						}
						$("#informacion").html("Mostrando registros del " + regInicial + " al " + regFinal + " de un total de " + datos.registrosLista);
						$("#pagina").val(datos.pagina);
						$("#paginacion").val(datos.paginacion);
						$("#orden").val(datos.orden);

						/*
						datos.paginacion
						datos.registrosLista
						datos.pagina
						*/
						var paginas = 0;
						if (datos.paginacion > 0) {
							paginas = datos.registrosLista / datos.paginacion;	
							paginas = Math.ceil(paginas);
						}
						var pagina = 0;
						pagina = datos.pagina;

						var btnPaginacion = "";
						var disabled = "";
/*
						if (paginas > 0) {
							disabled = "";
							if (datos.pagina == 1) {
								disabled = "disabled";
							}	
							btnPaginacion = '<button type="button" class="btnPag" pag="Anterior" id="anterior" '+disabled+'>Anterior</button>';
							for (i = 1; i <= paginas; i++) {
								disabled = "";
								if (datos.pagina == i) {
									disabled = "disabled";
								}		
								btnPaginacion += '<button type="button" class="btnPag" pag="'+i+'" '+disabled+'>'+i+'</button>';
							}
							disabled = "";
							if (datos.pagina == paginas) {
								disabled = "disabled";
							}	
							btnPaginacion += '<button type="button" class="btnPag" pag="Siguiente" id="siguiente" '+disabled+'>Siguiente</button>';
						}
*/
// ------------------------------------------------------------------------
if (paginas > 4){

	/*=============================================
	LOS BOTONES DE LAS PRIMERAS 4 PÁGINAS Y LA ÚLTIMA PÁG
	=============================================*/

	if (pagina == 1) {
		btnPaginacion += '<ul class="pagination">';
		for(i = 1; i <= 4; i ++){
			if (pagina == i) {
				btnPaginacion += '<li id="item'+i+'"><button type="button" class="btn btn-primary btnPag btnSel" pag="'+i+'">'+i+'</button></li>';
			} else {
				btnPaginacion += '<li id="item'+i+'"><button type="button" class="btn btn-default btnPag btnDef" pag="'+i+'">'+i+'</button></li>';
			}
		}
		btnPaginacion += ' <li class="disabled"><button type="button" class="btn btn-default btnPag btnDef" disabled>...</button></li>' + 
			   '<li id="item'+paginas+'"><button type="button" class="btn btn-default btnPag btnDef" pag="'+paginas+'">'+paginas+'</button></li>' + 
			   '<li><button type="button" class="btn btn-default btnPag btnDef" pag="Siguiente" id="siguiente"><span class="glyphicon glyphicon-chevron-right"></span></button></li>' + 
			'</ul>';
	}

	/*=============================================
	LOS BOTONES DE LA MITAD DE PÁGINAS HACIA ABAJO
	=============================================*/

	else if(pagina != paginas && 
		    pagina != 1 &&
		    pagina <  (paginas/2) &&
		    pagina < (paginas-3)
		    ){

			numPagActual = pagina;

			btnPaginacion += '<ul class="pagination">' +
				'<li><button type="button" class="btn btn-default btnPag btnDef" pag="Anterior" id="anterior"><span class="glyphicon glyphicon-chevron-left"></span></button></li>' + 
				'<li id="item1"><button type="button" class="btn btn-default btnPag btnDef" pag="1">1</button></li>' + 
			   '<li class="disabled"><button type="button" class="btn btn-default btnPag btnDef" disabled>...</button></li>';
			j = 0;
			for(i = numPagActual; i <= (paginas-1); i ++){
				j++;
				if (pagina == i) {
					btnPaginacion += '<li id="item'+i+'"><button type="button" class="btn btn-primary btnPag btnSel" pag="'+i+'">'+i+'</button></li>';
				} else {
					btnPaginacion += '<li id="item'+i+'"><button type="button" class="btn btn-default btnPag btnDef" pag="'+i+'">'+i+'</button></li>';
				}
				if (j == 4) {
					break;
				}
			}
			btnPaginacion += ' <li class="disabled"><button type="button" class="btn btn-default btnPag btnDef" disabled>...</button></li>' + 
				   '<li id="item'+paginas+'"><button type="button" class="btn btn-default btnPag btnDef" pag="'+paginas+'">'+paginas+'</button></li>' +
				   '<li><button type="button" class="btn btn-default btnPag btnDef" pag="Siguiente" id="siguiente"><span class="glyphicon glyphicon-chevron-right"></span></button></li>' + 
			'</ul>';

	}

	/*=============================================
	LOS BOTONES DE LA MITAD DE PÁGINAS HACIA ARRIBA
	=============================================*/

	else if(pagina != paginas && 
		    pagina != 1 &&
		    pagina >=  (paginas/2) &&
		    pagina < (paginas-3)
		    ){

			numPagActual = pagina;
		
			btnPaginacion += '<ul class="pagination">' + 
				'<li><button type="button" class="btn btn-default btnPag btnDef" pag="Anterior" id="anterior"><span class="glyphicon glyphicon-chevron-left"></span></button></li>' + 
				'<li id="item1"><button type="button" class="btn btn-default btnPag btnDef" pag="1">1</button></li>' + 
			   '<li class="disabled"><button type="button" class="btn btn-default btnPag btnDef" disabled>...</button></li>';
		
			j = 0;
			for(i = numPagActual; i <= paginas; i ++){
				j++;
				if (pagina == i) {
					btnPaginacion += '<li id="item'+i+'"><button type="button" class="btn btn-primary btnPag btnSel" pag="'+i+'">'+i+'</button></li>';
				} else {
					btnPaginacion += '<li id="item'+i+'"><button type="button" class="btn btn-default btnPag btnDef" pag="'+i+'">'+i+'</button></li>';
				}
				if (j == 4) {
					break;
				}
			}

			btnPaginacion += ' <li class="disabled"><button type="button" class="btn btn-default btnPag btnDef" disabled>...</button></li>' + 
				   '<li id="item'+paginas+'"><button type="button" class="btn btn-default btnPag btnDef" pag="'+paginas+'">'+paginas+'</button></li>' +
				   '<li><button type="button" class="btn btn-default btnPag btnDef" pag="Siguiente" id="siguiente"><span class="glyphicon glyphicon-chevron-right"></span></button></li>' + 
			'</ul>';

	}

	/*=============================================
	LOS BOTONES DE LAS ÚLTIMAS 4 PÁGINAS Y LA PRIMERA PÁG
	=============================================*/

	else{

		numPagActual = pagina;

		btnPaginacion += '<ul class="pagination">' + 
				'<li><button type="button" class="btn btn-default btnPag btnDef" pag="Anterior" id="anterior"><span class="glyphicon glyphicon-chevron-left"></span></button></li>' + 
				'<li id="item'+i+'"><button type="button" class="btn btn-default btnPag btnDef" pag="1">1</button></li>' + 
			   '<li class="disabled"><button type="button" class="btn btn-default btnPag btnDef" disabled>...</button></li>';
		
		for(i = (paginas-3); i <= paginas; i ++){
			if (pagina == i) {
				btnPaginacion += '<li id="item'+i+'"><button type="button" class="btn btn-primary btnPag btnSel" pag="'+i+'">'+i+'</button></li>';
			} else {
				btnPaginacion += '<li id="item'+i+'"><button type="button" class="btn btn-default btnPag btnDef" pag="'+i+'">'+i+'</button></li>';
			}
		}

		btnPaginacion += ' </ul>';

	}

}else{

	btnPaginacion += '<ul class="pagination">';
	
	for(i = 1; i <= paginas; i ++){

		if (pagina == i) {
			btnPaginacion += '<li id="item'+i+'"><button type="button" class="btn btn-primary btnPag btnSel" pag="'+i+'">'+i+'</button></li>';
		} else {
			btnPaginacion += '<li id="item'+i+'"><button type="button" class="btn btn-default btnPag btnDef" pag="'+i+'">'+i+'</button></li>';
		}

	}

	btnPaginacion += '</ul>';

}
// ------------------------------------------------------------------------
						$("#btnPaginacion").html(btnPaginacion);

                    }
                });
			}

			$(document).on("click", ".btnPag", function () {
				var pag = $(this).attr("pag");
				var pagina = Number($("#pagina").val());
				if (pag == "Anterior") {
					pagina = pagina - 1;
				} else {
					if (pag == "Siguiente") {
						pagina = pagina + 1;
					} else {
						pagina = pag;
					}
				}
				var paginacion = $("#paginacion").val();
				var orden = $("#orden").val();
				cargaLista(pagina, paginacion, orden, "");
			});
			$("#paginacion").change(function () {
				var pagina = $("#pagina").val();
				var paginacion = $("#paginacion").val();
				var orden = $("#orden").val();
				cargaLista(pagina, paginacion, orden, "");
			});
			$("#orden").change(function () {
				var pagina = $("#pagina").val();
				var paginacion = $("#paginacion").val();
				var orden = $("#orden").val();
				cargaLista(pagina, paginacion, orden, "");
			});

		});
	</script>

</div>

</body>
</html>

