$(document).ready(function() {
  jQuery.extend(jQuery.validator.messages, {
    required: "Este campo es obligatorio.",
    remote: "Por favor, rellena este campo.",
    email: "Por favor, escribe una dirección de correo válida",
    url: "Por favor, escribe una URL válida.",
    date: "Por favor, escribe una fecha válida.",
    dateISO: "Por favor, escribe una fecha (ISO) válida.",
    number: "Por favor, escribe un número entero válido.",
    digits: "Por favor, escribe sólo dígitos.",
    creditcard: "Por favor, escribe un número de tarjeta válido.",
    equalTo: "Por favor, escribe el mismo valor de nuevo.",
    accept: "Por favor, escribe un valor con una extensión aceptada.",
    maxlength: jQuery.validator.format("Por favor, no escribas más de {0} caracteres."),
    minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres."),
    rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
    range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}."),
    max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}."),
    min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
  });
});

$(function () {

	var tksec = proyVarS.sgch;

	$(".deseos").click(function () {
		var producto = $(this).attr("producto");
		$(this).removeClass("deseos");
		$(this).addClass("deseos_sel");
		//console.log(producto);

		$.ajax({
			url: proyVar.base_url + "ajax/lista_deseos",
			cache: false,
			type: "post",
			data: {"producto":producto, slcnts:tksec},
			success:function(cantidad) {
				$(".lista-deseos .cantidad").html(cantidad);
			},
			error: function (request, status, error) {
				swal({
					type: 'error',
					title: 'Ups...',
					text: 'Algo salio mal, intentalo nuevamente por favor'
				});                    
			}
		});

	});

	$(".deseos_sel").click(function () {
		var producto = $(this).attr("producto");
		$(this).removeClass("deseos_sel");
		$(this).addClass("deseos");
		//console.log(producto);
		$.ajax({
			url: proyVar.base_url + "ajax/lista_deseos",
			cache: false,
			type: "post",
			data: {"producto":producto, slcnts:tksec},
			success:function(cantidad) {
				$(".lista-deseos .cantidad").html(cantidad);
			},
			error: function (request, status, error) {
				swal({
					type: 'error',
					title: 'Ups...',
					text: 'Algo salio mal, intentalo nuevamente por favor'
				});                    
			}
		});

	});



	$("#enviar").click(function () {
		var nombres = $("#nombres").val();
		var email = $("#email").val();
		var telefono = $("#telefono").val();
		var mensaje = $("#mensaje").val();

		var proceso = true;

		if ( nombres.trim() == "" ) {
			proceso = false;
			swal({
				type: 'error',
				title: 'Ups...',
				text: 'Debes ingresar los nombres, intentalo nuevamente por favor'
			}); 			
		}

		if (proceso) {
			if ( email.trim() == "" ) {
				proceso = false;
				swal({
					type: 'error',
					title: 'Ups...',
					text: 'Debes ingresar el e-mail, intentalo nuevamente por favor'
				}); 			
			}
		}

		if (proceso) {
			email = email.trim();
		    expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		    if ( !expr.test(email) ) {
		    	proceso = false;
				swal({
					type: 'error',
					title: 'Ups...',
					text: 'Debes ingresar un e-mail valido, intentalo nuevamente por favor'
				}); 
			}
		}

		if (proceso) {
			if ( mensaje.trim() == "" ) {
				proceso = false;
				swal({
					type: 'error',
					title: 'Ups...',
					text: 'Debes ingresar el mensaje, intentalo nuevamente por favor'
				}); 			
			}
		}		

		if (proceso) {
			$.ajax({
				url: proyVar.base_url + "ajax/escribenos",
				cache: false,
				type: "post",
				data: {"nombres":nombres, "email":email, "telefono":telefono, "mensaje":mensaje, slcnts:tksec},
				success:function(respuesta) {
					if (respuesta == "Enviado") {
						swal({
						  position: 'top-end',
						  type: 'success',
						  title: 'Tu mensaje ha sido enviado con éxito.',
						  showConfirmButton: false,
						  timer: 1500
						});
						$("#nombres").val("");
						$("#email").val("");
						$("#telefono").val("");
						$("#mensaje").val("");
					} else {
						swal({
							type: 'error',
							title: 'Ups...',
							text: 'Algo salio mal, intentalo nuevamente por favor'
						}); 
					}
				},
				error: function (request, status, error) {
					swal({
						type: 'error',
						title: 'Ups...',
						text: 'Algo salio mal, intentalo nuevamente por favor'
					});                    
				}
			});

		}


	});


	$("#enviar_cotizacion").click(function () {

		var tipo_intalacion = $("#tipo_intalacion").val();
		var metraje = $("#metraje").val();
		var altura = "";
		var ciudad = $("#ciudad").val();
		var otra_ciudad = $("#otra_ciudad").val();

		altura = "si";
		if($("#altura_no").is(':checked')) { 
			altura = "no";
		}

		var proceso = true;

		if (proceso) {
			$.ajax({
				url: proyVar.base_url + "ajax/enviar_cotizacion",
				cache: false,
				type: "post",
				data: {
					"tipo_intalacion":tipo_intalacion, 
					"metraje":metraje, 
					"altura":altura, 
					"ciudad":ciudad, 
					"otra_ciudad":otra_ciudad, 
					slcnts:tksec},
				success:function(respuesta) {
					if (respuesta == "Enviado") {
						swal({
						  position: 'top-end',
						  type: 'success',
						  title: 'Tu cotización ha sido enviada con éxito.',
						  showConfirmButton: false,
						  timer: 1500
						});
						$("#tipo_intalacion").val("");
						$("#metraje").val("");
						$("#altura").val("");
						$("#ciudad").val("");
						$("#otra_ciudad").val("");

					} else {
						swal({
							type: 'error',
							title: 'Ups...',
							text: 'Algo salio mal, intentalo nuevamente por favor.'
						}); 
					}
				},
				error: function (request, status, error) {
					swal({
						type: 'error',
						title: 'Ups...',
						text: 'Algo salio mal, intentalo nuevamente por favor'
					});                    
				}
			});

		}


	});



	$("#btnBuscar").click(function () {
		buscar ();
	});

	$("#buscar").keypress(function (e) {
	   if(e.which ==13) {
			buscar ();
	   }
	});

	function buscar () {
		var buscar = $("#buscar").val();
		var proceso = true;
		if ( buscar.trim() == "" ) {
			proceso = false;
			swal({
				type: 'error',
				title: 'Ups...',
				text: 'Debes ingresar lo que quieres buscar, intentalo nuevamente por favor'
			}); 			
		}
		if (proceso) {
			location.href = proyVar.base_url + "buscar/" + buscar;
		}
	}


	$("#car").click(function () {
		location.href = proyVar.base_url + "carrito";
	});


	$("#mdlSalir").click(function (e) {
		e.preventDefault();
		location.href = proyVar.base_url + "ingreso/salir";
	});


	$("#mdlIngreso").click(function (e) {
		e.preventDefault();
		$(".ventana-ingreso").css("display","block");
	});
	$(".cerrarMdlIngreso").click(function (e) {
		e.preventDefault();
		$(".ventana-ingreso").css("display","none");
	});
	$("#mdlIngreso2").click(function (e) {
		e.preventDefault();
		$(".ventana-registro").css("display","none");
		$(".ventana-ingreso").css("display","block");
	});

	$("#mdlRegistro").click(function (e) {
		e.preventDefault();
		$(".ventana-registro").css("display","block");
	});
	$(".cerrarMdlRegistro").click(function (e) {
		e.preventDefault();
		$(".ventana-registro").css("display","none");
	});
	$("#mdlRegistro2").click(function (e) {
		e.preventDefault();
		$(".ventana-ingreso").css("display","none");
		$(".ventana-registro").css("display","block");
	});
	$("#mdlOlvido2").click(function (e) {
		e.preventDefault();
		$(".ventana-ingreso").css("display","none");
		$(".ventana-olvido-clave").css("display","block");
	});

	$(".cerrarMdlOlvidoClave").click(function (e) {
		e.preventDefault();
		$(".ventana-olvido-clave").css("display","none");
	});
	$("#mdlRegistro3").click(function (e) {
		e.preventDefault();
		$(".ventana-olvido-clave").css("display","none");
		$(".ventana-registro").css("display","block");
	});	

	// Validaciones
	
	$("#forma-nueva-clave").validate();
	$("#forma-ingreso").validate();
	if ($('#forma-ingreso2').length) {
		$("#forma-ingreso2").validate();
	}	
	$("#forma-olvido-clave").validate();
	if ($('#forma-olvido-clave2').length) {
		$("#forma-olvido-clave2").validate();
	}	
	$("#forma-registro").validate();
	if ($('#forma-registro2').length) {
		$("#forma-registro2").validate();
	}

/*
	// Solicitud Nueva Contraseña
	$(".btn-nueva-contrasena").click(function () {
		validar_nueva_contrasena();
	});
	$("#correo_oc").keypress(function (e) {
		if(e.which ==13) {
			validar_nueva_contrasena();
		}
	});
	function validar_nueva_contrasena() {
		var validacion = $('#forma-olvido-clave').valid();
		if (validacion) {
			var correo_oc = $("#correo_oc").val();

			$.ajax({
				url: proyVar.base_url + "ajax/olvido_clave",
				cache: false,
				type: "post",
				data: {"correo":correo_oc, slcnts:tksec},
				success:function(respuesta) {
					$("#correo_oc").val("");
					$(".ventana-olvido-clave").css("display","none");
					if (respuesta == "Enviado") {
						swal({
							  title: "SOLICITUD ENVIADA!",
							  text: "¡Por favor revisa la bandeja de entrada o la carpeta de SPAM de tu correo electrónico "+correo_oc+" para tu cambio de contraseña!",
							  type:"success",
							  confirmButtonText: "Cerrar"
						});
					} else {
						if (respuesta == "No Enviado Inactivo") {
							swal({
								  title: "¡ERROR!",
								  text: "¡El correo electrónico "+correo_oc+" no se encuentra activo en el sistema!",
								  type:"error",
								  confirmButtonText: "Cerrar"
							});
						} else {
							swal({
								  title: "¡ERROR!",
								  text: "¡El correo electrónico "+correo_oc+" no existe en el sistema!",
								  type:"error",
								  confirmButtonText: "Cerrar"
							});
						}
					}
				},
				error: function (request, status, error) {
					$("#correo_oc").val("");
					$(".ventana-olvido-clave").css("display","none");
					swal({
						type: 'error',
						title: 'Ups...',
						text: 'Algo salio mal, intentalo nuevamente por favor'
					});                    
				}
			});

		}
	}
*/

});