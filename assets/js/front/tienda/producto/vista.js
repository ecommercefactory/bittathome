$(window).load(function(){

  $('#carousel-producto').flexslider({
    animation: "slide",
    controlNav: true,
    animationLoop: false,
    slideshow: false,
    itemWidth: 210,
    itemMargin: 2,
    asNavFor: '#slider'
  });

  $('#slider').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    sync: "#carousel-producto"
  });

    $('.carousel').flexslider({
        animation: "slide",
        animationLoop: true,
        controlNav: false,
        slideshow: true,
        itemWidth: 250,
        itemMargin: 50,
        minItems: 1,
        maxItems: 4
    });

});

$(document).ready(function(){
	$('.ampliar ul li').zoom({
		magnify:3
	});
});

$(function() {

      var tksec = proyVarS.sgch;

/*
      swal({
        position: 'top-end',
        type: 'success',
        title: 'Todo muy bien',
        showConfirmButton: false,
        timer: 1500
      });
*/

      $('[data-quantity="plus"]').click(function(e){
        e.preventDefault();
        var currentVal = parseInt($("[name='quantity_to_add']").val());
        if (!isNaN(currentVal)) {
          if (currentVal < 10 ) {
            $("[name='quantity_to_add']").val(currentVal + 1);
          }
        } else {
          $("[name='quantity_to_add']").val(1);
        }
      });
      $('[data-quantity="minus"]').click(function(e) {
        e.preventDefault();
        var currentVal = parseInt($("[name='quantity_to_add']").val());
        if (!isNaN(currentVal) && currentVal > 1) {
          $("[name='quantity_to_add']").val(currentVal - 1);
        } else {
          $("[name='quantity_to_add']").val(1);
        }
      });

      $(".allownumericwithoutdecimal").on("keypress keyup blur",function (event) {    
         $(this).val($(this).val().replace(/[^\d].+/, ""));
          if ((event.which < 48 || event.which > 57)) {
              event.preventDefault();
          }
      });

      $(".add-carrito").click(function () {
          var cantidad = $("#quantity_to_add").val() * 1;
          var producto = $(this).attr("producto");
          if (cantidad > 0) {

              $.ajax({
                url: proyVar.base_url + "ajax/carrito",
                cache: false,
                type: "post",
                data: {"producto":producto, "cantidad":cantidad, slcnts:tksec},
                success:function(cantidadTotal) {

                  $("#car .cantidad").html(cantidadTotal);

                  if (cantidadTotal > 0) {

                      var textoProductos = '';
                      if (cantidad == 1) {
                          textoProductos = '1 producto';
                      } else {
                          textoProductos = cantidad + ' productos';
                      }

                      swal({
                          title: "",
                          text: "Se ha agregado " + textoProductos + " al carrito de compras!",
                          type: "success",
                          showCancelButton: true,
                          confirmButtonColor: "#39bba1",
                          confirmButtonText: "Ir a mi carrito de compras!",
                          cancelButtonText: "Continuar comprando!",
                          cancelButtonColor: '#663377'
                        }).then((result) => {
                          if (result.value) {
                            window.location = proyVar.base_url + "carrito";
                          }
                        });

                      $("[name='quantity_to_add']").val(1);

                  } else {
                    swal({
                      type: 'error',
                      title: 'Ups...',
                      text: 'Tu sesión expiró, intentalo nuevamente por favor'
                    }).then((result) => {
                      if (result.value) {
                        window.location = proyVar.base_url;
                      }
                    }); 
                  }
                },
                error: function (request, status, error) {
                  swal({
                    type: 'error',
                    title: 'Ups...',
                    text: 'Algo salio mal, intentalo nuevamente por favor'
                  });                    
                }
              });


          } else {
              swal({
                  type: 'error',
                  title: 'Ups...',
                  text: 'Debes indicar la cantidad'
              });
          }
      });

});

