$(function() {
      var tksec = proyVarS.sgch;

      $('.mas-cantidad').click(function(e){
        e.preventDefault();
        var cantidad = 0;
        var producto = $(this).attr("producto");
        var currentVal = parseInt($("#cantidad_" + producto).val());
        if (!isNaN(currentVal)) {
          if (currentVal < 10 ) {
            $("#cantidad_" + producto).val(currentVal + 1);
          }
        } else {
          $("#cantidad_" + producto).val(1);
        }
        cantidad = $("#cantidad_" + producto).val() * 1;
        productoTotal(producto);

        $.ajax({
          url: proyVar.base_url + "ajax/carrito_operaciones",
          cache: false,
          type: "post",
          data: {"producto":producto, "cantidad":cantidad, slcnts:tksec},
          success:function(resp) {
              if (resp == -1) {
                  swal({
                    type: 'error',
                    title: 'Ups...',
                    text: 'Tu sesión expiró, intentalo nuevamente por favor'
                  }).then((result) => {
                    if (result.value) {
                      window.location = proyVar.base_url;
                    }
                  }); 
              } 
          },
          error: function (request, status, error) {
            swal({
              type: 'error',
              title: 'Ups...',
              text: 'Algo salio mal, intentalo nuevamente por favor'
            });                    
          }            
        });

      });
      $('.menos-cantidad').click(function(e) {
        e.preventDefault();
        var cantidad = 0;
        var producto = $(this).attr("producto");
        var currentVal = parseInt($("#cantidad_" + producto).val());
        if (!isNaN(currentVal) && currentVal > 1) {
          $("#cantidad_" + producto).val(currentVal - 1);
        } else {
          $("#cantidad_" + producto).val(1);
        }
        cantidad = $("#cantidad_" + producto).val() * 1;
        productoTotal(producto);

        $.ajax({
          url: proyVar.base_url + "ajax/carrito_operaciones",
          cache: false,
          type: "post",
          data: {"producto":producto, "cantidad":cantidad, slcnts:tksec},
          success:function(resp) {
              if (resp == -1) {
                  swal({
                    type: 'error',
                    title: 'Ups...',
                    text: 'Tu sesión expiró, intentalo nuevamente por favor'
                  }).then((result) => {
                    if (result.value) {
                      window.location = proyVar.base_url;
                    }
                  }); 
              } 
          },
          error: function (request, status, error) {
            swal({
              type: 'error',
              title: 'Ups...',
              text: 'Algo salio mal, intentalo nuevamente por favor'
            });                    
          }    
        });

      });

      $(".eliminar-producto-carrito").click(function () {
          var producto = $(this).attr("producto");
          var cantidad = 0;

          $("#producto-registro_" + producto).fadeOut(function () {
              $("#producto-registro_" + producto).remove();
              totales();
          });

          $.ajax({
            url: proyVar.base_url + "ajax/carrito_operaciones",
            cache: false,
            type: "post",
            data: {"producto":producto, "cantidad":cantidad, slcnts:tksec},
            success:function(resp) {
                if (resp == -1) {
                    swal({
                      type: 'error',
                      title: 'Ups...',
                      text: 'Tu sesión expiró, intentalo nuevamente por favor'
                    }).then((result) => {
                      if (result.value) {
                        window.location = proyVar.base_url;
                      }
                    }); 
                } 
            },
            error: function (request, status, error) {
              swal({
                type: 'error',
                title: 'Ups...',
                text: 'Algo salio mal, intentalo nuevamente por favor'
              });                    
            }    
          });

      });

      function productoTotal(producto) {
          var productoCantidad = 0;
          var productoPrecio = 0;
          var productoTotal = 0;
          productoCantidad = $("#cantidad_" + producto).val() * 1;
          productoPrecio = $("#cantidad_" + producto).attr("precio") * 1;
          productoTotal = productoCantidad * productoPrecio;
          $("#productoTotal_" + producto).html("$"+numero_decimales(productoTotal));
          totales();
      }

      function totales() {
          var producto = 0;
          var productoCantidad = 0;
          var productoPrecio = 0;
          var productoTotal = 0;
          var subtotal = 0;
          var total = 0;

          $('.input-cantidad').each(function() {
              producto = $(this).attr("producto");
              productoCantidad = $(this).val() * 1;
              productoPrecio = $(this).attr("precio") * 1;
              productoTotal = productoCantidad * productoPrecio;
              subtotal += productoTotal;
          });

          total = subtotal;

          $(".valor-sub").html("$"+numero_decimales(subtotal));
          $(".valor-total").html("$"+numero_decimales(total));

          if (total == 0) {
              $("#msgNoCarrito").css("display","block");
              $(".total-cupon").css("display","none");
              $(".titulos-carrito").css("display","none");
          }

      }

      function numero_formato(num,dec,thou,pnt,curr_right,curr_left,negative_right,negative_left){
          var x = Math.round(num * Math.pow(10,dec));
          if (x >= 0){
            negative_right=negative_left='';
          }
          var y = (''+Math.abs(x)).split('');
          var z = y.length - dec; 
          if (z<0){
            z--;
          } 
          for(var i = z; i < 0; i++){
            y.unshift('0');
          }
          y.splice(z, 0, pnt); 
          if(y[0] == pnt){
            y.unshift('0');
          }
          while (z > 3){
            z-=3; 
            y.splice(z,0,thou);
          }
          var r = curr_right+negative_right+y.join('')+negative_left+curr_left;
          return r;
      }

      function numero_decimales(num){
          // return numero_formato(number,2,',','.','','','','');
          return numero_formato(num,0,',','','','','','');
      }

      $(".allownumericwithoutdecimal").on("keypress keyup blur",function (event) {    
         $(this).val($(this).val().replace(/[^\d].+/, ""));
          if ((event.which < 48 || event.which > 57)) {
              event.preventDefault();
          }
      });

      $(".btn-finalizar-compra").click(function () {
          location.href = proyVar.base_url + "carrito/correo";
      });


});
