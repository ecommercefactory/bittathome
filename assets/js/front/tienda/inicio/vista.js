$(window).load(function(){
    $('.carousel').flexslider({
        animation: "slide",
        animationLoop: false,
        controlNav: true,
        slideshow: false,
        itemWidth: 250,
        itemMargin: 50,
        minItems: 1,
        maxItems: 4
    });
});

/*
$(function () {

	var tksec = proyVarS.sgch;

	$(".deseos").click(function () {
		var producto = $(this).attr("producto");
		$(this).removeClass("deseos");
		$(this).addClass("deseos_sel");
		//console.log(producto);

		$.ajax({
			url: proyVar.base_url + "ajax/lista_deseos",
			cache: false,
			type: "post",
			data: {"producto":producto, slcnts:tksec}
		});

	});

	$(".deseos_sel").click(function () {
		var producto = $(this).attr("producto");
		$(this).removeClass("deseos_sel");
		$(this).addClass("deseos");
		//console.log(producto);

		$.ajax({
			url: proyVar.base_url + "ajax/lista_deseos",
			cache: false,
			type: "post",
			data: {"producto":producto, slcnts:tksec}
		});

	});



	$("#enviar").click(function () {
		var nombres = $("#nombres").val();
		//console.log(nombres);
		var email = $("#email").val();
		//console.log(email);
		var mensaje = $("#mensaje").val();
		//console.log(mensaje);

		$.ajax({
			url: proyVar.base_url + "ajax/escribenos",
			cache: false,
			type: "post",
			data: {"nombres":nombres, "email":email, "mensaje":mensaje, slcnts:tksec},
			success:function(respuesta) {
				if (respuesta == "Enviado") {
					swal({
					  position: 'top-end',
					  type: 'success',
					  title: 'Tu mensaje ha sido enviado con éxito.',
					  showConfirmButton: false,
					  timer: 1500
					});
					$("#nombres").val("");
					$("#email").val("");
					$("#mensaje").val("");
				} else {
					swal({
						type: 'error',
						title: 'Ups...',
						text: 'Algo salio mal, intentalo nuevamente por favor'
					}); 
				}
			},
			error: function (request, status, error) {
				swal({
					type: 'error',
					title: 'Ups...',
					text: 'Algo salio mal, intentalo nuevamente por favor'
				});                    
			}
		});

	});

});
*/