$(document).ready(function() {
  jQuery.extend(jQuery.validator.messages, {
    required: "Este campo es obligatorio.",
    remote: "Por favor, rellena este campo.",
    email: "Por favor, escribe una dirección de correo válida",
    url: "Por favor, escribe una URL válida.",
    date: "Por favor, escribe una fecha válida.",
    dateISO: "Por favor, escribe una fecha (ISO) válida.",
    number: "Por favor, escribe un número entero válido.",
    digits: "Por favor, escribe sólo dígitos.",
    creditcard: "Por favor, escribe un número de tarjeta válido.",
    equalTo: "Por favor, escribe el mismo valor de nuevo.",
    accept: "Por favor, escribe un valor con una extensión aceptada.",
    maxlength: jQuery.validator.format("Por favor, no escribas más de {0} caracteres."),
    minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres."),
    rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
    range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}."),
    max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}."),
    min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
  });
});

$(function () {

	var tksec = proyVarS.sgch;

    $("#formulario-pago").validate({
        rules: {
            numero_tarjeta_datos: {required: true,creditcard: true},
            cuotas_tarjeta_datos: {required: true},
            nombre_tarjeta_datos: {required: true,minlength: 2,maxlength: 50},
            mes_tarjeta_datos: {required: true},
            anio_tarjeta_datos: {required: true},
            codigo_seguridad_tarjeta_datos: {required: true,minlength: 3,maxlength: 4},
        }
    });

    $(document).on("blur, keypress, keydown, keyup", "#numero_tarjeta_datos", function () {
        var numero_tarjeta_datos = $("#numero_tarjeta_datos").val();
        var tipo_numero_tarjeta_datos = CCValidationWithType(numero_tarjeta_datos);
        if (tipo_numero_tarjeta_datos != "") {
            tipo_numero_tarjeta_datos =  "Tarjeta: " + tipo_numero_tarjeta_datos;
        }
        $("#tipo_numero_tarjeta_datos").html(tipo_numero_tarjeta_datos);
    });


/*
            direccion_1_datos: {required: true,minlength: 4,maxlength: 250},
            direccion_2_datos: {minlength: 2,maxlength: 250},
            barrio_datos: {required: true,minlength: 2,maxlength: 50},
            departamento_datos: {required: true,minlength: 2,maxlength: 50},
            municipio_datos: {required: true,minlength: 2,maxlength: 50},
            persona_recibe_datos: {minlength: 2,maxlength: 50},
*/

    function CCValidationWithType(cardNumber) {
        var tipo =  "";


        var cardNumberWithoutDashes = cardNumber.split("-").join("");
        if (cardNumberWithoutDashes.length > 16 || cardNumberWithoutDashes.length < 14) {
            tipo = ""; //"Not a Credit Card Number";
            return tipo;
        }

        var regExpressions = {
            "Visa": /^4\d{3}-?\d{4}-?\d{4}-?\d{4}$/,
            // Visa: length 16, prefix 4, dashes optional.
            "MasterCard": /^5[1-5]\d{2}-?\d{4}-?\d{4}-?\d{4}$/,
            // Mastercard: length 16, prefix 51-55, dashes optional.
            // "Discover": /^6011-?\d{4}-?\d{4}-?\d{4}$/,
            // Discover: length 16, prefix 6011, dashes optional.
            "American Express": /^3[4,7]\d{13}$/,
            // American Express: length 15, prefix 34 or 37.
            "Diners": /^3[0,6,8]\d{12}$/ // Diners: length 14, prefix 30, 36, or 38.
        };

        for (var cardType in regExpressions) {
            var exp = regExpressions[cardType];
            if (exp.test(cardNumber)) {
                tipo = cardType;
                break;
            }
            continue;
        }

        // Checksum ("Mod 10")
        // Add even digits in even length strings or odd digits in odd length strings.
        var checksum = 0;
        for (var i = (2 - (cardNumberWithoutDashes.length % 2)); i <= cardNumberWithoutDashes.length; i += 2) {
            checksum += parseInt(cardNumberWithoutDashes.charAt(i - 1));
        }
        // Analyze odd digits in even length strings or even digits in odd length strings.
        for (var i = (cardNumberWithoutDashes.length % 2) + 1; i < cardNumberWithoutDashes.length; i += 2) {
            var digit = parseInt(cardNumberWithoutDashes.charAt(i - 1)) * 2;
            if (digit < 10) {
                checksum += digit;
            } else {
                checksum += (digit - 9);
            }
        }
        if ((checksum % 10) == 0) {

        }else{
            tipo = ""; //"Invalid " + response.type + " Card Number";
        }
        return tipo;
    }


});

