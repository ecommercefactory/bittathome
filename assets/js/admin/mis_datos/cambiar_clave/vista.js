$(function () {

			$.validator.addMethod("notEqualTo", function(value, element, param) {
						 return this.optional(element) || value != $(param).val();
			}, 'La nueva clave debe ser diferente a la actual.');	
	
      $( "#forma" ).validate( {
        rules: {
					clave_actual: {
						required: true
					},
          clave: {
						required: true,
						minlength: 7,
						maxlength: 20,
						notEqualTo: "#clave_actual"
          },
          clave_validacion: {
            equalTo: "#clave"
          },
        },
        messages: {
          clave_actual: {
            required: "El campo Clave Actual es obligatorio.",
          },          
          clave: {
            required: "El campo Nueva Clave es obligatorio.",
            minlength: "Tu Nueva Clave al menos debe tener 7 caracteres.",
            maxlength: "Tu Nueva Clave debe tener máximo 20 caracteres."
          },          
          clave_validacion: {
            equalTo: "Confirmación incorrecta.",
          },          
        },
        errorElement: "em",
				errorPlacement: function ( error, element ) {
					error.addClass( "help-block" );
					if ( element.prop( "type" ) === "checkbox" ) {
						error.insertAfter( element.parent( "label" ) );
					} else {
						error.insertAfter( element );
					}
				},
				highlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
				},
				unhighlight: function (element, errorClass, validClass) {
					$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
				}
      } );
});