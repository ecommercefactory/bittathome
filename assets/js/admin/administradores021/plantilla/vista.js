$(document).ready(function() {
  jQuery.extend(jQuery.validator.messages, {
    required: "Este campo es obligatorio.",
    remote: "Por favor, rellena este campo.",
    email: "Por favor, escribe una dirección de correo válida",
    url: "Por favor, escribe una URL válida.",
    date: "Por favor, escribe una fecha válida.",
    dateISO: "Por favor, escribe una fecha (ISO) válida.",
    number: "Por favor, escribe un número entero válido.",
    digits: "Por favor, escribe sólo dígitos.",
    creditcard: "Por favor, escribe un número de tarjeta válido.",
    equalTo: "Por favor, escribe el mismo valor de nuevo.",
    accept: "Por favor, escribe un valor con una extensión aceptada.",
    maxlength: jQuery.validator.format("Por favor, no escribas más de {0} caracteres."),
    minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres."),
    rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
    range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}."),
    max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}."),
    min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
  });
});

$(function () {
	var tksec = proyVarS.sgch;

  //Colorpicker
  $(".color").colorpicker();  
  
  traerRegistro(1);

	function traerRegistro(id) {
		$.ajax({
			url: proyVar.base_url + "admin/administradores021/plantilla/traerRegistro",
			cache: false,
			dataType: "json",
			type: "post",
			data: {"id":id, slcnts:tksec},
			success:function(datos) {
				tksec = datos.tksec;
				if (datos.registro != null) {
					for (var campoId in datos.registro) {
						if ( $("#"+campoId+"_g") ) {
              var tipo = $("#"+campoId+"_g").attr("type");
              if (tipo != "file") {
                $("#"+campoId+"_g").val(datos.registro[campoId]);
              } 
              
              if (campoId === "logo") {
                if ( datos.registro[campoId] !== "" ) {
                $("#logo_g_ver").html("<img src='"+proyVar.base_url+"imagenes/plantilla/"+datos.registro[campoId]+"' width='150px' class='img-thumbnail'><br><br>");
                }
              }
              
              if (campoId === "icono") {
                if ( datos.registro[campoId] !== "" ) {
                $("#icono_g_ver").html("<img src='"+proyVar.base_url+"imagenes/plantilla/"+datos.registro[campoId]+"' width='40px' class='img-thumbnail'><br><br>");
                }
              }
              
						}
					}
					$("#modalEditar").modal({backdrop: "static"});
				} else {
					swal({
						type: 'error',
						title: 'Ups...',
						text: 'No fue posible cargar el registro'
					});  
				}
			},
			error: function (request, status, error) {
				swal({
					type: 'error',
					title: 'Ups...',
					text: request.responseText
				});                    
			}
		});
	}

	$( "#forma_g" ).validate( {
		rules: {
			barraSuperior: {},textoSuperior: {},colorFondo: {},colorTexto: {},logo: {},icono: {},
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			error.addClass( "help-block" );
			if ( element.prop( "type" ) === "checkbox" ) {
				error.insertAfter( element.parent( "label" ) );
			} else {
				error.insertAfter( element );
			}
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
		},
		unhighlight: function (element, errorClass, validClass) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
		}
	} ); 

});