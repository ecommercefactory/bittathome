$(document).ready(function() {
  jQuery.extend(jQuery.validator.messages, {
    required: "Este campo es obligatorio.",
    remote: "Por favor, rellena este campo.",
    email: "Por favor, escribe una dirección de correo válida",
    url: "Por favor, escribe una URL válida.",
    date: "Por favor, escribe una fecha válida.",
    dateISO: "Por favor, escribe una fecha (ISO) válida.",
    number: "Por favor, escribe un número entero válido.",
    digits: "Por favor, escribe sólo dígitos.",
    creditcard: "Por favor, escribe un número de tarjeta válido.",
    equalTo: "Por favor, escribe el mismo valor de nuevo.",
    accept: "Por favor, escribe un valor con una extensión aceptada.",
    maxlength: jQuery.validator.format("Por favor, no escribas más de {0} caracteres."),
    minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres."),
    rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
    range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}."),
    max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}."),
    min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
  });
});

$(function () {
	var tksec = proyVarS.sgch;

  //Date picker
  $('.datepicker').datepicker({
    format: 'yyyy-mm-dd',
    todayBtn: "linked",
    language: "es",
    todayHighlight: true,
    autoclose: true
  });  

  //Initialize Select2 Elements
  $(".select2").select2();
  
function formatNumber(num,dec,thou,pnt,curr_right,curr_left,negative_right,negative_left){
	var x = Math.round(num * Math.pow(10,dec));
	if (x >= 0){
		negative_right=negative_left='';
	}
	var y = (''+Math.abs(x)).split('');
	var z = y.length - dec; 
	if (z<0){
		z--;
	} 
	for(var i = z; i < 0; i++){
		y.unshift('0');
	}
	y.splice(z, 0, pnt); 
	if(y[0] == pnt){
		y.unshift('0');
	}
	while (z > 3){
		z-=3; 
		y.splice(z,0,thou);
	}
	var r = curr_right+negative_right+y.join('')+negative_left+curr_left;
	return r;
}   
  
	$('#lista').DataTable({
		"ajax":proyVar.base_url + "admin/administradores021/ordenes_productos_funciones/lista/" + $("#orden").val(),
    'deferRender': true,
    'retrieve': true,
    'processing': true,   
    
	        'footerCallback': function ( row, data, start, end, display ) {
	            var api = this.api();
	            var intVal = function ( i ) { return typeof i === 'string' ? i.replace(/[\$,]/g, '')*1 : typeof i === 'number' ? i : 0; };
	            var total = api.column( 6 ).data().reduce( function (a, b) { return intVal(a) + intVal(b); }, 0 );
              total = formatNumber(total,0,',','','','','','');
	            $( api.column( 6 ).footer() ).html('$' + total);
	        },       
    
		"language": {
			"sProcessing":     "Procesando...",
			"sLengthMenu":     "Mostrar _MENU_ registros",
			"sZeroRecords":    "No se encontraron resultados",
			"sEmptyTable":     "Ningún dato disponible en esta tabla",
			"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
			"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0",
			"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			"sInfoPostFix":    "",
			"sSearch":         "Buscar:",
			"sUrl":            "",
			"sInfoThousands":  ",",
			"sLoadingRecords": "Cargando...",
			"oPaginate": {
				"sFirst":    "Primero",
				"sLast":     "Último",
				"sNext":     "Siguiente",
				"sPrevious": "Anterior"
			},
			"oAria": {
				"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
				"sSortDescending": ": Activar para ordenar la columna de manera descendente"
			}
		},
		'order': [[ 7, 'desc' ]],
		'columnDefs': [ {'targets': 0,'orderable': false}, {'targets': 1,'orderable': false}, {'targets': 2,'orderable': false} ],
    dom: 'Bfrtip',
    buttons: [
        { extend:'copy', attr: { id: 'allan' } }, 'csv', 'excel', 'print'
    ]  
	});

	$("#seleccionarTodos").click(function () {
		var checkedCtr = $(this).prop("checked");
		$(".seleccion").each(function () {
			$(this).prop("checked", checkedCtr);
		});
	});

	$("#btnNuevo").click(function(){
		$("#modalNuevo").modal({backdrop: "static"});
	});

	$("#btnEditar").click(function(){
		var id ="";
		$(".seleccion").each(function () {
			if ($(this).prop("checked")) {
				id = $(this).attr("cod");
				return false;
			}
		});
		if (id !== "") {
			$("#id_g").val(id);
			traerRegistro(id);
		} else {
			swal({
				type: 'error',
				title: 'Ups...',
				text: 'Debes seleccionar un registro'
			});
		}
	});

	$(document).on("click", ".btnEditar", function(){
		var id ="";
		id = $(this).attr("cod");
		$("#id_g").val(id);
		traerRegistro(id);
	});

	function traerRegistro(id) {
		$.ajax({
			url: proyVar.base_url + "admin/administradores021/ordenes_productos_funciones/traerRegistro",
			cache: false,
			dataType: "json",
			type: "post",
			data: {"id":id, slcnts:tksec},
			success:function(datos) {
				tksec = datos.tksec;
				if (datos.registro != null) {
					for (var campoId in datos.registro) {
						if ( $("#"+campoId+"_g") ) {
              var tipo = $("#"+campoId+"_i").attr("type");
              if (tipo != "file") {
                $("#"+campoId+"_g").val(datos.registro[campoId]);
              } 
						}
					}
					$("#modalEditar").modal({backdrop: "static"});
				} else {
					swal({
						type: 'error',
						title: 'Ups...',
						text: 'No fue posible cargar el registro'
					});  
				}
			},
			error: function (request, status, error) {
				swal({
					type: 'error',
					title: 'Ups...',
					text: request.responseText
				});                    
			}
		});
	}

	$("#btnEliminar").click(function(){
		var id ="";
		$(".seleccion").each(function () {
			if ($(this).prop("checked")) {
				id += $(this).attr("cod") + ";";
			}
		});
		if (id !== "") {
			id += "0";
			$("#id_e").val(id);
			swal({
				title: 'Seguro que deseas eliminar?',
				text: "No podras revertir esta operación!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Si, eliminar!'
			}).then((result) => {
				if (result.value) {
					if (result.value) {
						$("#forma_e").submit();
					}
				}
			});
		} else {
			swal({
				type: 'error',
				title: 'Ups...',
				text: 'Debes seleccionar al menos un registro'
			});              
		}
	});

	$(document).on("click", ".btnEliminar", function(){
		var id ="";
		id = $(this).attr("cod");
		$("#id_e").val(id);
		swal({
			title: 'Seguro que deseas eliminar?',
			text: "No podras revertir esta operación!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Si, eliminar!'
		}).then((result) => {
			if (result.value) {
				$("#forma_e").submit();
			}
		});
	});      

	$( "#forma_i" ).validate( {
		rules: {
			producto: {required: true,number:true,},precio: {required: true,number:true,},cantidad: {required: true,number:true,},
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			error.addClass( "help-block" );
			if ( element.prop( "type" ) === "checkbox" ) {
				error.insertAfter( element.parent( "label" ) );
			} else {
				error.insertAfter( element );
			}
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
		},
		unhighlight: function (element, errorClass, validClass) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
		}
	} );    

	$( "#forma_g" ).validate( {
		rules: {
			producto: {required: true,number:true,},precio: {required: true,number:true,},cantidad: {required: true,number:true,},
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			error.addClass( "help-block" );
			if ( element.prop( "type" ) === "checkbox" ) {
				error.insertAfter( element.parent( "label" ) );
			} else {
				error.insertAfter( element );
			}
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
		},
		unhighlight: function (element, errorClass, validClass) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
		}
	} ); 

	$("#producto_i").change(function () {
		var producto = $("#producto_i").val();
		traerProducto(producto, "i");
	});

	$("#producto_g").change(function () {
		var producto = $("#producto_g").val();
		traerProducto(producto, "g");
	});

	function traerProducto(producto, modo) {
		$.ajax({
			url: proyVar.base_url + "admin/administradores021/ordenes_productos_funciones/traerProducto",
			cache: false,
			dataType: "json",
			type: "post",
			data: {"producto":producto, "modo":modo, slcnts:tksec},
			success:function(datos) {
				tksec = datos.tksec;
				$("#precio_"+modo).val(datos.registro.precio);
			},
			error: function (request, status, error) {
				swal({
					type: 'error',
					title: 'Ups...',
					text: request.responseText
				});                    
			}
		});
	}

  
  
  
  
  
  
  
});