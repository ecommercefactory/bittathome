$(function () {
    editAreaLoader.init({
      id: "codigo_php_config" // id of the textarea to transform    
      ,start_highlight: true  // if start with highlight
      ,allow_resize: "both"
      ,allow_toggle: true
      ,word_wrap: false
      ,language: proyVar.lang
      ,syntax: "php"  
    });
  
    editAreaLoader.init({
      id: "codigo_php_database" // id of the textarea to transform    
      ,start_highlight: true  // if start with highlight
      ,allow_resize: "both"
      ,allow_toggle: true
      ,word_wrap: false
      ,language: proyVar.lang
      ,syntax: "php"  
    });  
  
    editAreaLoader.init({
      id: "codigo_sql" // id of the textarea to transform    
      ,start_highlight: true  // if start with highlight
      ,allow_resize: "both"
      ,allow_toggle: true
      ,word_wrap: false
      ,language: "es"
      ,syntax: "sql"  
    });

    editAreaLoader.init({
      id: "codigo_apache" // id of the textarea to transform    
      ,start_highlight: true  // if start with highlight
      ,allow_resize: "both"
      ,allow_toggle: true
      ,word_wrap: false
      ,language: "es"
      ,syntax: "basic"  
    });

});