$(function () {
    editAreaLoader.init({
      id: "codigo_php" // id of the textarea to transform    
      ,start_highlight: true  // if start with highlight
      ,allow_resize: "both"
      ,allow_toggle: true
      ,word_wrap: true
      ,language: proyVar.lang
      ,syntax: "php"  
    });
    editAreaLoader.init({
      id: "codigo_js" // id of the textarea to transform    
      ,start_highlight: true  // if start with highlight
      ,allow_resize: "both"
      ,allow_toggle: true
      ,word_wrap: true
      ,language: "es"
      ,syntax: "js"  
    });
    editAreaLoader.init({
      id: "codigo_css" // id of the textarea to transform    
      ,start_highlight: true  // if start with highlight
      ,allow_resize: "both"
      ,allow_toggle: true
      ,word_wrap: true
      ,language: "es"
      ,syntax: "css"  
    });
    editAreaLoader.init({
      id: "codigo_sql" // id of the textarea to transform    
      ,start_highlight: true  // if start with highlight
      ,allow_resize: "both"
      ,allow_toggle: true
      ,word_wrap: true
      ,language: "es"
      ,syntax: "sql"  
    });
    editAreaLoader.init({
      id: "codigo_basic" // id of the textarea to transform    
      ,start_highlight: true  // if start with highlight
      ,allow_resize: "both"
      ,allow_toggle: true
      ,word_wrap: true
      ,language: "es"
      ,syntax: "basic"  
    });
    editAreaLoader.init({
      id: "codigo_html" // id of the textarea to transform    
      ,start_highlight: true  // if start with highlight
      ,allow_resize: "both"
      ,allow_toggle: true
      ,word_wrap: true
      ,language: "es"
      ,syntax: "html"  
    });
});