$(function () {
      $( "#forma" ).validate( {
        rules: {
          clave: {
						required: true,
						minlength: 7,
						maxlength: 20
          },
          clave_validacion: {
            equalTo: "#clave"
          },
        },
        messages: {
          clave: {
            required: txtVar.be_the_new_password_field_is_required,
            minlength: txtVar.be_your_new_password_must_have_at_least_7_characters,
            maxlength: txtVar.be_your_new_password_must_have_a_maximum_of_20_characters
          },          
          clave_validacion: {
            equalTo: txtVar.be_incorrect_confirmation
          },          
        },
        errorElement: "em",
				errorPlacement: function ( error, element ) {
					error.addClass( "help-block" );
					if ( element.prop( "type" ) === "checkbox" ) {
						error.insertAfter( element.parent( "label" ) );
					} else {
						error.insertAfter( element );
					}
				},
				highlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
				},
				unhighlight: function (element, errorClass, validClass) {
					$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
				}
      } );
});