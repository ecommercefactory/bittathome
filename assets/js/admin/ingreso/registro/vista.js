$(function () {
  

      $( "#forma" ).validate( {
        rules: {
          nombre: {
            required: true,
						maxlength: 50
          },
          apellidos: {
            required: true,
						maxlength: 50
          },
          correo: {
            required: true,
            email: true,
						maxlength: 70, 
						remote: {
							url: proyVar.base_url + "admin_ingreso/correo_registro",
							type: "post",
							cache: false,
							data: {
									correo: function() { return $("#correo").val(); },
									tu_id: function() { return $("#tu_id").val(); }
							}
						}		
          },
          clave: {
						required: true,
						minlength: 7,
						maxlength: 20
          },
          clave_validacion: {
            equalTo: "#clave"
          },
          terminos: {
            required: true
          },

        },
        messages: {
          nombre: {
            required: txtVar.be_the_first_name_field_is_required,
            maxlength: txtVar.be_your_name_must_have_a_maximum_of_50_characters
          },
          apellidos: {
            required: txtVar.be_the_last_names_field_is_required,
            maxlength: txtVar.be_your_last_names_must_have_a_maximum_of_50_characters
          },
          correo: {
            required: txtVar.be_the_email_field_is_required,
            email: txtVar.be_your_email_must_be_in_the_format_name_domain_com,
            maxlength: txtVar.be_your_email_must_have_a_maximum_of_70_characters,
						remote: jQuery.validator.format("{0} " + txtVar.be_already_exists)
          },
          clave: {
            required: txtVar.be_the_new_password_field_is_required,
            minlength: txtVar.be_your_new_password_must_have_at_least_7_characters,
            maxlength: txtVar.be_your_new_password_must_have_a_maximum_of_20_characters
          },          
          clave_validacion: {
            equalTo: txtVar.be_incorrect_confirmation
          },          
          terminos: {
            required: txtVar.be_you_must_accept_the_terms
          },          
        },
        errorElement: "em",
				errorPlacement: function ( error, element ) {
					error.addClass( "help-block" );
					if ( element.prop( "type" ) === "checkbox" ) {
						error.insertAfter( element.parent( "label" ) );
					} else {
						error.insertAfter( element );
					}
				},
				highlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
				},
				unhighlight: function (element, errorClass, validClass) {
					$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
				}
      } );

});