$(function () {

      $("input[name=correo]").focus();

      $( "#forma" ).validate( {
        rules: {
          correo: {
            required: true,
            email: true,
            maxlength: 70  
          },
          clave: {
						required: true,
						minlength: 7,
						maxlength: 20
          },
        },
        messages: {
          correo: {
            required: txtVar.be_the_email_field_is_required,
            email: txtVar.be_your_email_must_be_in_the_format_name_domain_com,
            maxlength: txtVar.be_your_email_must_have_a_maximum_of_70_characters
          },
          clave: {
            required: txtVar.be_the_password_field_is_required,
            minlength: txtVar.be_your_password_must_have_at_least_7_characters,
            maxlength: txtVar.be_your_password_must_have_a_maximum_of_20_characters
          },          
        },
        errorElement: "em",
				errorPlacement: function ( error, element ) {
					error.addClass( "help-block" );
					if ( element.prop( "type" ) === "checkbox" ) {
						error.insertAfter( element.parent( "label" ) );
					} else {
						error.insertAfter( element );
					}
				},
				highlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
				},
				unhighlight: function (element, errorClass, validClass) {
					$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
				}
      } );
});