<?php
	$lang['list_add'] 				= 'Lägg till';
	$lang['list_actions'] 			= 'Åtgärder';
	$lang['list_page'] 				= 'Sida';
	$lang['list_paging_of'] 		= 'av';
	$lang['list_displaying']		= 'Visar {start} till {end} av totalt {results} poster';
	$lang['list_filtered_from']		= '(filtrerad från {total_results} poster totalt)';
	$lang['list_show_entries']		= 'Visa {paging} poster';
	$lang['list_no_items']			= 'Inga poster att visa';
	$lang['list_zero_entries']		= 'Visar 0 från 0 av 0 poster';
	$lang['list_search'] 			= 'Sök';
	$lang['list_search_all'] 		= 'Sök samtliga';
	$lang['list_clear_filtering'] 	= 'Rensa filter';
	$lang['list_delete'] 			= 'Radera';
	$lang['list_edit'] 				= 'Ändra';
	$lang['list_paging_first'] 		= 'Första';
	$lang['list_paging_previous'] 	= 'Föregående';
	$lang['list_paging_next'] 		= 'Nästa';
	$lang['list_paging_last'] 		= 'Sista';
	$lang['list_loading'] 			= 'Laddar...';

	$lang['form_edit'] 				= 'Ändra';
	$lang['form_back_to_list'] 		= 'Tillbaka till listan';
	$lang['form_update_changes'] 	= 'Spara ändringar';
	$lang['form_cancel'] 			= 'Avbryt';
	$lang['form_update_loading'] 	= 'Laddar, uppdaterar ändringar...';
	$lang['update_success_message'] = 'Datat har uppdaterats.';
	$lang['form_go_back_to_list'] 	= 'Tillbaka till listan';

	$lang['form_add'] 				= 'Lägg till';
	$lang['insert_success_message'] = 'Datat har sparats i databasen.';
	$lang['form_or']				= 'eller';
	$lang['form_save'] 				= 'Spara';
	$lang['form_insert_loading'] 	= 'Laddar, sparar data...';

	$lang['form_upload_a_file'] 	= 'Ladda upp fil';
	$lang['form_upload_delete'] 	= 'radera';
	$lang['form_button_clear'] 		= 'Rensa';

	$lang['delete_success_message'] = 'Datat har raderats.';
	$lang['delete_error_message'] 	= 'Datat kunde inte raderas.';

	/* Javascript messages */
	$lang['alert_add_form']			= 'Datat kanske inte sparas.\\nVill du verkligen fortsätta?';
	$lang['alert_edit_form']		= 'Datat kanske inte sparas.\\nVill du verkligen fortsätta?';
	$lang['alert_delete']			= 'Vill du verkligen radera raden?';

	$lang['insert_error']			= 'Ett fel uppstod när datan skulle sparas.';
	$lang['update_error']			= 'Ett fel uppstod när datan skulle sparas.';

	/* Added in version 1.2.1 */
	$lang['set_relation_title']		= 'Välj {field_display_as}';
	$lang['list_record']			= 'Post';
	$lang['form_inactive']			= 'inaktiv';
	$lang['form_active']			= 'aktiv';

	/* Added in version 1.2.2 */
	$lang['form_save_and_go_back']	= 'Spara och gå tillbaka till listan';
	$lang['form_update_and_go_back']= 'Uppdatera och gå tillbaka till listan';

	/* Added in version 1.2.2 */
	$lang['form_save_and_go_back']	= 'Spara och gå tillbaka till listan';
	$lang['form_update_and_go_back']= 'Uppdatera och gå tillbaka till listan';

	/* Upload functionality */
	$lang['string_delete_file'] 	= "Raderar fil";
	$lang['string_progress'] 		= "Progress: ";
	$lang['error_on_uploading'] 	= "Ett fel uppstod vid uppladdning.";
	$lang['message_prompt_delete_file'] 	= "Vill du verkligen radera filen?";

	$lang['error_max_number_of_files'] 	= "Du kan bara ladda upp en fil åt gången.";
	$lang['error_accept_file_types'] 	= "Du får inte ladda upp den här filtypen.";
	$lang['error_max_file_size'] 		= "Den uppladdade filen överskrider {max_file_size}.";
	$lang['error_min_file_size'] 		= "Du kan inte ladda upp en tom fil.";

	/* Added in version 1.3.1 */
	$lang['list_export'] 	= "Exportera";
	$lang['list_print'] 	= "Skriv ut";
	$lang['minimize_maximize'] = 'Minimera/Maximera';
	
	/* Added in version 1.4 */
	$lang['list_view'] = 'Visa';

	/* Added in version 1.5.1 */
	$lang['ui_day'] = 'dd';
	$lang['ui_month'] = 'mm';
	$lang['ui_year'] = 'yyyy';

	/* Added in version 1.5.2 */
	$lang['list_more'] = 'More';

	/* Added in version 1.5.6 */
	$lang['list_search_column'] = 'Search {column_name}';

	/* Added in version 1.5.8 */
	$lang['alert_delete_multiple'] = 'Are you sure that you want to delete those {items_amount} items?';

	$lang['alert_delete_multiple_one'] = 'Are you sure that you want to delete this 1 item?';

