<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// NoPOO
class Utilities {
  
  public function code($num) {
      $str = "0123456789bcdfghjkmnpqrstvwxyz";
      $code = "";
      for($i=0;$i<20;$i++) $code .= substr($str,rand(0,30),1);
      return $code;
  }

  public function time($t) {
      $h = floor($t / (60*60));
      $m = floor(($t - $h*60*60) / (60));
      $s = $t - $h*60*60 - $m*60;
      if ($h<9) $h = '0'.$h;
      if ($m<9) $m = '0'.$m;
      if ($s<9) $s = '0'.$s;
      return $h . ':' . $m . ':' . $s;	
  }	  

  public function setLanguage($code = "") {
      $CI =& get_instance();
      $CI->load->model('Modelo'); 
      if ($code == "") {
          $languageCtr = $CI->Modelo->registros('languages', '', array('lang_default'=>4));
      } else {
          $languageCtr = $CI->Modelo->registros('languages', '', array('code'=>$code));
      }
      if (count($languageCtr)) {
          $CI->session->set_userdata($CI->config->item('raiz') . 'be_lang_name', $languageCtr[0]['name']);
          $CI->session->set_userdata($CI->config->item('raiz') . 'be_lang_code', $languageCtr[0]['code']);
          $CI->session->set_userdata($CI->config->item('raiz') . 'be_lang_own_name', $languageCtr[0]['own_name']);
          $CI->session->set_userdata($CI->config->item('raiz') . 'be_lang_value', $languageCtr[0]['value']);
          $CI->session->set_userdata($CI->config->item('raiz') . 'be_lang_recaptcha', $languageCtr[0]['recaptcha']);
      }

      $CI->lang->load('backend', $CI->session->userdata($CI->config->item('raiz') . 'be_lang_value'));
      $CI->config->set_item('language', $CI->session->userdata($CI->config->item('raiz') . 'be_lang_value') );

  }     
  
  // https://stackoverflow.com/questions/40002063/fsockopen-php-network-getaddresses-getaddrinfo-failed-name-or-service-not-k/40006576#40006576
  public function sendEmail($to, $subject, $message, $bcc = '') {
      $CI =& get_instance();
       $CI->load->library('email');
/*
      $config['protocol']    = 'smtp';
      $config['smtp_host']    = $CI->config->item('email_smtp_host');
      $config['smtp_user']    = $CI->config->item('email_smtp_user');
      $config['smtp_pass']    = $CI->config->item('email_smtp_pass');
      $config['smtp_port']    = $CI->config->item('email_smtp_port');
      $config['charset']    = 'utf-8'; // 'iso-8859-1'; //'utf-8';
      $config['newline']    = "\r\n";
      $config['crlf']    = "\r\n";
      $config['mailtype'] = 'html'; // text or html
      $config['validation'] = TRUE; // bool whether to validate email or not      
*/
$CI->email->initialize(array(
  'protocol' => 'smtp',
  'smtp_host' => $CI->config->item('email_smtp_host'),
  'smtp_user' => $CI->config->item('email_smtp_user'),
  'smtp_pass' => $CI->config->item('email_smtp_pass'),
  'smtp_port' => 587,
  'crlf' => "\r\n",
  'newline' => "\r\n"
));


      $CI->email->set_mailtype("html"); 


      $CI->email->from($CI->config->item('correo_escribenos'), $CI->config->item('email_smtp_user_name'));
      if ($bcc != '') $CI->email->bcc($bcc); 
      $CI->email->to($to); 
      $CI->email->subject($subject);
      $CI->email->message($message);  
      if ( $CI->email->send() ) {
          return true;
      } else {
          //return false;
          show_error($CI->email->print_debugger());
      }
  }

// - Archivos y Directorios -------------------------

  public function crear_archivo_ren ($dir, $nombre, $ext, $contenido) {
    $hoy = date('YmdHis');
    if ($this->existe_archivo ($dir, $nombre.$ext)) {
      if ($dir != '') $dir .= '/';
      $this->renombrar_archivo ($dir . $nombre.$ext, $nombre.'.'.$hoy.$ext);
    }
/*
    $mat = array();
    $filas = explode("\n", $contenido);
    foreach ($filas as $fila) {
      $mat[] = $fila;
    }
*/
    $this->crear_archivo ('.', $nombre.$ext, $contenido);  
  }

  public function cambiar_directorio ($dir) {
    if ( @chdir($dir) ) {
      return 'Cambio de directorio ' . $dir . ' exitoso<br/>';
    } else {
      return 'No fue posible cambiar al directorio ' . $dir . ' <br />';
    }
  }
  //echo 'Directorio actual: ' . actual_directorio () . '<br />';
  //cambiar_directorio ('carpetacreada');
  //echo 'Directorio actual: ' . actual_directorio () . '<br />';

  public function actual_directorio () {
    return getcwd();
  }

  public function leer_directorio ($dir) {
    $archivos = scandir($dir);
    if ( count($archivos) ) {
      sort($archivos);
      for ($i=0; $i<count($archivos); $i++) {
        $tipo = 'Arc';
        if ( is_dir($archivos[$i]) ) 
          $tipo = 'Dir';
        echo $archivos[$i] . ' -->> ' . $tipo . '<br />';
      }
    }
  }
  // Para Windows
  //leer_directorio ('.'); // Directorio actual
  //echo '<HR >';
  //leer_directorio ('..'); // Directorio padre
  //echo '<HR >';
  //leer_directorio ('/'); // Directorio raiz
  //echo '<HR >';
  //leer_directorio ('../bds'); // Directorio padre + otro directorio

  public function crear_directorio ($dir) {

    if (!file_exists($dir)) {
        if ( mkdir($dir, 0777, true) ) {
          return 'El directorio ' . $dir . ' fue creado con exito <br />'; 
        } else {
          return 'El directorio ' . $dir . ' no fue creado<br />'; 
        }
    } else {
        return "The directory $dir exists.";
    }

  }

  //echo crear_directorio ('kilo');
  //echo cambiar_directorio ('kilo');
  //echo crear_directorio ('jajaja');
  //echo cambiar_directorio ('jajaja');

  public function eliminar_directorio ($dir) {
    // Si falla presenta un error en pantalla y solo borra los directorios de la carpeta actual y q este vacio
    if ( @rmdir($dir) ) {
      return 'El directorio ' . $dir . ' fue eliminado con exito <br />'; 
    } else {
      return 'El directorio ' . $dir . ' no fue eliminado<br />'; 
    }
  }
  //echo cambiar_directorio ('kilo');
  //echo eliminar_directorio ('jajaja');
  //echo cambiar_directorio ('..');
  //echo 'Directorio actual: ' . actual_directorio () . '<br />';
  //echo eliminar_directorio ('kilo');

  public function existe_archivo ($dir,$archivo) {
    if (file_exists($dir.$archivo)) {
      return true; 
    } else {
      return false;
    }
  }
  //if ( existe_archivo ('prueba2','') ) {
  //  echo 'El directorio prueba existe <br />';
  //} else {
  //  echo 'El directorio prueba no existe <br />';
  //}

  public function copiar_archivo ($dir_ori, $archivo_ori, $dir_des, $archivo_des, $modo) {
    if ( $modo ) {
      if ( $this->existe_archivo ($dir_des,$archivo_des) ) {
        echo 'No se pudo copia el archivo '.$dir_des . $archivo_des.' porque ya existe un archivo con el mismo nombre <br />';
      } else {
        if (!copy($dir_ori . $archivo_ori, $dir_des . $archivo_des)) {
          echo 'No fue posible copiar el archivo '.$dir_des . $archivo_des.'<br />';
        } else {
          echo 'El archivo '.$dir_des . $archivo_des.' se copio con exito <br />';
        }
      }
    } else {
      if (!copy($dir_ori . $archivo_ori, $dir_des . $archivo_des)) {
        echo 'No fue posible copiar el archivo '.$dir_des . $archivo_des.'<br />';
      } else {
        echo 'El archivo '.$dir_des . $archivo_des.' se copio con exito <br />';
      }
    }
  }
  //copiar_archivo ('', 'sa_fxs.php', 'prueba/', 'funciones.txt', false);

  public function renombrar_archivo ($actual, $nuevo) {
    if ( rename ($actual, $nuevo) ) {
      return 'El archivo ' . $actual . ' fue renombrado como '.$nuevo.' <br />'; 
    } else {
      return 'El archivo ' . $actual . ' no fue renombrado como '.$nuevo.' <br />'; 
    }
  }
  //echo renombrar_archivo ('carpetacreada', 'prueba');
  //echo renombrar_archivo ('prueba/funciones.txt', 'prueba/borreme.txt');

  public function eliminar_archivo ($dir, $archivo) {
    if ( @unlink ($dir . $archivo) ) {
      return 'El archivo ' . $dir . $archivo . ' fue eliminado<br />'; 
    } else {
      return 'El archivo ' . $dir . $archivo . ' no fue eliminado<br />'; 
    }
  }
  //echo eliminar_archivo ('prueba/', 'funciones.txt');


  //$gestor = fopen("prueba_fopen.txt", "r"); // Lectura de un archivo
  //$gestor = fopen("prueba_fopen.txt", "w+"); // Escritura de un archivo desde el principio
  //$gestor = fopen("prueba_fopen.txt", "a+"); // Escritura de un archivo desde el principio
  //fclose($gestor); // Cierra el archivo

  public function leer_archivo ($dir, $archivo) {
    $gestor = @fopen($dir . $archivo, 'r');
    $mat = array();
    if ($gestor) {
      while (!feof($gestor)) {
        $mat[] = fgets($gestor, 4096);
      }
      fclose ($gestor);
    }
    return $mat;
  }
  //leer_archivo ('','sa_fxs.php');
  //leer_archivo ('prueba/','funciones.txt');


  public function obtener_contenido ($archivo) {
    $contenido = '';
    if (file_exists($archivo)) {
      $contenido = file_get_contents($archivo);
    }
    return $contenido;
  }



  public function abrir_archivo ($dir, $archivo, $modo) {
    return @fopen ($dir . $archivo, $modo);
  }

  public function cerrar_archivo ($gestor) {
    fclose($gestor);
  }

  public function escribir_archivo ($gestor, $contenido) {
    $contenido .= chr(13) . chr(10);
    if (fwrite($gestor, $contenido) === FALSE) {
      echo 'No se puede escribir el contenido: ' . $contenido;
    }
  }


  // crear_directorio ('prueba');

  //$mat = array();
  //$mat[] = 'Hola';
  //$mat[] = 'Kilo';
  //$mat[] = 'Linea 3';
  //crear_archivo ('prueba', 'kilo.php', $mat);


  public function crear_archivo ($dir, $archivo, $arr) {
    $abrir_arc = true;
    $dir_completo = '';
    if (trim($dir) != '')
      $dir_completo = $dir . '/'; 
    if ( $this->existe_archivo ($dir_completo, $archivo) ) {
      if ( !is_writable($dir_completo . $archivo) ) {
        $abrir_arc = false;
      }
    }
    if ( $abrir_arc ) {
      $gestor = $this->abrir_archivo ($dir_completo, $archivo, 'w+');
      if ( $gestor ) {
        foreach ($arr as $key => $value) {
          $this->escribir_archivo ($gestor,$value);
        }     
        $this->cerrar_archivo ($gestor);
        chmod($dir_completo . $archivo, 0777);
      } else {
        echo 'No fue posible abrir el archivo '.$dir . '/', $archivo.'<br />';
      }
    } else {
      echo 'No se puede escribir sobre el archivo '.$dir . '/', $archivo.'<br />';
    }
  }

  // Arreglar funciones y volver todo un objeto, la idea es q esta clase sirva para crear archivos y carpetas para la creacion de formularios

  public function adicionar_archivo ($dir, $archivo, $arr) {
    $abrir_arc = true;
    $dir_completo = '';
    if (trim($dir) != '')
      $dir_completo = $dir . '/';

      $modo = 'w+';
    if ( $this->existe_archivo ($dir_completo, $archivo) ) {
      if ( !is_writable($dir_completo, $archivo) ) {
        $abrir_arc = false;
              $modo = 'a+';
      }
    }
      $gestor = $this->abrir_archivo ($dir_completo, $archivo, $modo);
      if ( $gestor ) {
              foreach ($arr as $key => $value) {
                      $this->escribir_archivo ($gestor,$value);
              }
              $this->cerrar_archivo ($gestor);
      } else {
              echo 'No fue posible abrir el archivo '.$dir . '/', $archivo.'<br />';
      }

  }

  public function limpiarCadena($valor) {
    $valor = strtolower(trim($valor));
    $valor = str_replace("á","a",$valor);
    $valor = str_replace("Á","a",$valor);
    $valor = str_replace("é","e",$valor);
    $valor = str_replace("É","e",$valor);
    $valor = str_replace("í","i",$valor);
    $valor = str_replace("Í","i",$valor);
    $valor = str_replace("ó","o",$valor);
    $valor = str_replace("Ó","o",$valor);
    $valor = str_replace("ú","u",$valor);
    $valor = str_replace("Ú","u",$valor); 
    $valor = str_replace("ñ","n",$valor);
    $valor = str_replace("Ñ","n",$valor);
    $valor = str_replace(" ","-",$valor);
    return $valor;
  }
  
  function force_ssl() {
      if ($_SERVER['SERVER_NAME'] == "bittathome.com") {
        if (!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] != "on") {
            $url = "https://". $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
            redirect($url);
            exit;
        }
      }
  }

}