<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payu extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('cookie');
        $this->session->set_userdata($this->config->item('raiz') . 'fe_sesion_tienda', 1);

        if (!$this->session->has_userdata($this->config->item('raiz') . 'fe_codigo_referencia_pago')) {
            $referenceCode = rand(1000000,100000000);
            $this->session->set_userdata($this->config->item('raiz') . 'fe_codigo_referencia_pago', $referenceCode);
        }
// ----
        $tabla_pre_orden = $this->Modelo->registro($this->config->item('raiz_bd') . 'ordenes', $this->session->userdata($this->config->item('raiz') . 'fe_codigo_referencia_pago'), 'codigo_referencia_pago');
        if (count($tabla_pre_orden)) {
            if ($tabla_pre_orden->proceso_referencia == 1) {
                // actualizar datos de la orden
                $codigo = 0;
                $tabla_ordenes_tipos = $this->Modelo->registro($this->config->item('raiz_bd') . 'ordenes_tipos', 1);
                $codigo = $tabla_ordenes_tipos->secuencia_num + 1;
                $datosOT = array(
                    'secuencia_num' => $codigo,
                    'fecha_secuencia' => $this->config->item('YmdHis'),
                );
                $this->Modelo->actualizar($this->config->item('raiz_bd') . 'ordenes_tipos', $datosOT, 2);
                $datos = array(
                    'tipo_orden' => 1,
                    'codigo' => $codigo,);
                $idPreOrden = $this->Modelo->actualizar($this->config->item('raiz_bd') . 'ordenes', $datos, $this->session->userdata($this->config->item('raiz') . 'fe_codigo_referencia_pago'), 'codigo_referencia_pago');
                // refrescar datos
                $this->session->unset_userdata($this->config->item('raiz') . 'fe_carrito');
                $referenceCode = rand(1000000,100000000);
                $this->session->set_userdata($this->config->item('raiz') . 'fe_codigo_referencia_pago', $referenceCode);
            }
        }


    }

    public function index() {

        $this->utilities->force_ssl();
    
    	echo "PayU";
	}

	// PayU envia datos con URLs Amigables????
	public function respuesta($codigo, $valor) {
    	$respuesta = 0;
    	if ($codigo == 'transactionState') {
	        if (isset($valor)) {
	            $respuesta = $valor;
	        }
    	}
    	$this->parametros['respuesta'] = $respuesta;

        $tabla_categorias = $this->Modelo->registros($this->config->item('raiz_bd') . 'categorias', '', array(), 'nombre');      
        $this->parametros['tabla_categorias'] = $tabla_categorias;      

        $tabla_subcategorias = $this->Modelo->registros($this->config->item('raiz_bd') . 'subcategorias', '', array(), 'nombre');      
        $this->parametros['tabla_subcategorias'] = $tabla_subcategorias;      

        $proyVar = array(
            'base_url' => base_url(),
            'language' =>  $this->session->userdata($this->config->item('raiz') . 'be_lang_value'),
            'lang' =>  $this->session->userdata($this->config->item('raiz') . 'be_lang_code'),
        );
        $this->parametros['proyVar'] = '<script>var proyVar =' . json_encode($proyVar) . ';var proyVarS ={"sgctn":"'.$this->security->get_csrf_token_name().'","sgch":"'.$this->security->get_csrf_hash().'"};</script>';
      
        $lista_deseos = 0;
        if ($this->session->has_userdata($this->config->item('raiz') . 'fe_lista_deseos')) {
            $lista_deseos = count ($this->session->userdata($this->config->item('raiz') . 'fe_lista_deseos'));
        }
        $this->parametros['lista_deseos'] = $lista_deseos;

        $carrito_cantidad = 0;
        if ($this->session->has_userdata($this->config->item('raiz') . 'fe_carrito')) {
            foreach ($this->session->userdata($this->config->item('raiz') . 'fe_carrito') as $key => $value) {
                // $carrito_compras[$key] = $value;
                $carrito_cantidad += $value;
            }
        }
        $this->parametros['carrito_cantidad'] = $carrito_cantidad;

        $vista_css = '';
        $this->parametros['vista_css'] = $vista_css;

        $vista_js = '';
        $this->parametros['vista_js'] = $vista_js;

        $this->parametros['title'] = 'Bittat - Pagos Respuesta';

        $tabla_cms_contenidos = $this->Modelo->registros($this->config->item('raiz_bd') . 'cms_contenidos', '', array('estado'=>1), 'orden');      
        $tabla_cms_contenidos_ctr = array();
        if (count($tabla_cms_contenidos)) {
            foreach ($tabla_cms_contenidos as $reg_cms_contenidos) {
                $control = true;
                if ($reg_cms_contenidos['desde'] != "0000-00-00") {
                    if ($reg_cms_contenidos['desde'] <= $this->config->item('YmdHis')) {
                        if ($reg_cms_contenidos['hasta'] != "0000-00-00") {
                            if ($reg_cms_contenidos['hasta'] >= $this->config->item('YmdHis')) {
                                $control = true;
                            } else {
                                $control = false;
                            }
                        } else {
                            $control = true;
                        }
                    } else {
                        $control = false;
                    }
                } else {
                    if ($reg_cms_contenidos['hasta'] != "0000-00-00") {
                        if ($reg_cms_contenidos['hasta'] >= $this->config->item('YmdHis')) {
                            $control = true;
                        } else {
                            $control = false;
                        }
                    } else {
                        $control = true;
                    }                
                }
                if ($control) {
                    $tabla_cms_contenidos_ctr[] = $reg_cms_contenidos;
                }
            }
        }
        $this->parametros['tabla_cms_contenidos'] = $tabla_cms_contenidos_ctr;      

        $this->load->view('tienda/cabezote_1', $this->parametros);
        $this->load->view('tienda/payu_respuesta', $this->parametros);
        $this->load->view('tienda/pie_1', $this->parametros);
	}

	// PayU envia datos con URLs Amigables????
    public function confirmacion() {
    	// store/pagosonline_confirmacio.phtml
    	echo "Confirmacion";
	}

	// PayU envia datos con URLs Amigables????
    public function confirmacion_manual() {
    	// store/pagosonline_confirmacion_manula.phtml
    	echo "Confirmacion Manual";
	}

	public function pago_validacion() {
		// store/getpay.php

        $post = $this->input->post(NULL, TRUE);
        if (!empty($post)) {
            foreach ($post as $key => $value) {
                $post[$key] = $this->security->xss_clean($value);
            }
            echo "<pre>";
            print_r($post);
            echo "</pre>";
		} else {
			echo "No llegaron datos";
		}




/*
V154-V160
V154:
    WebCheckout
V155:
    Pruebas:
        Ingresar en el nombre del tarjetahabiente el valor APPROVED 
        si deseas que la transacción quede aprobada, REJECTED 
        si deseas que quede rechazada o PENDING si deseas que quede pendiente.
    Credenciales Produccion: 
        merchantId:     571258
        accountId:      573945
        apiKey:         5MwpVqeUW5gPRuu1Md32SzH654
        API Login:      33EV2z7WMII8fqn
    Credenciales Pruebas: 
        merchantId:     508029
        accountId:      512321
        apiKey:         4Vj8eK4rloUd272L48hsrarnUA
        API Login:      pRRXKOl8ikMmt9u
    URL WEBCheckOut:
        Pruebas:        https://sandbox.checkout.payulatam.com/ppp-web-gateway-payu/
        Produccion:     https://checkout.payulatam.com/ppp-web-gateway-payu/
    URL API:
        Pruebas:
                        Consultas:      https://sandbox.api.payulatam.com/reports-api/4.0/service.cgi
                        Pagos:          https://sandbox.api.payulatam.com/payments-api/4.0/service.cgi
        Produccion:
                        Consultas:      https://api.payulatam.com/reports-api/4.0/service.cgi
                        Pagos:          https://api.payulatam.com/payments-api/4.0/service.cgi


    signature:
        “ApiKey~merchantId~referenceCode~amount~currency”.

    $ApiKey = "5MwpVqeUW5gPRuu1Md32SzH654";
    $merchantId = "571258";
    $referenceCode = "kilo";
    $amount = 20000;
    $currency = "COP";
    $firma = "$ApiKey~$merchantId~$referenceCode~$amount~$currency";
    echo $firma;
    echo "<hr>";
    echo md5($firma);
    echo "<hr>";
    if (md5($firma) == "b7ffa470f4b6e4a1a83776c07c159146") {
        echo "Bien";
    } else {
        echo "Error";
    }
V156:
 <form method="post" action="https://sandbox.checkout.payulatam.com/ppp-web-gateway-payu/">
    <input name="merchantId"                    type="hidden"  value="571258"   >
    <input name="accountId"                     type="hidden"  value="573945" >
    <input name="description"                   type="hidden"  value="Todos los items agrupados"  >
        SUM(Nombre + Cantidad + SKU)
    <input name="referenceCode"                 type="hidden"  value="TestPayU" > // Codigo unico de la transaccion (# orden)
    <input name="amount"                        type="hidden"  value="20000"   > // Total de la transaccion
    <input name="tax"                           type="hidden"  value="3193"  > // Valor impuesto
    <input name="taxReturnBase"                 type="hidden"  value="16806" > // Base del tax
    <input name="currency"                      type="hidden"  value="COP" > // Moneda
    <input name="buyerEmail"                    type="hidden"  value="comprador@gmail.com" >
    
    <input name="responseUrl"                   type="hidden"  value="https://www.mitienda.com/payu/respuesta" >
    <input name="confirmationUrl"               type="hidden"  value="https://www.mitienda.com/payu/confirmacion" >
    <input name="declineResponseUrl"            type="hidden"  value="https://www.mitienda.com/payu/declinada" >

    <input name="displayShippingInformation"    type="hidden"  value="YES" >
    <input name="shippingAddress"               type="hidden"  value="Calle 95 #71-17" >
    <input name="shippingCity"                  type="hidden"  value="Bogota" >
    <input name="shippingCountry"               type="hidden"  value="CO" >

    <input name="test"                          type="hidden"  value="1" > // 1 =Prueba, 0 =Produccion
    <input name="signature"                     type="hidden"  value="7ee7cf808ce6a39b17481c54f2c57acc"  >

    <input name="Submit"                        type="submit"  value="Pagar" >

</form>



-------------------------------
Api key: 			5MwpVqeUW5gPRuu1Md32SzH654
Api login:			33EV2z7WMII8fqn
Llave pública:		PK6a4xX2b8YKNRwdy7u4X14yqv
Id Comercio:		571258
-------------------------------
Cuenta Id=			573945 
-------------------------------
Cuenta a debitar	573945

Las fechas con hora deben seguir el formato yyyy-MM-ddTHH:mm:ss, el formato de la hora es 24 horas. Ej. 2015-08-22T21:35:12.

En condiciones normales la conexión garantiza tiempos de respuesta de 3 segundos en promedio, si hay una situación anormal, el tiempo máximo de respuesta será de 1 minuto. Es altamente recomendable que configures los “timeouts” cuando te conectes con PayU.


Pruebas: https://sandbox.api.payulatam.com/payments-api/4.0/service.cgi 
Producción: https://api.payulatam.com/payments-api/4.0/service.cgi

<input type="hidden" name="url_respuesta" value="https://www.daflores.com/store/pagosonline_respuesta.phtml">
<input type="hidden" name="url_confirmacion" value="https://www.daflores.com/store/pagosonline_confirmacion.phtml">
<?




// --------------------------------------------------------------------

$cc_proc: Es igual a 1 x defecto

1: Authorize.net (Pagos en Dolares)
2: FirstData Global Gateway e4 (VISA y MasterCard + economicos q con PayU)
3: PayU Latam (Dinners y AMEX)

                if ( $_SESSION['currency'] == 'COP' ) {
                    if ( $card_type == 'diners' || $card_type == 'amex' ) {
                        $cc_proc = 3; // 3: PayU Latam
                        $cc_proc_acct = 7;
                    } else {
                        $cc_proc = 2; // 2: FirstData Global Gateway e4
                        $cc_proc_acct = 2;
                    }
                }
// ---------------------------------------------------------------------

fx_cc_authorize ( 
    $conf_cc_proc_user[$cc_proc_acct], 
    $conf_cc_proc_pass[$cc_proc_acct], 
    '0', 
    '1', 
    $order_num, 
    $total, 
    $ccnum, 
    $expdate, 
    $_POST['ccid'], 
    $_SESSION['cust_code'], 
    $customers['first_name'], 
    $customers['last_name'], 
    $customers['address1'], 
    $customers['city'], 
    $customers['state'], 
    $customers['zip'], 
    $customers['country'] );

function fx_cc_authorize ( 
    $user, 
    $password, 
    $test, 
    $capture, 
    $invoice_num, 
    $amount, 
    $ccnum, 
    $expdate, 
    $ccid, 
    $cust_code, 
    $first_name, 
    $last_name, 
    $address, 
    $city, 
    $state, 
    $zip, 
    $country )



API:

    Pruebas: https://sandbox.api.payulatam.com/payments-api/4.0/service.cgi 
    Producción: https://api.payulatam.com/payments-api/4.0/service.cgi

Todas las respuestas van con GET 
    asi que deben ir en una carpeta payu a la altura de la raiz

carrito/pago
    Aqui debe enviar las variables que requiere la autorizacion
    Seria muy bueno guardar la info en la orden a manera de "pre orden"
        Guardar en un campo un codigo de referencia para saber cual orden es
            echo(rand(1000000,100000000));
    Es muy importante saber cual es el metodo de pago
        Si es tajeta de credito debe procesar con la autorizacion en payu
        Si no es tarjeta de credito debe hacer el "Web Checkout" normal

*/

	}

	public function procesar() {
		// common_general/credit_card_procesor
	}




}