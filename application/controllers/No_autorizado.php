<?php
defined('BASEPATH') OR exit('Acceso no permitido');

class No_autorizado extends CI_Controller {

  public function __construct() {
    parent::__construct();
		//$this->ctrSegAdmin();
  }

  public function index() {

    $this->utilities->force_ssl();
    
    $datos['heading'] = 'Acceso Restringido';
    $datos['message'] = ''; //'No tenemos que ser adversarios. El emperador tendrá piedad de ti si me dices donde encontrar a los Jedi restantes.';
    
    $this->load->view('errors/html/error_general', $datos);
  }
  
}