<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backend extends MY_Controller {

    public function __construct() {
      parent::__construct();
      $this->load->helper('cookie');
      $this->load->library('encryption');
    }
  
  public function index() {

    $this->utilities->force_ssl();
    
    $ingreso = false;
    if (get_cookie($this->config->item('raiz') . 'ctrai1') and get_cookie($this->config->item('raiz') . 'ctrai2')) {
      $idEncriptado = get_cookie($this->config->item('raiz') . 'ctrai1');
      $id = $this->encryption->decrypt($idEncriptado);

      // Seleccion de la tabla de usuario correspondiente con la cookie si es que la tiene
      $t_usuario_id = $this->encryption->decrypt(get_cookie($this->config->item('raiz') . 'ctrai2'));
// ---------------------------------------------
      // $t_usuario = $this->Modelo->registro($this->config->item('raiz_bd') . 'datos_valores', $t_usuario_id, 'auxiliar_2');
      $t_usuario = $this->Modelo->registros($this->config->item('raiz_bd') . 'datos_valores', '', 'dato = (SELECT id FROM ' . $this->config->item('raiz_bd') . 'datos WHERE codigo = "tablas_usuarios") AND auxiliar_2 = "'.$t_usuario_id.'"');     
      $tabla_usuarios = $t_usuario[0]['auxiliar_1'];
// ---------------------------------------------
      $usuario = $this->Modelo->registros($this->config->item('raiz_bd') . $tabla_usuarios, '', array('id' => $id, 'estado' => 1) );
      if ( count($usuario) ) {
        $ctrCookie = false;
        $this->credenciales($usuario, $t_usuario_id, $ctrCookie);
      } else {
        delete_cookie($this->config->item('raiz') . 'ctrai1');        
        delete_cookie($this->config->item('raiz') . 'ctrai2');        
        $ingreso = true;
      }
    } else {
      $ingreso = true;
    }
    if ($ingreso) {

      // Lista de Tablas de Usuarios del Sistema
      $tablas_usuarios = $this->Modelo->registro($this->config->item('raiz_bd') . 'datos', 'tablas_usuarios', 'codigo');
      $t_usuarios = $this->Modelo->registros($this->config->item('raiz_bd') . 'datos_valores', '', array('dato'=>$tablas_usuarios->id), 'orden ASC' );
      if (count($t_usuarios) == 0) {
        echo $this->lang->line('be_user_tables_have_not_been_defined__contact_the_system_administrator');
        die();
      }
      $this->parametros['plantilla'] = 'ingreso';
      $this->parametros['vista'] = 'admin/ingreso/credenciales';
      $this->parametros['ruta_css'] = 'admin/ingreso';
      $this->parametros['datos']['t_usuarios'] = $t_usuarios; // Lista de Tablas de Usuarios del Sistema
      $languages = $this->Modelo->registros($this->config->item('raiz_bd') . 'languages', 'code, own_name', array(), 'code' );
      $this->parametros['datos']['languages'] = $languages;
      $this->load->view('plantilla_admin', $this->parametros);
      
    }
  }
  
  public function validacion() {
     $post = $this->input->post(NULL, TRUE);
     if (!empty($post)) {
        foreach ($post as $key => $value) {
            $post[$key] = $this->security->xss_clean($value);
        }

        if ($_SERVER['SERVER_NAME'] == "bittathome.com") {
            $recaptcha = $post['g-recaptcha-response'];
            if ($recaptcha != '') {
              $secret = $this->config->item('recaptcha_secret');
              $ip = $this->input->ip_address();
              $var = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secret&response=$recaptcha&remoteip=$ip");
              $array = json_decode($var, true);
            }
        } else {
            $recaptcha = 'kilo';
            $array['success'] = true;
        }


        if ($recaptcha != '') {
          if ($array['success']) {

              $this->form_validation->set_rules('tu_id', 'Tabla de Usuario', 'numeric|required|trim|xss_clean');
              $this->form_validation->set_rules('correo', 'Correo', 'max_length[70]|valid_email|required|trim|xss_clean', 
                   array(
                     'required' => $this->lang->line('be_the_email_field_is_required'),
                     'valid_email' => $this->lang->line('be_your_email_must_be_in_the_format_name_domain_com')
                   )                       
              );
              $this->form_validation->set_rules('clave', 'Clave', 'min_length[7]|max_length[20]|required|trim|xss_clean',
                   array(
                     'required' => $this->lang->line('be_the_password_field_is_required'),
                     'min_length' => $this->lang->line('be_your_password_must_have_at_least_7_characters'), 
                     'max_length' => $this->lang->line('be_your_password_must_have_a_maximum_of_20_characters')
                   )                       
              );
            
              if ($this->form_validation->run() == FALSE) {
                  $this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.validation_errors().$this->lang->line('be_please_try_again'));
                  $this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
                  redirect('/backend');           
              } else {
                  // Selecciona la tabla de usuario correspondiente
                  // $t_usuario = $this->Modelo->registro($this->config->item('raiz_bd') . 'datos_valores', $post['tu_id'], 'auxiliar_2');
                  // $tabla_usuarios = $t_usuario->auxiliar_1;
// ---------------------------------------------
                  $t_usuario = $this->Modelo->registros($this->config->item('raiz_bd') . 'datos_valores', '', 'dato = (SELECT id FROM ' . $this->config->item('raiz_bd') . 'datos WHERE codigo = "tablas_usuarios") AND auxiliar_2 = "'.$post['tu_id'].'"');
                  $tabla_usuarios = $t_usuario[0]['auxiliar_1'];
// ---------------------------------------------
                  $usuario = $this->Modelo->registros($this->config->item('raiz_bd') . $tabla_usuarios, '', array('correo' => $post['correo']) );
                  if ( count($usuario) ) {
                    if ($usuario[0]['clave'] === sha1($post['clave'])) {
                      if ($usuario[0]['estado'] == 1) {
                          $ctrCookie = false;
                          if (isset($post['recuerdame'])) {
                            $ctrCookie = true;
                          }
                          $this->credenciales($usuario, $post['tu_id'], $ctrCookie);
                      } else {
                          $this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.$this->lang->line('be_user_disabled__contact_your_system_administrator'));
                          $this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
                          redirect('/backend');    
                      }
                    } else {
                        $this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.$this->lang->line('be_invalid_password').'<br>'.$this->lang->line('be_please_try_again'));
                        $this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
                        redirect('/backend');    
                    }
                  } else {
                    $this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.$this->lang->line('be_invalid_email').'<br>'.$this->lang->line('be_please_try_again'));
                    $this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
                    redirect('/backend');    
                  }
              }
            
          } else {
            $this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.$this->lang->line('be_you_re_a_robot'));
            $this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
            redirect('/backend');    
          }
        } else {
          $this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.$this->lang->line('be_you_must_indicate_that_you_are_not_a_robot').'<br>'.$this->lang->line('be_please_try_again'));
          $this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
          redirect('/backend');    
        }

     }
  }

  public function credenciales($usuario, $tu_id, $ctrCookie) {
      if ($ctrCookie) {
        $cookie1 = array(
                'name'   => $this->config->item('raiz') . 'ctrai1',
                'value'  => $this->encryption->encrypt( $usuario[0]['id'] ),
                'expire' => $this->config->item('1mes')
                );
         $this->input->set_cookie($cookie1);
        $cookie2 = array(
                'name'   => $this->config->item('raiz') . 'ctrai2',
                'value'  => $this->encryption->encrypt($tu_id),
                'expire' => $this->config->item('1mes')
                );
         $this->input->set_cookie($cookie2);
      }

      // Selecciona la tabla de usuario correspondiente
      $t_usuario = $this->Modelo->registros($this->config->item('raiz_bd') . 'datos_valores', '', 'dato = (SELECT id FROM ' . $this->config->item('raiz_bd') . 'datos WHERE codigo = "tablas_usuarios") AND auxiliar_2 = "'.$tu_id.'"');
      $tabla_usuarios = $t_usuario[0]['auxiliar_1'];
    
      $session_id = $this->utilities->code(20);
      
      $datos1['fecha_ultimo_ingreso'] = $this->config->item('YmdHis'); // date('Y-m-d H:i:s');
      $datos1['ultima_ip'] = $this->input->ip_address();
      $datos1['session_id'] = $session_id;
      $this->Modelo->actualizar($this->config->item('raiz_bd') . $tabla_usuarios, $datos1, $usuario[0]['id']);

      $datos2['usuario'] = $usuario[0]['id'];
      $datos2['fecha_ingreso'] = $this->config->item('YmdHis'); // date('Y-m-d H:i:s');
      $datos2['ip'] = $this->input->ip_address();
      $pais = $this->config->item('default_country');
      $ch = curl_init("http://geoip3.maxmind.com/b?l=29gZpPNXhIMj&i=".$datos2['ip']);
      curl_setopt ($ch, CURLOPT_HEADER, 0);
      curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
      $response = curl_exec ($ch);
      $field = explode("," , $response);
      $ip_country_code = $field[0];
      if ( strlen($ip_country_code) ) {
        $regPais = $this->Modelo->registro($this->config->item('raiz_bd') . 'paises', $ip_country_code, 'codigo');
        if ($regPais) {
          $pais = $regPais->id;
        }
      }
      $datos2['pais'] = $pais;
      $tipo_agente = 0;
      $agente = '';
      $this->load->library('user_agent');
      if ($this->agent->is_mobile()) {
          $tipo_agente = 48;
          $agente = $this->agent->mobile() . ' ' . $this->agent->browser().' '.$this->agent->version();
      } else {
          if ($this->agent->is_robot()) {
              $tipo_agente = 47;
              $agente = $this->agent->robot();
          } else {
              if ($this->agent->is_browser()) {
                    $tipo_agente = 46;
                    $agente = $this->agent->browser().' '.$this->agent->version();
              }
          }
      }      
      $datos2['tipo_agente'] = $tipo_agente ;
      // $datos2['agente'] = $agente;    
// -------------------------------------    
      $agente_id = 0;
      $dato_agente = $this->Modelo->registros('datos', 'id', array('codigo'=>'agentes'));
      if (count($dato_agente)) {
          $dato_valor_agente = $this->Modelo->registros('datos_valores', 'id', array('dato'=>$dato_agente[0]['id'], 'nombre'=>$agente));
          if (count($dato_valor_agente)) {
              $agente_id = $dato_valor_agente[0]['id'];
          } else {
              $datosR['dato'] = $dato_agente[0]['id'];
              $datosR['nombre'] = $agente;
              $agente_id = $this->Modelo->insertar('datos_valores', $datosR);

              $numero_registros = $this->Modelo->filasDatos($dato_agente[0]['id']);
              $datosD['numero_registros'] = $numero_registros;
              $this->Modelo->actualizar($this->config->item('raiz_bd') . 'datos', $datosD, $dato_agente[0]['id']);                  
          }
      }
      $datos2['agente'] = $agente_id;    
// -------------------------------------
      $usuario_actividad_id = $this->Modelo->insertar($this->config->item('raiz_bd') . $tabla_usuarios . '_actividad', $datos2);

      $foto = ( trim($usuario[0]['foto']) == '' ? 'cart.png' : trim($usuario[0]['foto']) );
      
      $this->session->set_userdata($this->config->item('raiz') . 'be_usuario_id', $usuario[0]['id']);
      $this->session->set_userdata($this->config->item('raiz') . 'be_usuario_tabla', $tabla_usuarios);
      $this->session->set_userdata($this->config->item('raiz') . 'be_usuario_tabla_id', $t_usuario[0]['id']);
      $this->session->set_userdata($this->config->item('raiz') . 'be_usuario_perfil', $usuario[0]['perfil']);
      $this->session->set_userdata($this->config->item('raiz') . 'be_usuario_nombre', $usuario[0]['nombre']);
      $this->session->set_userdata($this->config->item('raiz') . 'be_usuario_apellidos', $usuario[0]['apellidos']);
      $this->session->set_userdata($this->config->item('raiz') . 'be_usuario_correo', $usuario[0]['correo']);
      $this->session->set_userdata($this->config->item('raiz') . 'be_usuario_foto', $foto);
      $this->session->set_userdata($this->config->item('raiz') . 'be_usuario_fecha_ingreso', $this->config->item('YmdHis'));
      $this->session->set_userdata($this->config->item('raiz') . 'be_usuario_fecha_cambio_clave', $usuario[0]['fecha_cambio_clave']);
      $this->session->set_userdata($this->config->item('raiz') . 'be_usuario_session_id', $session_id);
      $this->session->set_userdata($this->config->item('raiz') . 'be_usuario_actividad_id', $usuario_actividad_id);
      $languageCode = $this->Modelo->registro('languages',$usuario[0]['language']);
      $this->utilities->setLanguage($languageCode->code);

//      $this->lang->load('backend', $this->session->userdata($this->config->item('raiz') . 'be_lang_value'));
//      $this->config->set_item('language', $this->session->userdata($this->config->item('raiz') . 'be_lang_value') );

      // Footer
      $languages = $this->Modelo->registros($this->config->item('raiz_bd') . 'languages', 'code, own_name', array(), 'code' );
      $data_footer['languages'] = $languages;
      $plantilla_footer = $this->load->view('plantilla/admin/footer', $data_footer, true);
      $this->session->set_userdata($this->config->item('raiz') . 'be_plantilla_footer', $plantilla_footer);

      // Menu
      if ($tu_id == 1) // Usuarios Administrativos 
      {
        if ( $usuario[0]['perfil'] == 3) { // Administrador (ver tabla: estados)
          $plantilla_menu = $this->load->view('admin/menus/administrativo_administrador', '', true);
        }
        if ( $usuario[0]['perfil'] == 28) { // Invitado (ver tabla: estados)
          $plantilla_menu = $this->load->view('admin/menus/administrativo_invitado', '', true);
        }
      }
      else
      {
        $plantilla_menu = $this->load->view('admin/menus/' . $tabla_usuarios, '', true);
      }
      $this->session->set_userdata($this->config->item('raiz') . 'be_plantilla_menu', $plantilla_menu);

      // SideBar
      $plantilla_sidebar = $this->load->view('plantilla/admin/sidebar', '', true);
      $this->session->set_userdata($this->config->item('raiz') . 'be_plantilla_sidebar', $plantilla_sidebar);

      redirect('admin/panelcontrol');
     
  }
  
  public function salir() {
    if (get_cookie($this->config->item('raiz') . 'ctrai1') and get_cookie($this->config->item('raiz') . 'ctrai2')) {
        delete_cookie($this->config->item('raiz') . 'ctrai1');
        delete_cookie($this->config->item('raiz') . 'ctrai2');
    }

    $this->session->unset_userdata($this->config->item('raiz') . 'be_usuario_fecha_cambio_clave');
    $this->session->unset_userdata($this->config->item('raiz') . 'be_crud_tabla');
    $this->session->unset_userdata($this->config->item('raiz') . 'be_usuario_id');
    $this->session->unset_userdata($this->config->item('raiz') . 'be_usuario_tabla');
    $this->session->unset_userdata($this->config->item('raiz') . 'be_usuario_tabla_id');
    $this->session->unset_userdata($this->config->item('raiz') . 'be_usuario_perfil');
    $this->session->unset_userdata($this->config->item('raiz') . 'be_usuario_nombre');
    $this->session->unset_userdata($this->config->item('raiz') . 'be_usuario_apellidos');
    $this->session->unset_userdata($this->config->item('raiz') . 'be_usuario_correo');
    $this->session->unset_userdata($this->config->item('raiz') . 'be_usuario_foto');
    $this->session->unset_userdata($this->config->item('raiz') . 'be_usuario_fecha_ingreso');
    $this->session->unset_userdata($this->config->item('raiz') . 'be_usuario_fecha_cambio_clave');
    $this->session->unset_userdata($this->config->item('raiz') . 'be_usuario_session_id');
    $this->session->unset_userdata($this->config->item('raiz') . 'be_usuario_actividad_id');
    $this->session->unset_userdata($this->config->item('raiz') . 'be_plantilla_footer');
    $this->session->unset_userdata($this->config->item('raiz') . 'be_plantilla_menu');
    
    $this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-info"></i> '.$this->lang->line('be_successful_exit').'</h4>'.$this->lang->line('be_see_you_soon'));
    $this->session->set_flashdata('alertaTipo', 'info'); // success, info, warning, danger
    redirect('/backend');
  }

  public function olvido() {

    // Lista de Tablas de Usuarios del Sistema
    $tablas_usuarios = $this->Modelo->registro($this->config->item('raiz_bd') . 'datos', 'tablas_usuarios', 'codigo');
    $t_usuarios = $this->Modelo->registros($this->config->item('raiz_bd') . 'datos_valores', '', array('dato'=>$tablas_usuarios->id), 'orden ASC' );
    if (count($t_usuarios) == 0) {
      echo $this->lang->line('be_user_tables_have_not_been_defined__contact_the_system_administrator');
      die();
    }
    $this->parametros['plantilla'] = 'ingreso';
    $this->parametros['vista'] = 'admin/ingreso/olvido';
    $this->parametros['ruta_css'] = 'admin/ingreso';
    $this->parametros['datos']['t_usuarios'] = $t_usuarios; // Lista de Tablas de Usuarios del Sistema
    $languages = $this->Modelo->registros($this->config->item('raiz_bd') . 'languages', 'code', array(), 'code' );
    $this->parametros['datos']['languages'] = $languages;
    $this->load->view('plantilla_admin', $this->parametros);    
  }

  public function validacion_olvido() {
     // $post = $this->input->post();
     $post = $this->input->post(NULL, TRUE);
     if (!empty($post)) {
        foreach ($post as $key => $value) {
            $post[$key] = $this->security->xss_clean($value);
        }



        if ($_SERVER['SERVER_NAME'] == "bittathome.com") {
            $recaptcha = $post['g-recaptcha-response'];
            if ($recaptcha != '') {
              $secret = $this->config->item('recaptcha_secret');
              $ip = $this->input->ip_address();
              $var = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secret&response=$recaptcha&remoteip=$ip");
              $array = json_decode($var, true);
            }
        } else {
            $recaptcha = 'kilo';
            $array['success'] = true;
        }


        if ($recaptcha != '') {
          if ($array['success']) {


       
              $this->form_validation->set_rules('tu_id', 'Tabla de Usuario', 'numeric|required|trim|xss_clean');
              $this->form_validation->set_rules('correo', 'Correo', 'max_length[70]|valid_email|required|trim|xss_clean');
              if ($this->form_validation->run() == FALSE) {
                  $this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.validation_errors().'<br>'.$this->lang->line('be_please_try_again'));
                  $this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
                  redirect('/backend/olvido');           
              } else {

                  // Conociendo el codigo de la tabla lo trae directamente -> kiloCtr
                  // $t_usuario = $this->Modelo->registro($this->config->item('raiz_bd') . 'datos_valores', $post['tu_id'], 'auxiliar_2');
                  // $tabla_usuarios = $t_usuario->auxiliar_1;
// ---------------------------------------------
                  $t_usuario = $this->Modelo->registros($this->config->item('raiz_bd') . 'datos_valores', '', 'dato = (SELECT id FROM ' . $this->config->item('raiz_bd') . 'datos WHERE codigo = "tablas_usuarios") AND auxiliar_2 = "'.$post['tu_id'].'"');
                  $tabla_usuarios = $t_usuario[0]['auxiliar_1'];
// ---------------------------------------------

                  $usuario = $this->Modelo->registros($this->config->item('raiz_bd') . $tabla_usuarios, '', array('correo' => $post['correo']) );
                  if ( count($usuario) ) {

                    $codigo = $this->utilities->code(20);
                    $codigo .= $post['tu_id'];

                    $datos['codigo_clave_cambio'] = $codigo;
                    $datos['fecha_clave_cambio'] = $this->config->item('Ymd2His');
                    if ( $this->Modelo->actualizar($this->config->item('raiz_bd') . $tabla_usuarios, $datos, $usuario[0]['id']) ) {
              // -----------------------------------------------------------------------------------
                      $datosCorreo['codigo'] = $codigo;
                      $datosCorreo['nombre'] = $usuario[0]['nombre'];
                      $datosCorreo['apellidos'] = $usuario[0]['apellidos'];
                      $vista = $this->load->view('admin/correos/nueva_clave', $datosCorreo, true);
                      if ($this->utilities->sendEmail($post['correo'], $this->lang->line('be_define_a_new_password'), $vista, 'r.reyes@bittathome.com')) {
                        $this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-check"></i> '.$this->lang->line('be_successful_operation').'</h4>'.$this->lang->line('be_we_have_sent_a_message_to_your_email_to_define_a_new_password')); 
                        $this->session->set_flashdata('alertaTipo', 'success'); // success, info, warning, danger
                        redirect('/backend'); 
                      } else {
                        $this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.$this->lang->line('be_it_was_not_possible_to_process_your_request').'<br>'.$this->lang->line('be_please_try_again'));
                        $this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
                        redirect('/backend/olvido');  
                      }
                      
              // -----------------------------------------------------------------------------------
                    } else {
                      $this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.$this->lang->line('be_it_was_not_possible_to_process_your_request').'<br>'.$this->lang->line('be_please_try_again'));
                      $this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
                      redirect('/backend/olvido');  
                    }
                  } else {
                    $this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.$this->lang->line('be_email_not_found'));
                    $this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
                    redirect('/backend/olvido');    
                  }

              }

          } else {
            $this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.$this->lang->line('be_you_re_a_robot'));
            $this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
            redirect('/backend');    
          }
        } else {
          $this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.$this->lang->line('be_you_must_indicate_that_you_are_not_a_robot').'<br>'.$this->lang->line('be_please_try_again'));
          $this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
          redirect('/backend');    
        }
       
     }
  }
  
  public function nueva_clave() {
    $id = $this->uri->segment(3);
    $tu_id = substr($id, -1);

    // Teniedo el id se trae el nombre de la tabla de usuario directamente -> kiloCtr
    // $t_usuario = $this->Modelo->registro($this->config->item('raiz_bd') . 'datos_valores', $tu_id, 'auxiliar_2');
    // $tabla_usuarios = $t_usuario->auxiliar_1;
// ---------------------------------------------
    $t_usuario = $this->Modelo->registros($this->config->item('raiz_bd') . 'datos_valores', '', 'dato = (SELECT id FROM ' . $this->config->item('raiz_bd') . 'datos WHERE codigo = "tablas_usuarios") AND auxiliar_2 = "'.$tu_id.'"');    
    $tabla_usuarios = $t_usuario[0]['auxiliar_1'];
// ---------------------------------------------

    $usuario = $this->Modelo->registros($this->config->item('raiz_bd') . $tabla_usuarios, '', array('codigo_clave_cambio'=>$id) );
    if (count($usuario)) {
      if ( $usuario[0]['fecha_clave_cambio'] > $this->config->item('YmdHis') ) {    
        $this->parametros['datos']['id'] = $id;
        $this->parametros['datos']['tu_id'] = $tu_id;
        $this->parametros['plantilla'] = 'ingreso';
        $this->parametros['vista'] = 'admin/ingreso/nueva_clave';
        $this->parametros['ruta_css'] = 'admin/ingreso';
        $languages = $this->Modelo->registros($this->config->item('raiz_bd') . 'languages', 'code', array(), 'code' );
        $this->parametros['datos']['languages'] = $languages;
        $this->load->view('plantilla_admin', $this->parametros);    
      } else {
        $this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.$this->lang->line('be_you_have_exceeded_the_time_limit_to_define_your_password').'<br>'.$this->lang->line('be_please_try_again'));
        $this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
        redirect('/backend/olvido');    
      }
    } else {
      $this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.$this->lang->line('be_user_not_found').'<br>'.$this->lang->line('be_please_try_again'));
      $this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
      redirect('/backend/olvido');    
    }
  }
  
  public function validacion_nueva_clave() {
     // $post = $this->input->post();
     $post = $this->input->post(NULL, TRUE);
     if (!empty($post)) {
        foreach ($post as $key => $value) {
            $post[$key] = $this->security->xss_clean($value);
        }



        if ($_SERVER['SERVER_NAME'] == "bittathome.com") {
            $recaptcha = $post['g-recaptcha-response'];
            if ($recaptcha != '') {
              $secret = $this->config->item('recaptcha_secret');
              $ip = $this->input->ip_address();
              $var = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secret&response=$recaptcha&remoteip=$ip");
              $array = json_decode($var, true);
            }
        } else {
            $recaptcha = 'kilo';
            $array['success'] = true;
        }


        if ($recaptcha != '') {
          if ($array['success']) {



              $this->form_validation->set_rules('tu_id', 'Tabla de Usuario', 'numeric|required|trim|xss_clean');
              $this->form_validation->set_rules('clave', 'Clave', 'min_length[7]|max_length[20]|required|trim|xss_clean');
              if ($this->form_validation->run() == FALSE) {
                  $this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.validation_errors().'<br>'.$this->lang->line('be_please_try_again'));
                  $this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
                  redirect('/backend/nueva_clave');           
              } else {

                  // kiloCtr
                  // $t_usuario = $this->Modelo->registro($this->config->item('raiz_bd') . 'datos_valores', $post['tu_id'], 'auxiliar_2');
                  // $tabla_usuarios = $t_usuario->auxiliar_1;
// ---------------------------------------------
                  $t_usuario = $this->Modelo->registros($this->config->item('raiz_bd') . 'datos_valores', '', 'dato = (SELECT id FROM ' . $this->config->item('raiz_bd') . 'datos WHERE codigo = "tablas_usuarios") AND auxiliar_2 = "'.$post['tu_id'].'"');
                  $tabla_usuarios = $t_usuario[0]['auxiliar_1'];
// ---------------------------------------------

                  $usuario = $this->Modelo->registros($this->config->item('raiz_bd') . $tabla_usuarios, '', array('codigo_clave_cambio'=>$post['id']) );

                  if (count($usuario)) {
                    if ( $usuario[0]['fecha_clave_cambio'] > $this->config->item('YmdHis') ) {

                      $datos['clave'] = sha1($post['clave']);
                      $datos['codigo_clave_cambio'] = "";
                      $datos['fecha_clave_cambio'] = $this->config->item('YmdHis');
                      $datos['fecha_cambio_clave'] = $this->config->item('Y6md');

                      if ( $this->Modelo->actualizar($this->config->item('raiz_bd') . $tabla_usuarios, $datos, $usuario[0]['id']) ) {
              // -----------------------------------------------------------------------------------
                        $datosCorreo['nombre'] = $usuario[0]['nombre'];
                        $datosCorreo['apellidos'] = $usuario[0]['apellidos'];
                        $vista = $this->load->view('admin/correos/nueva_clave_actualizada', $datosCorreo, true);
                        
                        if ($this->utilities->sendEmail($usuario[0]['correo'], $this->lang->line('be_new_updated_password'), $vista, 'r.reyes@bittathome.com')) {
                            $this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-check"></i> '.$this->lang->line('be_successful_operation').'</h4>'.$this->lang->line('be_your_new_password_has_been_updated'));
                            $this->session->set_flashdata('alertaTipo', 'success'); // success, info, warning, danger
                        } else {
                            $this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.$this->lang->line('be_your_new_password_has_been_updated').' '.$this->lang->line('be_it_was_not_possible_to_send_the_process_confirmation_email').'<br>'.$this->lang->line('be_please_try_again'));
                            $this->session->set_flashdata('alertaTipo', 'info'); // success, info, warning, danger
                        }
                        
                        redirect('/backend');    
                      } else {
                        $this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.$this->lang->line('be_it_was_not_possible_to_update_your_new_password').'<br>'.$this->lang->line('be_please_try_again'));
                        $this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
                        redirect('/backend/nueva_clave');    
                      }
                    } else {
                        $this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.$this->lang->line('be_it_was_not_possible_to_update_your_new_password_because_you_have_exceeded_the_time_limit_to_do_so').'<br>'.$this->lang->line('be_please_try_again'));
                        $this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
                        redirect('/backend/nueva_clave');    
                    }
                  } else {
                    $this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.$this->lang->line('be_it_was_not_possible_to_update_your_new_password_because_your_user_was_not_found').'<br>'.$this->lang->line('be_please_try_again'));
                    $this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
                    redirect('/backend/nueva_clave');    
                  }
              }
          } else {
            $this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.$this->lang->line('be_you_re_a_robot'));
            $this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
            redirect('/backend');    
          }
        } else {
          $this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.$this->lang->line('be_you_must_indicate_that_you_are_not_a_robot').'<br>'.$this->lang->line('be_please_try_again'));
          $this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
          redirect('/backend');    
        }
       
     }
  }
  
  public function registro() {
    $tablas_usuarios = $this->Modelo->registro($this->config->item('raiz_bd') . 'datos', 'tablas_usuarios', 'codigo');

    // Lista de Tablas de Usuarios del Sistema
    $t_usuarios = $this->Modelo->registros($this->config->item('raiz_bd') . 'datos_valores', '', array('dato'=>$tablas_usuarios->id), 'orden ASC' );
    if (count($t_usuarios) == 0) {
      echo $this->lang->line('be_user_tables_have_not_been_defined__contact_the_system_administrator');
      die();
    }

    $this->parametros['plantilla'] = 'ingreso';
    $this->parametros['vista'] = 'admin/ingreso/registro';
    $this->parametros['ruta_css'] = 'admin/ingreso';
    $this->parametros['datos']['t_usuarios'] = $t_usuarios; // Lista de Tablas de Usuarios del Sistema
    $languages = $this->Modelo->registros($this->config->item('raiz_bd') . 'languages', 'code', array(), 'code' );
    $this->parametros['datos']['languages'] = $languages;
    $this->load->view('plantilla_admin', $this->parametros);    
  }
  
  public function validacion_registro() {
    // $post = $this->input->post();
    $post = $this->input->post(NULL, TRUE);
    
    if (!empty($post)) {
        foreach ($post as $key => $value) {
          $post[$key] = $this->security->xss_clean($value);
        }



        if ($_SERVER['SERVER_NAME'] == "bittathome.com") {
            $recaptcha = $post['g-recaptcha-response'];
            if ($recaptcha != '') {
              $secret = $this->config->item('recaptcha_secret');
              $ip = $this->input->ip_address();
              $var = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secret&response=$recaptcha&remoteip=$ip");
              $array = json_decode($var, true);
            }
        } else {
            $recaptcha = 'kilo';
            $array['success'] = true;
        }


        if ($recaptcha != '') {
          if ($array['success']) {



              $this->form_validation->set_rules('tu_id', 'Tabla de Usuario', 'numeric|required|trim|xss_clean');
              $this->form_validation->set_rules('nombre', 'Nombre', 'max_length[50]|required|trim|xss_clean');
              $this->form_validation->set_rules('apellidos', 'Apellidos', 'max_length[50]|required|trim|xss_clean');
              $this->form_validation->set_rules('correo', 'Correo', 'max_length[70]|valid_email|required|trim|xss_clean');
              $this->form_validation->set_rules('clave', 'Clave', 'min_length[7]|max_length[20]|required|trim|xss_clean');
              if ($this->form_validation->run() == FALSE) {
                  $this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.validation_errors().'<br>'.$this->lang->line('be_please_try_again'));
                  $this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
                  redirect('/backend/registro');    
              } else {

                // kiloCtr
                // $t_usuario = $this->Modelo->registro($this->config->item('raiz_bd') . 'datos_valores', $post['tu_id'], 'auxiliar_2');
                // $tabla_usuarios = $t_usuario->auxiliar_1;
// ---------------------------------------------
                $t_usuario = $this->Modelo->registros($this->config->item('raiz_bd') . 'datos_valores', '', 'dato = (SELECT id FROM ' . $this->config->item('raiz_bd') . 'datos WHERE codigo = "tablas_usuarios") AND auxiliar_2 = "'.$post['tu_id'].'"');
                $tabla_usuarios = $t_usuario[0]['auxiliar_1'];
// ---------------------------------------------

                $codigo = $this->utilities->code(20);
                $codigo .= $post['tu_id'];

                $datos = array(
                    'nombre' => $post['nombre'],
                    'apellidos' => $post['apellidos'],
                    'correo' => $post['correo'],
                    'clave' => sha1($post['clave']),
                    'perfil' => 28,
                    'fecha_ingreso' => $this->config->item('YmdHis'),
                    'estado' => 2, 
                    'codigo_activacion' => $codigo,
                    'fecha_cambio_clave' => $this->config->item('Y6md')
                );
                $id = $this->Modelo->insertar($this->config->item('raiz_bd') . $tabla_usuarios, $datos);
                if ($id > 0) {
      // -----------------------------------------------------------------------------------
                  $datosCorreo['codigo'] = $codigo;
                  $datosCorreo['nombre'] = $post['nombre'];
                  $datosCorreo['apellidos'] = $post['apellidos'];
                  $vista = $this->load->view('admin/correos/bienvenida', $datosCorreo, true);
                  
                  if ($this->utilities->sendEmail($post['correo'], $this->lang->line('be_welcome'), $vista, 'r.reyes@bittathome.com')) {
                      $this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-check"></i> '.$this->lang->line('be_successful_operation').'</h4>'.$this->lang->line('be_we_have_sent_a_message_to_your_email_to_activate_your_membership'));
                      $this->session->set_flashdata('alertaTipo', 'success'); // success, info, warning, danger
                  } else {
                      $this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.$this->lang->line('be_it_was_not_possible_to_send_the_process_confirmation_email').'<br>'.$this->lang->line('be_please_try_again'));
                      $this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
                  }
                  
                  redirect('/backend');    
                } else {
                  $this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.$this->lang->line('be_it_was_not_possible_to_create_your_membership').'<br>'.$this->lang->line('be_please_try_again'));
                  $this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
                  redirect('/backend/registro');    
                }          
              }
            
          } else {
            $this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.$this->lang->line('be_you_re_a_robot'));
            $this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
            redirect('/backend');    
          }
        } else {
          $this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.$this->lang->line('be_you_must_indicate_that_you_are_not_a_robot').'<br>'.$this->lang->line('be_please_try_again'));
          $this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
          redirect('/backend');    
        }
    }
  }
  
  public function correo_registro() {
    // $post = $this->input->post();
    $post = $this->input->post(NULL, TRUE);
    if (!empty($post)) {
        foreach ($post as $key => $value) {
          $post[$key] = $this->security->xss_clean($value);
        }

        // kiloCtr
        // $t_usuario = $this->Modelo->registro($this->config->item('raiz_bd') . 'datos_valores', $post['tu_id'], 'auxiliar_2');
        // $tabla_usuarios = $t_usuario->auxiliar_1;
// ---------------------------------------------
        $t_usuario = $this->Modelo->registros($this->config->item('raiz_bd') . 'datos_valores', '', 'dato = (SELECT id FROM ' . $this->config->item('raiz_bd') . 'datos WHERE codigo = "tablas_usuarios") AND auxiliar_2 = "'.$post['tu_id'].'"');
        $tabla_usuarios = $t_usuario[0]['auxiliar_1'];
// ---------------------------------------------

        $usuarioCorreoCtr = $this->Modelo->registros($this->config->item('raiz_bd') . $tabla_usuarios, '', array('correo'=>$post['correo']) );
        if (count($usuarioCorreoCtr)) {
            echo "false"; // ya existe
        } else {
            echo "true"; // no existe
        }
    } else {
      echo "false"; // error
    }
  }
  
  public function membresia_activacion() {
    $id = $this->uri->segment(3);
    $tu_id = substr($id, -1);

    // kiloCtr
    // $t_usuario = $this->Modelo->registro($this->config->item('raiz_bd') . 'datos_valores', $tu_id, 'auxiliar_2');
    // $tabla_usuarios = $t_usuario->auxiliar_1;
// ---------------------------------------------
    $t_usuario = $this->Modelo->registros($this->config->item('raiz_bd') . 'datos_valores', '', 'dato = (SELECT id FROM ' . $this->config->item('raiz_bd') . 'datos WHERE codigo = "tablas_usuarios") AND auxiliar_2 = "'.$tu_id.'"');
    $tabla_usuarios = $t_usuario[0]['auxiliar_1'];
// ---------------------------------------------

    
    $usuario = $this->Modelo->registros($this->config->item('raiz_bd') . $tabla_usuarios, '', array('codigo_activacion'=>$id) );
    if (count($usuario)) {
        $datos['estado'] = 1;
        $datos['fecha_activacion'] = $this->config->item('YmdHis');
        if ( $this->Modelo->actualizar($this->config->item('raiz_bd') . $tabla_usuarios, $datos, $usuario[0]['id']) ) {
// -----------------------------------------------------------------------------------
            $datosCorreo['nombre'] = $usuario[0]['nombre'];
            $datosCorreo['apellidos'] = $usuario[0]['apellidos'];
            $vista = $this->load->view('admin/correos/membresia_activada', $datosCorreo, true);

            if ($this->utilities->sendEmail($usuario[0]['correo'], $this->lang->line('be_congratulations__your_membership_has_been_activated'), $vista, 'r.reyes@bittathome.com')) {
                $this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_successful_operation').'</h4>'.$this->lang->line('be_your_membership_has_been_activated'));
                $this->session->set_flashdata('alertaTipo', 'success'); // success, info, warning, danger
            } else {
                $this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.$this->lang->line('be_your_membership_has_been_activated').' '.$this->lang->line('be_it_was_not_possible_to_send_the_process_confirmation_email').'<br>'.$this->lang->line('be_please_try_again'));
                $this->session->set_flashdata('alertaTipo', 'info'); // success, info, warning, danger
            }	          

            redirect('/backend');    
        } else {
          $this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.$this->lang->line('be_it_was_not_possible_to_activate_your_membership').'<br>'.$this->lang->line('be_please_try_again'));
          $this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
          redirect('/backend/registro');    
        }
    } else {
      $this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.$this->lang->line('be_user_not_found').'<br>'.$this->lang->line('be_please_try_again'));
      $this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
      redirect('/backend/registro');    
    }
  }

}