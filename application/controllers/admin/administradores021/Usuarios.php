<?php
defined("BASEPATH") OR exit("No esta permitido el acceso directo a este script.");

class Usuarios extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->ctrSegAdmin();
	}

	public function index() {
		$this->parametros['plantilla'] = 'datatables';
		$this->parametros['vista'] = 'admin/administradores021/usuarios';
		$this->parametros['datos']['titulo'] = 'Clientes';
		$this->parametros['datos']['subtitulo'] = ''; 
    $tabla_usuarios = $this->Modelo->registros($this->config->item('raiz_bd') . 'usuarios', '', array() );
		$this->parametros['datos']['lista'] = $tabla_usuarios;
    
    $tabla_datos_valores = $this->Modelo->registros($this->config->item('raiz_bd') . 'datos_valores', 'id, nombre', array('dato'=>17), 'orden ASC' ); 
    $this->parametros['datos']['tabla_datos_valores'] = $tabla_datos_valores;
    
		$this->load->view('plantilla_admin', $this->parametros);			
	}

	public function traerRegistro () {
		$post = $this->input->post(NULL, TRUE);
		$registro = $this->Modelo->registro($this->config->item('raiz_bd') . 'usuarios', $post['id']);
		$datos = array('registro'=>$registro, 'tksec'=>$this->security->get_csrf_hash());
		echo json_encode($datos);      
	}

	public function ingresar () {
		$post = $this->input->post(NULL, TRUE);
		if (!empty($post)) {
			foreach ($post as $key => $value) {
				$post[$key] = $this->security->xss_clean($value);
			}
      
			$this->form_validation->set_rules('nombre', 'Nombre', 'required|xss_clean');
//      $this->form_validation->set_rules('password', 'Password', 'xss_clean');
      $this->form_validation->set_rules('email', 'Correo', 'required|valid_email|xss_clean');
      $this->form_validation->set_rules('modo', 'Modo', 'required|xss_clean');
      $this->form_validation->set_rules('foto', 'Foto', 'xss_clean');
//      $this->form_validation->set_rules('verificacion', 'Verificacion', 'numeric|xss_clean');
//      $this->form_validation->set_rules('emailEncriptado', 'Emailencriptado', 'xss_clean');
      $this->form_validation->set_rules('fecha', 'Fecha', 'xss_clean');
      
			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.validation_errors().'<br><b>'.$this->lang->line('be_please_try_again').'</b>');
				$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
			} else {

				$config['upload_path'] = './imagenes/usuario/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 2048;
        $this->load->library('upload', $config); 
        $this->upload->initialize($config);
        $post['foto'] = '';
        if ($this->upload->do_upload('foto')) {
          $data_foto = array('upload_data' => $this->upload->data());
          $post['foto'] = $data_foto['upload_data']['file_name'];
        }
        
				$datos = array('nombre' => $post['nombre'],'email' => $post['email'],'modo' => $post['modo'],'foto' => $post['foto'],);

				$id = $this->Modelo->insertar($this->config->item('raiz_bd') . 'usuarios', $datos);
				if ($id > 0) {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-check"></i> '.$this->lang->line('be_successful_operation').'</h4>El registro se ingresó exitosamente.'); 
					$this->session->set_flashdata('alertaTipo', 'success'); // success, info, warning, danger
				} else {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No fue posible ingresar el registro.<br>'.$this->lang->line('be_please_try_again'));
					$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
				}     
			}
		} else {
			$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No se recibieron datos.<br>'.$this->lang->line('be_please_try_again'));
			$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
		}
		redirect('/admin/administradores021/usuarios');
	}

	public function guardar () {
		$post = $this->input->post(NULL, TRUE);
		if (!empty($post)) {
			foreach ($post as $key => $value) {
				$post[$key] = $this->security->xss_clean($value);
			}
      
			$this->form_validation->set_rules('nombre', 'Nombre', 'required|xss_clean');
//      $this->form_validation->set_rules('password', 'Password', 'xss_clean');
      $this->form_validation->set_rules('email', 'Correo', 'required|valid_email|xss_clean');
      $this->form_validation->set_rules('modo', 'Modo', 'required|xss_clean');
      $this->form_validation->set_rules('foto', 'Foto', 'xss_clean');
//      $this->form_validation->set_rules('verificacion', 'Verificacion', 'numeric|xss_clean');
//      $this->form_validation->set_rules('emailEncriptado', 'Emailencriptado', 'xss_clean');
      $this->form_validation->set_rules('fecha', 'Fecha', 'xss_clean');
      
			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.validation_errors().'<br><b>'.$this->lang->line('be_please_try_again').'</b>');
				$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
			} else {
        
        $post['foto'] = '';
				$config['upload_path'] = './imagenes/usuario/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 2048;
        $this->load->library('upload', $config); 
        $this->upload->initialize($config);
        $post['foto'] = '';
        if ($this->upload->do_upload('foto')) {
          $data_foto = array('upload_data' => $this->upload->data());
          $post['foto'] = $data_foto['upload_data']['file_name'];
        }


				$datos = array('nombre' => $post['nombre'],'email' => $post['email'],'modo' => $post['modo'],);
        if ($post['foto'] != '') $datos['foto'] = $post['foto'];

				if ($this->Modelo->actualizar($this->config->item('raiz_bd') . 'usuarios', $datos, $post['id'])) {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-check"></i> '.$this->lang->line('be_successful_operation').'</h4>El registro se guardo exitosamente.'); 
					$this->session->set_flashdata('alertaTipo', 'success'); // success, info, warning, danger
				} else {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No fue posible guardar el registro.<br>'.$this->lang->line('be_please_try_again'));
					$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
				}     

			}
		} else {
			$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No se recibieron datos.<br>'.$this->lang->line('be_please_try_again'));
			$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
		}
		redirect('/admin/administradores021/usuarios');   
	}

	public function eliminar () {
		$post = $this->input->post(NULL, TRUE);
		if (!empty($post)) {
			foreach ($post as $key => $value) {
				$post[$key] = $this->security->xss_clean($value);
			}
			$this->form_validation->set_rules('id', 'ID', 'required|trim|xss_clean');
			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.validation_errors().'<br><b>'.$this->lang->line('be_please_try_again').'</b>');
				$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
			} else {

				$mat_id = explode(";", $post['id']); 
				$proceso = false;          
				foreach ($mat_id as $id) {
					$this->Modelo->eliminar($this->config->item('raiz_bd') . 'usuarios', $id);
					$proceso = true;
				}

				if ($proceso) {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-check"></i> '.$this->lang->line('be_successful_operation').'</h4>El(los) registro(s) se eliminó(aron) exitosamente.'); 
					$this->session->set_flashdata('alertaTipo', 'success'); // success, info, warning, danger
				} else {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No fue posible eliminar el(los) registro(s).<br>'.$this->lang->line('be_please_try_again'));
					$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
				}     

			}
		} else {
			$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No se recibieron datos.<br>'.$this->lang->line('be_please_try_again'));
			$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
		}
		redirect('/admin/administradores021/usuarios');   
	}

  
  
	public function lista () {

		$registros = $this->Modelo->registros($this->config->item('raiz_bd') . 'usuarios');
    
  	$datosJson = '
  		{	
  			"data":[';  
    $datosJsonReg = '';
    foreach ($registros as $key => $registro) {
          $modo = $this->Modelo->registro($this->config->item('raiz_bd') . 'datos_valores', $registro['modo']);
          if ($modo) {
             $registro['modo'] = $modo->nombre;
          }
      
          if (trim($registro['foto']) != '') {
              $registro['foto'] = '<img src=\"'.base_url().'imagenes/usuario/'.$registro['foto'].'\" width=\"100px\"  class=\"img-thumbnail\">';
          }      
      
          $datosJsonReg .='["<input type=\"checkbox\" id=\"fila_'.$registro['id'].'\" class=\"seleccion\" cod=\"'.$registro['id'].'\">", "<button type=\"button\" class=\"btn btn-default btn-xs text-light-blue btnEditar\" cod=\"'.$registro['id'].'\"><span class=\"glyphicon glyphicon-pencil\"></span></button>", "<button type=\"button\" class=\"btn btn-default btn-xs text-red btnEliminar\" cod=\"'.$registro['id'].'\"><span class=\"glyphicon glyphicon-trash\"></span></button>","'.$registro['nombre'].'","'.$registro['email'].'","'.$registro['modo'].'","'.$registro['foto'].'","'.$registro['fecha'].'"],';

    }
    if ($datosJsonReg != '') {
          $datosJson .= substr($datosJsonReg, 0, -1);
    }
		
		$datosJson .= ']
		}';

		echo $datosJson;
    
	}
      
  
	public function lista_ssp () {
		$this->load->library('SSP');

		// DB table to use
		$table = 'usuarios';

		// Table's primary key
		$primaryKey = 'id';

		// Array of database columns
		$columns = array(array( 'db' => 'id', 'dt' => 0, 'field' => 'id', 'formatter' => function($d, $row) {return '<input type="checkbox" id="fila_' . $d . '" class="seleccion" cod="' . $d . '">';}),array( 'db' => 'id', 'dt' => 1, 'field' => 'id', 'formatter' => function($d, $row) {return '<button type="button" class="btn btn-default btn-xs text-light-blue btnEditar" cod="' . $d . '"><span class="glyphicon glyphicon-pencil"></span></button>';}), array( 'db' => 'id', 'dt' => 2, 'field' => 'id', 'formatter' => function($d, $row) {return '<button type="button" class="btn btn-default btn-xs text-red btnEliminar" cod="' . $d . '"><span class="glyphicon glyphicon-trash"></span></button>';}),array( 'db' => 'nombre', 'dt' => 3, 'field' => 'nombre' ),array( 'db' => 'password', 'dt' => 4, 'field' => 'password' ),array( 'db' => 'email', 'dt' => 5, 'field' => 'email' ),array( 'db' => 'modo', 'dt' => 6, 'field' => 'modo' ),array( 'db' => 'foto', 'dt' => 7, 'field' => 'foto' ),array( 'db' => 'verificacion', 'dt' => 8, 'field' => 'verificacion' ),array( 'db' => 'emailEncriptado', 'dt' => 9, 'field' => 'emailEncriptado' ),array( 'db' => 'fecha', 'dt' => 10, 'field' => 'fecha' ),);

		$sql_details = array(
			'user' => $this->db->username,
			'pass' => $this->db->password,
			'db'   => $this->db->database,
			'host' => $this->db->hostname
		);

		$joinQuery = "FROM usuarios";
		$extraWhere = ""; //"`u`.`valor` >= 90000";
		$groupBy = ""; //"`u`.`datos`";
		$having = ""; //"`u`.`valor` >= 140000";

		$_GET['tksec'] = $this->security->get_csrf_hash();  

		echo json_encode(
			SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having )
		);
	}

}