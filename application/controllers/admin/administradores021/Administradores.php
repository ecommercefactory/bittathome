<?php
defined("BASEPATH") OR exit("No esta permitido el acceso directo a este script.");

class Administradores extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->ctrSegAdmin();
	}

	public function index() {
		$this->parametros['plantilla'] = 'datatables';
		$this->parametros['vista'] = 'admin/administradores021/administradores';
		$this->parametros['datos']['titulo'] = 'Administradores';
		$this->parametros['datos']['subtitulo'] = ''; 
    
    $tabla_datos_valores_perfil = $this->Modelo->registros($this->config->item('raiz_bd') . 'datos_valores', 'id, nombre', array('dato'=>2), 'nombre ASC' ); 
    $this->parametros['datos']['tabla_datos_valores_perfil'] = $tabla_datos_valores_perfil;
    
    $tabla_languages = $this->Modelo->registros($this->config->item('raiz_bd') . 'languages', 'id, name', array('active'=>4), 'name ASC' ); 
    $this->parametros['datos']['tabla_languages'] = $tabla_languages;
    
    $tabla_paises = $this->Modelo->registros($this->config->item('raiz_bd') . 'paises', 'id, nombre_en', array(), 'nombre_en ASC' ); 
    $this->parametros['datos']['tabla_paises'] = $tabla_paises;
    
    $tabla_datos_valores_estado = $this->Modelo->registros($this->config->item('raiz_bd') . 'datos_valores', 'id, nombre', array('dato'=>1), 'nombre ASC' );
    $this->parametros['datos']['tabla_datos_valores_estado'] = $tabla_datos_valores_estado;
    
		$this->load->view('plantilla_admin', $this->parametros);			
	}

	public function traerRegistro () {
		$post = $this->input->post(NULL, TRUE);
		$registro = $this->Modelo->registro($this->config->item('raiz_bd') . 'administradores', $post['id']);
		$datos = array('registro'=>$registro, 'tksec'=>$this->security->get_csrf_hash());
		echo json_encode($datos);      
	}

	public function ingresar () {
		$post = $this->input->post(NULL, TRUE);
		if (!empty($post)) {
			foreach ($post as $key => $value) {
				$post[$key] = $this->security->xss_clean($value);
			}
      
			$this->form_validation->set_rules('nombre', 'Nombre', 'required|min_length[0]|max_length[50]|xss_clean');$this->form_validation->set_rules('apellidos', 'Apellidos', 'required|min_length[0]|max_length[50]|xss_clean');$this->form_validation->set_rules('perfil', 'Perfil', 'required|xss_clean');$this->form_validation->set_rules('correo', 'Correo', 'required|max_length[70]|valid_email|xss_clean');
        $this->form_validation->set_rules('foto', 'Foto', 'xss_clean');$this->form_validation->set_rules('language', 'Language', 'required|xss_clean');$this->form_validation->set_rules('fecha_nacimiento', 'Fecha Nacimiento', 'xss_clean');$this->form_validation->set_rules('pais', 'Pais', 'required|xss_clean');$this->form_validation->set_rules('departamento', 'Departamento', 'max_length[50]|xss_clean');$this->form_validation->set_rules('ciudad', 'Ciudad', 'max_length[50]|xss_clean');$this->form_validation->set_rules('telefono', 'Telefono', 'max_length[50]|xss_clean');$this->form_validation->set_rules('facebook', 'Facebook', 'max_length[50]|xss_clean');$this->form_validation->set_rules('whatsapp', 'Whatsapp', 'max_length[50]|xss_clean');$this->form_validation->set_rules('skype', 'Skype', 'max_length[50]|xss_clean');$this->form_validation->set_rules('instagram', 'Instagram', 'max_length[50]|xss_clean');$this->form_validation->set_rules('google', 'Google', 'max_length[50]|xss_clean');$this->form_validation->set_rules('linkdn', 'Linkdn', 'max_length[50]|xss_clean');
      $this->form_validation->set_rules('estado', 'Estado', 'required|xss_clean');

      
			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.validation_errors().'<br><b>'.$this->lang->line('be_please_try_again').'</b>');
				$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
			} else {

				$config['upload_path'] = './imagenes/administradores/';$config['allowed_types'] = 'gif|jpg|png|jpeg';$config['max_size'] = 2048;$this->load->library('upload', $config); $this->upload->initialize($config);$post['foto'] = '';if ($this->upload->do_upload('foto')) {$data_foto = array('upload_data' => $this->upload->data());$post['foto'] = $data_foto['upload_data']['file_name'];}
        
				$datos = array('nombre' => $post['nombre'],'apellidos' => $post['apellidos'],'perfil' => $post['perfil'],'correo' => $post['correo'],'foto' => $post['foto'],'language' => $post['language'],'fecha_nacimiento' => $post['fecha_nacimiento'],'pais' => $post['pais'],'departamento' => $post['departamento'],'ciudad' => $post['ciudad'],'telefono' => $post['telefono'],'facebook' => $post['facebook'],'whatsapp' => $post['whatsapp'],'skype' => $post['skype'],'instagram' => $post['instagram'],'google' => $post['google'],'linkdn' => $post['linkdn'],'estado' => $post['estado'],);

				$id = $this->Modelo->insertar($this->config->item('raiz_bd') . 'administradores', $datos);
				if ($id > 0) {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-check"></i> '.$this->lang->line('be_successful_operation').'</h4>El registro se ingresó exitosamente.'); 
					$this->session->set_flashdata('alertaTipo', 'success'); // success, info, warning, danger
				} else {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No fue posible ingresar el registro.<br>'.$this->lang->line('be_please_try_again'));
					$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
				}     
			}
		} else {
			$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No se recibieron datos.<br>'.$this->lang->line('be_please_try_again'));
			$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
		}
		redirect('/admin/administradores021/administradores');
	}

	public function guardar () {
		$post = $this->input->post(NULL, TRUE);
		if (!empty($post)) {
			foreach ($post as $key => $value) {
				$post[$key] = $this->security->xss_clean($value);
			}
      
			$this->form_validation->set_rules('nombre', 'Nombre', 'required|min_length[0]|max_length[50]|xss_clean');$this->form_validation->set_rules('apellidos', 'Apellidos', 'required|min_length[0]|max_length[50]|xss_clean');$this->form_validation->set_rules('perfil', 'Perfil', 'required|xss_clean');$this->form_validation->set_rules('correo', 'Correo', 'required|max_length[70]|valid_email|xss_clean');
      $this->form_validation->set_rules('foto', 'Foto', 'xss_clean');$this->form_validation->set_rules('language', 'Language', 'required|xss_clean');$this->form_validation->set_rules('fecha_nacimiento', 'Fecha Nacimiento', 'xss_clean');$this->form_validation->set_rules('pais', 'Pais', 'required|xss_clean');$this->form_validation->set_rules('departamento', 'Departamento', 'max_length[50]|xss_clean');$this->form_validation->set_rules('ciudad', 'Ciudad', 'max_length[50]|xss_clean');$this->form_validation->set_rules('telefono', 'Telefono', 'max_length[50]|xss_clean');$this->form_validation->set_rules('facebook', 'Facebook', 'max_length[50]|xss_clean');$this->form_validation->set_rules('whatsapp', 'Whatsapp', 'max_length[50]|xss_clean');$this->form_validation->set_rules('skype', 'Skype', 'max_length[50]|xss_clean');$this->form_validation->set_rules('instagram', 'Instagram', 'max_length[50]|xss_clean');$this->form_validation->set_rules('google', 'Google', 'max_length[50]|xss_clean');$this->form_validation->set_rules('linkdn', 'Linkdn', 'max_length[50]|xss_clean');
      $this->form_validation->set_rules('estado', 'Estado', 'required|xss_clean');
      
			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.validation_errors().'<br><b>'.$this->lang->line('be_please_try_again').'</b>');
				$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
			} else {

				$config['upload_path'] = './imagenes/administradores/';$config['allowed_types'] = 'gif|jpg|png|jpeg';$config['max_size'] = 2048;$this->load->library('upload', $config); $this->upload->initialize($config);$post['foto'] = '';if ($this->upload->do_upload('foto')) {$data_foto = array('upload_data' => $this->upload->data());$post['foto'] = $data_foto['upload_data']['file_name'];}

				$datos = array('nombre' => $post['nombre'],'apellidos' => $post['apellidos'],'perfil' => $post['perfil'],'correo' => $post['correo'],'language' => $post['language'],'fecha_nacimiento' => $post['fecha_nacimiento'],'pais' => $post['pais'],'departamento' => $post['departamento'],'ciudad' => $post['ciudad'],'telefono' => $post['telefono'],'facebook' => $post['facebook'],'whatsapp' => $post['whatsapp'],'skype' => $post['skype'],'instagram' => $post['instagram'],'google' => $post['google'],'linkdn' => $post['linkdn'],'estado' => $post['estado'],);

        if ($post['foto'] != '') $datos['foto'] = $post['foto'];        
        
				if ($this->Modelo->actualizar($this->config->item('raiz_bd') . 'administradores', $datos, $post['id'])) {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-check"></i> '.$this->lang->line('be_successful_operation').'</h4>El registro se guardo exitosamente.'); 
					$this->session->set_flashdata('alertaTipo', 'success'); // success, info, warning, danger
				} else {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No fue posible guardar el registro.<br>'.$this->lang->line('be_please_try_again'));
					$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
				}     

			}
		} else {
			$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No se recibieron datos.<br>'.$this->lang->line('be_please_try_again'));
			$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
		}
		redirect('/admin/administradores021/administradores');   
	}

	public function eliminar () {
		$post = $this->input->post(NULL, TRUE);
		if (!empty($post)) {
			foreach ($post as $key => $value) {
				$post[$key] = $this->security->xss_clean($value);
			}
			$this->form_validation->set_rules('id', 'ID', 'required|trim|xss_clean');
			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.validation_errors().'<br><b>'.$this->lang->line('be_please_try_again').'</b>');
				$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
			} else {

				$mat_id = explode(";", $post['id']); 
				$proceso = false;          
				foreach ($mat_id as $id) {
					$this->Modelo->eliminar($this->config->item('raiz_bd') . 'administradores', $id);
					$proceso = true;
				}

				if ($proceso) {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-check"></i> '.$this->lang->line('be_successful_operation').'</h4>El(los) registro(s) se eliminó(aron) exitosamente.'); 
					$this->session->set_flashdata('alertaTipo', 'success'); // success, info, warning, danger
				} else {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No fue posible eliminar el(los) registro(s).<br>'.$this->lang->line('be_please_try_again'));
					$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
				}     

			}
		} else {
			$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No se recibieron datos.<br>'.$this->lang->line('be_please_try_again'));
			$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
		}
		redirect('/admin/administradores021/administradores');   
	}

  
  
	public function lista () {

		$registros = $this->Modelo->registros($this->config->item('raiz_bd') . 'administradores');
    
  	$datosJson = '
  		{	
  			"data":[';  
    $datosJsonReg = '';
    foreach ($registros as $key => $registro) {
      
      
          $perfil = $this->Modelo->registro($this->config->item('raiz_bd') . 'datos_valores', $registro['perfil']);
          if ($perfil) {
             $registro['perfil'] = $perfil->nombre;
          }  
          $language = $this->Modelo->registro($this->config->item('raiz_bd') . 'languages', $registro['language']);
          if ($language) {
             $registro['language'] = $language->name;
          }  
          $pais = $this->Modelo->registro($this->config->item('raiz_bd') . 'paises', $registro['pais']);
          if ($pais) {
             $registro['pais'] = $pais->nombre_es;
          } else {
             $registro['pais'] = '';
          }
          $estado = $this->Modelo->registro($this->config->item('raiz_bd') . 'datos_valores', $registro['estado']);
          if ($estado) {
             $registro['estado'] = $estado->nombre;
          }  
      
          if (trim($registro['foto']) != '') {
              $registro['foto'] = '<img src=\"'.base_url().'imagenes/administradores/'.$registro['foto'].'\" width=\"100px\"  class=\"img-thumbnail\">';
          }      

      
          $datosJsonReg .='["<input type=\"checkbox\" id=\"fila_'.$registro['id'].'\" class=\"seleccion\" cod=\"'.$registro['id'].'\">", "<button type=\"button\" class=\"btn btn-default btn-xs text-light-blue btnEditar\" cod=\"'.$registro['id'].'\"><span class=\"glyphicon glyphicon-pencil\"></span></button>", "<button type=\"button\" class=\"btn btn-default btn-xs text-red btnEliminar\" cod=\"'.$registro['id'].'\"><span class=\"glyphicon glyphicon-trash\"></span></button>","'.$registro['nombre'].'","'.$registro['apellidos'].'","'.$registro['perfil'].'","'.$registro['correo'].'","'.$registro['foto'].'","'.$registro['language'].'","'.$registro['fecha_nacimiento'].'","'.$registro['pais'].'","'.$registro['departamento'].'","'.$registro['ciudad'].'","'.$registro['telefono'].'","'.$registro['facebook'].'","'.$registro['whatsapp'].'","'.$registro['skype'].'","'.$registro['instagram'].'","'.$registro['google'].'","'.$registro['linkdn'].'","'.$registro['estado'].'","'.$registro['fecha_ingreso'].'","'.$registro['fecha_ultimo_ingreso'].'","'.$registro['ultima_ip'].'","'.$registro['fecha_clave_cambio'].'","'.$registro['fecha_activacion'].'","'.$registro['fecha_cambio_clave'].'"],';

    }
    if ($datosJsonReg != '') {
          $datosJson .= substr($datosJsonReg, 0, -1);
    }
		
		$datosJson .= ']
		}';

		echo $datosJson;
    
	}
      
  
	public function lista_ssp () {
		$this->load->library('SSP');

		// DB table to use
		$table = 'administradores';

		// Table's primary key
		$primaryKey = 'id';

		// Array of database columns
		$columns = array(array( 'db' => 'id', 'dt' => 0, 'field' => 'id', 'formatter' => function($d, $row) {return '<input type="checkbox" id="fila_' . $d . '" class="seleccion" cod="' . $d . '">';}),array( 'db' => 'id', 'dt' => 1, 'field' => 'id', 'formatter' => function($d, $row) {return '<button type="button" class="btn btn-default btn-xs text-light-blue btnEditar" cod="' . $d . '"><span class="glyphicon glyphicon-pencil"></span></button>';}), array( 'db' => 'id', 'dt' => 2, 'field' => 'id', 'formatter' => function($d, $row) {return '<button type="button" class="btn btn-default btn-xs text-red btnEliminar" cod="' . $d . '"><span class="glyphicon glyphicon-trash"></span></button>';}),array( 'db' => 'nombre', 'dt' => 3, 'field' => 'nombre' ),array( 'db' => 'apellidos', 'dt' => 4, 'field' => 'apellidos' ),array( 'db' => 'perfil', 'dt' => 5, 'field' => 'perfil' ),array( 'db' => 'correo', 'dt' => 6, 'field' => 'correo' ),array( 'db' => 'clave', 'dt' => 7, 'field' => 'clave' ),array( 'db' => 'foto', 'dt' => 8, 'field' => 'foto' ),array( 'db' => 'language', 'dt' => 9, 'field' => 'language' ),array( 'db' => 'fecha_nacimiento', 'dt' => 10, 'field' => 'fecha_nacimiento' ),array( 'db' => 'pais', 'dt' => 11, 'field' => 'pais' ),array( 'db' => 'departamento', 'dt' => 12, 'field' => 'departamento' ),array( 'db' => 'ciudad', 'dt' => 13, 'field' => 'ciudad' ),array( 'db' => 'telefono', 'dt' => 14, 'field' => 'telefono' ),array( 'db' => 'facebook', 'dt' => 15, 'field' => 'facebook' ),array( 'db' => 'whatsapp', 'dt' => 16, 'field' => 'whatsapp' ),array( 'db' => 'skype', 'dt' => 17, 'field' => 'skype' ),array( 'db' => 'instagram', 'dt' => 18, 'field' => 'instagram' ),array( 'db' => 'google', 'dt' => 19, 'field' => 'google' ),array( 'db' => 'linkdn', 'dt' => 20, 'field' => 'linkdn' ),array( 'db' => 'fecha_ingreso', 'dt' => 21, 'field' => 'fecha_ingreso' ),array( 'db' => 'estado', 'dt' => 22, 'field' => 'estado' ),array( 'db' => 'fecha_ultimo_ingreso', 'dt' => 23, 'field' => 'fecha_ultimo_ingreso' ),array( 'db' => 'ultima_ip', 'dt' => 24, 'field' => 'ultima_ip' ),array( 'db' => 'session_id', 'dt' => 25, 'field' => 'session_id' ),array( 'db' => 'codigo_clave_cambio', 'dt' => 26, 'field' => 'codigo_clave_cambio' ),array( 'db' => 'fecha_clave_cambio', 'dt' => 27, 'field' => 'fecha_clave_cambio' ),array( 'db' => 'codigo_activacion', 'dt' => 28, 'field' => 'codigo_activacion' ),array( 'db' => 'fecha_activacion', 'dt' => 29, 'field' => 'fecha_activacion' ),array( 'db' => 'fecha_cambio_clave', 'dt' => 30, 'field' => 'fecha_cambio_clave' ),);

		$sql_details = array(
			'user' => $this->db->username,
			'pass' => $this->db->password,
			'db'   => $this->db->database,
			'host' => $this->db->hostname
		);

		$joinQuery = "FROM administradores";
		$extraWhere = ""; //"`u`.`valor` >= 90000";
		$groupBy = ""; //"`u`.`datos`";
		$having = ""; //"`u`.`valor` >= 140000";

		$_GET['tksec'] = $this->security->get_csrf_hash();  

		echo json_encode(
			SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having )
		);
	}

}