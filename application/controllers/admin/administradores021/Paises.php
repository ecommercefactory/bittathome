<?php
defined("BASEPATH") OR exit("No esta permitido el acceso directo a este script.");

class Paises extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->ctrSegAdmin();
	}

	public function index() {
		$this->parametros['plantilla'] = 'datatables';
		$this->parametros['vista'] = 'admin/administradores021/paises';
		$this->parametros['datos']['titulo'] = 'Paises';
		$this->parametros['datos']['subtitulo'] = ''; 
    $tabla_paises = $this->Modelo->registros($this->config->item('raiz_bd') . 'paises', '', array() );
		$this->parametros['datos']['lista'] = $tabla_paises;
    
    $tabla_datos_valores = $this->Modelo->registros($this->config->item('raiz_bd') . 'datos_valores', 'id, nombre', array(), 'nombre ASC' ); $this->parametros['datos']['tabla_datos_valores'] = $tabla_datos_valores;
    
		$this->load->view('plantilla_admin', $this->parametros);			
	}

	public function traerRegistro () {
		$post = $this->input->post(NULL, TRUE);
		$registro = $this->Modelo->registro($this->config->item('raiz_bd') . 'paises', $post['id']);
		$datos = array('registro'=>$registro, 'tksec'=>$this->security->get_csrf_hash());
		echo json_encode($datos);      
	}

	public function ingresar () {
		$post = $this->input->post(NULL, TRUE);
		if (!empty($post)) {
			foreach ($post as $key => $value) {
				$post[$key] = $this->security->xss_clean($value);
			}
      
			$this->form_validation->set_rules('codigo', 'Codigo', 'required|max_length[3]|xss_clean');$this->form_validation->set_rules('nombre_en', 'Nombre_En', 'max_length[30]|xss_clean');$this->form_validation->set_rules('nombre_es', 'Nombre_Es', 'max_length[30]|xss_clean');$this->form_validation->set_rules('nombre_fr', 'Nombre_Fr', 'max_length[30]|xss_clean');$this->form_validation->set_rules('nombre_it', 'Nombre_It', 'max_length[30]|xss_clean');$this->form_validation->set_rules('nombre_pt', 'Nombre_Pt', 'max_length[30]|xss_clean');$this->form_validation->set_rules('nombre_de', 'Nombre_De', 'max_length[30]|xss_clean');$this->form_validation->set_rules('nombre_cn', 'Nombre_Cn', 'max_length[30]|xss_clean');$this->form_validation->set_rules('nombre_ru', 'Nombre_Ru', 'max_length[30]|xss_clean');$this->form_validation->set_rules('codigo_telefono', 'Codigo_Telefono', 'max_length[10]|xss_clean');$this->form_validation->set_rules('moneda', 'Moneda', 'required|max_length[3]|xss_clean');$this->form_validation->set_rules('gmt', 'Gmt', 'max_length[11]|xss_clean');$this->form_validation->set_rules('lang', 'Lang', 'max_length[3]|xss_clean');
      
			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.validation_errors().'<br><b>'.$this->lang->line('be_please_try_again').'</b>');
				$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
			} else {

				
        
				$datos = array('codigo' => $post['codigo'],'nombre_en' => $post['nombre_en'],'nombre_es' => $post['nombre_es'],'nombre_fr' => $post['nombre_fr'],'nombre_it' => $post['nombre_it'],'nombre_pt' => $post['nombre_pt'],'nombre_de' => $post['nombre_de'],'nombre_cn' => $post['nombre_cn'],'nombre_ru' => $post['nombre_ru'],'codigo_telefono' => $post['codigo_telefono'],'moneda' => $post['moneda'],'gmt' => $post['gmt'],'lang' => $post['lang'],);

				$id = $this->Modelo->insertar($this->config->item('raiz_bd') . 'paises', $datos);
				if ($id > 0) {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-check"></i> '.$this->lang->line('be_successful_operation').'</h4>El registro se ingresó exitosamente.'); 
					$this->session->set_flashdata('alertaTipo', 'success'); // success, info, warning, danger
				} else {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No fue posible ingresar el registro.<br>'.$this->lang->line('be_please_try_again'));
					$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
				}     
			}
		} else {
			$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No se recibieron datos.<br>'.$this->lang->line('be_please_try_again'));
			$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
		}
		redirect('/admin/administradores021/paises');
	}

	public function guardar () {
		$post = $this->input->post(NULL, TRUE);
		if (!empty($post)) {
			foreach ($post as $key => $value) {
				$post[$key] = $this->security->xss_clean($value);
			}
      
			$this->form_validation->set_rules('codigo', 'Codigo', 'required|max_length[3]|xss_clean');$this->form_validation->set_rules('nombre_en', 'Nombre_En', 'max_length[30]|xss_clean');$this->form_validation->set_rules('nombre_es', 'Nombre_Es', 'max_length[30]|xss_clean');$this->form_validation->set_rules('nombre_fr', 'Nombre_Fr', 'max_length[30]|xss_clean');$this->form_validation->set_rules('nombre_it', 'Nombre_It', 'max_length[30]|xss_clean');$this->form_validation->set_rules('nombre_pt', 'Nombre_Pt', 'max_length[30]|xss_clean');$this->form_validation->set_rules('nombre_de', 'Nombre_De', 'max_length[30]|xss_clean');$this->form_validation->set_rules('nombre_cn', 'Nombre_Cn', 'max_length[30]|xss_clean');$this->form_validation->set_rules('nombre_ru', 'Nombre_Ru', 'max_length[30]|xss_clean');$this->form_validation->set_rules('codigo_telefono', 'Codigo_Telefono', 'max_length[10]|xss_clean');$this->form_validation->set_rules('moneda', 'Moneda', 'required|max_length[3]|xss_clean');$this->form_validation->set_rules('gmt', 'Gmt', 'max_length[11]|xss_clean');$this->form_validation->set_rules('lang', 'Lang', 'max_length[3]|xss_clean');
      
			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.validation_errors().'<br><b>'.$this->lang->line('be_please_try_again').'</b>');
				$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
			} else {

				
        
				$datos = array('codigo' => $post['codigo'],'nombre_en' => $post['nombre_en'],'nombre_es' => $post['nombre_es'],'nombre_fr' => $post['nombre_fr'],'nombre_it' => $post['nombre_it'],'nombre_pt' => $post['nombre_pt'],'nombre_de' => $post['nombre_de'],'nombre_cn' => $post['nombre_cn'],'nombre_ru' => $post['nombre_ru'],'codigo_telefono' => $post['codigo_telefono'],'moneda' => $post['moneda'],'gmt' => $post['gmt'],'lang' => $post['lang'],);

				if ($this->Modelo->actualizar($this->config->item('raiz_bd') . 'paises', $datos, $post['id'])) {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-check"></i> '.$this->lang->line('be_successful_operation').'</h4>El registro se guardo exitosamente.'); 
					$this->session->set_flashdata('alertaTipo', 'success'); // success, info, warning, danger
				} else {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No fue posible guardar el registro.<br>'.$this->lang->line('be_please_try_again'));
					$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
				}     

			}
		} else {
			$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No se recibieron datos.<br>'.$this->lang->line('be_please_try_again'));
			$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
		}
		redirect('/admin/administradores021/paises');   
	}

	public function eliminar () {
		$post = $this->input->post(NULL, TRUE);
		if (!empty($post)) {
			foreach ($post as $key => $value) {
				$post[$key] = $this->security->xss_clean($value);
			}
			$this->form_validation->set_rules('id', 'ID', 'required|trim|xss_clean');
			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.validation_errors().'<br><b>'.$this->lang->line('be_please_try_again').'</b>');
				$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
			} else {

				$mat_id = explode(";", $post['id']); 
				$proceso = false;          
				foreach ($mat_id as $id) {
					$this->Modelo->eliminar($this->config->item('raiz_bd') . 'paises', $id);
					$proceso = true;
				}

				if ($proceso) {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-check"></i> '.$this->lang->line('be_successful_operation').'</h4>El(los) registro(s) se eliminó(aron) exitosamente.'); 
					$this->session->set_flashdata('alertaTipo', 'success'); // success, info, warning, danger
				} else {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No fue posible eliminar el(los) registro(s).<br>'.$this->lang->line('be_please_try_again'));
					$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
				}     

			}
		} else {
			$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No se recibieron datos.<br>'.$this->lang->line('be_please_try_again'));
			$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
		}
		redirect('/admin/administradores021/paises');   
	}

  
  
	public function lista () {

		$registros = $this->Modelo->registros($this->config->item('raiz_bd') . 'paises');
    
  	$datosJson = '
  		{	
  			"data":[';  
    $datosJsonReg = '';
    foreach ($registros as $key => $registro) {
      
          $datosJsonReg .='["<input type=\"checkbox\" id=\"fila_'.$registro['id'].'\" class=\"seleccion\" cod=\"'.$registro['id'].'\">", "<button type=\"button\" class=\"btn btn-default btn-xs text-light-blue btnEditar\" cod=\"'.$registro['id'].'\"><span class=\"glyphicon glyphicon-pencil\"></span></button>", "<button type=\"button\" class=\"btn btn-default btn-xs text-red btnEliminar\" cod=\"'.$registro['id'].'\"><span class=\"glyphicon glyphicon-trash\"></span></button>","'.$registro['codigo'].'","'.$registro['nombre_en'].'","'.$registro['nombre_es'].'","'.$registro['nombre_fr'].'","'.$registro['nombre_it'].'","'.$registro['nombre_pt'].'","'.$registro['nombre_de'].'","'.$registro['nombre_cn'].'","'.$registro['nombre_ru'].'","'.$registro['codigo_telefono'].'","'.$registro['moneda'].'","'.$registro['gmt'].'","'.$registro['lang'].'"],';

    }
    if ($datosJsonReg != '') {
          $datosJson .= substr($datosJsonReg, 0, -1);
    }
		
		$datosJson .= ']
		}';

		echo $datosJson;
    
	}
      
  
	public function lista_ssp () {
		$this->load->library('SSP');

		// DB table to use
		$table = 'paises';

		// Table's primary key
		$primaryKey = 'id';

		// Array of database columns
		$columns = array(array( 'db' => 'id', 'dt' => 0, 'field' => 'id', 'formatter' => function($d, $row) {return '<input type="checkbox" id="fila_' . $d . '" class="seleccion" cod="' . $d . '">';}),array( 'db' => 'id', 'dt' => 1, 'field' => 'id', 'formatter' => function($d, $row) {return '<button type="button" class="btn btn-default btn-xs text-light-blue btnEditar" cod="' . $d . '"><span class="glyphicon glyphicon-pencil"></span></button>';}), array( 'db' => 'id', 'dt' => 2, 'field' => 'id', 'formatter' => function($d, $row) {return '<button type="button" class="btn btn-default btn-xs text-red btnEliminar" cod="' . $d . '"><span class="glyphicon glyphicon-trash"></span></button>';}),array( 'db' => 'codigo', 'dt' => 3, 'field' => 'codigo' ),array( 'db' => 'nombre_en', 'dt' => 4, 'field' => 'nombre_en' ),array( 'db' => 'nombre_es', 'dt' => 5, 'field' => 'nombre_es' ),array( 'db' => 'nombre_fr', 'dt' => 6, 'field' => 'nombre_fr' ),array( 'db' => 'nombre_it', 'dt' => 7, 'field' => 'nombre_it' ),array( 'db' => 'nombre_pt', 'dt' => 8, 'field' => 'nombre_pt' ),array( 'db' => 'nombre_de', 'dt' => 9, 'field' => 'nombre_de' ),array( 'db' => 'nombre_cn', 'dt' => 10, 'field' => 'nombre_cn' ),array( 'db' => 'nombre_ru', 'dt' => 11, 'field' => 'nombre_ru' ),array( 'db' => 'codigo_telefono', 'dt' => 12, 'field' => 'codigo_telefono' ),array( 'db' => 'moneda', 'dt' => 13, 'field' => 'moneda' ),array( 'db' => 'gmt', 'dt' => 14, 'field' => 'gmt' ),array( 'db' => 'lang', 'dt' => 15, 'field' => 'lang' ),);

		$sql_details = array(
			'user' => $this->db->username,
			'pass' => $this->db->password,
			'db'   => $this->db->database,
			'host' => $this->db->hostname
		);

		$joinQuery = "FROM paises";
		$extraWhere = ""; //"`u`.`valor` >= 90000";
		$groupBy = ""; //"`u`.`datos`";
		$having = ""; //"`u`.`valor` >= 140000";

		$_GET['tksec'] = $this->security->get_csrf_hash();  

		echo json_encode(
			SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having )
		);
	}

}