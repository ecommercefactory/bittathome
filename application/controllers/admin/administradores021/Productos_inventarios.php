<?php
defined("BASEPATH") OR exit("No esta permitido el acceso directo a este script.");

class Productos_inventarios extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->ctrSegAdmin();
	}

    function _remap($param) {
        $this->index($param);
    }
  
    public function index($id) {  
    	if (!is_numeric($id)) {
    		redirect('/admin/administradores021/productos');
    	}
		$producto = $this->Modelo->registro($this->config->item('raiz_bd') . 'productos', $id);

		$this->parametros['plantilla'] = 'datatables';
		$this->parametros['vista'] = 'admin/administradores021/productos_inventarios';
		$this->parametros['datos']['titulo'] = 'Inventarios (' . $producto->inventario . ')';
		$this->parametros['datos']['subtitulo'] = 'de ' . $producto->nombre; 
        $this->parametros['datos']['breadcrumb'] = '
            <ol class="breadcrumb">
                <li><a href="'.base_url().'admin/administradores021/productos">Productos</a></li>
                <li class="active">Inventarios</li>
            </ol>';			
		$this->parametros['datos']['producto'] = $id; 

		/*
    $tabla_productos_inventarios = $this->Modelo->registros($this->config->item('raiz_bd') . 'productos_inventarios', '', array() );
		$this->parametros['datos']['lista'] = $tabla_productos_inventarios;
		*/

    $tabla_datos_valores = $this->Modelo->registros($this->config->item('raiz_bd') . 'datos_valores', 'id, nombre', array('dato'=>15), 'nombre ASC' ); 
    $this->parametros['datos']['tabla_datos_valores'] = $tabla_datos_valores;
    
		$this->load->view('plantilla_admin', $this->parametros);			
	}

}