<?php
defined("BASEPATH") OR exit("No esta permitido el acceso directo a este script.");

class Plantilla extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->ctrSegAdmin();
	}

	public function index() {
		$this->parametros['plantilla'] = 'datatables';
		$this->parametros['vista'] = 'admin/administradores021/plantilla';
		$this->parametros['datos']['titulo'] = 'Plantilla';
		$this->parametros['datos']['subtitulo'] = ''; 
    $tabla_plantilla = $this->Modelo->registros($this->config->item('raiz_bd') . 'plantilla', '', array() );
		$this->parametros['datos']['lista'] = $tabla_plantilla;
    
    
    
		$this->load->view('plantilla_admin', $this->parametros);			
	}

	public function traerRegistro () {
		$post = $this->input->post(NULL, TRUE);
		$registro = $this->Modelo->registro($this->config->item('raiz_bd') . 'plantilla', $post['id']);
		$datos = array('registro'=>$registro, 'tksec'=>$this->security->get_csrf_hash());
		echo json_encode($datos);      
	}

	public function guardar () {
		$post = $this->input->post(NULL, TRUE);
		if (!empty($post)) {
			foreach ($post as $key => $value) {
				$post[$key] = $this->security->xss_clean($value);
			}
      
			$this->form_validation->set_rules('barraSuperior', 'Barra Superior', 'xss_clean');
      $this->form_validation->set_rules('textoSuperior', 'Texto Superior', 'xss_clean');
      $this->form_validation->set_rules('colorFondo', 'Color Fondo', 'xss_clean');
      $this->form_validation->set_rules('colorTexto', 'ColorTexto', 'xss_clean');
      $this->form_validation->set_rules('logo', 'Logo', 'xss_clean');
      $this->form_validation->set_rules('icono', 'Icono', 'xss_clean');
      
			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.validation_errors().'<br><b>'.$this->lang->line('be_please_try_again').'</b>');
				$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
			} else {

				$config['upload_path'] = './imagenes/plantilla/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 2048;
        $this->load->library('upload', $config); 
        $this->upload->initialize($config);
        $post['logo'] = '';
        if ($this->upload->do_upload('logo')) {
          $data_valor = array('upload_data' => $this->upload->data());
          $post['logo'] = $data_valor['upload_data']['file_name'];
        }        
        
				$config['upload_path'] = './imagenes/plantilla/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 2048;
        $this->load->library('upload', $config); 
        $this->upload->initialize($config);
        $post['icono'] = '';
        if ($this->upload->do_upload('icono')) {
          $data_valor = array('upload_data' => $this->upload->data());
          $post['icono'] = $data_valor['upload_data']['file_name'];
        }                
        
				$datos = array('barraSuperior' => $post['barraSuperior'],'textoSuperior' => $post['textoSuperior'],'colorFondo' => $post['colorFondo'],'colorTexto' => $post['colorTexto'],);
        if ($post['logo'] != '') $datos['logo'] = $post['logo'];
        if ($post['icono'] != '') $datos['icono'] = $post['icono'];

				if ($this->Modelo->actualizar($this->config->item('raiz_bd') . 'plantilla', $datos, $post['id'])) {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-check"></i> '.$this->lang->line('be_successful_operation').'</h4>El registro se guardo exitosamente.'); 
					$this->session->set_flashdata('alertaTipo', 'success'); // success, info, warning, danger
				} else {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No fue posible guardar el registro.<br>'.$this->lang->line('be_please_try_again'));
					$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
				}     

			}
		} else {
			$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No se recibieron datos.<br>'.$this->lang->line('be_please_try_again'));
			$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
		}
		redirect('/admin/administradores021/plantilla');   
	}

}