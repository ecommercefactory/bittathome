<?php
defined("BASEPATH") OR exit("No esta permitido el acceso directo a este script.");

class Administradores_actividad extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->ctrSegAdmin();
	}

	public function index() {
		$this->parametros['plantilla'] = 'datatables';
		$this->parametros['vista'] = 'admin/administradores021/administradores_actividad';
		$this->parametros['datos']['titulo'] = 'Administradores Actividad';
		$this->parametros['datos']['subtitulo'] = ''; 
    $tabla_administradores_actividad = $this->Modelo->registros($this->config->item('raiz_bd') . 'administradores_actividad', '', array() );
		$this->parametros['datos']['lista'] = $tabla_administradores_actividad;
    
    $tabla_paises = $this->Modelo->registros($this->config->item('raiz_bd') . 'paises', 'id, nombre_en', array(), 'nombre_en ASC' ); $this->parametros['datos']['tabla_paises'] = $tabla_paises;$tabla_datos_valores = $this->Modelo->registros($this->config->item('raiz_bd') . 'datos_valores', 'id, nombre', array(), 'nombre ASC' ); $this->parametros['datos']['tabla_datos_valores'] = $tabla_datos_valores;$tabla_datos_valores = $this->Modelo->registros($this->config->item('raiz_bd') . 'datos_valores', 'id, nombre', array(), 'nombre ASC' ); $this->parametros['datos']['tabla_datos_valores'] = $tabla_datos_valores;
    
		$this->load->view('plantilla_admin', $this->parametros);			
	}

	public function traerRegistro () {
		$post = $this->input->post(NULL, TRUE);
		$registro = $this->Modelo->registro($this->config->item('raiz_bd') . 'administradores_actividad', $post['id']);
		$datos = array('registro'=>$registro, 'tksec'=>$this->security->get_csrf_hash());
		echo json_encode($datos);      
	}

	public function ingresar () {
		$post = $this->input->post(NULL, TRUE);
		if (!empty($post)) {
			foreach ($post as $key => $value) {
				$post[$key] = $this->security->xss_clean($value);
			}
      
			$this->form_validation->set_rules('usuario', 'Usuario', 'xss_clean');$this->form_validation->set_rules('fecha_ingreso', 'Fecha Ingreso', 'xss_clean');$this->form_validation->set_rules('ip', 'Ip', 'xss_clean');$this->form_validation->set_rules('pais', 'Pais', 'required|xss_clean');$this->form_validation->set_rules('tipo_agente', 'Tipo Agente', 'required|xss_clean');$this->form_validation->set_rules('agente', 'Agente', 'required|xss_clean');$this->form_validation->set_rules('fecha_ultima_accion', 'Fecha Ultima Accion', 'xss_clean');$this->form_validation->set_rules('permanencia', 'Permanencia', 'xss_clean');
      
			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.validation_errors().'<br><b>'.$this->lang->line('be_please_try_again').'</b>');
				$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
			} else {

				
        
				$datos = array('usuario' => $post['usuario'],'fecha_ingreso' => $post['fecha_ingreso'],'ip' => $post['ip'],'pais' => $post['pais'],'tipo_agente' => $post['tipo_agente'],'agente' => $post['agente'],'fecha_ultima_accion' => $post['fecha_ultima_accion'],'permanencia' => $post['permanencia'],);

				$id = $this->Modelo->insertar($this->config->item('raiz_bd') . 'administradores_actividad', $datos);
				if ($id > 0) {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-check"></i> '.$this->lang->line('be_successful_operation').'</h4>El registro se ingresó exitosamente.'); 
					$this->session->set_flashdata('alertaTipo', 'success'); // success, info, warning, danger
				} else {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No fue posible ingresar el registro.<br>'.$this->lang->line('be_please_try_again'));
					$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
				}     
			}
		} else {
			$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No se recibieron datos.<br>'.$this->lang->line('be_please_try_again'));
			$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
		}
		redirect('/admin/administradores021/administradores_actividad');
	}

	public function guardar () {
		$post = $this->input->post(NULL, TRUE);
		if (!empty($post)) {
			foreach ($post as $key => $value) {
				$post[$key] = $this->security->xss_clean($value);
			}
      
			$this->form_validation->set_rules('usuario', 'Usuario', 'xss_clean');$this->form_validation->set_rules('fecha_ingreso', 'Fecha Ingreso', 'xss_clean');$this->form_validation->set_rules('ip', 'Ip', 'xss_clean');$this->form_validation->set_rules('pais', 'Pais', 'required|xss_clean');$this->form_validation->set_rules('tipo_agente', 'Tipo Agente', 'required|xss_clean');$this->form_validation->set_rules('agente', 'Agente', 'required|xss_clean');$this->form_validation->set_rules('fecha_ultima_accion', 'Fecha Ultima Accion', 'xss_clean');$this->form_validation->set_rules('permanencia', 'Permanencia', 'xss_clean');
      
			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.validation_errors().'<br><b>'.$this->lang->line('be_please_try_again').'</b>');
				$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
			} else {

				
        
				$datos = array('usuario' => $post['usuario'],'fecha_ingreso' => $post['fecha_ingreso'],'ip' => $post['ip'],'pais' => $post['pais'],'tipo_agente' => $post['tipo_agente'],'agente' => $post['agente'],'fecha_ultima_accion' => $post['fecha_ultima_accion'],'permanencia' => $post['permanencia'],);

				if ($this->Modelo->actualizar($this->config->item('raiz_bd') . 'administradores_actividad', $datos, $post['id'])) {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-check"></i> '.$this->lang->line('be_successful_operation').'</h4>El registro se guardo exitosamente.'); 
					$this->session->set_flashdata('alertaTipo', 'success'); // success, info, warning, danger
				} else {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No fue posible guardar el registro.<br>'.$this->lang->line('be_please_try_again'));
					$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
				}     

			}
		} else {
			$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No se recibieron datos.<br>'.$this->lang->line('be_please_try_again'));
			$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
		}
		redirect('/admin/administradores021/administradores_actividad');   
	}

	public function eliminar () {
		$post = $this->input->post(NULL, TRUE);
		if (!empty($post)) {
			foreach ($post as $key => $value) {
				$post[$key] = $this->security->xss_clean($value);
			}
			$this->form_validation->set_rules('id', 'ID', 'required|trim|xss_clean');
			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.validation_errors().'<br><b>'.$this->lang->line('be_please_try_again').'</b>');
				$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
			} else {

				$mat_id = explode(";", $post['id']); 
				$proceso = false;          
				foreach ($mat_id as $id) {
					$this->Modelo->eliminar($this->config->item('raiz_bd') . 'administradores_actividad', $id);
					$proceso = true;
				}

				if ($proceso) {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-check"></i> '.$this->lang->line('be_successful_operation').'</h4>El(los) registro(s) se eliminó(aron) exitosamente.'); 
					$this->session->set_flashdata('alertaTipo', 'success'); // success, info, warning, danger
				} else {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No fue posible eliminar el(los) registro(s).<br>'.$this->lang->line('be_please_try_again'));
					$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
				}     

			}
		} else {
			$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No se recibieron datos.<br>'.$this->lang->line('be_please_try_again'));
			$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
		}
		redirect('/admin/administradores021/administradores_actividad');   
	}

  
  
	public function lista () {

		$registros = $this->Modelo->registros($this->config->item('raiz_bd') . 'administradores_actividad');
    
  	$datosJson = '
  		{	
  			"data":[';  
    $datosJsonReg = '';
    foreach ($registros as $key => $registro) {
      
          $datosJsonReg .='["<input type=\"checkbox\" id=\"fila_'.$registro['id'].'\" class=\"seleccion\" cod=\"'.$registro['id'].'\">", "<button type=\"button\" class=\"btn btn-default btn-xs text-light-blue btnEditar\" cod=\"'.$registro['id'].'\"><span class=\"glyphicon glyphicon-pencil\"></span></button>", "<button type=\"button\" class=\"btn btn-default btn-xs text-red btnEliminar\" cod=\"'.$registro['id'].'\"><span class=\"glyphicon glyphicon-trash\"></span></button>","'.$registro['usuario'].'","'.$registro['fecha_ingreso'].'","'.$registro['ip'].'","'.$registro['pais'].'","'.$registro['tipo_agente'].'","'.$registro['agente'].'","'.$registro['fecha_ultima_accion'].'","'.$registro['permanencia'].'"],';

    }
    if ($datosJsonReg != '') {
          $datosJson .= substr($datosJsonReg, 0, -1);
    }
		
		$datosJson .= ']
		}';

		echo $datosJson;
    
	}
      
  
	public function lista_ssp () {
		$this->load->library('SSP');

		// DB table to use
		$table = 'administradores_actividad';

		// Table's primary key
		$primaryKey = 'id';

		// Array of database columns
		$columns = array(array( 'db' => 'id', 'dt' => 0, 'field' => 'id', 'formatter' => function($d, $row) {return '<input type="checkbox" id="fila_' . $d . '" class="seleccion" cod="' . $d . '">';}),array( 'db' => 'id', 'dt' => 1, 'field' => 'id', 'formatter' => function($d, $row) {return '<button type="button" class="btn btn-default btn-xs text-light-blue btnEditar" cod="' . $d . '"><span class="glyphicon glyphicon-pencil"></span></button>';}), array( 'db' => 'id', 'dt' => 2, 'field' => 'id', 'formatter' => function($d, $row) {return '<button type="button" class="btn btn-default btn-xs text-red btnEliminar" cod="' . $d . '"><span class="glyphicon glyphicon-trash"></span></button>';}),array( 'db' => 'usuario', 'dt' => 3, 'field' => 'usuario' ),array( 'db' => 'fecha_ingreso', 'dt' => 4, 'field' => 'fecha_ingreso' ),array( 'db' => 'ip', 'dt' => 5, 'field' => 'ip' ),array( 'db' => 'pais', 'dt' => 6, 'field' => 'pais' ),array( 'db' => 'tipo_agente', 'dt' => 7, 'field' => 'tipo_agente' ),array( 'db' => 'agente', 'dt' => 8, 'field' => 'agente' ),array( 'db' => 'fecha_ultima_accion', 'dt' => 9, 'field' => 'fecha_ultima_accion' ),array( 'db' => 'permanencia', 'dt' => 10, 'field' => 'permanencia' ),);

		$sql_details = array(
			'user' => $this->db->username,
			'pass' => $this->db->password,
			'db'   => $this->db->database,
			'host' => $this->db->hostname
		);

		$joinQuery = "FROM administradores_actividad";
		$extraWhere = ""; //"`u`.`valor` >= 90000";
		$groupBy = ""; //"`u`.`datos`";
		$having = ""; //"`u`.`valor` >= 140000";

		$_GET['tksec'] = $this->security->get_csrf_hash();  

		echo json_encode(
			SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having )
		);
	}

}