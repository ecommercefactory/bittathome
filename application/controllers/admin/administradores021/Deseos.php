<?php
defined("BASEPATH") OR exit("No esta permitido el acceso directo a este script.");

class Deseos extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->ctrSegAdmin();
	}

	public function index() {
		$this->parametros['plantilla'] = 'datatables';
		$this->parametros['vista'] = 'admin/administradores021/deseos';
		$this->parametros['datos']['titulo'] = 'Deseos';
		$this->parametros['datos']['subtitulo'] = ''; 
    $tabla_deseos = $this->Modelo->registros($this->config->item('raiz_bd') . 'deseos', '', array() );
		$this->parametros['datos']['lista'] = $tabla_deseos;
    
    $tabla_usuarios = $this->Modelo->registros($this->config->item('raiz_bd') . 'usuarios', 'id, nombre', array(), 'nombre ASC' ); $this->parametros['datos']['tabla_usuarios'] = $tabla_usuarios;$tabla_productos = $this->Modelo->registros($this->config->item('raiz_bd') . 'productos', 'id, nombre', array(), 'nombre ASC' ); $this->parametros['datos']['tabla_productos'] = $tabla_productos;
    
		$this->load->view('plantilla_admin', $this->parametros);			
	}

	public function eliminar () {
		$post = $this->input->post(NULL, TRUE);
		if (!empty($post)) {
			foreach ($post as $key => $value) {
				$post[$key] = $this->security->xss_clean($value);
			}
			$this->form_validation->set_rules('id', 'ID', 'required|trim|xss_clean');
			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.validation_errors().'<br><b>'.$this->lang->line('be_please_try_again').'</b>');
				$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
			} else {

				$mat_id = explode(";", $post['id']); 
				$proceso = false;          
				foreach ($mat_id as $id) {
					$this->Modelo->eliminar($this->config->item('raiz_bd') . 'deseos', $id);
					$proceso = true;
				}

				if ($proceso) {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-check"></i> '.$this->lang->line('be_successful_operation').'</h4>El(los) registro(s) se eliminó(aron) exitosamente.'); 
					$this->session->set_flashdata('alertaTipo', 'success'); // success, info, warning, danger
				} else {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No fue posible eliminar el(los) registro(s).<br>'.$this->lang->line('be_please_try_again'));
					$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
				}     

			}
		} else {
			$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No se recibieron datos.<br>'.$this->lang->line('be_please_try_again'));
			$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
		}
		redirect('/admin/administradores021/deseos');   
	}

  
  
	public function lista () {

		$registros = $this->Modelo->registros($this->config->item('raiz_bd') . 'deseos');
    
  	$datosJson = '
  		{	
  			"data":[';  
    $datosJsonReg = '';
    foreach ($registros as $key => $registro) {

          $usuario = $this->Modelo->registro($this->config->item('raiz_bd') . 'usuarios', $registro['usuario']);
          if ($usuario) {
             $registro['usuario'] = $usuario->nombre;
          }        
          $producto = $this->Modelo->registro($this->config->item('raiz_bd') . 'productos', $registro['producto']);
          if ($producto) {
             $registro['producto'] = $producto->nombre;
          }    
      
          $datosJsonReg .='["<input type=\"checkbox\" id=\"fila_'.$registro['id'].'\" class=\"seleccion\" cod=\"'.$registro['id'].'\">", "<button type=\"button\" class=\"btn btn-default btn-xs text-red btnEliminar\" cod=\"'.$registro['id'].'\"><span class=\"glyphicon glyphicon-trash\"></span></button>","'.$registro['usuario'].'","'.$registro['producto'].'","'.$registro['fecha'].'"],';

    }
    if ($datosJsonReg != '') {
          $datosJson .= substr($datosJsonReg, 0, -1);
    }
		
		$datosJson .= ']
		}';

		echo $datosJson;
    
	}
      
}