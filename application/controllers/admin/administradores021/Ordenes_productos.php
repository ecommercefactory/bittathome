<?php
defined("BASEPATH") OR exit("No esta permitido el acceso directo a este script.");

class Ordenes_productos extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->ctrSegAdmin();
	}

    function _remap($param) {
        $this->index($param);
    }
  
    public function index($id) {  
    	if (!is_numeric($id)) {
    		redirect('/admin/administradores021/ordenes');
    	}
		$orden = $this->Modelo->registro($this->config->item('raiz_bd') . 'ordenes', $id);
    	if (count($orden) == 0) {
    		redirect('/admin/administradores021/ordenes');
    	}

		$this->parametros['plantilla'] = 'datatables';
		$this->parametros['vista'] = 'admin/administradores021/ordenes_productos';
		$this->parametros['datos']['titulo'] = 'Productos';
		$this->parametros['datos']['subtitulo'] = 'de la Orden #:' . $orden->id; 
        $this->parametros['datos']['breadcrumb'] = '
            <ol class="breadcrumb">
                <li><a href="'.base_url().'admin/administradores021/ordenes">Ordenes</a></li>
                <li class="active">Productos</li>
            </ol>';			
		$this->parametros['datos']['orden'] = $id; 
//    $tabla_ordenes_productos = $this->Modelo->registros($this->config->item('raiz_bd') . 'ordenes_productos', '', array() );
//		$this->parametros['datos']['lista'] = $tabla_ordenes_productos;
    
    $tabla_productos = $this->Modelo->registros($this->config->item('raiz_bd') . 'productos', 'id, nombre', array(), 'nombre ASC' ); 
    $this->parametros['datos']['tabla_productos'] = $tabla_productos;
    
		$this->load->view('plantilla_admin', $this->parametros);			
	}

}