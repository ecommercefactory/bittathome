<?php
defined("BASEPATH") OR exit("No esta permitido el acceso directo a este script.");

class Productos_especificaciones extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->ctrSegAdmin();
	}

    function _remap($param) {
        $this->index($param);
    }
  
    public function index($id) {  
    	if (!is_numeric($id)) {
    		redirect('/admin/administradores021/productos');
    	}
		$producto = $this->Modelo->registro($this->config->item('raiz_bd') . 'productos', $id);

		$this->parametros['plantilla'] = 'datatables';
		$this->parametros['vista'] = 'admin/administradores021/productos_especificaciones';
		$this->parametros['datos']['titulo'] = 'Especificaciones';
		$this->parametros['datos']['subtitulo'] = 'de ' . $producto->nombre; 
        $this->parametros['datos']['breadcrumb'] = '
            <ol class="breadcrumb">
                <li><a href="'.base_url().'admin/administradores021/productos">Productos</a></li>
                <li class="active">Especificaciones</li>
            </ol>';			
		$this->parametros['datos']['producto'] = $id; 
/*
    $tabla_productos_especificaciones = $this->Modelo->registros($this->config->item('raiz_bd') . 'productos_especificaciones', '', array() );
		$this->parametros['datos']['lista'] = $tabla_productos_especificaciones;
*/    
    $tabla_productos = $this->Modelo->registros($this->config->item('raiz_bd') . 'productos', 'id, nombre', array(), 'nombre ASC' ); 
    $this->parametros['datos']['tabla_productos'] = $tabla_productos;
    
    $tabla_especificaciones = $this->Modelo->registros($this->config->item('raiz_bd') . 'especificaciones', 'id, nombre', array(), 'nombre ASC' ); 
    $this->parametros['datos']['tabla_especificaciones'] = $tabla_especificaciones;

    $tabla_especificaciones_datos = $this->Modelo->registros($this->config->item('raiz_bd') . 'especificaciones_datos', 'id, valor', array(), 'valor ASC' ); 
    $this->parametros['datos']['tabla_especificaciones_datos'] = $tabla_especificaciones_datos;
    
		$this->load->view('plantilla_admin', $this->parametros);			
	}

}