<?php
defined("BASEPATH") OR exit("No esta permitido el acceso directo a este script.");

class Plantilla_redes_sociales_funciones extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->ctrSegAdmin();
	}

	public function index() {
		echo "Plantilla_redes_sociales_funciones";
	}

	public function traerRegistro () {
		$post = $this->input->post(NULL, TRUE);
		$registro = $this->Modelo->registro($this->config->item('raiz_bd') . 'plantilla_redes_sociales', $post['id']);
		$datos = array('registro'=>$registro, 'tksec'=>$this->security->get_csrf_hash());
		echo json_encode($datos);      
	}

	public function ingresar () {
		$post = $this->input->post(NULL, TRUE);
		if (!empty($post)) {
			foreach ($post as $key => $value) {
				$post[$key] = $this->security->xss_clean($value);
				//$post[$key] = str_replace('"',"'",$value);
      }
      
			// $this->form_validation->set_rules('plantilla', 'Plantilla', 'required|xss_clean');
      $this->form_validation->set_rules('nombre', 'Nombre', 'required|max_length[50]|xss_clean');
      $this->form_validation->set_rules('url', 'URL', 'required|max_length[100]|valid_url|xss_clean');
      $this->form_validation->set_rules('codigo', 'Codigo', 'xss_clean');
      $this->form_validation->set_rules('logo', 'Logo', 'xss_clean');
      $this->form_validation->set_rules('color', 'Color', 'xss_clean');
      $this->form_validation->set_rules('orden', 'Orden', 'numeric|xss_clean');
      
			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.validation_errors().'<br><b>'.$this->lang->line('be_please_try_again').'</b>');
				$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
			} else {

				$config['upload_path'] = './imagenes/redes_sociales/';$config['allowed_types'] = 'gif|jpg|png|jpeg';$config['max_size'] = 2048;$this->load->library('upload', $config); $this->upload->initialize($config);$post['logo'] = '';if ($this->upload->do_upload('logo')) {$data_logo = array('upload_data' => $this->upload->data());$post['logo'] = $data_logo['upload_data']['file_name'];}
        
				$datos = array('nombre' => $post['nombre'],
                       'url' => $post['url'],
                       'codigo' => $post['codigo'],
                       'logo' => $post['logo'],
                       'color' => $post['color'],
                       'orden' => $post['orden'],);

				$id = $this->Modelo->insertar($this->config->item('raiz_bd') . 'plantilla_redes_sociales', $datos);
				if ($id > 0) {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-check"></i> '.$this->lang->line('be_successful_operation').'</h4>El registro se ingresó exitosamente.'); 
					$this->session->set_flashdata('alertaTipo', 'success'); // success, info, warning, danger
				} else {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No fue posible ingresar el registro.<br>'.$this->lang->line('be_please_try_again'));
					$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
				}     
			}
		} else {
			$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No se recibieron datos.<br>'.$this->lang->line('be_please_try_again'));
			$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
		}
		redirect('/admin/administradores021/plantilla_redes_sociales/' . $post['plantilla']);
	}

	public function guardar () {
		$post = $this->input->post(NULL, TRUE);
		if (!empty($post)) {
			foreach ($post as $key => $value) {
				$post[$key] = $this->security->xss_clean($value);
				//$post[$key] = str_replace('"',"'",$value);
			}
      
			$this->form_validation->set_rules('nombre', 'Nombre', 'required|max_length[50]|xss_clean');
      $this->form_validation->set_rules('url', 'URL', 'required|max_length[100]|valid_url|xss_clean');
      $this->form_validation->set_rules('codigo', 'Codigo', 'xss_clean');
      $this->form_validation->set_rules('logo', 'Logo', 'xss_clean');
      $this->form_validation->set_rules('color', 'Color', 'xss_clean');
      $this->form_validation->set_rules('orden', 'Orden', 'numeric|xss_clean');
      
			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.validation_errors().'<br><b>'.$this->lang->line('be_please_try_again').'</b>');
				$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
			} else {

				$config['upload_path'] = './imagenes/redes_sociales/';$config['allowed_types'] = 'gif|jpg|png|jpeg';$config['max_size'] = 2048;$this->load->library('upload', $config); $this->upload->initialize($config);$post['logo'] = '';if ($this->upload->do_upload('logo')) {$data_logo = array('upload_data' => $this->upload->data());$post['logo'] = $data_logo['upload_data']['file_name'];}

				$datos = array('nombre' => $post['nombre'],'url' => $post['url'],'codigo' => $post['codigo'],'color' => $post['color'],
                       'orden' => $post['orden'],);
        if ($post['logo'] != '') $datos['logo'] = $post['logo'];

				if ($this->Modelo->actualizar($this->config->item('raiz_bd') . 'plantilla_redes_sociales', $datos, $post['id'])) {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-check"></i> '.$this->lang->line('be_successful_operation').'</h4>El registro se guardo exitosamente.'); 
					$this->session->set_flashdata('alertaTipo', 'success'); // success, info, warning, danger
				} else {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No fue posible guardar el registro.<br>'.$this->lang->line('be_please_try_again'));
					$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
				}     

			}
		} else {
			$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No se recibieron datos.<br>'.$this->lang->line('be_please_try_again'));
			$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
		}
		redirect('/admin/administradores021/plantilla_redes_sociales/' . $post['plantilla']);
	}

	public function eliminar () {
		$post = $this->input->post(NULL, TRUE);
		if (!empty($post)) {
			foreach ($post as $key => $value) {
				$post[$key] = $this->security->xss_clean($value);
			}
			$this->form_validation->set_rules('id', 'ID', 'required|trim|xss_clean');
			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.validation_errors().'<br><b>'.$this->lang->line('be_please_try_again').'</b>');
				$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
			} else {

				$mat_id = explode(";", $post['id']); 
				$proceso = false;          
				foreach ($mat_id as $id) {
					$this->Modelo->eliminar($this->config->item('raiz_bd') . 'plantilla_redes_sociales', $id);
					$proceso = true;
				}

				if ($proceso) {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-check"></i> '.$this->lang->line('be_successful_operation').'</h4>El(los) registro(s) se eliminó(aron) exitosamente.'); 
					$this->session->set_flashdata('alertaTipo', 'success'); // success, info, warning, danger
				} else {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No fue posible eliminar el(los) registro(s).<br>'.$this->lang->line('be_please_try_again'));
					$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
				}     

			}
		} else {
			$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No se recibieron datos.<br>'.$this->lang->line('be_please_try_again'));
			$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
		}
		redirect('/admin/administradores021/plantilla_redes_sociales/' . $post['plantilla']);
	}

  
  
	public function lista () {

		$registros = $this->Modelo->registros($this->config->item('raiz_bd') . 'plantilla_redes_sociales');
    
  	$datosJson = '
  		{	
  			"data":[';  
    $datosJsonReg = '';
    foreach ($registros as $key => $registro) {
/*
          if ($registro['codigo'] !== '') {
              $registro['codigo'] = htmlentities($registro['codigo']);
          }
*/

          foreach ($registro as $r_key => $r_value) {
              $registro[$r_key] = htmlentities($r_value);
          }      

      
      
          if (trim($registro['logo']) != '') {
              $registro['logo'] = '<img src=\"'.base_url().'imagenes/redes_sociales/'.$registro['logo'].'\" width=\"30px\"  class=\"img-thumbnail\">';
          }      
      
          $datosJsonReg .='["<input type=\"checkbox\" id=\"fila_'.$registro['id'].'\" class=\"seleccion\" cod=\"'.$registro['id'].'\">", "<button type=\"button\" class=\"btn btn-default btn-xs text-light-blue btnEditar\" cod=\"'.$registro['id'].'\"><span class=\"glyphicon glyphicon-pencil\"></span></button>", "<button type=\"button\" class=\"btn btn-default btn-xs text-red btnEliminar\" cod=\"'.$registro['id'].'\"><span class=\"glyphicon glyphicon-trash\"></span></button>","'.$registro['nombre'].'","'.$registro['url'].'","'.$registro['codigo'].'","'.$registro['logo'].'","'.$registro['color'].'","'.$registro['orden'].'","'.$registro['fecha'].'"],';

    }
    if ($datosJsonReg != '') {
          $datosJson .= substr($datosJsonReg, 0, -1);
    }
		
		$datosJson .= ']
		}';

		echo $datosJson;
    
	}
      
  
	public function lista_ssp () {
		$this->load->library('SSP');

		// DB table to use
		$table = 'plantilla_redes_sociales';

		// Table's primary key
		$primaryKey = 'id';

		// Array of database columns
		$columns = array(array( 'db' => 'id', 'dt' => 0, 'field' => 'id', 'formatter' => function($d, $row) {return '<input type="checkbox" id="fila_' . $d . '" class="seleccion" cod="' . $d . '">';}),array( 'db' => 'id', 'dt' => 1, 'field' => 'id', 'formatter' => function($d, $row) {return '<button type="button" class="btn btn-default btn-xs text-light-blue btnEditar" cod="' . $d . '"><span class="glyphicon glyphicon-pencil"></span></button>';}), array( 'db' => 'id', 'dt' => 2, 'field' => 'id', 'formatter' => function($d, $row) {return '<button type="button" class="btn btn-default btn-xs text-red btnEliminar" cod="' . $d . '"><span class="glyphicon glyphicon-trash"></span></button>';}),array( 'db' => 'plantilla', 'dt' => 3, 'field' => 'plantilla' ),array( 'db' => 'nombre', 'dt' => 4, 'field' => 'nombre' ),array( 'db' => 'url', 'dt' => 5, 'field' => 'url' ),array( 'db' => 'codigo', 'dt' => 6, 'field' => 'codigo' ),array( 'db' => 'logo', 'dt' => 7, 'field' => 'logo' ),array( 'db' => 'color', 'dt' => 8, 'field' => 'color' ),array( 'db' => 'fecha', 'dt' => 9, 'field' => 'fecha' ),);

		$sql_details = array(
			'user' => $this->db->username,
			'pass' => $this->db->password,
			'db'   => $this->db->database,
			'host' => $this->db->hostname
		);

		$joinQuery = "FROM plantilla_redes_sociales";
		$extraWhere = ""; //"`u`.`valor` >= 90000";
		$groupBy = ""; //"`u`.`datos`";
		$having = ""; //"`u`.`valor` >= 140000";

		$_GET['tksec'] = $this->security->get_csrf_hash();  

		echo json_encode(
			SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having )
		);
	}  
  
}  
