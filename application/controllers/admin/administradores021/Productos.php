<?php
defined("BASEPATH") OR exit("No esta permitido el acceso directo a este script.");

class Productos extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->ctrSegAdmin();
	}

	public function index() {
		$this->parametros['plantilla'] = 'datatables';
		$this->parametros['vista'] = 'admin/administradores021/productos';
		$this->parametros['datos']['titulo'] = 'Productos';
		$this->parametros['datos']['subtitulo'] = ''; 
//    $tabla_productos = $this->Modelo->registros($this->config->item('raiz_bd') . 'productos', '', array() );
//		$this->parametros['datos']['lista'] = $tabla_productos;
    
    $tabla_categorias = $this->Modelo->registros($this->config->item('raiz_bd') . 'categorias', 'id, nombre', array(), 'nombre ASC' ); 
    $this->parametros['datos']['tabla_categorias'] = $tabla_categorias;
    
    $tabla_subcategorias = $this->Modelo->registros($this->config->item('raiz_bd') . 'subcategorias', 'id, nombre', array(), 'nombre ASC' ); 
    $this->parametros['datos']['tabla_subcategorias'] = $tabla_subcategorias;
    
    $tabla_datos_valores_estado = $this->Modelo->registros($this->config->item('raiz_bd') . 'datos_valores', 'id, nombre', array('dato' => 1), 'nombre ASC' ); 
    $this->parametros['datos']['tabla_datos_valores_estado'] = $tabla_datos_valores_estado;
    
    $tabla_datos_valores_ofertado_categoria = $this->Modelo->registros($this->config->item('raiz_bd') . 'datos_valores', 'id, nombre', array('dato' => 3), 'nombre ASC' ); 
    $this->parametros['datos']['tabla_datos_valores_ofertado_categoria'] = $tabla_datos_valores_ofertado_categoria;
    
    $tabla_datos_valores_ofertado_subcategoria = $this->Modelo->registros($this->config->item('raiz_bd') . 'datos_valores', 'id, nombre', array('dato' => 3), 'nombre ASC' ); 
    $this->parametros['datos']['tabla_datos_valores_ofertado_subcategoria'] = $tabla_datos_valores_ofertado_subcategoria;
    
    $tabla_datos_valores_oferta = $this->Modelo->registros($this->config->item('raiz_bd') . 'datos_valores', 'id, nombre', array('dato' => 3), 'nombre ASC' ); 
    $this->parametros['datos']['tabla_datos_valores_oferta'] = $tabla_datos_valores_oferta;
    
		$this->load->view('plantilla_admin', $this->parametros);			
	}

	public function traerRegistro () {
		$post = $this->input->post(NULL, TRUE);
		$registro = $this->Modelo->registro($this->config->item('raiz_bd') . 'productos', $post['id']);
    if ($registro->finOferta == '0000-00-00') $registro->finOferta = '';
		$datos = array('registro'=>$registro, 'tksec'=>$this->security->get_csrf_hash());
		echo json_encode($datos);      
	}

  public function traerSubcategorias () {
    $post = $this->input->post(NULL, TRUE);
    $tabla_subcategorias = $this->Modelo->registros($this->config->item('raiz_bd') . 'subcategorias', '', array('categoria'=>$post['categoria']) );
    $lista_subcategorias = "<select class='form-control' id='subcategoria_".$post['modo']."' name='subcategoria'><option></option>";
    foreach ($tabla_subcategorias as $registro) {
        $lista_subcategorias .= "<option value=" . $registro['id'] . ">" . $registro['nombre'] . "</option>";
    }
    $lista_subcategorias .= "</select>";
    $datos = array('lista_subcategorias'=>$lista_subcategorias, 'tksec'=>$this->security->get_csrf_hash());
    echo json_encode($datos);      
  }

  public function crearURLAmigable () {
    $post = $this->input->post(NULL, TRUE);
    $url = '';
    $texto = preg_replace('/[^a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]/', '', $post['nombre']);
    $encontrar = array('à','á','â','ã','ä', 'ç', 'è','é','ê','ë', 'ì','í','î','ï', 'ñ', 'ò','ó','ô','õ','ö', 'ù','ú','û','ü', 'ý','ÿ', 'À','Á','Â','Ã','Ä', 'Ç', 'È','É','Ê','Ë', 'Ì','Í','Î','Ï', 'Ñ', 'Ò','Ó','Ô','Õ','Ö', 'Ù','Ú','Û','Ü', 'Ý', '-', ' ');
    $reemplazar = array('a','a','a','a','a', 'c', 'e','e','e','e', 'i','i','i','i', 'n', 'o','o','o','o','o', 'u','u','u','u', 'y','y', 'A','A','A','A','A', 'C', 'E','E','E','E', 'I','I','I','I', 'N', 'O','O','O','O','O', 'U','U','U','U', 'Y', '_', '-');
    $url = urlencode(strtolower(str_replace($encontrar, $reemplazar, $texto)));
    $datos = array('url'=>$url, 'tksec'=>$this->security->get_csrf_hash());
    echo json_encode($datos);      
  }
  
	public function ingresar () {
		$post = $this->input->post(NULL, TRUE);
		if (!empty($post)) {
			foreach ($post as $key => $value) {
				$post[$key] = $this->security->xss_clean($value);
			}
      
			$this->form_validation->set_rules('categoria', 'Categoria', 'required|xss_clean');$this->form_validation->set_rules('subcategoria', 'Subcategoria', 'xss_clean');$this->form_validation->set_rules('nombre', 'Nombre', 'required|xss_clean');$this->form_validation->set_rules('ruta', 'Ruta', 'required|max_length[250]|xss_clean');$this->form_validation->set_rules('sku', 'Sku', 'required|xss_clean');$this->form_validation->set_rules('estado', 'Estado', 'required|xss_clean');$this->form_validation->set_rules('titulo', 'Titulo', 'xss_clean');$this->form_validation->set_rules('descripcionCorta', 'Descripcion Corta', 'xss_clean');$this->form_validation->set_rules('descripcion', 'Descripcion', 'xss_clean');$this->form_validation->set_rules('palabrasClaves', 'Palabras Claves', 'max_length[250]|xss_clean');$this->form_validation->set_rules('portadaMarcado', 'Portada Marcado', 'xss_clean');
      //$this->form_validation->set_rules('inventario', 'Inventario', 'numeric|xss_clean');
      $this->form_validation->set_rules('precio', 'Precio', 'numeric|xss_clean');$this->form_validation->set_rules('portada', 'Portada', 'xss_clean');$this->form_validation->set_rules('vistas', 'Vistas', 'xss_clean');$this->form_validation->set_rules('ventas', 'Ventas', 'xss_clean');$this->form_validation->set_rules('ofertadoPorCategoria', 'Ofertado por Categoria', 'xss_clean');$this->form_validation->set_rules('ofertadoPorSubCategoria', 'Ofertado por Subcategoria', 'xss_clean');$this->form_validation->set_rules('oferta', 'Oferta', 'xss_clean');$this->form_validation->set_rules('precioOferta', 'Precio Oferta', 'numeric|xss_clean');$this->form_validation->set_rules('descuentoOferta', 'Descuento Oferta', 'less_than[100]|numeric|xss_clean');$this->form_validation->set_rules('imgOferta', 'Imagen Oferta', 'xss_clean');$this->form_validation->set_rules('finOferta', 'Fin Oferta', 'xss_clean');$this->form_validation->set_rules('peso', 'Peso', 'xss_clean');
      
			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.validation_errors().'<br><b>'.$this->lang->line('be_please_try_again').'</b>');
				$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
			} else {

				$config['upload_path'] = './imagenes/producto/portada/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 2048;
        $this->load->library('upload', $config); 
        $this->upload->initialize($config);
        $post['portadaMarcado'] = '';
        if ($this->upload->do_upload('portadaMarcado')) {
          $data_portadaMarcado = array('upload_data' => $this->upload->data());
          $post['portadaMarcado'] = $data_portadaMarcado['upload_data']['file_name'];
        }
        
        $config['upload_path'] = './imagenes/producto/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 2048;
        $this->load->library('upload', $config); 
        $this->upload->initialize($config);
        $post['portada'] = '';
        if ($this->upload->do_upload('portada')) {
          $data_portada = array('upload_data' => $this->upload->data());
          $post['portada'] = $data_portada['upload_data']['file_name'];
        }
        
        $config['upload_path'] = './imagenes/producto/oferta/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 2048;
        $this->load->library('upload', $config); 
        $this->upload->initialize($config);
        $post['imgOferta'] = '';
        if ($this->upload->do_upload('imgOferta')) {
          $data_imgOferta = array('upload_data' => $this->upload->data());
          $post['imgOferta'] = $data_imgOferta['upload_data']['file_name'];
        }
        
				$datos = array('categoria' => $post['categoria'],'subcategoria' => $post['subcategoria'],'nombre' => $post['nombre'],'ruta' => $post['ruta'],'sku' => $post['sku'],'estado' => $post['estado'],'titulo' => $post['titulo'],'descripcionCorta' => $post['descripcionCorta'],'descripcion' => $post['descripcion'],'palabrasClaves' => $post['palabrasClaves'],'portadaMarcado' => $post['portadaMarcado'],'precio' => $post['precio'],'portada' => $post['portada'],'vistas' => $post['vistas'],'ventas' => $post['ventas'],'ofertadoPorCategoria' => $post['ofertadoPorCategoria'],'ofertadoPorSubCategoria' => $post['ofertadoPorSubCategoria'],'oferta' => $post['oferta'],'precioOferta' => $post['precioOferta'],'descuentoOferta' => $post['descuentoOferta'],'imgOferta' => $post['imgOferta'],'finOferta' => $post['finOferta'],'peso' => $post['peso'],);

				$id = $this->Modelo->insertar($this->config->item('raiz_bd') . 'productos', $datos);
				if ($id > 0) {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-check"></i> '.$this->lang->line('be_successful_operation').'</h4>El registro se ingresó exitosamente.'); 
					$this->session->set_flashdata('alertaTipo', 'success'); // success, info, warning, danger
				} else {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No fue posible ingresar el registro.<br>'.$this->lang->line('be_please_try_again'));
					$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
				}     
			}
		} else {
			$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No se recibieron datos.<br>'.$this->lang->line('be_please_try_again'));
			$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
		}
		redirect('/admin/administradores021/productos');
	}

	public function guardar () {
		$post = $this->input->post(NULL, TRUE);
		if (!empty($post)) {
			foreach ($post as $key => $value) {
				$post[$key] = $this->security->xss_clean($value);
			}
      
			$this->form_validation->set_rules('categoria', 'Categoria', 'required|xss_clean');$this->form_validation->set_rules('subcategoria', 'Subcategoria', 'xss_clean');$this->form_validation->set_rules('nombre', 'Nombre', 'required|xss_clean');$this->form_validation->set_rules('ruta', 'Ruta', 'required|max_length[250]|xss_clean');$this->form_validation->set_rules('sku', 'Sku', 'required|xss_clean');$this->form_validation->set_rules('estado', 'Estado', 'required|xss_clean');$this->form_validation->set_rules('titulo', 'Titulo', 'xss_clean');$this->form_validation->set_rules('descripcionCorta', 'Descripcion Corta', 'xss_clean');$this->form_validation->set_rules('descripcion', 'Descripcion', 'xss_clean');$this->form_validation->set_rules('palabrasClaves', 'Palabras Claves', 'max_length[250]|xss_clean');$this->form_validation->set_rules('portadaMarcado', 'Portada Marcado', 'xss_clean');
      //$this->form_validation->set_rules('inventario', 'Inventario', 'numeric|xss_clean');
      $this->form_validation->set_rules('precio', 'Precio', 'numeric|xss_clean');$this->form_validation->set_rules('portada', 'Portada', 'xss_clean');$this->form_validation->set_rules('vistas', 'Vistas', 'xss_clean');$this->form_validation->set_rules('ventas', 'Ventas', 'xss_clean');$this->form_validation->set_rules('ofertadoPorCategoria', 'Ofertado por Categoria', 'xss_clean');$this->form_validation->set_rules('ofertadoPorSubCategoria', 'Ofertado por Subcategoria', 'xss_clean');$this->form_validation->set_rules('oferta', 'Oferta', 'xss_clean');$this->form_validation->set_rules('precioOferta', 'Precio Oferta', 'numeric|xss_clean');$this->form_validation->set_rules('descuentoOferta', 'Descuento Oferta', 'less_than[100]|numeric|xss_clean');$this->form_validation->set_rules('imgOferta', 'Imagen Oferta', 'xss_clean');$this->form_validation->set_rules('finOferta', 'Fin Oferta', 'xss_clean');$this->form_validation->set_rules('peso', 'Peso', 'xss_clean');
      
			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.validation_errors().'<br><b>'.$this->lang->line('be_please_try_again').'</b>');
				$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
			} else {
/*
				$config['upload_path'] = './imagenes/producto/portada/';$config['allowed_types'] = 'gif|jpg|png|jpeg';$config['max_size'] = 2048;$this->load->library('upload', $config); $this->upload->initialize($config);$post['portadaMarcado'] = '';if ($this->upload->do_upload('portadaMarcado')) {$data_portadaMarcado = array('upload_data' => $this->upload->data());$post['portadaMarcado'] = $data_portadaMarcado['upload_data']['file_name'];}$config['upload_path'] = './imagenes/producto/';$config['allowed_types'] = 'gif|jpg|png|jpeg';$config['max_size'] = 2048;$this->load->library('upload', $config); $this->upload->initialize($config);$post['portada'] = '';if ($this->upload->do_upload('portada')) {$data_portada = array('upload_data' => $this->upload->data());$post['portada'] = $data_portada['upload_data']['file_name'];}$config['upload_path'] = './imagenes/producto/oferta/';$config['allowed_types'] = 'gif|jpg|png|jpeg';$config['max_size'] = 2048;$this->load->library('upload', $config); $this->upload->initialize($config);$post['imgOferta'] = '';if ($this->upload->do_upload('imgOferta')) {$data_imgOferta = array('upload_data' => $this->upload->data());$post['imgOferta'] = $data_imgOferta['upload_data']['file_name'];}
*/

				$config['upload_path'] = './imagenes/producto/portada/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 2048;
        $this->load->library('upload', $config); 
        $this->upload->initialize($config);
        $post['portadaMarcado'] = '';
        if ($this->upload->do_upload('portadaMarcado')) {
          $data_portadaMarcado = array('upload_data' => $this->upload->data());
          $post['portadaMarcado'] = $data_portadaMarcado['upload_data']['file_name'];
        }
        
        $config['upload_path'] = './imagenes/producto/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 2048;
        $this->load->library('upload', $config); 
        $this->upload->initialize($config);
        $post['portada'] = '';
        if ($this->upload->do_upload('portada')) {
          $data_portada = array('upload_data' => $this->upload->data());
          $post['portada'] = $data_portada['upload_data']['file_name'];
        }
        
        $config['upload_path'] = './imagenes/producto/oferta/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 2048;
        $this->load->library('upload', $config); 
        $this->upload->initialize($config);
        $post['imgOferta'] = '';
        if ($this->upload->do_upload('imgOferta')) {
          $data_imgOferta = array('upload_data' => $this->upload->data());
          $post['imgOferta'] = $data_imgOferta['upload_data']['file_name'];
        }
                
        
  /*      
				$datos = array(
          'categoria' => $post['categoria'],
          'subcategoria' => $post['subcategoria'],
          'nombre' => $post['nombre'],
          'ruta' => $post['ruta'],
          'sku' => $post['sku'],
          'estado' => $post['estado'],
          'titulo' => $post['titulo'],
          'descripcionCorta' => $post['descripcionCorta'],
          'descripcion' => $post['descripcion'],
          'palabrasClaves' => $post['palabrasClaves'],
          'portadaMarcado' => $post['portadaMarcado'],
          'inventario' => $post['inventario'],
          'precio' => $post['precio'],
          'portada' => $post['portada'],
          'vistas' => $post['vistas'],
          'ventas' => $post['ventas'],
          'ofertadoPorCategoria' => $post['ofertadoPorCategoria'],
          'ofertadoPorSubCategoria' => $post['ofertadoPorSubCategoria'],
          'oferta' => $post['oferta'],
          'precioOferta' => $post['precioOferta'],
          'descuentoOferta' => $post['descuentoOferta'],
          'imgOferta' => $post['imgOferta'],
          'finOferta' => $post['finOferta'],
          'peso' => $post['peso'],);
*/

$datos['categoria'] = $post['categoria'];
$datos['subcategoria'] = $post['subcategoria'];
$datos['nombre'] = $post['nombre'];
$datos['ruta'] = $post['ruta'];
$datos['sku'] = $post['sku'];
$datos['estado'] = $post['estado'];
$datos['titulo'] = $post['titulo'];
$datos['descripcionCorta'] = $post['descripcionCorta'];
$datos['descripcion'] = $post['descripcion'];
$datos['palabrasClaves'] = $post['palabrasClaves'];
        
if ($post['portadaMarcado'] != '') {
  $datos['portadaMarcado'] = $post['portadaMarcado'];
}        
        
//$datos['inventario'] = $post['inventario'];
$datos['precio'] = $post['precio'];
        
        
if ($post['portada'] != '') {
  $datos['portada'] = $post['portada'];
}
        
$datos['vistas'] = $post['vistas'];
$datos['ventas'] = $post['ventas'];
$datos['ofertadoPorCategoria'] = $post['ofertadoPorCategoria'];
$datos['ofertadoPorSubCategoria'] = $post['ofertadoPorSubCategoria'];
$datos['oferta'] = $post['oferta'];
$datos['precioOferta'] = $post['precioOferta'];
$datos['descuentoOferta'] = $post['descuentoOferta'];

if ($post['imgOferta'] != '') {
  $datos['imgOferta'] = $post['imgOferta'];
}        

$datos['finOferta'] = $post['finOferta'];
$datos['peso'] = $post['peso'];        
        
				if ($this->Modelo->actualizar($this->config->item('raiz_bd') . 'productos', $datos, $post['id'])) {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-check"></i> '.$this->lang->line('be_successful_operation').'</h4>El registro se guardo exitosamente.'); 
					$this->session->set_flashdata('alertaTipo', 'success'); // success, info, warning, danger
				} else {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No fue posible guardar el registro.<br>'.$this->lang->line('be_please_try_again'));
					$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
				}     

			}
		} else {
			$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No se recibieron datos.<br>'.$this->lang->line('be_please_try_again'));
			$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
		}
		redirect('/admin/administradores021/productos');   
	}

	public function eliminar () {
		$post = $this->input->post(NULL, TRUE);
		if (!empty($post)) {
			foreach ($post as $key => $value) {
				$post[$key] = $this->security->xss_clean($value);
			}
			$this->form_validation->set_rules('id', 'ID', 'required|trim|xss_clean');
			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.validation_errors().'<br><b>'.$this->lang->line('be_please_try_again').'</b>');
				$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
			} else {

				$mat_id = explode(";", $post['id']); 
				$proceso = false;          
				foreach ($mat_id as $id) {
					$this->Modelo->eliminar($this->config->item('raiz_bd') . 'productos', $id);
          
					$this->Modelo->eliminar($this->config->item('raiz_bd') . 'productos_inventarios', '', '', array('producto'=>$id));
					$this->Modelo->eliminar($this->config->item('raiz_bd') . 'productos_especificaciones', '', '', array('producto'=>$id));
					$this->Modelo->eliminar($this->config->item('raiz_bd') . 'productos_multimedia', '', '', array('producto'=>$id));
          
					$proceso = true;
				}

				if ($proceso) {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-check"></i> '.$this->lang->line('be_successful_operation').'</h4>El(los) registro(s) se eliminó(aron) exitosamente.'); 
					$this->session->set_flashdata('alertaTipo', 'success'); // success, info, warning, danger
				} else {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No fue posible eliminar el(los) registro(s).<br>'.$this->lang->line('be_please_try_again'));
					$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
				}     

			}
		} else {
			$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No se recibieron datos.<br>'.$this->lang->line('be_please_try_again'));
			$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
		}
		redirect('/admin/administradores021/productos');   
	}

  
  
	public function lista () {

		$registros = $this->Modelo->registros($this->config->item('raiz_bd') . 'productos');
    
  	$datosJson = '
  		{	
  			"data":[';  
    $datosJsonReg = '';
    foreach ($registros as $key => $registro) {

          $productos_multimedia = $this->Modelo->registros($this->config->item('raiz_bd') . 'productos_multimedia', '', array('producto'=>$registro['id']) );
          $num_productos_multimedia = count($productos_multimedia);      
      
          $productos_especificaciones = $this->Modelo->registros($this->config->item('raiz_bd') . 'productos_especificaciones', '', array('producto'=>$registro['id']) );
          $num_productos_especificaciones = count($productos_especificaciones);      

          $productos_inventarios = $this->Modelo->registros($this->config->item('raiz_bd') . 'productos_inventarios', '', array('producto'=>$registro['id']) );
          $num_productos_inventarios = count($productos_inventarios);      
      
          $categoria = $this->Modelo->registro($this->config->item('raiz_bd') . 'categorias', $registro['categoria']);
          if ($categoria) {
             $registro['categoria'] = $categoria->nombre;
          }      
          $categoria = $this->Modelo->registro($this->config->item('raiz_bd') . 'subcategorias', $registro['subcategoria']);
          if ($categoria) {
             $registro['subcategoria'] = $categoria->nombre;
          }      
          $estado = $this->Modelo->registro($this->config->item('raiz_bd') . 'datos_valores', $registro['estado']);
          if ($estado) {
             $registro['estado'] = $estado->nombre;
          }
          $ofertadoPorCategoria = $this->Modelo->registro($this->config->item('raiz_bd') . 'datos_valores', $registro['ofertadoPorCategoria']);
          if ($ofertadoPorCategoria) {
             $registro['ofertadoPorCategoria'] = $ofertadoPorCategoria->nombre;
          } 
          $ofertadoPorSubCategoria = $this->Modelo->registro($this->config->item('raiz_bd') . 'datos_valores', $registro['ofertadoPorSubCategoria']);
          if ($ofertadoPorSubCategoria) {
             $registro['ofertadoPorSubCategoria'] = $ofertadoPorSubCategoria->nombre;
          } 
          $oferta = $this->Modelo->registro($this->config->item('raiz_bd') . 'datos_valores', $registro['oferta']);
          if ($oferta) {
             $registro['oferta'] = $oferta->nombre;
          }         
      
          if ($registro['finOferta'] == '0000-00-00') $registro['finOferta'] = '';
      
          $registro['precio'] = number_format($registro['precio'],0);

          foreach ($registro as $r_key => $r_value) {
              $registro[$r_key] = htmlentities($r_value);
          }      
      
          $datosJsonReg .='["<input type=\"checkbox\" id=\"fila_'.$registro['id'].'\" class=\"seleccion\" cod=\"'.$registro['id'].'\">", "<button type=\"button\" class=\"btn btn-default btn-xs text-light-blue btnEditar\" cod=\"'.$registro['id'].'\"><span class=\"glyphicon glyphicon-pencil\"></span></button>", "<button type=\"button\" class=\"btn btn-default btn-xs text-red btnEliminar\" cod=\"'.$registro['id'].'\"><span class=\"glyphicon glyphicon-trash\"></span></button>",
          "<a href=\"'.base_url().'admin/administradores021/productos_multimedia/'.$registro['id'].'\" class=\"btn btn-default btn-xs text-light-blue\">Multimedia ('.$num_productos_multimedia.')</a>",
          "<a href=\"'.base_url().'admin/administradores021/productos_especificaciones/'.$registro['id'].'\" class=\"btn btn-default btn-xs text-light-blue\">Especificaciones ('.$num_productos_especificaciones.')</a>",
          "<a href=\"'.base_url().'admin/administradores021/productos_inventarios/'.$registro['id'].'\" class=\"btn btn-default btn-xs text-light-blue\">Inventario ('.$num_productos_inventarios.')</a>",
          "'.$registro['sku'].'","'.$registro['categoria'].'","'.$registro['subcategoria'].'","'.$registro['nombre'].'","'.$registro['ruta'].'","'.$registro['estado'].'","'.$registro['titulo'].'","'.$registro['palabrasClaves'].'","'.$registro['portadaMarcado'].'","'.$registro['inventario'].'","'.$registro['precio'].'","'.$registro['portada'].'","'.$registro['vistas'].'","'.$registro['ventas'].'","'.$registro['ofertadoPorCategoria'].'","'.$registro['ofertadoPorSubCategoria'].'","'.$registro['oferta'].'","'.$registro['precioOferta'].'","'.$registro['descuentoOferta'].'","'.$registro['imgOferta'].'","'.$registro['finOferta'].'","'.$registro['peso'].'","'.$registro['fecha'].'"],';

// ,"'.substr($registro['descripcionCorta'],0,30).'...","'.substr($registro['descripcion'],0,30).'..."      
      
    }
    if ($datosJsonReg != '') {
          $datosJson .= substr($datosJsonReg, 0, -1);
    }
		
		$datosJson .= ']
		}';

		echo $datosJson;
    
	}
      
  
	public function lista_ssp () {
		$this->load->library('SSP');

		// DB table to use
		$table = 'productos';

		// Table's primary key
		$primaryKey = 'id';

		// Array of database columns
		$columns = array(array( 'db' => 'id', 'dt' => 0, 'field' => 'id', 'formatter' => function($d, $row) {return '<input type="checkbox" id="fila_' . $d . '" class="seleccion" cod="' . $d . '">';}),array( 'db' => 'id', 'dt' => 1, 'field' => 'id', 'formatter' => function($d, $row) {return '<button type="button" class="btn btn-default btn-xs text-light-blue btnEditar" cod="' . $d . '"><span class="glyphicon glyphicon-pencil"></span></button>';}), array( 'db' => 'id', 'dt' => 2, 'field' => 'id', 'formatter' => function($d, $row) {return '<button type="button" class="btn btn-default btn-xs text-red btnEliminar" cod="' . $d . '"><span class="glyphicon glyphicon-trash"></span></button>';}),array( 'db' => 'id_categoria', 'dt' => 3, 'field' => 'id_categoria' ),array( 'db' => 'id_subcategoria', 'dt' => 4, 'field' => 'id_subcategoria' ),array( 'db' => 'nombre', 'dt' => 5, 'field' => 'nombre' ),array( 'db' => 'ruta', 'dt' => 6, 'field' => 'ruta' ),array( 'db' => 'sku', 'dt' => 7, 'field' => 'sku' ),array( 'db' => 'estado', 'dt' => 8, 'field' => 'estado' ),array( 'db' => 'titulo', 'dt' => 9, 'field' => 'titulo' ),array( 'db' => 'descripcionCorta', 'dt' => 10, 'field' => 'descripcionCorta' ),array( 'db' => 'descripcion', 'dt' => 11, 'field' => 'descripcion' ),array( 'db' => 'palabrasClaves', 'dt' => 12, 'field' => 'palabrasClaves' ),array( 'db' => 'portadaMarcado', 'dt' => 13, 'field' => 'portadaMarcado' ),array( 'db' => 'inventario', 'dt' => 14, 'field' => 'inventario' ),array( 'db' => 'precio', 'dt' => 15, 'field' => 'precio' ),array( 'db' => 'portada', 'dt' => 16, 'field' => 'portada' ),array( 'db' => 'vistas', 'dt' => 17, 'field' => 'vistas' ),array( 'db' => 'ventas', 'dt' => 18, 'field' => 'ventas' ),array( 'db' => 'ofertadoPorCategoria', 'dt' => 19, 'field' => 'ofertadoPorCategoria' ),array( 'db' => 'ofertadoPorSubCategoria', 'dt' => 20, 'field' => 'ofertadoPorSubCategoria' ),array( 'db' => 'oferta', 'dt' => 21, 'field' => 'oferta' ),array( 'db' => 'precioOferta', 'dt' => 22, 'field' => 'precioOferta' ),array( 'db' => 'descuentoOferta', 'dt' => 23, 'field' => 'descuentoOferta' ),array( 'db' => 'imgOferta', 'dt' => 24, 'field' => 'imgOferta' ),array( 'db' => 'finOferta', 'dt' => 25, 'field' => 'finOferta' ),array( 'db' => 'peso', 'dt' => 26, 'field' => 'peso' ),array( 'db' => 'fecha', 'dt' => 27, 'field' => 'fecha' ),);

		$sql_details = array(
			'user' => $this->db->username,
			'pass' => $this->db->password,
			'db'   => $this->db->database,
			'host' => $this->db->hostname
		);

		$joinQuery = "FROM productos";
		$extraWhere = ""; //"`u`.`valor` >= 90000";
		$groupBy = ""; //"`u`.`datos`";
		$having = ""; //"`u`.`valor` >= 140000";

		$_GET['tksec'] = $this->security->get_csrf_hash();  

		echo json_encode(
			SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having )
		);
	}

  public function exportar_csv() {

        $mat = array();
        $tabla_productos = $this->Modelo->registros($this->config->item('raiz_bd') . 'productos', '', array() );
        if (count($tabla_productos)) {
            foreach ($tabla_productos as $reg) {
              $mat[] = $reg['id'] . ';' . 
              $reg['categoria'] . ';' . 
              $reg['subcategoria'] . ';' . 
              $reg['nombre'] . ';' . 
              $reg['ruta'] . ';' . 
              $reg['sku'] . ';' . 
              $reg['ean'] . ';' . 
              $reg['estado'] . ';' . 
              $reg['titulo'] . ';' . 
              $reg['palabrasClaves'] . ';' . 
              $reg['portadaMarcado'] . ';' . 
              $reg['inventario'] . ';' . 
              $reg['precio'] . ';' . 
              $reg['portada'] . ';' . 
              $reg['vistas'] . ';' . 
              $reg['ventas'] . ';' . 
              $reg['ofertadoPorCategoria'] . ';' . 
              $reg['ofertadoPorSubCategoria'] . ';' . 
              $reg['oferta'] . ';' . 
              $reg['precioOferta'] . ';' . 
              $reg['descuentoOferta'] . ';' . 
              $reg['imgOferta'] . ';' . 
              $reg['finOferta'] . ';' . 
              $reg['peso'] . ';' . 
              $reg['fecha'];
            }
        }

        if (count($mat)) {
          $this->utilities->cambiar_directorio('obranaranja');
          $this->utilities->cambiar_directorio('archivos');
          $this->utilities->crear_archivo ('', 'productos_'.$this->config->item('YmdHis').'.csv', $mat);
          //$this->utilities->leer_directorio('.');
          $this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-check"></i> '.$this->lang->line('be_successful_operation').'</h4>El(los) registro(s) se exporto(aron) exitosamente.'); 
          $this->session->set_flashdata('alertaTipo', 'success'); // success, info, warning, danger
        } else {
          $this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No fue posible exportar el(los) registro(s).<br>'.$this->lang->line('be_please_try_again'));
          $this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
        }

        redirect('/admin/administradores021/productos');

  }


}