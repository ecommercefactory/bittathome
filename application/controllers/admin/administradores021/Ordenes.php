<?php
defined("BASEPATH") OR exit("No esta permitido el acceso directo a este script.");

class Ordenes extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->ctrSegAdmin();
	}

	public function index() {
		$this->parametros['plantilla'] = 'datatables';
		$this->parametros['vista'] = 'admin/administradores021/ordenes';
		$this->parametros['datos']['titulo'] = 'Ordenes';
		$this->parametros['datos']['subtitulo'] = ''; 
//    $tabla_ordenes = $this->Modelo->registros($this->config->item('raiz_bd') . 'ordenes', '', array() );
//		$this->parametros['datos']['lista'] = $tabla_ordenes;
    $tabla_clientes = $this->Modelo->registros($this->config->item('raiz_bd') . 'usuarios', 'id, nombre', array('activo'=>4), 'nombre ASC' ); 
    $this->parametros['datos']['tabla_clientes'] = $tabla_clientes;

    
    $tabla_paises = $this->Modelo->registros($this->config->item('raiz_bd') . 'paises', 'id, nombre_es', array('activo'=>4), 'nombre_es ASC' ); 
    $this->parametros['datos']['tabla_paises'] = $tabla_paises;

//    $tabla_paises = $this->Modelo->registros($this->config->item('raiz_bd') . 'paises', 'id, nombre_es', array(), 'nombre_es ASC' ); 
//    $this->parametros['datos']['tabla_paises'] = $tabla_paises;

    $tabla_datos_valores_estado = $this->Modelo->registros($this->config->item('raiz_bd') . 'datos_valores', 'id, nombre', array('dato'=>16), 'orden ASC' ); 
    $this->parametros['datos']['tabla_datos_valores_estado'] = $tabla_datos_valores_estado;

    $tabla_administradores = $this->Modelo->registros($this->config->item('raiz_bd') . 'administradores', 'id, CONCAT(nombre, " ", apellidos) AS nombre', array(), 'nombre ASC' ); 
    $this->parametros['datos']['tabla_administradores'] = $tabla_administradores;

    $tabla_monedas = $this->Modelo->registros($this->config->item('raiz_bd') . 'monedas', 'id, nombre', array('mostrar'=>4), 'nombre' ); 
    $this->parametros['datos']['tabla_monedas'] = $tabla_monedas;
    
    
    
		$this->load->view('plantilla_admin', $this->parametros);			
	}

	public function traerRegistro () {
		$post = $this->input->post(NULL, TRUE);
		$registro = $this->Modelo->registro($this->config->item('raiz_bd') . 'ordenes', $post['id']);
    if ($registro->entrega_fecha == '0000-00-00') $registro->entrega_fecha = '';
		$datos = array('registro'=>$registro, 'tksec'=>$this->security->get_csrf_hash());
		echo json_encode($datos);      
	}

	public function ingresar () {
		$post = $this->input->post(NULL, TRUE);
		if (!empty($post)) {
			foreach ($post as $key => $value) {
				$post[$key] = $this->security->xss_clean($value);
			}
      
			$this->form_validation->set_rules('apellidos', 'Apellidos', 'required|xss_clean');$this->form_validation->set_rules('nombre', 'Nombre', 'required|xss_clean');$this->form_validation->set_rules('correo', 'Correo', 'required|valid_email|xss_clean');$this->form_validation->set_rules('pais', 'Pais', 'required|xss_clean');$this->form_validation->set_rules('empresa', 'Empresa', 'xss_clean');$this->form_validation->set_rules('cargo', 'Cargo', 'xss_clean');$this->form_validation->set_rules('direccion1', 'Direccion 1', 'xss_clean');$this->form_validation->set_rules('direccion2', 'Direccion 2', 'xss_clean');$this->form_validation->set_rules('departamento', 'Departamento', 'xss_clean');$this->form_validation->set_rules('ciudad', 'Ciudad', 'xss_clean');$this->form_validation->set_rules('zip', 'Zip', 'xss_clean');$this->form_validation->set_rules('telefono', 'Telefono', 'xss_clean');$this->form_validation->set_rules('envio_apellidos', 'Envio Apellidos', 'xss_clean');$this->form_validation->set_rules('envio_nombre', 'Envio Nombre', 'xss_clean');$this->form_validation->set_rules('envio_correo', 'Envio Correo', 'valid_email|xss_clean');$this->form_validation->set_rules('envio_pais', 'Envio Pais', 'xss_clean');$this->form_validation->set_rules('envio_empresa', 'Envio Empresa', 'xss_clean');$this->form_validation->set_rules('envio_cargo', 'Envio Cargo', 'xss_clean');$this->form_validation->set_rules('envio_direccion1', 'Envio Direccion 1', 'xss_clean');$this->form_validation->set_rules('envio_direccion2', 'Envio Direccion 2', 'xss_clean');$this->form_validation->set_rules('envio_departamento', 'Envio Departamento', 'xss_clean');$this->form_validation->set_rules('envio_ciudad', 'Envio Ciudad', 'xss_clean');$this->form_validation->set_rules('envio_zip', 'Envio Zip', 'xss_clean');$this->form_validation->set_rules('envio_telefono1', 'Envio Telefono 1', 'xss_clean');$this->form_validation->set_rules('envio_telefono2', 'Envio Telefono 2', 'xss_clean');$this->form_validation->set_rules('ip', 'Ip', 'xss_clean');$this->form_validation->set_rules('estado', 'Estado', 'xss_clean');
      
      //$this->form_validation->set_rules('estado_fecha', 'Estado Fecha', 'xss_clean');
      
      $this->form_validation->set_rules('estado_usuario', 'Estado Usuario', 'xss_clean');$this->form_validation->set_rules('codigo_descuento', 'Codigo Descuento', 'xss_clean');$this->form_validation->set_rules('descuento', 'Descuento', 'xss_clean');$this->form_validation->set_rules('subtotal', 'Subtotal', 'xss_clean');$this->form_validation->set_rules('impuestos', 'Impuestos', 'xss_clean');$this->form_validation->set_rules('envio', 'Envio', 'xss_clean');$this->form_validation->set_rules('moneda', 'Moneda', 'required|xss_clean');$this->form_validation->set_rules('total', 'Total', 'xss_clean');$this->form_validation->set_rules('entrega_fecha', 'Entrega Fecha', 'xss_clean');$this->form_validation->set_rules('entrega_hora', 'Entrega Hora', 'xss_clean');
      
			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.validation_errors().'<br><b>'.$this->lang->line('be_please_try_again').'</b>');
				$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
			} else {

				
        
				$datos = array('cliente' => $post['cliente'],'apellidos' => $post['apellidos'],'nombre' => $post['nombre'],'correo' => $post['correo'],'pais' => $post['pais'],'empresa' => $post['empresa'],'cargo' => $post['cargo'],'direccion1' => $post['direccion1'],'direccion2' => $post['direccion2'],'departamento' => $post['departamento'],'ciudad' => $post['ciudad'],'zip' => $post['zip'],'telefono' => $post['telefono'],'envio_apellidos' => $post['envio_apellidos'],'envio_nombre' => $post['envio_nombre'],'envio_correo' => $post['envio_correo'],'envio_pais' => $post['envio_pais'],'envio_empresa' => $post['envio_empresa'],'envio_cargo' => $post['envio_cargo'],'envio_direccion1' => $post['envio_direccion1'],'envio_direccion2' => $post['envio_direccion2'],'envio_departamento' => $post['envio_departamento'],'envio_ciudad' => $post['envio_ciudad'],'envio_zip' => $post['envio_zip'],'envio_telefono1' => $post['envio_telefono1'],'envio_telefono2' => $post['envio_telefono2'],'ip' => $post['ip'],'estado' => $post['estado'],'estado_fecha' => $this->config->item('YmdHis'),'estado_usuario' => $post['estado_usuario'],'codigo_descuento' => $post['codigo_descuento'],'descuento' => $post['descuento'],'subtotal' => $post['subtotal'],'impuestos' => $post['impuestos'],'envio' => $post['envio'],'moneda' => $post['moneda'],'total' => $post['total'],'entrega_fecha' => $post['entrega_fecha'],'entrega_hora' => $post['entrega_hora'],);

				$id = $this->Modelo->insertar($this->config->item('raiz_bd') . 'ordenes', $datos);
				if ($id > 0) {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-check"></i> '.$this->lang->line('be_successful_operation').'</h4>El registro se ingresó exitosamente.'); 
					$this->session->set_flashdata('alertaTipo', 'success'); // success, info, warning, danger
				} else {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No fue posible ingresar el registro.<br>'.$this->lang->line('be_please_try_again'));
					$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
				}     
			}
		} else {
			$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No se recibieron datos.<br>'.$this->lang->line('be_please_try_again'));
			$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
		}
		redirect('/admin/administradores021/ordenes');
	}

	public function guardar () {
		$post = $this->input->post(NULL, TRUE);
		if (!empty($post)) {
			foreach ($post as $key => $value) {
				$post[$key] = $this->security->xss_clean($value);
			}
      
			$this->form_validation->set_rules('apellidos', 'Apellidos', 'required|xss_clean');$this->form_validation->set_rules('nombre', 'Nombre', 'required|xss_clean');$this->form_validation->set_rules('correo', 'Correo', 'required|valid_email|xss_clean');$this->form_validation->set_rules('pais', 'Pais', 'required|xss_clean');$this->form_validation->set_rules('empresa', 'Empresa', 'xss_clean');$this->form_validation->set_rules('cargo', 'Cargo', 'xss_clean');$this->form_validation->set_rules('direccion1', 'Direccion 1', 'xss_clean');$this->form_validation->set_rules('direccion2', 'Direccion 2', 'xss_clean');$this->form_validation->set_rules('departamento', 'Departamento', 'xss_clean');$this->form_validation->set_rules('ciudad', 'Ciudad', 'xss_clean');$this->form_validation->set_rules('zip', 'Zip', 'xss_clean');$this->form_validation->set_rules('telefono', 'Telefono', 'xss_clean');$this->form_validation->set_rules('envio_apellidos', 'Envio Apellidos', 'xss_clean');$this->form_validation->set_rules('envio_nombre', 'Envio Nombre', 'xss_clean');$this->form_validation->set_rules('envio_correo', 'Envio Correo', 'valid_email|xss_clean');$this->form_validation->set_rules('envio_pais', 'Envio Pais', 'xss_clean');$this->form_validation->set_rules('envio_empresa', 'Envio Empresa', 'xss_clean');$this->form_validation->set_rules('envio_cargo', 'Envio Cargo', 'xss_clean');$this->form_validation->set_rules('envio_direccion1', 'Envio Direccion 1', 'xss_clean');$this->form_validation->set_rules('envio_direccion2', 'Envio Direccion 2', 'xss_clean');$this->form_validation->set_rules('envio_departamento', 'Envio Departamento', 'xss_clean');$this->form_validation->set_rules('envio_ciudad', 'Envio Ciudad', 'xss_clean');$this->form_validation->set_rules('envio_zip', 'Envio Zip', 'xss_clean');$this->form_validation->set_rules('envio_telefono1', 'Envio Telefono 1', 'xss_clean');$this->form_validation->set_rules('envio_telefono2', 'Envio Telefono 2', 'xss_clean');$this->form_validation->set_rules('ip', 'Ip', 'xss_clean');$this->form_validation->set_rules('estado', 'Estado', 'xss_clean');

      //$this->form_validation->set_rules('estado_fecha', 'Estado Fecha', 'xss_clean');
      
      $this->form_validation->set_rules('estado_usuario', 'Estado Usuario', 'xss_clean');$this->form_validation->set_rules('codigo_descuento', 'Codigo Descuento', 'xss_clean');$this->form_validation->set_rules('descuento', 'Descuento', 'xss_clean');$this->form_validation->set_rules('subtotal', 'Subtotal', 'xss_clean');$this->form_validation->set_rules('impuestos', 'Impuestos', 'xss_clean');$this->form_validation->set_rules('envio', 'Envio', 'xss_clean');$this->form_validation->set_rules('moneda', 'Moneda', 'required|xss_clean');$this->form_validation->set_rules('total', 'Total', 'xss_clean');$this->form_validation->set_rules('entrega_fecha', 'Entrega Fecha', 'xss_clean');$this->form_validation->set_rules('entrega_hora', 'Entrega Hora', 'xss_clean');
      
			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.validation_errors().'<br><b>'.$this->lang->line('be_please_try_again').'</b>');
				$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
			} else {

//                        'estado_fecha' => $post['estado_fecha'],
				
        
				$datos = array('cliente' => $post['cliente'],'apellidos' => $post['apellidos'],'nombre' => $post['nombre'],'correo' => $post['correo'],'pais' => $post['pais'],'empresa' => $post['empresa'],'cargo' => $post['cargo'],'direccion1' => $post['direccion1'],'direccion2' => $post['direccion2'],'departamento' => $post['departamento'],'ciudad' => $post['ciudad'],'zip' => $post['zip'],'telefono' => $post['telefono'],'envio_apellidos' => $post['envio_apellidos'],'envio_nombre' => $post['envio_nombre'],'envio_correo' => $post['envio_correo'],'envio_pais' => $post['envio_pais'],'envio_empresa' => $post['envio_empresa'],'envio_cargo' => $post['envio_cargo'],'envio_direccion1' => $post['envio_direccion1'],'envio_direccion2' => $post['envio_direccion2'],'envio_departamento' => $post['envio_departamento'],'envio_ciudad' => $post['envio_ciudad'],'envio_zip' => $post['envio_zip'],'envio_telefono1' => $post['envio_telefono1'],'envio_telefono2' => $post['envio_telefono2'],'ip' => $post['ip'],'estado' => $post['estado'],
                       'estado_usuario' => $post['estado_usuario'],'codigo_descuento' => $post['codigo_descuento'],'descuento' => $post['descuento'],'subtotal' => $post['subtotal'],'impuestos' => $post['impuestos'],'envio' => $post['envio'],'moneda' => $post['moneda'],'total' => $post['total'],'entrega_fecha' => $post['entrega_fecha'],'entrega_hora' => $post['entrega_hora'],);

				if ($this->Modelo->actualizar($this->config->item('raiz_bd') . 'ordenes', $datos, $post['id'])) {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-check"></i> '.$this->lang->line('be_successful_operation').'</h4>El registro se guardo exitosamente.'); 
					$this->session->set_flashdata('alertaTipo', 'success'); // success, info, warning, danger
				} else {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No fue posible guardar el registro.<br>'.$this->lang->line('be_please_try_again'));
					$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
				}     

			}
		} else {
			$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No se recibieron datos.<br>'.$this->lang->line('be_please_try_again'));
			$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
		}
		redirect('/admin/administradores021/ordenes');   
	}

	public function eliminar () {
		$post = $this->input->post(NULL, TRUE);
		if (!empty($post)) {
			foreach ($post as $key => $value) {
				$post[$key] = $this->security->xss_clean($value);
			}
			$this->form_validation->set_rules('id', 'ID', 'required|trim|xss_clean');
			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.validation_errors().'<br><b>'.$this->lang->line('be_please_try_again').'</b>');
				$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
			} else {

				$mat_id = explode(";", $post['id']); 
				$proceso = false;          
				foreach ($mat_id as $id) {
					$this->Modelo->eliminar($this->config->item('raiz_bd') . 'ordenes', $id);
          
          $this->Modelo->eliminar($this->config->item('raiz_bd') . 'ordenes_productos', '', '', array('orden'=>$id));  
          
          
					$proceso = true;
				}

				if ($proceso) {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-check"></i> '.$this->lang->line('be_successful_operation').'</h4>El(los) registro(s) se eliminó(aron) exitosamente.'); 
					$this->session->set_flashdata('alertaTipo', 'success'); // success, info, warning, danger
				} else {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No fue posible eliminar el(los) registro(s).<br>'.$this->lang->line('be_please_try_again'));
					$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
				}     

			}
		} else {
			$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No se recibieron datos.<br>'.$this->lang->line('be_please_try_again'));
			$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
		}
		redirect('/admin/administradores021/ordenes');   
	}

  
  
	public function lista () {

		$registros = $this->Modelo->registros($this->config->item('raiz_bd') . 'ordenes');
    
  	$datosJson = '
  		{	
  			"data":[';  
    $datosJsonReg = '';
    foreach ($registros as $key => $registro) {

          $ordenes_productos = $this->Modelo->registros($this->config->item('raiz_bd') . 'ordenes_productos', '', array('orden'=>$registro['id']) );
          $num_ordenes_productos = count($ordenes_productos);        
      
          $cliente = $this->Modelo->registro($this->config->item('raiz_bd') . 'usuarios', $registro['cliente']);
          if ($cliente) {
             $registro['cliente'] = $cliente->nombre;
          } else {
              $registro['cliente'] = 'Sin Asignar';
          }
  
          $pais = $this->Modelo->registro($this->config->item('raiz_bd') . 'paises', $registro['pais']);
          if ($pais) {
             $registro['pais'] = $pais->nombre_es;
          }      
      
          $envio_pais = $this->Modelo->registro($this->config->item('raiz_bd') . 'paises', $registro['envio_pais']);
          if ($envio_pais) {
             $registro['envio_pais'] = $envio_pais->nombre_es;
          }    

          $estado = $this->Modelo->registro($this->config->item('raiz_bd') . 'datos_valores', $registro['estado']);
          if ($estado) {
             $registro['estado'] = $estado->nombre;
          } 
      
          $estado_usuario = $this->Modelo->registro($this->config->item('raiz_bd') . 'administradores', $registro['estado_usuario']);
          if ($estado_usuario) {
             $registro['estado_usuario'] = $estado_usuario->nombre . " " . $estado_usuario->apellidos;
          }     

          $moneda = $this->Modelo->registro($this->config->item('raiz_bd') . 'monedas', $registro['moneda']);
          if ($moneda) {
             $registro['moneda'] = $moneda->nombre;
          }       
      
          $datosJsonReg .='["<input type=\"checkbox\" id=\"fila_'.$registro['id'].'\" class=\"seleccion\" cod=\"'.$registro['id'].'\">", "<button type=\"button\" class=\"btn btn-default btn-xs text-light-blue btnEditar\" cod=\"'.$registro['id'].'\"><span class=\"glyphicon glyphicon-pencil\"></span></button>", "<button type=\"button\" class=\"btn btn-default btn-xs text-red btnEliminar\" cod=\"'.$registro['id'].'\"><span class=\"glyphicon glyphicon-trash\"></span></button>",
 
"<a href=\"'.base_url().'admin/administradores021/ordenes_productos/'.$registro['id'].'\" class=\"btn btn-default btn-xs text-light-blue\">Productos ('.$num_ordenes_productos.')</a>", 
          "'.$registro['cliente'].'","'.$registro['nombre'].'","'.$registro['apellidos'].'","'.$registro['correo'].'","'.$registro['pais'].'","'.$registro['empresa'].'","'.$registro['cargo'].'","'.$registro['direccion1'].'","'.$registro['direccion2'].'","'.$registro['departamento'].'","'.$registro['ciudad'].'","'.$registro['zip'].'","'.$registro['telefono'].'","'.$registro['envio_apellidos'].'","'.$registro['envio_nombre'].'","'.$registro['envio_correo'].'","'.$registro['envio_pais'].'","'.$registro['envio_empresa'].'","'.$registro['envio_cargo'].'","'.$registro['envio_direccion1'].'","'.$registro['envio_direccion2'].'","'.$registro['envio_departamento'].'","'.$registro['envio_ciudad'].'","'.$registro['envio_zip'].'","'.$registro['envio_telefono1'].'","'.$registro['envio_telefono2'].'","'.$registro['ip'].'","'.$registro['estado'].'","'.$registro['estado_fecha'].'","'.$registro['estado_usuario'].'","'.$registro['codigo_descuento'].'","'.$registro['descuento'].'","'.$registro['subtotal'].'","'.$registro['impuestos'].'","'.$registro['envio'].'","'.$registro['moneda'].'","'.$registro['total'].'","'.$registro['entrega_fecha'].'","'.$registro['entrega_hora'].'","'.$registro['fecha'].'"],';

    }
    if ($datosJsonReg != '') {
          $datosJson .= substr($datosJsonReg, 0, -1);
    }
		
		$datosJson .= ']
		}';

		echo $datosJson;
    
	}
      
  
	public function lista_ssp () {
		$this->load->library('SSP');

		// DB table to use
		$table = 'ordenes';

		// Table's primary key
		$primaryKey = 'id';

		// Array of database columns
		$columns = array(array( 'db' => 'id', 'dt' => 0, 'field' => 'id', 'formatter' => function($d, $row) {return '<input type="checkbox" id="fila_' . $d . '" class="seleccion" cod="' . $d . '">';}),array( 'db' => 'id', 'dt' => 1, 'field' => 'id', 'formatter' => function($d, $row) {return '<button type="button" class="btn btn-default btn-xs text-light-blue btnEditar" cod="' . $d . '"><span class="glyphicon glyphicon-pencil"></span></button>';}), array( 'db' => 'id', 'dt' => 2, 'field' => 'id', 'formatter' => function($d, $row) {return '<button type="button" class="btn btn-default btn-xs text-red btnEliminar" cod="' . $d . '"><span class="glyphicon glyphicon-trash"></span></button>';}),array( 'db' => 'apellidos', 'dt' => 3, 'field' => 'apellidos' ),array( 'db' => 'nombre', 'dt' => 4, 'field' => 'nombre' ),array( 'db' => 'correo', 'dt' => 5, 'field' => 'correo' ),array( 'db' => 'pais', 'dt' => 6, 'field' => 'pais' ),array( 'db' => 'empresa', 'dt' => 7, 'field' => 'empresa' ),array( 'db' => 'cargo', 'dt' => 8, 'field' => 'cargo' ),array( 'db' => 'direccion1', 'dt' => 9, 'field' => 'direccion1' ),array( 'db' => 'direccion2', 'dt' => 10, 'field' => 'direccion2' ),array( 'db' => 'departamento', 'dt' => 11, 'field' => 'departamento' ),array( 'db' => 'ciudad', 'dt' => 12, 'field' => 'ciudad' ),array( 'db' => 'zip', 'dt' => 13, 'field' => 'zip' ),array( 'db' => 'telefono', 'dt' => 14, 'field' => 'telefono' ),array( 'db' => 'envio_apellidos', 'dt' => 15, 'field' => 'envio_apellidos' ),array( 'db' => 'envio_nombre', 'dt' => 16, 'field' => 'envio_nombre' ),array( 'db' => 'envio_correo', 'dt' => 17, 'field' => 'envio_correo' ),array( 'db' => 'envio_pais', 'dt' => 18, 'field' => 'envio_pais' ),array( 'db' => 'envio_empresa', 'dt' => 19, 'field' => 'envio_empresa' ),array( 'db' => 'envio_cargo', 'dt' => 20, 'field' => 'envio_cargo' ),array( 'db' => 'envio_direccion1', 'dt' => 21, 'field' => 'envio_direccion1' ),array( 'db' => 'envio_direccion2', 'dt' => 22, 'field' => 'envio_direccion2' ),array( 'db' => 'envio_departamento', 'dt' => 23, 'field' => 'envio_departamento' ),array( 'db' => 'envio_ciudad', 'dt' => 24, 'field' => 'envio_ciudad' ),array( 'db' => 'envio_zip', 'dt' => 25, 'field' => 'envio_zip' ),array( 'db' => 'envio_telefono1', 'dt' => 26, 'field' => 'envio_telefono1' ),array( 'db' => 'envio_telefono2', 'dt' => 27, 'field' => 'envio_telefono2' ),array( 'db' => 'ip', 'dt' => 28, 'field' => 'ip' ),array( 'db' => 'estado', 'dt' => 29, 'field' => 'estado' ),array( 'db' => 'estado_fecha', 'dt' => 30, 'field' => 'estado_fecha' ),array( 'db' => 'estado_usuario', 'dt' => 31, 'field' => 'estado_usuario' ),array( 'db' => 'codigo_descuento', 'dt' => 32, 'field' => 'codigo_descuento' ),array( 'db' => 'descuento', 'dt' => 33, 'field' => 'descuento' ),array( 'db' => 'subtotal', 'dt' => 34, 'field' => 'subtotal' ),array( 'db' => 'impuestos', 'dt' => 35, 'field' => 'impuestos' ),array( 'db' => 'envio', 'dt' => 36, 'field' => 'envio' ),array( 'db' => 'moneda', 'dt' => 37, 'field' => 'moneda' ),array( 'db' => 'total', 'dt' => 38, 'field' => 'total' ),array( 'db' => 'entrega_fecha', 'dt' => 39, 'field' => 'entrega_fecha' ),array( 'db' => 'entrega_hora', 'dt' => 40, 'field' => 'entrega_hora' ),array( 'db' => 'fecha', 'dt' => 41, 'field' => 'fecha' ),);

		$sql_details = array(
			'user' => $this->db->username,
			'pass' => $this->db->password,
			'db'   => $this->db->database,
			'host' => $this->db->hostname
		);

		$joinQuery = "FROM ordenes";
		$extraWhere = ""; //"`u`.`valor` >= 90000";
		$groupBy = ""; //"`u`.`datos`";
		$having = ""; //"`u`.`valor` >= 140000";

		$_GET['tksec'] = $this->security->get_csrf_hash();  

		echo json_encode(
			SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having )
		);
	}

}