<?php
defined("BASEPATH") OR exit("No esta permitido el acceso directo a este script.");

class Subcategorias extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->ctrSegAdmin();
	}

	public function index() {
		$this->parametros['plantilla'] = 'datatables';
		$this->parametros['vista'] = 'admin/administradores021/subcategorias';
		$this->parametros['datos']['titulo'] = 'Subcategorias';
		$this->parametros['datos']['subtitulo'] = ''; 
    $tabla_subcategorias = $this->Modelo->registros($this->config->item('raiz_bd') . 'subcategorias', '', array() );
		$this->parametros['datos']['lista'] = $tabla_subcategorias;
    
    $tabla_categorias = $this->Modelo->registros($this->config->item('raiz_bd') . 'categorias', 'id, nombre', array(), 'nombre ASC' ); 
    $this->parametros['datos']['tabla_categorias'] = $tabla_categorias;
    
    $tabla_datos_valores_estado = $this->Modelo->registros($this->config->item('raiz_bd') . 'datos_valores', 'id, nombre', array('dato' => 1), 'nombre ASC' ); 
    $this->parametros['datos']['tabla_datos_valores_estado'] = $tabla_datos_valores_estado;
    
    $tabla_datos_valores_ofertado_categoria = $this->Modelo->registros($this->config->item('raiz_bd') . 'datos_valores', 'id, nombre', array('dato' => 3), 'nombre ASC' ); 
    $this->parametros['datos']['tabla_datos_valores_ofertado_categoria'] = $tabla_datos_valores_ofertado_categoria;
    
    $tabla_datos_valores_oferta = $this->Modelo->registros($this->config->item('raiz_bd') . 'datos_valores', 'id, nombre', array('dato' => 3), 'nombre ASC' ); 
    $this->parametros['datos']['tabla_datos_valores_oferta'] = $tabla_datos_valores_oferta;
    
		$this->load->view('plantilla_admin', $this->parametros);			
	}

	public function traerRegistro () {
		$post = $this->input->post(NULL, TRUE);
		$registro = $this->Modelo->registro($this->config->item('raiz_bd') . 'subcategorias', $post['id']);
    if ($registro->finOferta == '0000-00-00') $registro->finOferta = '';
		$datos = array('registro'=>$registro, 'tksec'=>$this->security->get_csrf_hash());
		echo json_encode($datos);      
	}

  public function crearURLAmigable () {
    $post = $this->input->post(NULL, TRUE);
    $url = '';
    $texto = preg_replace('/[^a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]/', '', $post['nombre']);
    $encontrar = array('à','á','â','ã','ä', 'ç', 'è','é','ê','ë', 'ì','í','î','ï', 'ñ', 'ò','ó','ô','õ','ö', 'ù','ú','û','ü', 'ý','ÿ', 'À','Á','Â','Ã','Ä', 'Ç', 'È','É','Ê','Ë', 'Ì','Í','Î','Ï', 'Ñ', 'Ò','Ó','Ô','Õ','Ö', 'Ù','Ú','Û','Ü', 'Ý', '-', ' ');
    $reemplazar = array('a','a','a','a','a', 'c', 'e','e','e','e', 'i','i','i','i', 'n', 'o','o','o','o','o', 'u','u','u','u', 'y','y', 'A','A','A','A','A', 'C', 'E','E','E','E', 'I','I','I','I', 'N', 'O','O','O','O','O', 'U','U','U','U', 'Y', '_', '-');
    $url = urlencode(strtolower(str_replace($encontrar, $reemplazar, $texto)));
    $datos = array('url'=>$url, 'tksec'=>$this->security->get_csrf_hash());
    echo json_encode($datos);      
  }  
  
	public function ingresar () {
		$post = $this->input->post(NULL, TRUE);
		if (!empty($post)) {
			foreach ($post as $key => $value) {
				$post[$key] = $this->security->xss_clean($value);
			}
      
			$this->form_validation->set_rules('nombre', 'Nombre', 'xss_clean');$this->form_validation->set_rules('categoria', 'Categoria', 'required|xss_clean');$this->form_validation->set_rules('ruta', 'Ruta', 'required|max_length[250]|xss_clean');$this->form_validation->set_rules('titulo', 'Titulo', 'required|max_length[250]|xss_clean');$this->form_validation->set_rules('descripcionCorta', 'Descripcion Corta', 'xss_clean');$this->form_validation->set_rules('descripcion', 'Descripcion', 'xss_clean');$this->form_validation->set_rules('palabrasClaves', 'Palabras Claves', 'max_length[250]|xss_clean');$this->form_validation->set_rules('portadaMarcado', 'Portada Marcado', 'xss_clean');$this->form_validation->set_rules('estado', 'Estado', 'required|xss_clean');$this->form_validation->set_rules('ofertadoPorCategoria', 'Ofertado por Categoria', 'xss_clean');$this->form_validation->set_rules('oferta', 'Oferta', 'required|xss_clean');$this->form_validation->set_rules('precioOferta', 'Precio Oferta', 'numeric|xss_clean');$this->form_validation->set_rules('descuentoOferta', 'Descuento Oferta', 'less_than[100]|numeric|xss_clean');$this->form_validation->set_rules('imgOferta', 'Imagen Oferta', 'xss_clean');$this->form_validation->set_rules('finOferta', 'Fin Oferta', 'xss_clean');
      
			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.validation_errors().'<br><b>'.$this->lang->line('be_please_try_again').'</b>');
				$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
			} else {

				$config['upload_path'] = './imagenes/subcategoria/portada/';$config['allowed_types'] = 'gif|jpg|png|jpeg';$config['max_size'] = 2048;$this->load->library('upload', $config); $this->upload->initialize($config);$post['portadaMarcado'] = '';if ($this->upload->do_upload('portadaMarcado')) {$data_portadaMarcado = array('upload_data' => $this->upload->data());$post['portadaMarcado'] = $data_portadaMarcado['upload_data']['file_name'];}$config['upload_path'] = './imagenes/subcategoria/oferta/';$config['allowed_types'] = 'gif|jpg|png|jpeg';$config['max_size'] = 2048;$this->load->library('upload', $config); $this->upload->initialize($config);$post['imgOferta'] = '';if ($this->upload->do_upload('imgOferta')) {$data_imgOferta = array('upload_data' => $this->upload->data());$post['imgOferta'] = $data_imgOferta['upload_data']['file_name'];}
        
				$datos = array('nombre' => $post['nombre'],'categoria' => $post['categoria'],'ruta' => $post['ruta'],'titulo' => $post['titulo'],'descripcionCorta' => $post['descripcionCorta'],'descripcion' => $post['descripcion'],'palabrasClaves' => $post['palabrasClaves'],'portadaMarcado' => $post['portadaMarcado'],'estado' => $post['estado'],'ofertadoPorCategoria' => $post['ofertadoPorCategoria'],'oferta' => $post['oferta'],'precioOferta' => $post['precioOferta'],'descuentoOferta' => $post['descuentoOferta'],'imgOferta' => $post['imgOferta'],'finOferta' => $post['finOferta'],);

				$id = $this->Modelo->insertar($this->config->item('raiz_bd') . 'subcategorias', $datos);
				if ($id > 0) {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-check"></i> '.$this->lang->line('be_successful_operation').'</h4>El registro se ingresó exitosamente.'); 
					$this->session->set_flashdata('alertaTipo', 'success'); // success, info, warning, danger
				} else {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No fue posible ingresar el registro.<br>'.$this->lang->line('be_please_try_again'));
					$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
				}     
			}
		} else {
			$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No se recibieron datos.<br>'.$this->lang->line('be_please_try_again'));
			$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
		}
		redirect('/admin/administradores021/subcategorias');
	}

	public function guardar () {
		$post = $this->input->post(NULL, TRUE);
		if (!empty($post)) {
			foreach ($post as $key => $value) {
				$post[$key] = $this->security->xss_clean($value);
			}
      
			$this->form_validation->set_rules('nombre', 'Nombre', 'xss_clean');$this->form_validation->set_rules('categoria', 'Categoria', 'required|xss_clean');$this->form_validation->set_rules('ruta', 'Ruta', 'required|max_length[250]|xss_clean');$this->form_validation->set_rules('titulo', 'Titulo', 'required|max_length[250]|xss_clean');$this->form_validation->set_rules('descripcionCorta', 'Descripcion Corta', 'xss_clean');$this->form_validation->set_rules('descripcion', 'Descripcion', 'xss_clean');$this->form_validation->set_rules('palabrasClaves', 'Palabras Claves', 'max_length[250]|xss_clean');$this->form_validation->set_rules('portadaMarcado', 'Portada Marcado', 'xss_clean');$this->form_validation->set_rules('estado', 'Estado', 'required|xss_clean');$this->form_validation->set_rules('ofertadoPorCategoria', 'Ofertado por Categoria', 'xss_clean');$this->form_validation->set_rules('oferta', 'Oferta', 'required|xss_clean');$this->form_validation->set_rules('precioOferta', 'Precio Oferta', 'numeric|xss_clean');$this->form_validation->set_rules('descuentoOferta', 'Descuento Oferta', 'less_than[100]|numeric|xss_clean');$this->form_validation->set_rules('imgOferta', 'Imagen Oferta', 'xss_clean');$this->form_validation->set_rules('finOferta', 'Fin Oferta', 'xss_clean');
      
			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.validation_errors().'<br><b>'.$this->lang->line('be_please_try_again').'</b>');
				$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
			} else {

				$config['upload_path'] = './imagenes/subcategoria/portada/';$config['allowed_types'] = 'gif|jpg|png|jpeg';$config['max_size'] = 2048;$this->load->library('upload', $config); $this->upload->initialize($config);$post['portadaMarcado'] = '';if ($this->upload->do_upload('portadaMarcado')) {$data_portadaMarcado = array('upload_data' => $this->upload->data());$post['portadaMarcado'] = $data_portadaMarcado['upload_data']['file_name'];}$config['upload_path'] = './imagenes/subcategoria/oferta/';$config['allowed_types'] = 'gif|jpg|png|jpeg';$config['max_size'] = 2048;$this->load->library('upload', $config); $this->upload->initialize($config);$post['imgOferta'] = '';if ($this->upload->do_upload('imgOferta')) {$data_imgOferta = array('upload_data' => $this->upload->data());$post['imgOferta'] = $data_imgOferta['upload_data']['file_name'];}

				$datos = array('nombre' => $post['nombre'],'categoria' => $post['categoria'],'ruta' => $post['ruta'],'titulo' => $post['titulo'],'descripcionCorta' => $post['descripcionCorta'],'descripcion' => $post['descripcion'],'palabrasClaves' => $post['palabrasClaves'],'portadaMarcado' => $post['portadaMarcado'],'estado' => $post['estado'],'ofertadoPorCategoria' => $post['ofertadoPorCategoria'],'oferta' => $post['oferta'],'precioOferta' => $post['precioOferta'],'descuentoOferta' => $post['descuentoOferta'],'finOferta' => $post['finOferta'],);
        if ($post['portadaMarcado'] != '') $datos['portadaMarcado'] = $post['portadaMarcado'];
        if ($post['imgOferta'] != '') $datos['imgOferta'] = $post['imgOferta'];

				if ($this->Modelo->actualizar($this->config->item('raiz_bd') . 'subcategorias', $datos, $post['id'])) {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-check"></i> '.$this->lang->line('be_successful_operation').'</h4>El registro se guardo exitosamente.'); 
					$this->session->set_flashdata('alertaTipo', 'success'); // success, info, warning, danger
				} else {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No fue posible guardar el registro.<br>'.$this->lang->line('be_please_try_again'));
					$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
				}     

			}
		} else {
			$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No se recibieron datos.<br>'.$this->lang->line('be_please_try_again'));
			$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
		}
		redirect('/admin/administradores021/subcategorias');   
	}

	public function eliminar () {
		$post = $this->input->post(NULL, TRUE);
		if (!empty($post)) {
			foreach ($post as $key => $value) {
				$post[$key] = $this->security->xss_clean($value);
			}
			$this->form_validation->set_rules('id', 'ID', 'required|trim|xss_clean');
			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.validation_errors().'<br><b>'.$this->lang->line('be_please_try_again').'</b>');
				$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
			} else {

				$mat_id = explode(";", $post['id']); 
				$proceso = false;          
				foreach ($mat_id as $id) {
					$this->Modelo->eliminar($this->config->item('raiz_bd') . 'subcategorias', $id);
					$proceso = true;
				}

				if ($proceso) {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-check"></i> '.$this->lang->line('be_successful_operation').'</h4>El(los) registro(s) se eliminó(aron) exitosamente.'); 
					$this->session->set_flashdata('alertaTipo', 'success'); // success, info, warning, danger
				} else {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No fue posible eliminar el(los) registro(s).<br>'.$this->lang->line('be_please_try_again'));
					$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
				}     

			}
		} else {
			$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No se recibieron datos.<br>'.$this->lang->line('be_please_try_again'));
			$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
		}
		redirect('/admin/administradores021/subcategorias');   
	}

  
  
	public function lista () {

		$registros = $this->Modelo->registros($this->config->item('raiz_bd') . 'subcategorias');
    
  	$datosJson = '
  		{	
  			"data":[';  
    $datosJsonReg = '';
    foreach ($registros as $key => $registro) {

      

          $categoria = $this->Modelo->registro($this->config->item('raiz_bd') . 'categorias', $registro['categoria']);
          if ($categoria) {
             $registro['categoria'] = $categoria->nombre;
          }      
          $estado = $this->Modelo->registro($this->config->item('raiz_bd') . 'datos_valores', $registro['estado']);
          if ($estado) {
             $registro['estado'] = $estado->nombre;
          }
          $ofertadoPorCategoria = $this->Modelo->registro($this->config->item('raiz_bd') . 'datos_valores', $registro['ofertadoPorCategoria']);
          if ($ofertadoPorCategoria) {
             $registro['ofertadoPorCategoria'] = $ofertadoPorCategoria->nombre;
          } 
          $oferta = $this->Modelo->registro($this->config->item('raiz_bd') . 'datos_valores', $registro['oferta']);
          if ($oferta) {
             $registro['oferta'] = $oferta->nombre;
          }    
      
          if ($registro['finOferta'] == '0000-00-00') $registro['finOferta'] = '';
            
      
          $datosJsonReg .='["<input type=\"checkbox\" id=\"fila_'.$registro['id'].'\" class=\"seleccion\" cod=\"'.$registro['id'].'\">", "<button type=\"button\" class=\"btn btn-default btn-xs text-light-blue btnEditar\" cod=\"'.$registro['id'].'\"><span class=\"glyphicon glyphicon-pencil\"></span></button>", "<button type=\"button\" class=\"btn btn-default btn-xs text-red btnEliminar\" cod=\"'.$registro['id'].'\"><span class=\"glyphicon glyphicon-trash\"></span></button>","'.$registro['nombre'].'","'.$registro['categoria'].'","'.$registro['ruta'].'","'.$registro['titulo'].'","'.$registro['descripcionCorta'].'","'.$registro['descripcion'].'","'.$registro['palabrasClaves'].'","'.$registro['portadaMarcado'].'","'.$registro['estado'].'","'.$registro['ofertadoPorCategoria'].'","'.$registro['oferta'].'","'.$registro['precioOferta'].'","'.$registro['descuentoOferta'].'","'.$registro['imgOferta'].'","'.$registro['finOferta'].'","'.$registro['fecha'].'"],';

    }
    if ($datosJsonReg != '') {
          $datosJson .= substr($datosJsonReg, 0, -1);
    }
		
		$datosJson .= ']
		}';

		echo $datosJson;
    
	}
      
  
	public function lista_ssp () {
		$this->load->library('SSP');

		// DB table to use
		$table = 'subcategorias';

		// Table's primary key
		$primaryKey = 'id';

		// Array of database columns
		$columns = array(array( 'db' => 'id', 'dt' => 0, 'field' => 'id', 'formatter' => function($d, $row) {return '<input type="checkbox" id="fila_' . $d . '" class="seleccion" cod="' . $d . '">';}),array( 'db' => 'id', 'dt' => 1, 'field' => 'id', 'formatter' => function($d, $row) {return '<button type="button" class="btn btn-default btn-xs text-light-blue btnEditar" cod="' . $d . '"><span class="glyphicon glyphicon-pencil"></span></button>';}), array( 'db' => 'id', 'dt' => 2, 'field' => 'id', 'formatter' => function($d, $row) {return '<button type="button" class="btn btn-default btn-xs text-red btnEliminar" cod="' . $d . '"><span class="glyphicon glyphicon-trash"></span></button>';}),array( 'db' => 'subcategoria', 'dt' => 3, 'field' => 'subcategoria' ),array( 'db' => 'id_categoria', 'dt' => 4, 'field' => 'id_categoria' ),array( 'db' => 'ruta', 'dt' => 5, 'field' => 'ruta' ),array( 'db' => 'titulo', 'dt' => 6, 'field' => 'titulo' ),array( 'db' => 'descripcionCorta', 'dt' => 7, 'field' => 'descripcionCorta' ),array( 'db' => 'descripcion', 'dt' => 8, 'field' => 'descripcion' ),array( 'db' => 'palabrasClaves', 'dt' => 9, 'field' => 'palabrasClaves' ),array( 'db' => 'portadaMarcado', 'dt' => 10, 'field' => 'portadaMarcado' ),array( 'db' => 'estado', 'dt' => 11, 'field' => 'estado' ),array( 'db' => 'ofertadoPorCategoria', 'dt' => 12, 'field' => 'ofertadoPorCategoria' ),array( 'db' => 'oferta', 'dt' => 13, 'field' => 'oferta' ),array( 'db' => 'precioOferta', 'dt' => 14, 'field' => 'precioOferta' ),array( 'db' => 'descuentoOferta', 'dt' => 15, 'field' => 'descuentoOferta' ),array( 'db' => 'imgOferta', 'dt' => 16, 'field' => 'imgOferta' ),array( 'db' => 'finOferta', 'dt' => 17, 'field' => 'finOferta' ),array( 'db' => 'fecha', 'dt' => 18, 'field' => 'fecha' ),);

		$sql_details = array(
			'user' => $this->db->username,
			'pass' => $this->db->password,
			'db'   => $this->db->database,
			'host' => $this->db->hostname
		);

		$joinQuery = "FROM subcategorias";
		$extraWhere = ""; //"`u`.`valor` >= 90000";
		$groupBy = ""; //"`u`.`datos`";
		$having = ""; //"`u`.`valor` >= 140000";

		$_GET['tksec'] = $this->security->get_csrf_hash();  

		echo json_encode(
			SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having )
		);
	}

}