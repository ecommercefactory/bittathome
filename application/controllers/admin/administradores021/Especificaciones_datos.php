<?php
defined("BASEPATH") OR exit("No esta permitido el acceso directo a este script.");

class Especificaciones_datos extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->ctrSegAdmin();
	}

    function _remap($param) {
        $this->index($param);
    }
  
    public function index($id) {  
    	if (!is_numeric($id)) {
    		redirect('/admin/administradores021/especificaciones');
    	}
   		$especificaciones = $this->Modelo->registro($this->config->item('raiz_bd') . 'especificaciones', $id);
      
      
		$this->parametros['plantilla'] = 'datatables';
		$this->parametros['vista'] = 'admin/administradores021/especificaciones_datos';
		$this->parametros['datos']['titulo'] = 'Datos';
		$this->parametros['datos']['subtitulo'] = 'de ' . $especificaciones->nombre;  
    $this->parametros['datos']['breadcrumb'] = '
        <ol class="breadcrumb">
            <li><a href="'.base_url().'admin/administradores021/especificaciones">Especificaciones</a></li>
            <li class="active">Datos</li>
        </ol>';		
		$this->parametros['datos']['especificacion'] = $id;       
      
    $tabla_especificaciones_datos = $this->Modelo->registros($this->config->item('raiz_bd') . 'especificaciones_datos', '', array() );
		$this->parametros['datos']['lista'] = $tabla_especificaciones_datos;
    
    $tabla_especificaciones = $this->Modelo->registros($this->config->item('raiz_bd') . 'especificaciones', 'id, nombre', array(), 'nombre ASC' ); $this->parametros['datos']['tabla_especificaciones'] = $tabla_especificaciones;
    
		$this->load->view('plantilla_admin', $this->parametros);			
	}

}