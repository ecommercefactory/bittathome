<?php
defined("BASEPATH") OR exit("No esta permitido el acceso directo a este script.");

class Comercio extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->ctrSegAdmin();
	}

	public function index() {
		$this->parametros['plantilla'] = 'datatables';
		$this->parametros['vista'] = 'admin/administradores021/comercio';
		$this->parametros['datos']['titulo'] = 'Comercio';
		$this->parametros['datos']['subtitulo'] = ''; 
    
    $tabla_paises = $this->Modelo->registros($this->config->item('raiz_bd') . 'paises', 'id, nombre_en', array(), 'nombre_en ASC' );
    $this->parametros['datos']['tabla_paises'] = $tabla_paises;
    
    $tabla_datos_valores_paypal_modo = $this->Modelo->registros($this->config->item('raiz_bd') . 'datos_valores', 'id, nombre', array('dato' => 18), 'orden ASC' );
     $this->parametros['datos']['tabla_datos_valores_paypal_modo'] = $tabla_datos_valores_paypal_modo;
    
    $tabla_datos_valores_payu_modo = $this->Modelo->registros($this->config->item('raiz_bd') . 'datos_valores', 'id, nombre', array('dato' => 19), 'orden ASC' );
     $this->parametros['datos']['tabla_datos_valores_payu_modo'] = $tabla_datos_valores_payu_modo;
    
		$this->load->view('plantilla_admin', $this->parametros);			
	}

	public function traerRegistro () {
		$post = $this->input->post(NULL, TRUE);
		$registro = $this->Modelo->registro($this->config->item('raiz_bd') . 'comercio', $post['id']);
		$datos = array('registro'=>$registro, 'tksec'=>$this->security->get_csrf_hash());
		echo json_encode($datos);      
	}

	public function guardar () {
		$post = $this->input->post(NULL, TRUE);
		if (!empty($post)) {
			foreach ($post as $key => $value) {
				$post[$key] = $this->security->xss_clean($value);
			}
      
			$this->form_validation->set_rules('impuesto', 'Impuesto', 'xss_clean');
      $this->form_validation->set_rules('envioNacional', 'Envio Nacional', 'xss_clean');
      /*
      $this->form_validation->set_rules('envioInternacional', 'Envio Internacional', 'xss_clean');
      $this->form_validation->set_rules('tasaMinimaNal', 'Tasa Minima Nacional', 'xss_clean');
      $this->form_validation->set_rules('tasaMinimaInt', 'Tasa Minima Internacional', 'xss_clean');
      */
      $this->form_validation->set_rules('pais', 'Pais', 'required|xss_clean');
      $this->form_validation->set_rules('modoPaypal', 'Modo Paypal', 'xss_clean');
      $this->form_validation->set_rules('clienteIdPaypal', 'Cliente Id Paypal', 'xss_clean');
      $this->form_validation->set_rules('llaveSecretaPaypal', 'Llave Secreta Paypal', 'xss_clean');
      $this->form_validation->set_rules('modoPayu', 'Modo PayU', 'xss_clean');
      $this->form_validation->set_rules('merchantIdPayu', 'Merchant Id PayU', 'xss_clean');
      $this->form_validation->set_rules('accountIdPayu', 'Account Id PayU', 'xss_clean');
      $this->form_validation->set_rules('apiKeyPayu', 'Api Key PayU', 'xss_clean');
      
			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.validation_errors().'<br><b>'.$this->lang->line('be_please_try_again').'</b>');
				$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
			} else {
/*        
          'envioInternacional' => $post['envioInternacional'],
          'tasaMinimaNal' => $post['tasaMinimaNal'],
          'tasaMinimaInt' => $post['tasaMinimaInt'],
*/
				$datos = array(
          'impuesto' => $post['impuesto'],
          'envioNacional' => $post['envioNacional'],
          'pais' => $post['pais'],
          'modoPaypal' => $post['modoPaypal'],
          'clienteIdPaypal' => $post['clienteIdPaypal'],
          'llaveSecretaPaypal' => $post['llaveSecretaPaypal'],
          'modoPayu' => $post['modoPayu'],
          'merchantIdPayu' => $post['merchantIdPayu'],
          'accountIdPayu' => $post['accountIdPayu'],
          'apiKeyPayu' => $post['apiKeyPayu'],);

				if ($this->Modelo->actualizar($this->config->item('raiz_bd') . 'comercio', $datos, $post['id'])) {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-check"></i> '.$this->lang->line('be_successful_operation').'</h4>El registro se guardo exitosamente.'); 
					$this->session->set_flashdata('alertaTipo', 'success'); // success, info, warning, danger
				} else {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No fue posible guardar el registro.<br>'.$this->lang->line('be_please_try_again'));
					$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
				}     

			}
		} else {
			$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No se recibieron datos.<br>'.$this->lang->line('be_please_try_again'));
			$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
		}
		redirect('/admin/administradores021/comercio');   
	}

}