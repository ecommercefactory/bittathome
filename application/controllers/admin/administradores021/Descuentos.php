<?php
defined("BASEPATH") OR exit("No esta permitido el acceso directo a este script.");

class Descuentos extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->ctrSegAdmin();
	}

	public function index() {
		$this->parametros['plantilla'] = 'datatables';
		$this->parametros['vista'] = 'admin/administradores021/descuentos';
		$this->parametros['datos']['titulo'] = 'Descuentos';
		$this->parametros['datos']['subtitulo'] = ''; 
    
    //$tabla_descuentos = $this->Modelo->registros($this->config->item('raiz_bd') . 'descuentos', '', array() );
		//$this->parametros['datos']['lista'] = $tabla_descuentos;
    
    $tabla_usuarios = $this->Modelo->registros($this->config->item('raiz_bd') . 'usuarios', 'id, nombre', array(), 'nombre ASC' ); 
    $this->parametros['datos']['tabla_usuarios'] = $tabla_usuarios;
    
    $tabla_datos_valores_envio_gratis = $this->Modelo->registros($this->config->item('raiz_bd') . 'datos_valores', 'id, nombre', array('dato' => 3), 'orden DESC' ); 
    $this->parametros['datos']['tabla_datos_valores_envio_gratis'] = $tabla_datos_valores_envio_gratis;
    
    $tabla_categorias = $this->Modelo->registros($this->config->item('raiz_bd') . 'categorias', 'id, nombre', array(), 'nombre ASC' ); 
    $this->parametros['datos']['tabla_categorias'] = $tabla_categorias;
    
    $tabla_subcategorias = $this->Modelo->registros($this->config->item('raiz_bd') . 'subcategorias', 'id, nombre', array(), 'nombre ASC' ); 
    
    //$tabla_subcategorias = $this->Modelo->registros($this->config->item('raiz_bd') . 'categorias c, subcategorias s', 's.id, CONCAT(c.nombre, ". ", s.nombre) AS nombre', array('c.id = s.categoria'), 'nombre' );    
    
    $this->parametros['datos']['tabla_subcategorias'] = $tabla_subcategorias;
    
		$this->load->view('plantilla_admin', $this->parametros);			
	}

	public function traerRegistro () {
		$post = $this->input->post(NULL, TRUE);
		$registro = $this->Modelo->registro($this->config->item('raiz_bd') . 'descuentos', $post['id']);
    
    if ($registro->desde == "0000-00-00") $registro->desde = "";
    if ($registro->hasta == "0000-00-00") $registro->hasta = "";
    if ($registro->entrega_desde == "0000-00-00") $registro->entrega_desde = "";
    if ($registro->entrega_hasta == "0000-00-00") $registro->entrega_hasta = "";
    
		$datos = array('registro'=>$registro, 'tksec'=>$this->security->get_csrf_hash());
		echo json_encode($datos);      
	}

	public function ingresar () {
		$post = $this->input->post(NULL, TRUE);
		if (!empty($post)) {
			foreach ($post as $key => $value) {
				$post[$key] = $this->security->xss_clean($value);
			}
      
			$this->form_validation->set_rules('codigo_descuento', 'Codigo Descuento', 'required|max_length[10]|xss_clean');$this->form_validation->set_rules('cliente', 'Cliente', 'xss_clean');$this->form_validation->set_rules('desde', 'Desde', 'required|xss_clean');$this->form_validation->set_rules('hasta', 'Hasta', 'required|xss_clean');$this->form_validation->set_rules('descuento', 'Descuento', 'xss_clean');$this->form_validation->set_rules('descuento_porcentaje', 'Descuento Porcentaje', 'xss_clean');$this->form_validation->set_rules('envio_gratis', 'Envio Gratis', 'required|xss_clean');$this->form_validation->set_rules('limite_usos', 'Limite Usos', 'numeric|xss_clean');$this->form_validation->set_rules('categoria', 'Categoria', 'xss_clean');$this->form_validation->set_rules('subcategoria', 'Subcategoria', 'xss_clean');$this->form_validation->set_rules('entrega_desde', 'Entrega Desde', 'xss_clean');$this->form_validation->set_rules('entrega_hasta', 'Entrega Hasta', 'xss_clean');$this->form_validation->set_rules('subtotal_minimo', 'Subtotal Minimo', 'numeric|xss_clean');
      
			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.validation_errors().'<br><b>'.$this->lang->line('be_please_try_again').'</b>');
				$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
			} else {

				
        
				$datos = array('codigo_descuento' => $post['codigo_descuento'],'cliente' => $post['cliente'],'desde' => $post['desde'],'hasta' => $post['hasta'],'descuento' => $post['descuento'],'descuento_porcentaje' => $post['descuento_porcentaje'],'envio_gratis' => $post['envio_gratis'],'limite_usos' => $post['limite_usos'],'categoria' => $post['categoria'],'subcategoria' => $post['subcategoria'],'entrega_desde' => $post['entrega_desde'],'entrega_hasta' => $post['entrega_hasta'],'subtotal_minimo' => $post['subtotal_minimo'],);

				$id = $this->Modelo->insertar($this->config->item('raiz_bd') . 'descuentos', $datos);
				if ($id > 0) {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-check"></i> '.$this->lang->line('be_successful_operation').'</h4>El registro se ingresó exitosamente.'); 
					$this->session->set_flashdata('alertaTipo', 'success'); // success, info, warning, danger
				} else {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No fue posible ingresar el registro.<br>'.$this->lang->line('be_please_try_again'));
					$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
				}     
			}
		} else {
			$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No se recibieron datos.<br>'.$this->lang->line('be_please_try_again'));
			$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
		}
		redirect('/admin/administradores021/descuentos');
	}

	public function guardar () {
		$post = $this->input->post(NULL, TRUE);
		if (!empty($post)) {
			foreach ($post as $key => $value) {
				$post[$key] = $this->security->xss_clean($value);
			}
      
			$this->form_validation->set_rules('codigo_descuento', 'Codigo Descuento', 'required|max_length[10]|xss_clean');$this->form_validation->set_rules('cliente', 'Cliente', 'xss_clean');$this->form_validation->set_rules('desde', 'Desde', 'required|xss_clean');$this->form_validation->set_rules('hasta', 'Hasta', 'required|xss_clean');$this->form_validation->set_rules('descuento', 'Descuento', 'xss_clean');$this->form_validation->set_rules('descuento_porcentaje', 'Descuento Porcentaje', 'xss_clean');$this->form_validation->set_rules('envio_gratis', 'Envio Gratis', 'required|xss_clean');$this->form_validation->set_rules('limite_usos', 'Limite Usos', 'numeric|xss_clean');$this->form_validation->set_rules('categoria', 'Categoria', 'xss_clean');$this->form_validation->set_rules('subcategoria', 'Subcategoria', 'xss_clean');$this->form_validation->set_rules('entrega_desde', 'Entrega Desde', 'xss_clean');$this->form_validation->set_rules('entrega_hasta', 'Entrega Hasta', 'xss_clean');$this->form_validation->set_rules('subtotal_minimo', 'Subtotal Minimo', 'numeric|xss_clean');
      
			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.validation_errors().'<br><b>'.$this->lang->line('be_please_try_again').'</b>');
				$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
			} else {

				
        
				$datos = array('codigo_descuento' => $post['codigo_descuento'],'cliente' => $post['cliente'],'desde' => $post['desde'],'hasta' => $post['hasta'],'descuento' => $post['descuento'],'descuento_porcentaje' => $post['descuento_porcentaje'],'envio_gratis' => $post['envio_gratis'],'limite_usos' => $post['limite_usos'],'categoria' => $post['categoria'],'subcategoria' => $post['subcategoria'],'entrega_desde' => $post['entrega_desde'],'entrega_hasta' => $post['entrega_hasta'],'subtotal_minimo' => $post['subtotal_minimo'],);

				if ($this->Modelo->actualizar($this->config->item('raiz_bd') . 'descuentos', $datos, $post['id'])) {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-check"></i> '.$this->lang->line('be_successful_operation').'</h4>El registro se guardo exitosamente.'); 
					$this->session->set_flashdata('alertaTipo', 'success'); // success, info, warning, danger
				} else {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No fue posible guardar el registro.<br>'.$this->lang->line('be_please_try_again'));
					$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
				}     

			}
		} else {
			$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No se recibieron datos.<br>'.$this->lang->line('be_please_try_again'));
			$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
		}
		redirect('/admin/administradores021/descuentos');   
	}

	public function eliminar () {
		$post = $this->input->post(NULL, TRUE);
		if (!empty($post)) {
			foreach ($post as $key => $value) {
				$post[$key] = $this->security->xss_clean($value);
			}
			$this->form_validation->set_rules('id', 'ID', 'required|trim|xss_clean');
			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.validation_errors().'<br><b>'.$this->lang->line('be_please_try_again').'</b>');
				$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
			} else {

				$mat_id = explode(";", $post['id']); 
				$proceso = false;          
				foreach ($mat_id as $id) {
					$this->Modelo->eliminar($this->config->item('raiz_bd') . 'descuentos', $id);
					$proceso = true;
				}

				if ($proceso) {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-check"></i> '.$this->lang->line('be_successful_operation').'</h4>El(los) registro(s) se eliminó(aron) exitosamente.'); 
					$this->session->set_flashdata('alertaTipo', 'success'); // success, info, warning, danger
				} else {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No fue posible eliminar el(los) registro(s).<br>'.$this->lang->line('be_please_try_again'));
					$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
				}     

			}
		} else {
			$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No se recibieron datos.<br>'.$this->lang->line('be_please_try_again'));
			$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
		}
		redirect('/admin/administradores021/descuentos');   
	}

  
  
	public function lista () {

		$registros = $this->Modelo->registros($this->config->item('raiz_bd') . 'descuentos');
    
  	$datosJson = '
  		{	
  			"data":[';  
    $datosJsonReg = '';
    foreach ($registros as $key => $registro) {

          $usuario = $this->Modelo->registro($this->config->item('raiz_bd') . 'usuarios', $registro['cliente']);
          if ($usuario) {
             $registro['cliente'] = $usuario->nombre;
          } else {
             $registro['cliente'] = "";
          }  
          $categoria = $this->Modelo->registro($this->config->item('raiz_bd') . 'categorias', $registro['categoria']);
          if ($categoria) {
             $registro['categoria'] = $categoria->nombre;
          } else {
             $registro['categoria'] = "";
          }


          $subcategoria = $this->Modelo->registro($this->config->item('raiz_bd') . 'subcategorias', $registro['subcategoria']);
          if ($subcategoria) {
             $registro['subcategoria'] = $subcategoria->nombre;
          } else {
             $registro['subcategoria'] = "";
          }

/*
        if ($registro['subcategoria'] > 0) {
              $subcategoria = $this->Modelo->registros('categorias c, subcategorias s', 's.id, CONCAT(c.nombre, ". ", s.nombre) AS nombreSubcategoria', array('c.id = s.categoria') );                
              foreach ($subcategoria as $subcategoriaReg) {
                    if ($subcategoriaReg['id'] == $registro['subcategoria']) {
                      $registro['subcategoria'] = $subcategoriaReg['nombreSubcategoria'];
                      break;
                    }
              } 
          } else {
              $registro['subcategoria'] = "";
          }
*/
      
          $envio_gratis = $this->Modelo->registro($this->config->item('raiz_bd') . 'datos_valores', $registro['envio_gratis']);
          if ($envio_gratis) {
             $registro['envio_gratis'] = $envio_gratis->nombre;
          }
      
          if ($registro['desde'] == "0000-00-00") $registro['desde'] = "";
          if ($registro['hasta'] == "0000-00-00") $registro['hasta'] = "";
          if ($registro['entrega_desde'] == "0000-00-00") $registro['entrega_desde'] = "";
          if ($registro['entrega_hasta'] == "0000-00-00") $registro['entrega_hasta'] = "";      
      
      
          $datosJsonReg .='["<input type=\"checkbox\" id=\"fila_'.$registro['id'].'\" class=\"seleccion\" cod=\"'.$registro['id'].'\">", "<button type=\"button\" class=\"btn btn-default btn-xs text-light-blue btnEditar\" cod=\"'.$registro['id'].'\"><span class=\"glyphicon glyphicon-pencil\"></span></button>", "<button type=\"button\" class=\"btn btn-default btn-xs text-red btnEliminar\" cod=\"'.$registro['id'].'\"><span class=\"glyphicon glyphicon-trash\"></span></button>","'.$registro['codigo_descuento'].'","'.$registro['cliente'].'","'.$registro['desde'].'","'.$registro['hasta'].'","'.$registro['descuento'].'","'.$registro['descuento_porcentaje'].'","'.$registro['envio_gratis'].'","'.$registro['limite_usos'].'","'.$registro['categoria'].'","'.$registro['subcategoria'].'","'.$registro['entrega_desde'].'","'.$registro['entrega_hasta'].'","'.$registro['subtotal_minimo'].'","'.$registro['fecha'].'"],';

    }
    if ($datosJsonReg != '') {
          $datosJson .= substr($datosJsonReg, 0, -1);
    }
		
		$datosJson .= ']
		}';

		echo $datosJson;
    
	}
      
  
	public function lista_ssp () {
		$this->load->library('SSP');

		// DB table to use
		$table = 'descuentos';

		// Table's primary key
		$primaryKey = 'id';

		// Array of database columns
		$columns = array(array( 'db' => 'id', 'dt' => 0, 'field' => 'id', 'formatter' => function($d, $row) {return '<input type="checkbox" id="fila_' . $d . '" class="seleccion" cod="' . $d . '">';}),array( 'db' => 'id', 'dt' => 1, 'field' => 'id', 'formatter' => function($d, $row) {return '<button type="button" class="btn btn-default btn-xs text-light-blue btnEditar" cod="' . $d . '"><span class="glyphicon glyphicon-pencil"></span></button>';}), array( 'db' => 'id', 'dt' => 2, 'field' => 'id', 'formatter' => function($d, $row) {return '<button type="button" class="btn btn-default btn-xs text-red btnEliminar" cod="' . $d . '"><span class="glyphicon glyphicon-trash"></span></button>';}),array( 'db' => 'codigo_descuento', 'dt' => 3, 'field' => 'codigo_descuento' ),array( 'db' => 'cliente', 'dt' => 4, 'field' => 'cliente' ),array( 'db' => 'desde', 'dt' => 5, 'field' => 'desde' ),array( 'db' => 'hasta', 'dt' => 6, 'field' => 'hasta' ),array( 'db' => 'descuento', 'dt' => 7, 'field' => 'descuento' ),array( 'db' => 'descuento_porcentaje', 'dt' => 8, 'field' => 'descuento_porcentaje' ),array( 'db' => 'envio_gratis', 'dt' => 9, 'field' => 'envio_gratis' ),array( 'db' => 'limite_usos', 'dt' => 10, 'field' => 'limite_usos' ),array( 'db' => 'categoria', 'dt' => 11, 'field' => 'categoria' ),array( 'db' => 'subcategoria', 'dt' => 12, 'field' => 'subcategoria' ),array( 'db' => 'entrega_desde', 'dt' => 13, 'field' => 'entrega_desde' ),array( 'db' => 'entrega_hasta', 'dt' => 14, 'field' => 'entrega_hasta' ),array( 'db' => 'subtotal_minimo', 'dt' => 15, 'field' => 'subtotal_minimo' ),array( 'db' => 'fecha', 'dt' => 16, 'field' => 'fecha' ),);

		$sql_details = array(
			'user' => $this->db->username,
			'pass' => $this->db->password,
			'db'   => $this->db->database,
			'host' => $this->db->hostname
		);

		$joinQuery = "FROM descuentos";
		$extraWhere = ""; //"`u`.`valor` >= 90000";
		$groupBy = ""; //"`u`.`datos`";
		$having = ""; //"`u`.`valor` >= 140000";

		$_GET['tksec'] = $this->security->get_csrf_hash();  

		echo json_encode(
			SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having )
		);
	}

}