<?php
defined("BASEPATH") OR exit("No esta permitido el acceso directo a este script.");

class Cms_contenidos extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->ctrSegAdmin();
	}

	public function index() {
		$this->parametros['plantilla'] = 'datatables';
		$this->parametros['vista'] = 'admin/administradores021/cms_contenidos';
		$this->parametros['datos']['titulo'] = 'Contenidos';
		$this->parametros['datos']['subtitulo'] = ''; 
//    $tabla_categorias = $this->Modelo->registros($this->config->item('raiz_bd') . 'categorias', '', array() );
//		$this->parametros['datos']['lista'] = $tabla_categorias;
    
    $tabla_datos_valores_estado = $this->Modelo->registros($this->config->item('raiz_bd') . 'datos_valores', 'id, nombre', array('dato' => 1), 'nombre ASC' ); 
    $this->parametros['datos']['tabla_datos_valores_estado'] = $tabla_datos_valores_estado;

		$this->load->view('plantilla_admin', $this->parametros);			
	}

	public function traerRegistro () {
		$post = $this->input->post(NULL, TRUE);
		$registro = $this->Modelo->registro($this->config->item('raiz_bd') . 'cms_contenidos', $post['id']);
    	if ($registro->desde == '0000-00-00') $registro->desde = '';
    	if ($registro->hasta == '0000-00-00') $registro->hasta = '';
		$datos = array('registro'=>$registro, 'tksec'=>$this->security->get_csrf_hash());
		echo json_encode($datos);      
	}

  public function crearURLAmigable () {
    $post = $this->input->post(NULL, TRUE);
    $url = '';
    $texto = preg_replace('/[^a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]/', '', $post['titulo']);
    $encontrar = array('à','á','â','ã','ä', 'ç', 'è','é','ê','ë', 'ì','í','î','ï', 'ñ', 'ò','ó','ô','õ','ö', 'ù','ú','û','ü', 'ý','ÿ', 'À','Á','Â','Ã','Ä', 'Ç', 'È','É','Ê','Ë', 'Ì','Í','Î','Ï', 'Ñ', 'Ò','Ó','Ô','Õ','Ö', 'Ù','Ú','Û','Ü', 'Ý', '-', ' ');
    $reemplazar = array('a','a','a','a','a', 'c', 'e','e','e','e', 'i','i','i','i', 'n', 'o','o','o','o','o', 'u','u','u','u', 'y','y', 'A','A','A','A','A', 'C', 'E','E','E','E', 'I','I','I','I', 'N', 'O','O','O','O','O', 'U','U','U','U', 'Y', '_', '-');
    $url = urlencode(strtolower(str_replace($encontrar, $reemplazar, $texto)));
    $datos = array('url'=>$url, 'tksec'=>$this->security->get_csrf_hash());
    echo json_encode($datos);      
  }  
  
	public function ingresar () {
		$post = $this->input->post(NULL, TRUE);
		if (!empty($post)) {
			foreach ($post as $key => $value) {
				$post[$key] = $this->security->xss_clean($value);
			}
      
		      $this->form_validation->set_rules('titulo', 'Titulo', 'max_length[250]|xss_clean');
		      $this->form_validation->set_rules('ruta', 'Ruta', 'required|max_length[250]|xss_clean');
		      $this->form_validation->set_rules('contenido', 'Contenido', 'xss_clean');
		      $this->form_validation->set_rules('estado', 'Estado', 'required|xss_clean');
		      $this->form_validation->set_rules('desde', 'Desde', 'xss_clean');
		      $this->form_validation->set_rules('hasta', 'Hasta', 'xss_clean');
			  $this->form_validation->set_rules('orden', 'Orden', 'required|greater_than[0]|numeric|xss_clean');
      
			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.validation_errors().'<br><b>'.$this->lang->line('be_please_try_again').'</b>');
				$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
			} else {

        		$datos = array(
					'titulo' => $post['titulo'],
					'ruta' => $post['ruta'],
					'contenido' => $post['contenido'],
					'estado' => $post['estado'],
					'desde' => $post['desde'],
					'hasta' => $post['hasta'],
					'orden' => $post['orden'],);

				$id = $this->Modelo->insertar($this->config->item('raiz_bd') . 'cms_contenidos', $datos);
				if ($id > 0) {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-check"></i> '.$this->lang->line('be_successful_operation').'</h4>El registro se ingresó exitosamente.'); 
					$this->session->set_flashdata('alertaTipo', 'success'); // success, info, warning, danger
				} else {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No fue posible ingresar el registro.<br>'.$this->lang->line('be_please_try_again'));
					$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
				}     
			}
		} else {
			$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No se recibieron datos.<br>'.$this->lang->line('be_please_try_again'));
			$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
		}
		redirect('/admin/administradores021/cms_contenidos');
	}

	public function guardar () {
		$post = $this->input->post(NULL, TRUE);
		if (!empty($post)) {
			foreach ($post as $key => $value) {
				$post[$key] = $this->security->xss_clean($value);
			}
      
		      $this->form_validation->set_rules('titulo', 'Titulo', 'max_length[250]|xss_clean');
		      $this->form_validation->set_rules('ruta', 'Ruta', 'required|max_length[250]|xss_clean');
		      $this->form_validation->set_rules('contenido', 'Contenido', 'xss_clean');
		      $this->form_validation->set_rules('estado', 'Estado', 'required|xss_clean');
		      $this->form_validation->set_rules('desde', 'Desde', 'xss_clean');
		      $this->form_validation->set_rules('hasta', 'Hasta', 'xss_clean');
			  $this->form_validation->set_rules('orden', 'Orden', 'required|greater_than[0]|numeric|xss_clean');
      
			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.validation_errors().'<br><b>'.$this->lang->line('be_please_try_again').'</b>');
				$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
			} else {

        		$datos = array(
					'titulo' => $post['titulo'],
					'ruta' => $post['ruta'],
					'contenido' => $post['contenido'],
					'estado' => $post['estado'],
					'desde' => $post['desde'],
					'hasta' => $post['hasta'],
					'orden' => $post['orden'],);
        
				if ($this->Modelo->actualizar($this->config->item('raiz_bd') . 'cms_contenidos', $datos, $post['id'])) {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-check"></i> '.$this->lang->line('be_successful_operation').'</h4>El registro se guardo exitosamente.'); 
					$this->session->set_flashdata('alertaTipo', 'success'); // success, info, warning, danger
				} else {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No fue posible guardar el registro.<br>'.$this->lang->line('be_please_try_again'));
					$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
				}     

			}
		} else {
			$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No se recibieron datos.<br>'.$this->lang->line('be_please_try_again'));
			$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
		}
		redirect('/admin/administradores021/cms_contenidos');   
	}

	public function eliminar () {
		$post = $this->input->post(NULL, TRUE);
		if (!empty($post)) {
			foreach ($post as $key => $value) {
				$post[$key] = $this->security->xss_clean($value);
			}
			$this->form_validation->set_rules('id', 'ID', 'required|trim|xss_clean');
			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.validation_errors().'<br><b>'.$this->lang->line('be_please_try_again').'</b>');
				$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
			} else {

				$mat_id = explode(";", $post['id']); 
				$proceso = false;          
				foreach ($mat_id as $id) {
					$this->Modelo->eliminar($this->config->item('raiz_bd') . 'cms_contenidos', $id);
					$proceso = true;
				}

				if ($proceso) {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-check"></i> '.$this->lang->line('be_successful_operation').'</h4>El(los) registro(s) se eliminó(aron) exitosamente.'); 
					$this->session->set_flashdata('alertaTipo', 'success'); // success, info, warning, danger
				} else {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No fue posible eliminar el(los) registro(s).<br>'.$this->lang->line('be_please_try_again'));
					$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
				}     

			}
		} else {
			$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No se recibieron datos.<br>'.$this->lang->line('be_please_try_again'));
			$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
		}
		redirect('/admin/administradores021/cms_contenidos');   
	}

  
  
	public function lista () {

		$registros = $this->Modelo->registros($this->config->item('raiz_bd') . 'cms_contenidos');
    
	  	$datosJson = '
	  		{	
	  			"data":[';  
	    $datosJsonReg = '';
	    foreach ($registros as $key => $registro) {
      
	          $estado = $this->Modelo->registro($this->config->item('raiz_bd') . 'datos_valores', $registro['estado']);
	          if ($estado) {
	             $registro['estado'] = $estado->nombre;
	          }
	      
	          if ($registro['desde'] == '0000-00-00') $registro['desde'] = '';
	          if ($registro['hasta'] == '0000-00-00') $registro['hasta'] = '';
	      
	          $datosJsonReg .='["<input type=\"checkbox\" id=\"fila_'.$registro['id'].'\" class=\"seleccion\" cod=\"'.$registro['id'].'\">", "<button type=\"button\" class=\"btn btn-default btn-xs text-light-blue btnEditar\" cod=\"'.$registro['id'].'\"><span class=\"glyphicon glyphicon-pencil\"></span></button>", "<button type=\"button\" class=\"btn btn-default btn-xs text-red btnEliminar\" cod=\"'.$registro['id'].'\"><span class=\"glyphicon glyphicon-trash\"></span></button>",
	          "'.$registro['titulo'].'",
	          "'.$registro['ruta'].'",
	          "'.$registro['estado'].'",
	          "'.$registro['desde'].'",
	          "'.$registro['hasta'].'",
	          "'.$registro['visitas'].'",
	          "'.$registro['orden'].'",
	          "'.$registro['fecha'].'"],';

	    }
	    if ($datosJsonReg != '') {
	          $datosJson .= substr($datosJsonReg, 0, -1);
	    }
		
		$datosJson .= ']
		}';

		echo $datosJson;
	}

}