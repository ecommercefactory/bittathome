<?php
defined("BASEPATH") OR exit("No esta permitido el acceso directo a este script.");

class Categorias extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->ctrSegAdmin();
	}

	public function index() {
		$this->parametros['plantilla'] = 'datatables';
		$this->parametros['vista'] = 'admin/administradores021/categorias';
		$this->parametros['datos']['titulo'] = 'Categorias';
		$this->parametros['datos']['subtitulo'] = ''; 
    $tabla_categorias = $this->Modelo->registros($this->config->item('raiz_bd') . 'categorias', '', array() );
		$this->parametros['datos']['lista'] = $tabla_categorias;
    
    $tabla_datos_valores_estado = $this->Modelo->registros($this->config->item('raiz_bd') . 'datos_valores', 'id, nombre', array('dato' => 1), 'nombre ASC' ); 
    $this->parametros['datos']['tabla_datos_valores_estado'] = $tabla_datos_valores_estado;

    $tabla_datos_valores_oferta = $this->Modelo->registros($this->config->item('raiz_bd') . 'datos_valores', 'id, nombre', array('dato' => 3), 'nombre ASC' ); 
    $this->parametros['datos']['tabla_datos_valores_oferta'] = $tabla_datos_valores_oferta;
    
		$this->load->view('plantilla_admin', $this->parametros);			
	}

	public function traerRegistro () {
		$post = $this->input->post(NULL, TRUE);
		$registro = $this->Modelo->registro($this->config->item('raiz_bd') . 'categorias', $post['id']);
    if ($registro->finOferta == '0000-00-00') $registro->finOferta = '';
		$datos = array('registro'=>$registro, 'tksec'=>$this->security->get_csrf_hash());
		echo json_encode($datos);      
	}

  public function crearURLAmigable () {
    $post = $this->input->post(NULL, TRUE);
    $url = '';
    $texto = preg_replace('/[^a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]/', '', $post['nombre']);
    $encontrar = array('à','á','â','ã','ä', 'ç', 'è','é','ê','ë', 'ì','í','î','ï', 'ñ', 'ò','ó','ô','õ','ö', 'ù','ú','û','ü', 'ý','ÿ', 'À','Á','Â','Ã','Ä', 'Ç', 'È','É','Ê','Ë', 'Ì','Í','Î','Ï', 'Ñ', 'Ò','Ó','Ô','Õ','Ö', 'Ù','Ú','Û','Ü', 'Ý', '-', ' ');
    $reemplazar = array('a','a','a','a','a', 'c', 'e','e','e','e', 'i','i','i','i', 'n', 'o','o','o','o','o', 'u','u','u','u', 'y','y', 'A','A','A','A','A', 'C', 'E','E','E','E', 'I','I','I','I', 'N', 'O','O','O','O','O', 'U','U','U','U', 'Y', '_', '-');
    $url = urlencode(strtolower(str_replace($encontrar, $reemplazar, $texto)));
    $datos = array('url'=>$url, 'tksec'=>$this->security->get_csrf_hash());
    echo json_encode($datos);      
  }  
  
	public function ingresar () {
		$post = $this->input->post(NULL, TRUE);
		if (!empty($post)) {
			foreach ($post as $key => $value) {
				$post[$key] = $this->security->xss_clean($value);
			}
      
			$this->form_validation->set_rules('nombre', 'Nombre', 'required|max_length[250]|xss_clean');
      $this->form_validation->set_rules('ruta', 'Ruta', 'required|max_length[250]|xss_clean');
      $this->form_validation->set_rules('titulo', 'Titulo', 'max_length[250]|xss_clean');
      $this->form_validation->set_rules('descripcionCorta', 'Descripcion Corta', 'xss_clean');
      $this->form_validation->set_rules('descripcion', 'Descripcion', 'xss_clean');
      $this->form_validation->set_rules('palabrasClaves', 'Palabras Claves', 'max_length[250]|xss_clean');
      $this->form_validation->set_rules('portadaMarcado', 'Portada Marcado', 'xss_clean');
      $this->form_validation->set_rules('estado', 'Estado', 'required|xss_clean');
      $this->form_validation->set_rules('oferta', 'Oferta', 'required|xss_clean');
      $this->form_validation->set_rules('precioOferta', 'Precio Oferta', 'numeric|xss_clean');
      $this->form_validation->set_rules('descuentoOferta', 'Descuento Oferta', 'less_than[100]|numeric|xss_clean');
      $this->form_validation->set_rules('imgOferta', 'Imagen Oferta', 'xss_clean');
      $this->form_validation->set_rules('finOferta', 'Fin Oferta', 'xss_clean');
      
			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.validation_errors().'<br><b>'.$this->lang->line('be_please_try_again').'</b>');
				$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
			} else {

				$config['upload_path'] = './imagenes/categoria/portada/';$config['allowed_types'] = 'gif|jpg|png|jpeg';$config['max_size'] = 2048;$this->load->library('upload', $config); $this->upload->initialize($config);$post['portadaMarcado'] = '';if ($this->upload->do_upload('portadaMarcado')) {$data_portadaMarcado = array('upload_data' => $this->upload->data());$post['portadaMarcado'] = $data_portadaMarcado['upload_data']['file_name'];}$config['upload_path'] = './imagenes/categoria/oferta/';$config['allowed_types'] = 'gif|jpg|png|jpeg';$config['max_size'] = 2048;$this->load->library('upload', $config); $this->upload->initialize($config);$post['imgOferta'] = '';if ($this->upload->do_upload('imgOferta')) {$data_imgOferta = array('upload_data' => $this->upload->data());$post['imgOferta'] = $data_imgOferta['upload_data']['file_name'];}
        
				$datos = array('nombre' => $post['nombre'],'ruta' => $post['ruta'],'titulo' => $post['titulo'],'descripcionCorta' => $post['descripcionCorta'],'descripcion' => $post['descripcion'],'palabrasClaves' => $post['palabrasClaves'],'portadaMarcado' => $post['portadaMarcado'],'estado' => $post['estado'],'oferta' => $post['oferta'],'precioOferta' => $post['precioOferta'],'descuentoOferta' => $post['descuentoOferta'],'imgOferta' => $post['imgOferta'],'finOferta' => $post['finOferta'],);

				$id = $this->Modelo->insertar($this->config->item('raiz_bd') . 'categorias', $datos);
				if ($id > 0) {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-check"></i> '.$this->lang->line('be_successful_operation').'</h4>El registro se ingresó exitosamente.'); 
					$this->session->set_flashdata('alertaTipo', 'success'); // success, info, warning, danger
				} else {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No fue posible ingresar el registro.<br>'.$this->lang->line('be_please_try_again'));
					$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
				}     
			}
		} else {
			$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No se recibieron datos.<br>'.$this->lang->line('be_please_try_again'));
			$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
		}
		redirect('/admin/administradores021/categorias');
	}

	public function guardar () {
		$post = $this->input->post(NULL, TRUE);
		if (!empty($post)) {
			foreach ($post as $key => $value) {
				$post[$key] = $this->security->xss_clean($value);
			}
      
			$this->form_validation->set_rules('nombre', 'Nombre', 'required|max_length[250]|xss_clean');
      $this->form_validation->set_rules('ruta', 'Ruta', 'required|max_length[250]|xss_clean');
      $this->form_validation->set_rules('titulo', 'Titulo', 'max_length[250]|xss_clean');
      $this->form_validation->set_rules('descripcionCorta', 'Descripcion Corta', 'xss_clean');
      $this->form_validation->set_rules('descripcion', 'Descripcion', 'xss_clean');
      $this->form_validation->set_rules('palabrasClaves', 'Palabras Claves', 'max_length[250]|xss_clean');
      $this->form_validation->set_rules('portadaMarcado', 'Portada Marcado', 'xss_clean');
      $this->form_validation->set_rules('estado', 'Estado', 'required|xss_clean');
      $this->form_validation->set_rules('oferta', 'Oferta', 'required|xss_clean');
      $this->form_validation->set_rules('precioOferta', 'Precio Oferta', 'numeric|xss_clean');
      $this->form_validation->set_rules('descuentoOferta', 'Descuento Oferta', 'less_than[100]|numeric|xss_clean');
      $this->form_validation->set_rules('imgOferta', 'Imagen Oferta', 'xss_clean');
      $this->form_validation->set_rules('finOferta', 'Fin Oferta', 'xss_clean');
      
			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.validation_errors().'<br><b>'.$this->lang->line('be_please_try_again').'</b>');
				$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
			} else {

				$config['upload_path'] = './imagenes/categoria/portada/';$config['allowed_types'] = 'gif|jpg|png|jpeg';$config['max_size'] = 2048;$this->load->library('upload', $config); $this->upload->initialize($config);$post['portadaMarcado'] = '';if ($this->upload->do_upload('portadaMarcado')) {$data_portadaMarcado = array('upload_data' => $this->upload->data());$post['portadaMarcado'] = $data_portadaMarcado['upload_data']['file_name'];}$config['upload_path'] = './imagenes/categoria/oferta/';$config['allowed_types'] = 'gif|jpg|png|jpeg';$config['max_size'] = 2048;$this->load->library('upload', $config); $this->upload->initialize($config);$post['imgOferta'] = '';if ($this->upload->do_upload('imgOferta')) {$data_imgOferta = array('upload_data' => $this->upload->data());$post['imgOferta'] = $data_imgOferta['upload_data']['file_name'];}

				$datos = array('nombre' => $post['nombre'],'ruta' => $post['ruta'],'titulo' => $post['titulo'],'descripcionCorta' => $post['descripcionCorta'],'descripcion' => $post['descripcion'],'palabrasClaves' => $post['palabrasClaves'],'estado' => $post['estado'],'oferta' => $post['oferta'],'precioOferta' => $post['precioOferta'],'descuentoOferta' => $post['descuentoOferta'],'finOferta' => $post['finOferta'],);

        if ($post['portadaMarcado'] != '') $datos['portadaMarcado'] = $post['portadaMarcado'];
        if ($post['imgOferta'] != '') $datos['imgOferta'] = $post['imgOferta'];        
        
				if ($this->Modelo->actualizar($this->config->item('raiz_bd') . 'categorias', $datos, $post['id'])) {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-check"></i> '.$this->lang->line('be_successful_operation').'</h4>El registro se guardo exitosamente.'); 
					$this->session->set_flashdata('alertaTipo', 'success'); // success, info, warning, danger
				} else {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No fue posible guardar el registro.<br>'.$this->lang->line('be_please_try_again'));
					$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
				}     

			}
		} else {
			$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No se recibieron datos.<br>'.$this->lang->line('be_please_try_again'));
			$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
		}
		redirect('/admin/administradores021/categorias');   
	}

	public function eliminar () {
		$post = $this->input->post(NULL, TRUE);
		if (!empty($post)) {
			foreach ($post as $key => $value) {
				$post[$key] = $this->security->xss_clean($value);
			}
			$this->form_validation->set_rules('id', 'ID', 'required|trim|xss_clean');
			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.validation_errors().'<br><b>'.$this->lang->line('be_please_try_again').'</b>');
				$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
			} else {

				$mat_id = explode(";", $post['id']); 
				$proceso = false;          
				foreach ($mat_id as $id) {
					$this->Modelo->eliminar($this->config->item('raiz_bd') . 'categorias', $id);
					$proceso = true;
				}

				if ($proceso) {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-check"></i> '.$this->lang->line('be_successful_operation').'</h4>El(los) registro(s) se eliminó(aron) exitosamente.'); 
					$this->session->set_flashdata('alertaTipo', 'success'); // success, info, warning, danger
				} else {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No fue posible eliminar el(los) registro(s).<br>'.$this->lang->line('be_please_try_again'));
					$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
				}     

			}
		} else {
			$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No se recibieron datos.<br>'.$this->lang->line('be_please_try_again'));
			$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
		}
		redirect('/admin/administradores021/categorias');   
	}

  
  
	public function lista () {

		$registros = $this->Modelo->registros($this->config->item('raiz_bd') . 'categorias');
    
  	$datosJson = '
  		{	
  			"data":[';  
    $datosJsonReg = '';
    foreach ($registros as $key => $registro) {
      
          $estado = $this->Modelo->registro($this->config->item('raiz_bd') . 'datos_valores', $registro['estado']);
          if ($estado) {
             $registro['estado'] = $estado->nombre;
          }
          $oferta = $this->Modelo->registro($this->config->item('raiz_bd') . 'datos_valores', $registro['oferta']);
          if ($oferta) {
             $registro['oferta'] = $oferta->nombre;
          }
      
          if ($registro['finOferta'] == '0000-00-00') $registro['finOferta'] = '';
      
          $datosJsonReg .='["<input type=\"checkbox\" id=\"fila_'.$registro['id'].'\" class=\"seleccion\" cod=\"'.$registro['id'].'\">", "<button type=\"button\" class=\"btn btn-default btn-xs text-light-blue btnEditar\" cod=\"'.$registro['id'].'\"><span class=\"glyphicon glyphicon-pencil\"></span></button>", "<button type=\"button\" class=\"btn btn-default btn-xs text-red btnEliminar\" cod=\"'.$registro['id'].'\"><span class=\"glyphicon glyphicon-trash\"></span></button>","'.$registro['nombre'].'","'.$registro['ruta'].'","'.$registro['titulo'].'","'.$registro['descripcionCorta'].'","'.$registro['descripcion'].'","'.$registro['palabrasClaves'].'","'.$registro['portadaMarcado'].'","'.$registro['estado'].'","'.$registro['oferta'].'","'.$registro['precioOferta'].'","'.$registro['descuentoOferta'].'","'.$registro['imgOferta'].'","'.$registro['finOferta'].'","'.$registro['fecha'].'"],';

    }
    if ($datosJsonReg != '') {
          $datosJson .= substr($datosJsonReg, 0, -1);
    }
		
		$datosJson .= ']
		}';

		echo $datosJson;
    
	}
      
  
	public function lista_ssp () {
		$this->load->library('SSP');

		// DB table to use
		$table = 'categorias';

		// Table's primary key
		$primaryKey = 'id';

		// Array of database columns
		$columns = array(array( 'db' => 'id', 'dt' => 0, 'field' => 'id', 'formatter' => function($d, $row) {return '<input type="checkbox" id="fila_' . $d . '" class="seleccion" cod="' . $d . '">';}),array( 'db' => 'id', 'dt' => 1, 'field' => 'id', 'formatter' => function($d, $row) {return '<button type="button" class="btn btn-default btn-xs text-light-blue btnEditar" cod="' . $d . '"><span class="glyphicon glyphicon-pencil"></span></button>';}), array( 'db' => 'id', 'dt' => 2, 'field' => 'id', 'formatter' => function($d, $row) {return '<button type="button" class="btn btn-default btn-xs text-red btnEliminar" cod="' . $d . '"><span class="glyphicon glyphicon-trash"></span></button>';}),array( 'db' => 'categoria', 'dt' => 3, 'field' => 'categoria' ),array( 'db' => 'ruta', 'dt' => 4, 'field' => 'ruta' ),array( 'db' => 'titulo', 'dt' => 5, 'field' => 'titulo' ),array( 'db' => 'descripcionCorta', 'dt' => 6, 'field' => 'descripcionCorta' ),array( 'db' => 'descripcion', 'dt' => 7, 'field' => 'descripcion' ),array( 'db' => 'palabrasClaves', 'dt' => 8, 'field' => 'palabrasClaves' ),array( 'db' => 'portadaMarcado', 'dt' => 9, 'field' => 'portadaMarcado' ),array( 'db' => 'estado', 'dt' => 10, 'field' => 'estado' ),array( 'db' => 'oferta', 'dt' => 11, 'field' => 'oferta' ),array( 'db' => 'precioOferta', 'dt' => 12, 'field' => 'precioOferta' ),array( 'db' => 'descuentoOferta', 'dt' => 13, 'field' => 'descuentoOferta' ),array( 'db' => 'imgOferta', 'dt' => 14, 'field' => 'imgOferta' ),array( 'db' => 'finOferta', 'dt' => 15, 'field' => 'finOferta' ),array( 'db' => 'fecha', 'dt' => 16, 'field' => 'fecha' ),);

		$sql_details = array(
			'user' => $this->db->username,
			'pass' => $this->db->password,
			'db'   => $this->db->database,
			'host' => $this->db->hostname
		);

		$joinQuery = "FROM categorias";
		$extraWhere = ""; //"`u`.`valor` >= 90000";
		$groupBy = ""; //"`u`.`datos`";
		$having = ""; //"`u`.`valor` >= 140000";

		$_GET['tksec'] = $this->security->get_csrf_hash();  

		echo json_encode(
			SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having )
		);
	}

}