<?php
defined("BASEPATH") OR exit("No esta permitido el acceso directo a este script.");

class Plantilla_redes_sociales extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->ctrSegAdmin();
	}

    function _remap($param) {
        $this->index($param);
    }
  
    public function index($id) {  
    	if (!is_numeric($id)) {
    		redirect('/admin/administradores021/plantilla');
    	}
   		//$plantilla = $this->Modelo->registro($this->config->item('raiz_bd') . 'plantilla', $id);

		$this->parametros['plantilla'] = 'datatables';
		$this->parametros['vista'] = 'admin/administradores021/plantilla_redes_sociales';
		$this->parametros['datos']['titulo'] = 'Redes Sociales';
		$this->parametros['datos']['subtitulo'] = 'de la Plantilla'; 
    $this->parametros['datos']['breadcrumb'] = '
        <ol class="breadcrumb">
            <li><a href="'.base_url().'admin/administradores021/plantilla">Plantilla</a></li>
            <li class="active">Redes Sociales</li>
        </ol>';		
		$this->parametros['datos']['plantilla'] = $id;
//    $tabla_plantilla_redes_sociales = $this->Modelo->registros($this->config->item('raiz_bd') . 'plantilla_redes_sociales', '', array() );
//		$this->parametros['datos']['lista'] = $tabla_plantilla_redes_sociales;
    
    //$tabla_plantilla = $this->Modelo->registros($this->config->item('raiz_bd') . 'plantilla', 'id, nombre', array(), 'nombre ASC' ); 
    //$this->parametros['datos']['tabla_plantilla'] = $tabla_plantilla;
    
		$this->load->view('plantilla_admin', $this->parametros);			
	}

}