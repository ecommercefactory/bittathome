<?php
defined("BASEPATH") OR exit("No esta permitido el acceso directo a este script.");

class Banner extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->ctrSegAdmin();
	}

	public function index() {
		$this->parametros['plantilla'] = 'datatables';
		$this->parametros['vista'] = 'admin/administradores021/banner';
		$this->parametros['datos']['titulo'] = 'Banner';
		$this->parametros['datos']['subtitulo'] = ''; 
    //$tabla_banner = $this->Modelo->registros($this->config->item('raiz_bd') . 'banner', '', array() );
		//$this->parametros['datos']['lista'] = $tabla_banner;
    
    $tabla_datos_valores_tipo = $this->Modelo->registros($this->config->item('raiz_bd') . 'datos_valores', 'id, nombre', array('dato'=>20), 'orden ASC' ); 
    $this->parametros['datos']['tabla_datos_valores_tipo'] = $tabla_datos_valores_tipo;
    
    $tabla_datos_valores = $this->Modelo->registros($this->config->item('raiz_bd') . 'datos_valores', 'id, nombre', array(), 'nombre ASC' ); 
    $this->parametros['datos']['tabla_datos_valores'] = $tabla_datos_valores;
    
		$this->load->view('plantilla_admin', $this->parametros);			
	}

	public function traerRegistro () {
		$post = $this->input->post(NULL, TRUE);
		$registro = $this->Modelo->registro($this->config->item('raiz_bd') . 'banner', $post['id']);
		$datos = array('registro'=>$registro, 'tksec'=>$this->security->get_csrf_hash());
		echo json_encode($datos);      
	}

  public function traerRutas () {
    $post = $this->input->post(NULL, TRUE);
    
    if ($post['tipo']==1036) { // "Inicio"
        $lista_rutas = "<select class='form-control' id='ruta_".$post['modo']."' name='ruta'><option value='0'>Inicio</option></select>";
    } else {
        if ($post['tipo']==1037) { // "Categoria"
            $tabla_rutas = $this->Modelo->registros($this->config->item('raiz_bd') . 'categorias', '', array() );
            $lista_rutas = "<select class='form-control' id='ruta_".$post['modo']."' name='ruta'><option></option>";
            foreach ($tabla_rutas as $registro) {
                $lista_rutas .= "<option value=" . $registro['id'] . ">" . $registro['nombre'] . "</option>";
            }
            $lista_rutas .= "</select>";          
        }
        if ($post['tipo']==1038) { // "Subcategoria"
            $tabla_rutas = $this->Modelo->registros($this->config->item('raiz_bd') . 'subcategorias', 'id, nombre', array(), 'nombre' );
            $lista_rutas = "<select class='form-control' id='ruta_".$post['modo']."' name='ruta'><option></option>";
            foreach ($tabla_rutas as $registro) {
                $lista_rutas .= "<option value=" . $registro['id'] . ">" . $registro['nombre'] . "</option>";
            }
            $lista_rutas .= "</select>";          
        }
    }
    
    $datos = array('lista_rutas'=>$lista_rutas, 'tksec'=>$this->security->get_csrf_hash());
    echo json_encode($datos);      
  }  
  
	public function ingresar () {
		$post = $this->input->post(NULL, TRUE);
		if (!empty($post)) {
			foreach ($post as $key => $value) {
				$post[$key] = $this->security->xss_clean($value);
			}
      
			$this->form_validation->set_rules('ruta', 'Ruta', 'required|xss_clean');
      $this->form_validation->set_rules('tipo', 'Tipo', 'required|xss_clean');
      //$this->form_validation->set_rules('imgDesktop', 'Imagen Desktop', 'required|xss_clean');
      //$this->form_validation->set_rules('imgMovil', 'Imagen Movil', 'required|xss_clean');    
      
			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.validation_errors().'<br><b>'.$this->lang->line('be_please_try_again').'</b>');
				$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
			} else {

				$config['upload_path'] = './imagenes/banner/desktop/';$config['allowed_types'] = 'gif|jpg|png|jpeg';$config['max_size'] = 2048;$this->load->library('upload', $config); $this->upload->initialize($config);$post['imgDesktop'] = '';if ($this->upload->do_upload('imgDesktop')) {$data_imgDesktop = array('upload_data' => $this->upload->data());$post['imgDesktop'] = $data_imgDesktop['upload_data']['file_name'];}$config['upload_path'] = './imagenes/banner/movil/';$config['allowed_types'] = 'gif|jpg|png|jpeg';$config['max_size'] = 2048;$this->load->library('upload', $config); $this->upload->initialize($config);$post['imgMovil'] = '';if ($this->upload->do_upload('imgMovil')) {$data_imgMovil = array('upload_data' => $this->upload->data());$post['imgMovil'] = $data_imgMovil['upload_data']['file_name'];}
        
				$datos = array('ruta' => $post['ruta'],'tipo' => $post['tipo'],'imgDesktop' => $post['imgDesktop'],'imgMovil' => $post['imgMovil'],);

				$id = $this->Modelo->insertar($this->config->item('raiz_bd') . 'banner', $datos);
				if ($id > 0) {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-check"></i> '.$this->lang->line('be_successful_operation').'</h4>El registro se ingresó exitosamente.'); 
					$this->session->set_flashdata('alertaTipo', 'success'); // success, info, warning, danger
				} else {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No fue posible ingresar el registro.<br>'.$this->lang->line('be_please_try_again'));
					$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
				}     
			}
		} else {
			$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No se recibieron datos.<br>'.$this->lang->line('be_please_try_again'));
			$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
		}
		redirect('/admin/administradores021/banner');
	}

	public function guardar () {
		$post = $this->input->post(NULL, TRUE);
		if (!empty($post)) {
			foreach ($post as $key => $value) {
				$post[$key] = $this->security->xss_clean($value);
			}
      
			$this->form_validation->set_rules('ruta', 'Ruta', 'required|xss_clean');
      $this->form_validation->set_rules('tipo', 'Tipo', 'required|xss_clean');
      //$this->form_validation->set_rules('imgDesktop', 'Imagen Desktop', 'required|xss_clean');
      //$this->form_validation->set_rules('imgMovil', 'Imagen Movil', 'required|xss_clean');    
      
			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.validation_errors().'<br><b>'.$this->lang->line('be_please_try_again').'</b>');
				$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
			} else {

				$config['upload_path'] = './imagenes/banner/desktop/';$config['allowed_types'] = 'gif|jpg|png|jpeg';$config['max_size'] = 2048;$this->load->library('upload', $config); $this->upload->initialize($config);$post['imgDesktop'] = '';if ($this->upload->do_upload('imgDesktop')) {$data_imgDesktop = array('upload_data' => $this->upload->data());$post['imgDesktop'] = $data_imgDesktop['upload_data']['file_name'];}$config['upload_path'] = './imagenes/banner/movil/';$config['allowed_types'] = 'gif|jpg|png|jpeg';$config['max_size'] = 2048;$this->load->library('upload', $config); $this->upload->initialize($config);$post['imgMovil'] = '';if ($this->upload->do_upload('imgMovil')) {$data_imgMovil = array('upload_data' => $this->upload->data());$post['imgMovil'] = $data_imgMovil['upload_data']['file_name'];}

				$datos = array('ruta' => $post['ruta'],'tipo' => $post['tipo'],);

        if ($post['imgDesktop'] != '') $datos['imgDesktop'] = $post['imgDesktop'];
        if ($post['imgMovil'] != '') $datos['imgMovil'] = $post['imgMovil'];
        
				if ($this->Modelo->actualizar($this->config->item('raiz_bd') . 'banner', $datos, $post['id'])) {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-check"></i> '.$this->lang->line('be_successful_operation').'</h4>El registro se guardo exitosamente.'); 
					$this->session->set_flashdata('alertaTipo', 'success'); // success, info, warning, danger
				} else {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No fue posible guardar el registro.<br>'.$this->lang->line('be_please_try_again'));
					$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
				}     

			}
		} else {
			$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No se recibieron datos.<br>'.$this->lang->line('be_please_try_again'));
			$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
		}
		redirect('/admin/administradores021/banner');   
	}

	public function eliminar () {
		$post = $this->input->post(NULL, TRUE);
		if (!empty($post)) {
			foreach ($post as $key => $value) {
				$post[$key] = $this->security->xss_clean($value);
			}
			$this->form_validation->set_rules('id', 'ID', 'required|trim|xss_clean');
			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.validation_errors().'<br><b>'.$this->lang->line('be_please_try_again').'</b>');
				$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
			} else {

				$mat_id = explode(";", $post['id']); 
				$proceso = false;          
				foreach ($mat_id as $id) {
					$this->Modelo->eliminar($this->config->item('raiz_bd') . 'banner', $id);
					$proceso = true;
				}

				if ($proceso) {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-check"></i> '.$this->lang->line('be_successful_operation').'</h4>El(los) registro(s) se eliminó(aron) exitosamente.'); 
					$this->session->set_flashdata('alertaTipo', 'success'); // success, info, warning, danger
				} else {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No fue posible eliminar el(los) registro(s).<br>'.$this->lang->line('be_please_try_again'));
					$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
				}     

			}
		} else {
			$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No se recibieron datos.<br>'.$this->lang->line('be_please_try_again'));
			$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
		}
		redirect('/admin/administradores021/banner');   
	}

  
  
	public function lista () {

		$registros = $this->Modelo->registros($this->config->item('raiz_bd') . 'banner');
    
  	$datosJson = '
  		{	
  			"data":[';  
    $datosJsonReg = '';
    foreach ($registros as $key => $registro) {

          if ($registro['tipo']==1036) { // "Inicio"
              $registro['ruta'] = "Inicio";
          } else {
              if ($registro['tipo']==1037) { // "Categoria"
                  $categoria = $this->Modelo->registro($this->config->item('raiz_bd') . 'categorias', $registro['ruta']);
                  if ($categoria) {
                     $registro['ruta'] = $categoria->nombre;
                  }         
              }
              if ($registro['tipo']==1038) { // "Subcategoria"
                  $subcategoria = $this->Modelo->registro('subcategorias', $registro['ruta'] );                
                  if ($subcategoria) {
                     $registro['ruta'] = $subcategoria->nombre;
                  }                
              }
          }      
   
   
   
          $tipo = $this->Modelo->registro($this->config->item('raiz_bd') . 'datos_valores', $registro['tipo']);
          if ($tipo) {
             $registro['tipo'] = $tipo->nombre;
          }      
       
      
      
          if (trim($registro['imgDesktop']) != '') {
              $registro['imgDesktop'] = '<img src=\"'.base_url().'imagenes/banner/desktop/'.$registro['imgDesktop'].'\" width=\"200px\"  class=\"img-thumbnail\">';
          }  

          if (trim($registro['imgMovil']) != '') {
              $registro['imgMovil'] = '<img src=\"'.base_url().'imagenes/banner/movil/'.$registro['imgMovil'].'\" width=\"200px\"  class=\"img-thumbnail\">';
          }
  
          $datosJsonReg .='["<input type=\"checkbox\" id=\"fila_'.$registro['id'].'\" class=\"seleccion\" cod=\"'.$registro['id'].'\">", "<button type=\"button\" class=\"btn btn-default btn-xs text-light-blue btnEditar\" cod=\"'.$registro['id'].'\"><span class=\"glyphicon glyphicon-pencil\"></span></button>", "<button type=\"button\" class=\"btn btn-default btn-xs text-red btnEliminar\" cod=\"'.$registro['id'].'\"><span class=\"glyphicon glyphicon-trash\"></span></button>","'.$registro['tipo'].'","'.$registro['ruta'].'","'.$registro['imgDesktop'].'","'.$registro['imgMovil'].'","'.$registro['fecha'].'"],';

    }
    if ($datosJsonReg != '') {
          $datosJson .= substr($datosJsonReg, 0, -1);
    }
		
		$datosJson .= ']
		}';

		echo $datosJson;
    
	}
      
  
	public function lista_ssp () {
		$this->load->library('SSP');

		// DB table to use
		$table = 'banner';

		// Table's primary key
		$primaryKey = 'id';

		// Array of database columns
		$columns = array(array( 'db' => 'id', 'dt' => 0, 'field' => 'id', 'formatter' => function($d, $row) {return '<input type="checkbox" id="fila_' . $d . '" class="seleccion" cod="' . $d . '">';}),array( 'db' => 'id', 'dt' => 1, 'field' => 'id', 'formatter' => function($d, $row) {return '<button type="button" class="btn btn-default btn-xs text-light-blue btnEditar" cod="' . $d . '"><span class="glyphicon glyphicon-pencil"></span></button>';}), array( 'db' => 'id', 'dt' => 2, 'field' => 'id', 'formatter' => function($d, $row) {return '<button type="button" class="btn btn-default btn-xs text-red btnEliminar" cod="' . $d . '"><span class="glyphicon glyphicon-trash"></span></button>';}),array( 'db' => 'ruta', 'dt' => 3, 'field' => 'ruta' ),array( 'db' => 'tipo', 'dt' => 4, 'field' => 'tipo' ),array( 'db' => 'imgDesktop', 'dt' => 5, 'field' => 'imgDesktop' ),array( 'db' => 'imgMovil', 'dt' => 6, 'field' => 'imgMovil' ),array( 'db' => 'estado', 'dt' => 7, 'field' => 'estado' ),array( 'db' => 'fecha', 'dt' => 8, 'field' => 'fecha' ),);

		$sql_details = array(
			'user' => $this->db->username,
			'pass' => $this->db->password,
			'db'   => $this->db->database,
			'host' => $this->db->hostname
		);

		$joinQuery = "FROM banner";
		$extraWhere = ""; //"`u`.`valor` >= 90000";
		$groupBy = ""; //"`u`.`datos`";
		$having = ""; //"`u`.`valor` >= 140000";

		$_GET['tksec'] = $this->security->get_csrf_hash();  

		echo json_encode(
			SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having )
		);
	}

}