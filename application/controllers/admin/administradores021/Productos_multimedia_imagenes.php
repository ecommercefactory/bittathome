<?php
defined("BASEPATH") OR exit("No esta permitido el acceso directo a este script.");

class Productos_multimedia_imagenes extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->ctrSegAdmin();
    }

    public function index() {  
        echo "<h3>FIX de imagenes</h3>";
        $max_execution_time = 60*20;
        set_time_limit ( $max_execution_time );
        echo ini_get('max_execution_time');
        echo "<hr>";
        echo dirname ( __FILE__ );
      
/*
        $imagen = '';
        $url = "zktcr20e";
        $ruta = base_url()."imagenes/producto/multimedia/";
        $rutareal = "/home/solucio4/public_html/ts02/imagenes/producto/multimedia/";
        echo "ZKTCR20E: (712x712) " . $imagen;
        echo "<br>";
        echo "<img src='".$ruta.$imagen."' style='border:1px solid grey;'>";

        $this->imagenfix($imagen, $ruta, $rutareal, $url, 700, 700, "grande");
        $this->imagenfix($imagen, $ruta, $rutareal, $url, 350, 350, "mediana");
        $this->imagenfix($imagen, $ruta, $rutareal, $url, 100, 100, "pequena");
        $this->imagenfix($imagen, $ruta, $rutareal, $url, 50, 50, "mini");
*/
    }

    public function lista() {
        $ruta = base_url()."imagenes/producto/multimedia/";
//$rutareal = "/Users/davidamador/Sites/Bittat/BittatHome/imagenes/producto/multimedia/";
        // /Users/davidamador/Sites/Bittat/BittatHome/application/controllers/admin/administradores021
//$rutareal = "/var/www/bittathome.com/htdocs/imagenes/producto/multimedia/";
        // /var/www/bittathome.com/htdocs/application/controllers/admin/administradores021

if ($_SERVER['SERVER_NAME'] == "bittathome.com") {
    $rutareal = "/var/www/bittathome.com/htdocs/imagenes/producto/multimedia/";
} else {
    $rutareal = "/Users/davidamador/Sites/Bittat/BittatHome/imagenes/producto/multimedia/";
}


        $tabla_productos = $this->Modelo->registros($this->config->item('raiz_bd') . 'productos');
        $tabla_productos_multimedia = $this->Modelo->registros($this->config->item('raiz_bd') . 'productos_multimedia' );

        echo "<h3>FIX de imagenes<h3>";
      
        $max_execution_time = 60*20;
        set_time_limit ( $max_execution_time );
        echo "max_execution_time: ";
        echo ini_get('max_execution_time');
        echo "<hr>";
      
        echo "<ul>";
        foreach ($tabla_productos as $reg_producto) {
            if ($reg_producto["id"] >= 77 and $reg_producto["id"] <= 93) {
                $url = $reg_producto["ruta"];
                echo "<li>$reg_producto[id] -> $reg_producto[ruta]";
                echo "<ul>";
                foreach ($tabla_productos_multimedia as $reg_producto_multimedia) {
                    if ($reg_producto["id"] == $reg_producto_multimedia["producto"]) {
                        $imagen = $reg_producto_multimedia["imagen"];
                        $id = $reg_producto_multimedia["id"];
                        echo "<li>$reg_producto_multimedia[id] -> $reg_producto_multimedia[imagen]";  
                        echo "<ul>";

                        $this->imagenfix($imagen, $ruta, $rutareal, $url, $id, 700, 700, "grande");
                        $this->imagenfix($imagen, $ruta, $rutareal, $url, $id, 350, 350, "mediana");
                        $this->imagenfix($imagen, $ruta, $rutareal, $url, $id, 100, 100, "pequena");
                        $this->imagenfix($imagen, $ruta, $rutareal, $url, $id, 50, 50, "mini");                    

                        echo "</ul>";
                        echo "</li>";
                    }
                }      
                echo "</ul>";
                echo "</li>";
            }
        }      
        echo "</ul>";
      
    }  
  
  
    public function imagenfix ($imagen, $ruta, $rutareal, $url, $id, $ancho, $alto, $carpeta) {
        $this->load->library('image_lib');
        $partes_ruta = pathinfo($imagen);
        $filename = $partes_ruta['filename'];
        $extension = $partes_ruta['extension'];
        $config['image_library'] = 'gd2';
        $config['source_image'] = $rutareal . $imagen;
        $config['create_thumb'] = FALSE;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = $ancho;
        $config['height']   = $alto;
        $config['new_image'] = $rutareal . $carpeta . "/" . $url . "-".$id."." . $extension;
        

        $data[$carpeta] = $url . "-".$id."." . $extension;
        $this->Modelo->actualizar($this->config->item('raiz_bd') . 'productos_multimedia', $data, $id);

        $this->image_lib->clear();
        $this->image_lib->initialize($config);
        //$this->image_lib->resize();

        if (!$this->image_lib->resize()) {
            //echo "<br>Error1: (" . $config['source_image'] . ")" . $this->image_lib->display_errors();
            echo "<li>" . $carpeta . "/" . $url . "-".$id."." . $extension . "<hr>".$this->image_lib->display_errors()."</li>";
        } else {
            echo "<li>" . $carpeta . "/" . $url . "-".$id."." . $extension . "</li>";
        }

    }  
  
}      
      