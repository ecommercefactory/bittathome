<?php
defined("BASEPATH") OR exit("No esta permitido el acceso directo a este script.");

class Productos_multimedia extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->ctrSegAdmin();
	}

    function _remap($param) {
        $this->index($param);
    }
  
    public function index($id) {  
    	if (!is_numeric($id)) {
    		redirect('/admin/administradores021/productos');
    	}
		$producto = $this->Modelo->registro($this->config->item('raiz_bd') . 'productos', $id);

		$this->parametros['plantilla'] = 'datatables';
		$this->parametros['vista'] = 'admin/administradores021/productos_multimedia';
		$this->parametros['datos']['titulo'] = 'Multimedia';
		$this->parametros['datos']['subtitulo'] = 'de ' . $producto->nombre . ' (SKU: ' . $producto->sku . ')'; 
        $this->parametros['datos']['breadcrumb'] = '
            <ol class="breadcrumb">
                <li><a href="'.base_url().'admin/administradores021/productos">Productos</a></li>
                <li class="active">Multimedia</li>
            </ol>';			
		$this->parametros['datos']['producto'] = $id; 
/*
    $tabla_productos_multimedia = $this->Modelo->registros($this->config->item('raiz_bd') . 'productos_multimedia', '', array() );
		$this->parametros['datos']['lista'] = $tabla_productos_multimedia;
*/
    $tabla_productos = $this->Modelo->registros($this->config->item('raiz_bd') . 'productos', 'id, nombre', array(), 'nombre ASC' ); 
    $this->parametros['datos']['tabla_productos'] = $tabla_productos;
    
		$this->load->view('plantilla_admin', $this->parametros);			
	}

}