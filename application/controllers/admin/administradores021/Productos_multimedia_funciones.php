<?php
defined("BASEPATH") OR exit("No esta permitido el acceso directo a este script.");

class Productos_multimedia_funciones extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->ctrSegAdmin();
	}
  
    public function index() {  
		echo "Productos_multimedia_funciones";
	}	


	public function traerRegistro () {
		$post = $this->input->post(NULL, TRUE);
		$registro = $this->Modelo->registro($this->config->item('raiz_bd') . 'productos_multimedia', $post['id']);
		$datos = array('registro'=>$registro, 'tksec'=>$this->security->get_csrf_hash());
		echo json_encode($datos);      
	}

	public function ingresar () {
		$post = $this->input->post(NULL, TRUE);
		if (!empty($post)) {
			foreach ($post as $key => $value) {
				$post[$key] = $this->security->xss_clean($value);
			}
      
			$this->form_validation->set_rules('video', 'Video', 'max_length[50]|xss_clean');$this->form_validation->set_rules('imagen', 'Imagen', 'xss_clean');$this->form_validation->set_rules('orden', 'Orden', 'numeric|xss_clean');
      
			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.validation_errors().'<br><b>'.$this->lang->line('be_please_try_again').'</b>');
				$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
			} else {

				$config['upload_path'] = './imagenes/producto/multimedia/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$config['max_size'] = 2048;
				$this->load->library('upload', $config); 
				$this->upload->initialize($config);
				$post['imagen'] = '';
				if ($this->upload->do_upload('imagen')) {
					$data_valor = array('upload_data' => $this->upload->data());
					$post['imagen'] = $data_valor['upload_data']['file_name'];
				}
        
				$datos = array('producto' => $post['producto'],'video' => $post['video'],'imagen' => $post['imagen'],'orden' => $post['orden'],);

				$id = $this->Modelo->insertar($this->config->item('raiz_bd') . 'productos_multimedia', $datos);
				if ($id > 0) {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-check"></i> '.$this->lang->line('be_successful_operation').'</h4>El registro se ingresó exitosamente.'); 
					$this->session->set_flashdata('alertaTipo', 'success'); // success, info, warning, danger

if ($post['imagen'] != '') {
	$ruta = base_url()."imagenes/producto/multimedia/";
	if ($_SERVER['SERVER_NAME'] == "bittathome.com") {
	    $rutareal = "/var/www/bittathome.com/htdocs/imagenes/producto/multimedia/";
	} else {
	    $rutareal = "/Users/davidamador/Sites/Bittat/BittatHome/imagenes/producto/multimedia/";
	}
	$producto = $this->Modelo->registro($this->config->item('raiz_bd') . 'productos', $post['producto']);
	$url = $producto->ruta;
	$imagen = $post['imagen'];
	$this->imagenfix($imagen, $ruta, $rutareal, $url, $id, 700, 700, "grande");
	$this->imagenfix($imagen, $ruta, $rutareal, $url, $id, 350, 350, "mediana");
	$this->imagenfix($imagen, $ruta, $rutareal, $url, $id, 100, 100, "pequena");
	$this->imagenfix($imagen, $ruta, $rutareal, $url, $id, 50, 50, "mini");
}

				} else {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No fue posible ingresar el registro.<br>'.$this->lang->line('be_please_try_again'));
					$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
				}     
			}
		} else {
			$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No se recibieron datos.<br>'.$this->lang->line('be_please_try_again'));
			$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
		}
		redirect('/admin/administradores021/productos_multimedia/' . $post['producto']);
	}

	public function guardar () {
		$post = $this->input->post(NULL, TRUE);
		if (!empty($post)) {
			foreach ($post as $key => $value) {
				$post[$key] = $this->security->xss_clean($value);
			}
      
			$this->form_validation->set_rules('video', 'Video', 'max_length[50]|xss_clean');$this->form_validation->set_rules('imagen', 'Imagen', 'xss_clean');$this->form_validation->set_rules('orden', 'Orden', 'numeric|xss_clean');
      
			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.validation_errors().'<br><b>'.$this->lang->line('be_please_try_again').'</b>');
				$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
			} else {

				$config['upload_path'] = './imagenes/producto/multimedia/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$config['max_size'] = 2048;
				$this->load->library('upload', $config); 
				$this->upload->initialize($config);
				$post['imagen'] = '';
				if ($this->upload->do_upload('imagen')) {
					$data_valor = array('upload_data' => $this->upload->data());
					$post['imagen'] = $data_valor['upload_data']['file_name'];
				}

				$datos = array('producto' => $post['producto'],'video' => $post['video'],'orden' => $post['orden'],);
        		if ($post['imagen'] != '') $datos['imagen'] = $post['imagen'];

				if ($this->Modelo->actualizar($this->config->item('raiz_bd') . 'productos_multimedia', $datos, $post['id'])) {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-check"></i> '.$this->lang->line('be_successful_operation').'</h4>El registro se guardo exitosamente.'); 
					$this->session->set_flashdata('alertaTipo', 'success'); // success, info, warning, danger

if ($post['imagen'] != '') {
	$ruta = base_url()."imagenes/producto/multimedia/";
	if ($_SERVER['SERVER_NAME'] == "bittathome.com") {
	    $rutareal = "/var/www/bittathome.com/htdocs/imagenes/producto/multimedia/";
	} else {
	    $rutareal = "/Users/davidamador/Sites/Bittat/BittatHome/imagenes/producto/multimedia/";
	}
	$producto = $this->Modelo->registro($this->config->item('raiz_bd') . 'productos', $post['producto']);
	$url = $producto->ruta;
	$imagen = $post['imagen'];
	$id = $post['id'];
	$this->imagenfix($imagen, $ruta, $rutareal, $url, $id, 700, 700, "grande");
	$this->imagenfix($imagen, $ruta, $rutareal, $url, $id, 350, 350, "mediana");
	$this->imagenfix($imagen, $ruta, $rutareal, $url, $id, 100, 100, "pequena");
	$this->imagenfix($imagen, $ruta, $rutareal, $url, $id, 50, 50, "mini");
}

				} else {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No fue posible guardar el registro.<br>'.$this->lang->line('be_please_try_again'));
					$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
				}     

			}
		} else {
			$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No se recibieron datos.<br>'.$this->lang->line('be_please_try_again'));
			$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
		}
		redirect('/admin/administradores021/productos_multimedia/' . $post['producto']);
	}

	public function eliminar () {
		$post = $this->input->post(NULL, TRUE);
		if (!empty($post)) {
			foreach ($post as $key => $value) {
				$post[$key] = $this->security->xss_clean($value);
			}
			$this->form_validation->set_rules('id', 'ID', 'required|trim|xss_clean');
			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.validation_errors().'<br><b>'.$this->lang->line('be_please_try_again').'</b>');
				$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
			} else {

				$mat_id = explode(";", $post['id']); 
				$proceso = false;          
				foreach ($mat_id as $id) {
					$this->Modelo->eliminar($this->config->item('raiz_bd') . 'productos_multimedia', $id);
					$proceso = true;
				}

				if ($proceso) {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-check"></i> '.$this->lang->line('be_successful_operation').'</h4>El(los) registro(s) se eliminó(aron) exitosamente.'); 
					$this->session->set_flashdata('alertaTipo', 'success'); // success, info, warning, danger
				} else {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No fue posible eliminar el(los) registro(s).<br>'.$this->lang->line('be_please_try_again'));
					$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
				}     

			}
		} else {
			$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No se recibieron datos.<br>'.$this->lang->line('be_please_try_again'));
			$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
		}
		redirect('/admin/administradores021/productos_multimedia/' . $post['producto']);
	}

  
  
	public function lista ($id) {

		$registros = $this->Modelo->registros($this->config->item('raiz_bd') . 'productos_multimedia', '', array('producto'=>$id) );
    
  	$datosJson = '
  		{	
  			"data":[';  
    $datosJsonReg = '';
    foreach ($registros as $key => $registro) {
      
          if (trim($registro['imagen']) != '') {
              $registro['imagen'] = '<img src=\"'.base_url().'imagenes/producto/multimedia/'.$registro['imagen'].'\" width=\"200px\"  class=\"img-thumbnail\">';
          }
      
          $datosJsonReg .='["<input type=\"checkbox\" id=\"fila_'.$registro['id'].'\" class=\"seleccion\" cod=\"'.$registro['id'].'\">", "<button type=\"button\" class=\"btn btn-default btn-xs text-light-blue btnEditar\" cod=\"'.$registro['id'].'\"><span class=\"glyphicon glyphicon-pencil\"></span></button>", "<button type=\"button\" class=\"btn btn-default btn-xs text-red btnEliminar\" cod=\"'.$registro['id'].'\"><span class=\"glyphicon glyphicon-trash\"></span></button>","'.$registro['imagen'].'","'.$registro['video'].'","'.$registro['orden'].'","'.$registro['fecha'].'"],';

    }
    if ($datosJsonReg != '') {
          $datosJson .= substr($datosJsonReg, 0, -1);
    }
		
		$datosJson .= ']
		}';

		echo $datosJson;
    
	}
      
  
	public function lista_ssp () {
		$this->load->library('SSP');

		// DB table to use
		$table = 'productos_multimedia';

		// Table's primary key
		$primaryKey = 'id';

		// Array of database columns
		$columns = array(array( 'db' => 'id', 'dt' => 0, 'field' => 'id', 'formatter' => function($d, $row) {return '<input type="checkbox" id="fila_' . $d . '" class="seleccion" cod="' . $d . '">';}),array( 'db' => 'id', 'dt' => 1, 'field' => 'id', 'formatter' => function($d, $row) {return '<button type="button" class="btn btn-default btn-xs text-light-blue btnEditar" cod="' . $d . '"><span class="glyphicon glyphicon-pencil"></span></button>';}), array( 'db' => 'id', 'dt' => 2, 'field' => 'id', 'formatter' => function($d, $row) {return '<button type="button" class="btn btn-default btn-xs text-red btnEliminar" cod="' . $d . '"><span class="glyphicon glyphicon-trash"></span></button>';}),array( 'db' => 'producto', 'dt' => 3, 'field' => 'producto' ),array( 'db' => 'tipo', 'dt' => 4, 'field' => 'tipo' ),array( 'db' => 'valor', 'dt' => 5, 'field' => 'valor' ),array( 'db' => 'orden', 'dt' => 6, 'field' => 'orden' ),array( 'db' => 'fecha', 'dt' => 7, 'field' => 'fecha' ),);

		$sql_details = array(
			'user' => $this->db->username,
			'pass' => $this->db->password,
			'db'   => $this->db->database,
			'host' => $this->db->hostname
		);

		$joinQuery = "FROM productos_multimedia";
		$extraWhere = ""; //"`u`.`valor` >= 90000";
		$groupBy = ""; //"`u`.`datos`";
		$having = ""; //"`u`.`valor` >= 140000";

		$_GET['tksec'] = $this->security->get_csrf_hash();  

		echo json_encode(
			SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having )
		);
	}

    public function imagenfix ($imagen, $ruta, $rutareal, $url, $id, $ancho, $alto, $carpeta) {
        $this->load->library('image_lib');
        $partes_ruta = pathinfo($imagen);
        $filename = $partes_ruta['filename'];
        $extension = $partes_ruta['extension'];
        $config['image_library'] = 'gd2';
        $config['source_image'] = $rutareal . $imagen;
        $config['create_thumb'] = FALSE;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = $ancho;
        $config['height']   = $alto;
        $config['new_image'] = $rutareal . $carpeta . "/" . $url . "-".$id."." . $extension;

        $data[$carpeta] = $url . "-".$id."." . $extension;
        $this->Modelo->actualizar($this->config->item('raiz_bd') . 'productos_multimedia', $data, $id);

        $this->image_lib->clear();
        $this->image_lib->initialize($config);
        $this->image_lib->resize();

    }  



}