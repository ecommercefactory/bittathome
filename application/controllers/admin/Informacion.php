<?php
defined('BASEPATH') OR exit('No esta permitido el acceso directo a este script.');
/**
 * @autor: Ing. David Amador - r.reyes@bittathome.com
 * @nombre: xxxxx
 * @version: 1.1
 * @fecha: Septiembre 2017 
 * @descripcion: EditArea
 * */

class Informacion extends MY_Controller {

    var $parametros;

    public function __construct() {
        parent::__construct();
				$this->ctrSegAdmin();
    }

    public function index() {
        $this->parametros['plantilla'] = 'informacion';
        $this->parametros['vista'] = 'admin/informacion';
        $this->parametros['datos']['titulo'] = 'Juummm...';
        $this->parametros['datos']['informacion'] = 'Acceso no autorizado.';
			
        $this->load->view('plantilla_admin', $this->parametros);
	}

}