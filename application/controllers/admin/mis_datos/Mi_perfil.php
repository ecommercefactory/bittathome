<?php
defined("BASEPATH") OR exit("No esta permitido el acceso directo a este script.");

class Mi_perfil extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->ctrSegAdmin();
	}

	public function index() {
		$this->parametros['plantilla'] = 'datatables';
		$this->parametros['vista'] = 'admin/mis_datos/mi_perfil';
		$this->parametros['datos']['titulo'] = 'Mi Perfil';
		$this->parametros['datos']['subtitulo'] = ''; 

    $tabla_datos_valores_perfil = $this->Modelo->registros($this->config->item('raiz_bd') . 'datos_valores', 'id, nombre', array('dato'=>2), 'nombre ASC' ); 
    $this->parametros['datos']['tabla_datos_valores_perfil'] = $tabla_datos_valores_perfil;
    
    $tabla_languages = $this->Modelo->registros($this->config->item('raiz_bd') . 'languages', 'id, name', array('active'=>4), 'name ASC' ); 
    $this->parametros['datos']['tabla_languages'] = $tabla_languages;
    
    $tabla_paises = $this->Modelo->registros($this->config->item('raiz_bd') . 'paises', 'id, nombre_en', array(), 'nombre_en ASC' ); 
    $this->parametros['datos']['tabla_paises'] = $tabla_paises;
    
    $tabla_datos_valores_estado = $this->Modelo->registros($this->config->item('raiz_bd') . 'datos_valores', 'id, nombre', array('dato'=>1), 'nombre ASC' );
    $this->parametros['datos']['tabla_datos_valores_estado'] = $tabla_datos_valores_estado;

		$this->load->view('plantilla_admin', $this->parametros);			

	}

	public function traerRegistro () {
		//$post = $this->input->post(NULL, TRUE);
    // $this->session->userdata($this->config->item('raiz') . 'be_usuario_id')
		$registro = $this->Modelo->registro($this->config->item('raiz_bd') . 'administradores', $this->session->userdata($this->config->item('raiz') . 'be_usuario_id'));
		$datos = array('registro'=>$registro, 'tksec'=>$this->security->get_csrf_hash());
		echo json_encode($datos);      
	}

	public function guardar () {
		$post = $this->input->post(NULL, TRUE);
		if (!empty($post)) {
			foreach ($post as $key => $value) {
				$post[$key] = $this->security->xss_clean($value);
			}
      
			$this->form_validation->set_rules('nombre', 'Nombre', 'required|min_length[0]|max_length[50]|xss_clean');$this->form_validation->set_rules('apellidos', 'Apellidos', 'required|min_length[0]|max_length[50]|xss_clean');
      
      //$this->form_validation->set_rules('perfil', 'Perfil', 'required|xss_clean');
      
      //$this->form_validation->set_rules('correo', 'Correo', 'required|max_length[70]|valid_email|xss_clean');
      
      
      $this->form_validation->set_rules('foto', 'Foto', 'xss_clean');$this->form_validation->set_rules('language', 'Language', 'required|xss_clean');$this->form_validation->set_rules('fecha_nacimiento', 'Fecha Nacimiento', 'xss_clean');$this->form_validation->set_rules('pais', 'Pais', 'required|xss_clean');$this->form_validation->set_rules('departamento', 'Departamento', 'max_length[50]|xss_clean');$this->form_validation->set_rules('ciudad', 'Ciudad', 'max_length[50]|xss_clean');$this->form_validation->set_rules('telefono', 'Telefono', 'max_length[50]|xss_clean');$this->form_validation->set_rules('facebook', 'Facebook', 'max_length[50]|xss_clean');$this->form_validation->set_rules('whatsapp', 'Whatsapp', 'max_length[50]|xss_clean');$this->form_validation->set_rules('skype', 'Skype', 'max_length[50]|xss_clean');$this->form_validation->set_rules('instagram', 'Instagram', 'max_length[50]|xss_clean');$this->form_validation->set_rules('google', 'Google', 'max_length[50]|xss_clean');$this->form_validation->set_rules('linkdn', 'Linkdn', 'max_length[50]|xss_clean');
      
      //$this->form_validation->set_rules('estado', 'Estado', 'required|xss_clean');
      
			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.validation_errors().'<br><b>'.$this->lang->line('be_please_try_again').'</b>');
				$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
			} else {

				$config['upload_path'] = './imagenes/administrador/';$config['allowed_types'] = 'gif|jpg|png|jpeg';$config['max_size'] = 2048;$this->load->library('upload', $config); $this->upload->initialize($config);$post['foto'] = '';if ($this->upload->do_upload('foto')) {$data_foto = array('upload_data' => $this->upload->data());$post['foto'] = $data_foto['upload_data']['file_name'];}

				$datos = array('nombre' => $post['nombre'],'apellidos' => $post['apellidos'],'language' => $post['language'],'fecha_nacimiento' => $post['fecha_nacimiento'],'pais' => $post['pais'],'departamento' => $post['departamento'],'ciudad' => $post['ciudad'],'telefono' => $post['telefono'],'facebook' => $post['facebook'],'whatsapp' => $post['whatsapp'],'skype' => $post['skype'],'instagram' => $post['instagram'],'google' => $post['google'],'linkdn' => $post['linkdn'],);

        if ($post['foto'] != '') $datos['foto'] = $post['foto'];        
        
				if ($this->Modelo->actualizar($this->config->item('raiz_bd') . 'administradores', $datos, $post['id'])) {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-check"></i> '.$this->lang->line('be_successful_operation').'</h4>El registro se guardo exitosamente.'); 
					$this->session->set_flashdata('alertaTipo', 'success'); // success, info, warning, danger
				} else {
					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No fue posible guardar el registro.<br>'.$this->lang->line('be_please_try_again'));
					$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
				}     

			}
		} else {
			$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No se recibieron datos.<br>'.$this->lang->line('be_please_try_again'));
			$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
		}
		redirect('/admin/mis_datos/mi_perfil');   
	}

}