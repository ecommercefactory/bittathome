<?php
defined('BASEPATH') OR exit('No esta permitido el acceso directo a este script.');

class Perfil extends MY_Controller {

	public $usuario = 0;
  
    public function __construct() {
        parent::__construct();
				$this->ctrSegAdmin();
				$this->load->library('grocery_CRUD');
    }

	  public function index() {
        $crud = new grocery_CRUD();
				$crud->set_language($this->session->userdata($this->config->item('raiz') . 'be_lang_value'));
			
				$idUsuario = $this->uri->segment(6);
				$tablaUsuario = $this->session->userdata($this->config->item('raiz') . 'be_usuario_tabla');

				if (trim($idUsuario) != '') {
						if ( is_numeric($idUsuario) ) {
							if ($this->session->userdata($this->config->item('raiz') . 'be_usuario_id') != $idUsuario) redirect('admin/informacion');
						} else {
							redirect('admin/informacion');
						}
				}   
            
        $crud->set_table($this->config->item('raiz_bd') . $tablaUsuario);
        $crud->where($this->config->item('raiz_bd') . $tablaUsuario . '.id', $this->session->userdata($this->config->item('raiz') . 'be_usuario_id'));			
			
        $crud->columns('perfil', 'nombre', 'apellidos', 'correo', 'language');     
				$crud->set_relation('perfil', $this->config->item('raiz_bd') . 'datos_valores', 'nombre', 'dato = (SELECT id FROM ' . $this->config->item('raiz_bd') . 'datos WHERE codigo = "perfiles") AND auxiliar_1 = "'.$tablaUsuario.'"', 'orden ASC');
				$crud->set_relation('language', $this->config->item('raiz_bd') . 'languages', 'name');
        $crud->set_field_upload('foto','assets/fotos/' . $tablaUsuario);
				$crud->fields('nombre', 'apellidos', 'perfil', 'correo', 'foto', 'language', 'fecha_nacimiento', 'pais', 'departamento', 'ciudad', 'telefono', 'facebook', 'whatsapp', 'skype', 'instagram', 'google', 'linkdn');

        $crud->unset_add();
        $crud->unset_delete();
        $crud->unset_read();
        $crud->unset_print();
        $crud->unset_export();
        $crud->unset_list();
				$crud->unset_back_to_list();

			 	$state_code = $crud->getState();
			 	if($state_code == 'edit') {
						$crud->change_field_type('perfil', 'readonly');
						$crud->change_field_type('correo', 'readonly');
			 	}					

				$crud->callback_before_update(array($this,'xss_clean'));
				$crud->callback_before_insert(array($this,'xss_clean'));			
			
				try {
						$crudTabla = $crud->render();
						$this->crudver($crudTabla, 'Perfil');
				} catch(Exception $e) {
						if($e->getCode() == 14) {
								redirect ('/no_autorizado');
						} else {
								show_error($e->getMessage());
						}
				}		

    }

		function xss_clean($post_array, $primary_key = null) {
				foreach ($post_array as $key => $value) {
						$post_array[$key] = $this->security->xss_clean($value);
				}
				return $post_array;
		}
	
	
/*
    function _remap($method,$args) {
        if (method_exists($this, $method)) {
               $this->$method($args);
        } else {
            $this->index($method,$args);
        }
    }
	
    public function index($method, $args) {
        $crud = new grocery_CRUD();

				$idUsuario = $method;
				$idTabla = $args[0];			
			
				if ( is_numeric($idUsuario) ) {
					if ($this->session->userdata($this->config->item('raiz') . 'be_usuario_id') != $idUsuario) redirect('admin/informacion');
				} else {
					redirect('admin/informacion');
				}
				if ( !is_numeric($idTabla) ) {
					redirect('admin/informacion');
				}
			
        $crud->set_table('usuarios');
        $crud->where('usuarios.id', $this->session->userdata($this->config->item('raiz') . 'be_usuario_id'));			
			
        $crud->columns('perfil', 'nombre', 'apellidos', 'correo');     
				$crud->set_relation('perfil', 'datos_valores', 'nombre', 'dato = (SELECT id FROM datos WHERE codigo = "perfiles_administrativos")', 'nombre ASC');
        $crud->set_field_upload('foto','assets/fotos/usuarios');
				$crud->unset_fields('clave', 'fecha_ingreso', 'fecha_ultimo_ingreso', 'ultima_ip', 'estado');

        $crud->unset_add();
        $crud->unset_delete();
        $crud->unset_read();
        $crud->unset_print();
        $crud->unset_export();

			 	$state_code = $crud->getState();
			 	if($state_code == 'edit') {
						$crud->change_field_type('perfil', 'readonly');
						$crud->change_field_type('correo', 'readonly');
			 	}					
			
        $crudTabla = $crud->render();
        $this->crudver($crudTabla, 'Perfil');
    }

	public function actividad($id) {
        $this->usuario = $id;
        $crud = new grocery_CRUD();
        $crud->set_table('usuarios_actividad');
        $crud->where('usuario', $id);			
        $crud->columns('fecha_ingreso', 'ip');
        $crud->unset_operations();
        $crud->order_by('fecha_ingreso','desc');
        $crudTabla = $crud->render();

        // --------------------------------------------------------------
        $nombreUsuario= '';
        $usuario = $this->Modelo->registro('usuarios', $id);
        if ( count($usuario) ) {
            $nombreUsuario = $usuario->nombre . " " . $usuario->apellidos;
        } else {
            $nombreUsuario = "Usuario no encontrado";
        }
        // --------------------------------------------------------------

				$this->crudver($crudTabla, 'Actividad del usuario "' . $nombreUsuario . '"');
    }
*/
}