<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cambiar_clave extends MY_Controller {

    public $usuario = 0;
    public $tabla = '';
    public $parametros;	
  
    public function __construct() {
        parent::__construct();
				$this->ctrSegAdmin();
    }
	
	public function index() {
			
        if ($this->session->userdata($this->config->item('raiz') . 'be_usuario_fecha_cambio_clave') != '0000-00-00') {
					if ( $this->session->flashdata('alertaMensaje') ) {
					} else {
							if ($this->session->userdata($this->config->item('raiz') . 'be_usuario_fecha_cambio_clave') <= $this->config->item('Ymd')) {
								$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-warning"></i> Atencion!</h4>Debes realizar el cambio de tu clave.');
								$this->session->set_flashdata('alertaTipo', 'info'); // success, info, warning, danger
							}			
					}
				}
			
				$this->parametros['plantilla'] = 'formulario';
        $this->parametros['vista'] = 'admin/mis_datos/cambiar_clave';
        $this->parametros['datos']['titulo'] = 'Cambiar Clave';
        $this->parametros['datos']['subtitulo'] = '';
        $this->load->view('plantilla_admin', $this->parametros);			
		}
		
	public function proceso() {
     //$post = $this->input->post();
		 $post = $this->input->post(NULL, TRUE);
     if (!empty($post)) {
        foreach ($post as $key => $value) {
            $post[$key] = $this->security->xss_clean($value);
        }
        $this->form_validation->set_rules('clave_actual', 'Clave Actual', 'required|trim|xss_clean');
        $this->form_validation->set_rules('clave', 'Clave', 'min_length[7]|max_length[20]|required|trim|xss_clean');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.validation_errors().'<br>'.$this->lang->line('be_please_try_again'));
            $this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
        } else {
					
						$this->usuario = $this->session->userdata($this->config->item('raiz') . 'be_usuario_id');
						$this->tabla = $this->session->userdata($this->config->item('raiz') . 'be_usuario_tabla');			
					
						$usuario = $this->Modelo->registro($this->config->item('raiz_bd') . $this->tabla, $this->usuario);
						if ($usuario->clave == sha1($post['clave_actual'])) {
								$datos['clave'] = sha1($post['clave']);
								$datos['fecha_cambio_clave'] = $this->config->item('Y6md');
								$this->Modelo->actualizar($this->config->item('raiz_bd') . $this->tabla, $datos, $this->usuario);

								$this->session->set_userdata($this->config->item('raiz') . 'be_usuario_fecha_cambio_clave', $this->config->item('Y6md'));
							
								$datosCorreo['nombre'] = $usuario->nombre;
								$datosCorreo['apellidos'] = $usuario->apellidos;
								$vista = $this->load->view('admin/correos/nueva_clave_actualizada', $datosCorreo, true);
								//$this->load->library('utilities');	
								if ($this->utilities->sendEmail($usuario->correo, 'Nueva Clave Actualizada', $vista, 'r.reyes@bittathome.com')) {
										$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-check"></i> '.$this->lang->line('be_successful_operation').'</h4>La clave se ha actualizado.'); 
										$this->session->set_flashdata('alertaTipo', 'success'); // success, info, warning, danger
								} else {
										$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>La clave se ha actualizado. No fue posible enviar el correo de confirmación del proceso.<br>'.$this->lang->line('be_please_try_again'));
										$this->session->set_flashdata('alertaTipo', 'info'); // success, info, warning, danger
								}							
/*
			// -----------------------------------------------------------------------------------
								$this->load->library('email');
								$config['protocol']    = 'smtp';
								$config['smtp_host']    = 'ssl://smtp.gmail.com';
								$config['smtp_port']    = '465';
								$config['smtp_timeout'] = '7';
								$config['smtp_user']    = 'r.reyes@bittathome.com';
								$config['smtp_pass']    = '@madoR1974';
								$config['charset']    = 'utf-8'; // 'iso-8859-1'; //'utf-8';
								$config['newline']    = "\r\n";
								$config['mailtype'] = 'html'; // text or html
								$config['validation'] = TRUE; // bool whether to validate email or not      
								$this->email->initialize($config);		
								$this->email->set_mailtype("html"); 
								$this->email->from($config['smtp_user'], 'Solucionaticos');
								$this->email->bcc('r.reyes@bittathome.com'); 
								$this->email->to($usuario->correo); 
								$this->email->subject('Nueva Clave Actualizada');
								$this->email->message($vista);  
								$this->email->send();
			// -----------------------------------------------------------------------------------  
								$this->Modelo->actualizar($this->config->item('raiz_bd') . $this->tabla, $datos, $this->usuario);
								$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-check"></i> '.$this->lang->line('be_successful_operation').'</h4>La clave se ha actualizado.'); 
								$this->session->set_flashdata('alertaTipo', 'success'); // success, info, warning, danger
*/
						} else {
								$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>La clave actual no coincide<br>'.$this->lang->line('be_please_try_again'));
								$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
						}
        }
        redirect('/admin/mis_datos/cambiar_clave');
     }

		}
	
/*
    function _remap($method,$args) {
        if (method_exists($this, $method)) {
               $this->$method($args);
        } else {
            $this->index($method,$args);
        }
    }  

    public function index ($method, $args) {  
				$this->usuario = $method;
				$this->tabla = $args[0];
        echo "Al cambiar la clave se debe indicar de que tabla se va a hacer (lo mismo para el caso del perfil)", "<hr>";
        echo "Usuario: ", $this->usuario, "<hr>";
        echo "Tabla: ", $this->tabla, "<hr>";
        if (isset($args[1])) echo $args[1], "<hr>";
		}
  
    public function kilo ($args) {
        if (isset($args[0])) echo $args[0], "<hr>";
        if (isset($args[1])) echo $args[1], "<hr>";
        if (isset($args[2])) echo $args[2], "<hr>";
    }
*/
}
  