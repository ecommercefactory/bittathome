<?php
defined('BASEPATH') OR exit('No esta permitido el acceso directo a este script.');
/**
 * @autor: Ing. David Amador - r.reyes@bittathome.com
 * @nombre: xxxxx
 * @version: 1.1
 * @fecha: Septiembre 2017 
 * @descripcion: PanelControl
 * */

class Panelcontrol extends MY_Controller {

    public function __construct() {
        parent::__construct();
		$this->ctrSegAdmin();
    }

    public function index() {
        $this->parametros['plantilla'] = 'panelcontrol2';
        $this->parametros['vista'] = 'admin/panelcontrol2';
        $this->parametros['datos']['titulo'] = 'Panel de Control';
        $this->parametros['datos']['subtitulo'] = '';
        $this->parametros['datos']['breadcrumb'] = '';
        $this->load->view('plantilla_admin', $this->parametros);
	}

}