<?php
if (count($tabla_productos)) {
    foreach ($tabla_productos as $registro) {

        $imgCtr = false;
        foreach ($tabla_productos_multimedia as $registro2) {
            if ($registro2['producto'] == $registro['id']) {
            	$imgCtr = true;
            	if (trim($registro2['imagen']) != '') {
					echo $registro2['mediana'] 
					echo $version;
            	} else {
					echo $version;
            	}
                break;
            }
        }
        if (!$imgCtr) {
			echo $version;
    	}

		$class_deseos = 'deseos';
		if ($this->session->has_userdata($this->config->item('raiz') . 'fe_lista_deseos')) {
		    foreach ($this->session->userdata($this->config->item('raiz') . 'fe_lista_deseos') as $key => $value) {
		        if ($registro['id'] == $key) {
					$class_deseos = 'deseos_sel';
		        	break;
		        }
		    }
		}
		echo $class_deseos; 

		echo $registro['id']; 
		echo $registro['ruta']; 
		echo $registro['nombre'];

		echo number_format($registro['precio'],0); 

    }
} else {
	// Si no hay datos debe traer otra vista

?>		
			<div style='color:#e59200;font-size: 16px;margin-bottom:200px;'>No se ha encontrado ningún producto con esta búsqueda.</div>
<?php
}
?>       