<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('cookie');
    }

    public function index() {
        echo "Ajax Index";
    }

    public function lista_deseos() {

        $post = $this->input->post(NULL, TRUE);

        $lista_deseos = array();

        if ($this->session->has_userdata($this->config->item('raiz') . 'fe_lista_deseos')) {
            foreach ($this->session->userdata($this->config->item('raiz') . 'fe_lista_deseos') as $key => $value) {
                $lista_deseos[$key] = $value;
            }
        }
        if (isset( $lista_deseos[$post['producto']] )) {
            unset($lista_deseos[$post['producto']]);
        } else {
            $lista_deseos[$post['producto']] = $post['producto'];
        }

        $this->session->set_userdata($this->config->item('raiz') . 'fe_lista_deseos', $lista_deseos);

        echo count($lista_deseos);

    }


    public function enviar_cotizacion() {
        $post = $this->input->post(NULL, TRUE);

        $datosCorreo['tipo_intalacion'] = $post['tipo_intalacion'];
        $datosCorreo['metraje'] = $post['metraje'];
        $datosCorreo['altura'] = $post['altura'];
        $datosCorreo['ciudad'] = $post['ciudad'];
        $datosCorreo['otra_ciudad'] = $post['otra_ciudad'];

        $vista = $this->load->view('tienda/correos/enviar_cotizacion', $datosCorreo, true);
        if ($this->utilities->sendEmail($this->config->item('correo_escribenos'), 'Te han dejado una cotización', $vista, 'r.reyes@bittathome.com')) {
            echo "Enviado";
        } else {
            echo "No Enviado";
        }

    }

    public function escribenos() {
        $post = $this->input->post(NULL, TRUE);
        $datosCorreo['nombres'] = $post['nombres'];
        $datosCorreo['email'] = $post['email'];
        $datosCorreo['telefono'] = $post['telefono'];
        $datosCorreo['mensaje'] = $post['mensaje'];
        $vista = $this->load->view('tienda/correos/escribenos', $datosCorreo, true);
        if ($this->utilities->sendEmail($this->config->item('correo_escribenos'), 'Te han dejado un mensaje', $vista, 'r.reyes@bittathome.com')) {
                //$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-check"></i> '.$this->lang->line('be_successful_operation').'</h4>La clave se ha actualizado.'); 
                //$this->session->set_flashdata('alertaTipo', 'success'); // success, info, warning, danger
            echo "Enviado";
        } else {
                //$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>La clave se ha actualizado. No fue posible enviar el correo de confirmación del proceso.<br>'.$this->lang->line('be_please_try_again'));
                //$this->session->set_flashdata('alertaTipo', 'info'); // success, info, warning, danger
            echo "No Enviado";
        }

    }

    public function carrito() {

        if (!$this->session->has_userdata($this->config->item('raiz') . 'fe_sesion_tienda')) {

            echo -1;

        } else {

            $post = $this->input->post(NULL, TRUE);

            $cantidad = 0;

            if ($this->session->has_userdata($this->config->item('raiz') . 'fe_carrito')) {
                foreach ($this->session->userdata($this->config->item('raiz') . 'fe_carrito') as $key => $value) {
                    $carrito_compras[$key] = $value;
                    $cantidad += $value;
                }
            }

            if (isset($carrito_compras[$post['producto']])) {
                $carrito_compras[$post['producto']] += $post['cantidad'];
            } else {
                $carrito_compras[$post['producto']] = $post['cantidad'];
            }
            $cantidad += ($post['cantidad'] * 1);
            
            if ($post['cantidad'] == 0) {
                unset($carrito_compras[$post['producto']]);
            }

            if (isset($carrito_compras)) {
                $this->session->set_userdata($this->config->item('raiz') . 'fe_carrito', $carrito_compras);
            } else {
                if ($this->session->has_userdata($this->config->item('raiz') . 'fe_carrito')) {
                    $this->session->unset_userdata($this->config->item('raiz') . 'fe_carrito');
                }
            }

            echo $cantidad;

        }

    }

    public function carrito_operaciones() {

        if (!$this->session->has_userdata($this->config->item('raiz') . 'fe_sesion_tienda')) {

            echo -1;

        } else {

            $post = $this->input->post(NULL, TRUE);

            $cantidad = 0;

            if ($this->session->has_userdata($this->config->item('raiz') . 'fe_carrito')) {
                foreach ($this->session->userdata($this->config->item('raiz') . 'fe_carrito') as $key => $value) {
                    $carrito_compras[$key] = $value;
                }
            }

            $carrito_compras[$post['producto']] = $post['cantidad'];

            $cantidad = ($post['cantidad'] * 1);
            
            if ($cantidad == 0) {
                unset($carrito_compras[$post['producto']]);
            }

            if (isset($carrito_compras)) {
                $this->session->set_userdata($this->config->item('raiz') . 'fe_carrito', $carrito_compras);
            } else {
                if ($this->session->has_userdata($this->config->item('raiz') . 'fe_carrito')) {
                    $this->session->unset_userdata($this->config->item('raiz') . 'fe_carrito');
                }
            }

            echo 1;

        }

    }

    public function remover_carrito() {
        $this->session->unset_userdata($this->config->item('raiz') . 'fe_sesion_tienda');
        $this->session->unset_userdata($this->config->item('raiz') . 'fe_carrito');
        echo "Remover Carrito";
    }

    public function olvido_clave() {
        $post = $this->input->post(NULL, TRUE);

        $correo = trim($post['correo']);
        $tabla_usuario = $this->Modelo->registro($this->config->item('raiz_bd') . 'usuarios', $correo, 'email');

        if (count($tabla_usuario)) {
            if ($tabla_usuario->activo == 4) {
                $codigo = $this->utilities->code(20);
                $datos['codigo_clave_cambio'] = $codigo;
                $datos['fecha_clave_cambio'] = $this->config->item('Ymd2His');
                if ( $this->Modelo->actualizar($this->config->item('raiz_bd') . 'usuarios', $datos, $tabla_usuario->id) ) {
                    $datosCorreo['nombre'] = $tabla_usuario->nombre;
                    $datosCorreo['codigo'] = $codigo;
                    $vista = $this->load->view('tienda/correos/olvido_clave', $datosCorreo, true);
                    if ($this->utilities->sendEmail($correo, 'Solicitud de Nueva Contraseña', $vista, 'r.reyes@bittathome.com')) {
                        echo "Enviado";
                    } else {
                        echo "No Enviado";
                    }
                } else {
                    echo "No Enviado";
                }
            } else {
                echo "No Enviado Inactivo";
            }
        } else {
            echo "No Enviado";
        }

    }


}  