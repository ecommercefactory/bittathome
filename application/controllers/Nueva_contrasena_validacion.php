<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nueva_contrasena_validacion extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->helper('cookie');
		$this->session->set_userdata($this->config->item('raiz') . 'fe_sesion_tienda', 1);

        if (!$this->session->has_userdata($this->config->item('raiz') . 'fe_codigo_referencia_pago')) {
            $referenceCode = rand(1000000,100000000);
            $this->session->set_userdata($this->config->item('raiz') . 'fe_codigo_referencia_pago', $referenceCode);
        }
// ----
        $tabla_pre_orden = $this->Modelo->registro($this->config->item('raiz_bd') . 'ordenes', $this->session->userdata($this->config->item('raiz') . 'fe_codigo_referencia_pago'), 'codigo_referencia_pago');
        if (count($tabla_pre_orden)) {
            if ($tabla_pre_orden->proceso_referencia == 1) {
                // actualizar datos de la orden
                $codigo = 0;
                $tabla_ordenes_tipos = $this->Modelo->registro($this->config->item('raiz_bd') . 'ordenes_tipos', 1);
                $codigo = $tabla_ordenes_tipos->secuencia_num + 1;
                $datosOT = array(
                    'secuencia_num' => $codigo,
                    'fecha_secuencia' => $this->config->item('YmdHis'),
                );
                $this->Modelo->actualizar($this->config->item('raiz_bd') . 'ordenes_tipos', $datosOT, 2);
                $datos = array(
                    'tipo_orden' => 1,
                    'codigo' => $codigo,);
                $idPreOrden = $this->Modelo->actualizar($this->config->item('raiz_bd') . 'ordenes', $datos, $this->session->userdata($this->config->item('raiz') . 'fe_codigo_referencia_pago'), 'codigo_referencia_pago');

                // Ajuste de inventarios
                $tabla_ordenes_productos = $this->Modelo->registros($this->config->item('raiz_bd') . 'ordenes_productos', '', array('orden'=>$tabla_pre_orden->id));      
                if (count($tabla_ordenes_productos) > 0) {
                    foreach ($tabla_ordenes_productos as $reg_op) {
                        $inventario = 0;
                        $tabla_productos = $this->Modelo->registro($this->config->item('raiz_bd') . 'productos', $reg_op['producto']);
                        $inventario = $tabla_productos->inventario;
                        $inventario = $inventario - $reg_op['cantidad'];                        
                        $datos_p = array('inventario' => $inventario);
                        $this->Modelo->actualizar($this->config->item('raiz_bd') . 'productos', $datos_p, $reg_op['producto']);
                    }
                }

                // Enviar Correo a: servicioalcliente@bittathome.com y r.reyes@bittathome.com
                $datosCorreo['orden'] = $tabla_pre_orden->id;
                $vista = $this->load->view('tienda/correos/orden_venta_nueva', $datosCorreo, true);
                $this->utilities->sendEmail(
                    'servicioalcliente@bittathome.com', 
                    'Nueva Orden de Venta Creada', 
                    $vista, 
                    'r.reyes@bittathome.com');
                
                // refrescar datos
                $this->session->unset_userdata($this->config->item('raiz') . 'fe_carrito');
                $referenceCode = rand(1000000,100000000);
                $this->session->set_userdata($this->config->item('raiz') . 'fe_codigo_referencia_pago', $referenceCode);
            }
        }


	}


	public function index() {

	    $this->utilities->force_ssl();
    
		$post = $this->input->post(NULL, TRUE);
		if (!empty($post)) {
			foreach ($post as $key => $value) {
				$post[$key] = $this->security->xss_clean($value);
			}

			if ($_SERVER['SERVER_NAME'] == "bittathome.com") {
				$recaptcha = $post['g-recaptcha-response'];
				if ($recaptcha != '') {
					$secret = $this->config->item('recaptcha_secret');
					$ip = $this->input->ip_address();
					$var = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secret&response=$recaptcha&remoteip=$ip");
					$array = json_decode($var, true);
				}
			} else {
				$recaptcha = 'kilo';
				$array['success'] = true;
			}


			if ($recaptcha != '') {
				if ($array['success']) {
					$this->form_validation->set_rules('password', 'Contraseña', 'min_length[7]|max_length[20]|required|trim|xss_clean');
					if ($this->form_validation->run() == FALSE) {
						$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.validation_errors().'<br>'.$this->lang->line('be_please_try_again'));
                  		$this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
                  		redirect('nueva_contrasena/' . $post['codigo']);           
              		} else {

              			$usuario = $this->Modelo->registros($this->config->item('raiz_bd') . 'usuarios', '', array('codigo_clave_cambio'=>$post['codigo']) );

		              	if (count($usuario)) {
		              		if ( $usuario[0]['fecha_clave_cambio'] > $this->config->item('YmdHis') ) {

		              			$datos['password'] = sha1($post['password']);
		              			$datos['codigo_clave_cambio'] = "";
		              			$datos['fecha_clave_cambio'] = $this->config->item('YmdHis');
		              			$datos['fecha_cambio_clave'] = $this->config->item('Y6md');

		              			if ( $this->Modelo->actualizar($this->config->item('raiz_bd') . 'usuarios', $datos, $usuario[0]['id']) ) {
		              // -----------------------------------------------------------------------------------
		              				$datosCorreo['nombre'] = $usuario[0]['nombre'];
		              				$vista = $this->load->view('tienda/correos/nueva_clave_actualizada', $datosCorreo, true);

		              				if ($this->utilities->sendEmail($usuario[0]['email'], $this->lang->line('be_new_updated_password'), $vista, 'r.reyes@bittathome.com')) {
		              					$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-check"></i> '.$this->lang->line('be_successful_operation').'</h4>'.$this->lang->line('be_your_new_password_has_been_updated'));
			                            $this->session->set_flashdata('alertaTipo', 'success'); // success, info, warning, danger
			                        } else {
			                        	$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.$this->lang->line('be_your_new_password_has_been_updated').' '.$this->lang->line('be_it_was_not_possible_to_send_the_process_confirmation_email').'<br>'.$this->lang->line('be_please_try_again'));
			                            $this->session->set_flashdata('alertaTipo', 'info'); // success, info, warning, danger
			                        }
			                        
			                        redirect('nueva_contrasena'); 
			                    } else {
			                    	$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.$this->lang->line('be_it_was_not_possible_to_update_your_new_password').'<br>'.$this->lang->line('be_please_try_again'));
			                        $this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
			                        redirect('nueva_contrasena/' . $post['codigo']);  
			                    }
			                } else {
			                	$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.$this->lang->line('be_it_was_not_possible_to_update_your_new_password_because_you_have_exceeded_the_time_limit_to_do_so').'<br>'.$this->lang->line('be_please_try_again'));
			                        $this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
			                        redirect('nueva_contrasena/' . $post['codigo']);
			                }
		                } else {
		                	$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.$this->lang->line('be_it_was_not_possible_to_update_your_new_password_because_your_user_was_not_found').'<br>'.$this->lang->line('be_please_try_again'));
		                    $this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
		                    redirect('nueva_contrasena/' . $post['codigo']);  
		                }
		            }
		        } else {
		        	$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.$this->lang->line('be_you_re_a_robot'));
		            $this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
		            redirect('nueva_contrasena/' . $post['codigo']);
		        }
		    } else {
		    	$this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.$this->lang->line('be_you_must_indicate_that_you_are_not_a_robot').'<br>'.$this->lang->line('be_please_try_again'));
		          $this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
		          redirect('nueva_contrasena/' . $post['codigo']);
		    }

	  	}

	}


}  