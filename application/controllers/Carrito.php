<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Carrito extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('cookie');
        $this->session->set_userdata($this->config->item('raiz') . 'fe_sesion_tienda', 1);

        if (!$this->session->has_userdata($this->config->item('raiz') . 'fe_codigo_referencia_pago')) {
            $referenceCode = rand(1000000,100000000);
            $this->session->set_userdata($this->config->item('raiz') . 'fe_codigo_referencia_pago', $referenceCode);
        }
// ----
        $tabla_pre_orden = $this->Modelo->registro($this->config->item('raiz_bd') . 'ordenes', $this->session->userdata($this->config->item('raiz') . 'fe_codigo_referencia_pago'), 'codigo_referencia_pago');
        if (count($tabla_pre_orden)) {
            if ($tabla_pre_orden->proceso_referencia == 1) {
                // actualizar datos de la orden
                $codigo = 0;
                $tabla_ordenes_tipos = $this->Modelo->registro($this->config->item('raiz_bd') . 'ordenes_tipos', 1);
                $codigo = $tabla_ordenes_tipos->secuencia_num + 1;
                $datosOT = array(
                    'secuencia_num' => $codigo,
                    'fecha_secuencia' => $this->config->item('YmdHis'),
                );
                $this->Modelo->actualizar($this->config->item('raiz_bd') . 'ordenes_tipos', $datosOT, 2);
                $datos = array(
                    'tipo_orden' => 1,
                    'codigo' => $codigo,);
                $idPreOrden = $this->Modelo->actualizar($this->config->item('raiz_bd') . 'ordenes', $datos, $this->session->userdata($this->config->item('raiz') . 'fe_codigo_referencia_pago'), 'codigo_referencia_pago');

                // Ajuste de inventarios
                $tabla_ordenes_productos = $this->Modelo->registros($this->config->item('raiz_bd') . 'ordenes_productos', '', array('orden'=>$tabla_pre_orden->id));      
                if (count($tabla_ordenes_productos) > 0) {
                    foreach ($tabla_ordenes_productos as $reg_op) {
                        $inventario = 0;
                        $tabla_productos = $this->Modelo->registro($this->config->item('raiz_bd') . 'productos', $reg_op['producto']);
                        $inventario = $tabla_productos->inventario;
                        $inventario = $inventario - $reg_op['cantidad'];                        
                        $datos_p = array('inventario' => $inventario);
                        $this->Modelo->actualizar($this->config->item('raiz_bd') . 'productos', $datos_p, $reg_op['producto']);
                    }
                }

                // Enviar Correo a: servicioalcliente@bittathome.com y r.reyes@bittathome.com
                $datosCorreo['orden'] = $tabla_pre_orden->id;
                $vista = $this->load->view('tienda/correos/orden_venta_nueva', $datosCorreo, true);
                $this->utilities->sendEmail(
                    'servicioalcliente@bittathome.com', 
                    'Nueva Orden de Venta Creada', 
                    $vista, 
                    'r.reyes@bittathome.com');
                
                // refrescar datos
                $this->session->unset_userdata($this->config->item('raiz') . 'fe_carrito');
                $referenceCode = rand(1000000,100000000);
                $this->session->set_userdata($this->config->item('raiz') . 'fe_codigo_referencia_pago', $referenceCode);
            }
        }


    }

    public function index() {

        $this->utilities->force_ssl();
          
        $tabla_comercio = $this->Modelo->registro($this->config->item('raiz_bd') . 'comercio', '1');
        $this->parametros['tabla_comercio'] = $tabla_comercio;      

        $tabla_productos_multimedia = $this->Modelo->registros($this->config->item('raiz_bd') . 'productos_multimedia', '', array(), 'orden'); 
        $this->parametros['tabla_productos_multimedia'] = $tabla_productos_multimedia;   

        $proyVar = array(
            'base_url' => base_url(),
            'language' =>  $this->session->userdata($this->config->item('raiz') . 'be_lang_value'),
            'lang' =>  $this->session->userdata($this->config->item('raiz') . 'be_lang_code'),
        );
        $this->parametros['proyVar'] = '<script>var proyVar =' . json_encode($proyVar) . ';var proyVarS ={"sgctn":"'.$this->security->get_csrf_token_name().'","sgch":"'.$this->security->get_csrf_hash().'"};</script>';
      
        $carrito_compras = array();
        $productos = '';
        if ($this->session->has_userdata($this->config->item('raiz') . 'fe_carrito')) {
            foreach ($this->session->userdata($this->config->item('raiz') . 'fe_carrito') as $key => $value) {
                $carrito_compras[$key] = $value;
                $productos .= "$key,";
            }
            $productos .= "0";
        }
        $this->parametros['carrito_compras'] = $carrito_compras; 

        if ($productos != '') {
            $tabla_productos = $this->Modelo->registros($this->config->item('raiz_bd') . 'productos', '', "id IN ($productos)", 'id DESC');
        } else {
            $tabla_productos = array();

        }
        $this->parametros['tabla_productos'] = $tabla_productos;   


        $vista_css = '';
        $this->parametros['vista_css'] = $vista_css;

        $vista_js = '<script src="'.base_url().'assets/js/front/tienda/carrito/vista.js?v='.$this->config->item('version').'"></script>';
        $this->parametros['vista_js'] = $vista_js;
      
        $this->parametros['title'] = 'Bittat - Carrito de Compras';


        $this->load->view('tienda/cabezote_2', $this->parametros);
        $this->load->view('tienda/carrito', $this->parametros);
        $this->load->view('tienda/pie_2', $this->parametros);
    }

    public function correo() {


        if ($this->session->has_userdata($this->config->item('raiz') . 'fe_carrito_direccion_1')) {

            redirect('carrito/pago');  

        }

        if ($this->session->has_userdata($this->config->item('raiz') . 'fe_carrito_nombre')) {

            redirect('carrito/envio');  

        }

        if ($this->session->has_userdata($this->config->item('raiz') . 'fe_carrito_correo')) {

            redirect('carrito/perfil');  

        }

        $proyVar = array(
            'base_url' => base_url(),
            'language' =>  $this->session->userdata($this->config->item('raiz') . 'be_lang_value'),
            'lang' =>  $this->session->userdata($this->config->item('raiz') . 'be_lang_code'),
        );
        $this->parametros['proyVar'] = '<script>var proyVar =' . json_encode($proyVar) . ';var proyVarS ={"sgctn":"'.$this->security->get_csrf_token_name().'","sgch":"'.$this->security->get_csrf_hash().'"};</script>';

        $vista_css = '';
        $this->parametros['vista_css'] = $vista_css;

        $vista_js = '<script src="'.base_url().'assets/js/front/tienda/carrito_correo/vista.js?v='.$this->config->item('version').'"></script>';
        $this->parametros['vista_js'] = $vista_js;
      
        $this->parametros['title'] = 'Bittat - Carrito de Compras - Correo';

        $this->parametros['email'] = '';
        if ( $this->session->flashdata('email') ) {
            $this->parametros['email'] = $this->session->flashdata('email');
        }

        $this->load->view('tienda/cabezote_2', $this->parametros);
        $this->load->view('tienda/carrito_correo', $this->parametros);
        $this->load->view('tienda/pie_2', $this->parametros);
        


    }

    public function correo_validacion() {
        $post = $this->input->post(NULL, TRUE);
        if (!empty($post)) {
            foreach ($post as $key => $value) {
                $post[$key] = $this->security->xss_clean($value);
            }

           $this->form_validation->set_rules('email', 'Correo Electrónico', 'required|min_length[10]|max_length[70]|valid_email|xss_clean');
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.validation_errors().'<br><b>'.$this->lang->line('be_please_try_again').'</b>');
                $this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
                redirect('carrito/correo');  
            } else {

                $usuarioCorreoControl = $this->Modelo->registros($this->config->item('raiz_bd') . 'usuarios', '', array('email'=>$post['email']), '', '', array(), '1' );

                if (count($usuarioCorreoControl)) {

                    if (isset($post['password'])) {
                        if (sha1($post['password']) == $usuarioCorreoControl[0]['password']) {

                            if ($usuarioCorreoControl[0]['activo'] == 4) {
                                $this->credenciales($usuarioCorreoControl);
                            } else {

                              $this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.$this->lang->line('be_user_disabled__contact_your_system_administrator'));
                              $this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
                              redirect('carrito/correo');   

                            }

                        } else {
                          $this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.$this->lang->line('be_invalid_password').'<br>'.$this->lang->line('be_please_try_again'));
                          $this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
                          $this->session->set_flashdata('email', $post['email']); 

                          redirect('carrito/correo');  

                        }
                    } else {

                      $this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-info-circle"></i> Atención </h4>'.'Ya existe un usuario con este mismo correo.'.'<br>Ingresa tu contraseña para autenticarte.');
                      $this->session->set_flashdata('alertaTipo', 'info'); // success, info, warning, danger

                      $this->session->set_flashdata('email', $post['email']); 

                      redirect('carrito/correo');  

                    }

                } else {
                    $this->session->set_userdata($this->config->item('raiz') . 'fe_carrito_correo', $post['email']);

// ------------------------------------------------------------------------------------------------------------------------------------------------------
                      // -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
                      $session_id = $this->utilities->code(20);

                      $datos1['nombre'] = $post['email']; 
                      $datos1['email'] = $post['email']; 
                      $datos1['modo'] = 1026; 
                      $datos1['activo'] = 4; 

                      $datos1['fecha_ultimo_ingreso'] = $this->config->item('YmdHis'); // date('Y-m-d H:i:s');
                      $datos1['ultima_ip'] = $this->input->ip_address();
                      $datos1['session_id'] = $session_id;
                      $idUsuario = $this->Modelo->insertar($this->config->item('raiz_bd') . 'usuarios', $datos1);
                      $usuario = $this->Modelo->registros($this->config->item('raiz_bd') . 'usuarios', '', array('email'=>$post['email']), '', '', array(), '1' );

                      $tabla_comercio = $this->Modelo->registro($this->config->item('raiz_bd') . 'comercio', '1');

                      $foto = ( trim($usuario[0]['foto']) == '' ? 'cart.png' : trim($usuario[0]['foto']) );

                      // fe_usuario
                      $this->session->set_userdata($this->config->item('raiz') . 'fe_usuario_id', $usuario[0]['id']);
                      $this->session->set_userdata($this->config->item('raiz') . 'fe_usuario_nombre', $usuario[0]['nombre']);
                      $this->session->set_userdata($this->config->item('raiz') . 'fe_usuario_correo', $usuario[0]['email']);

                      // fe_carrito
                      $this->session->set_userdata($this->config->item('raiz') . 'fe_carrito_id', $usuario[0]['id']);
                      $this->session->set_userdata($this->config->item('raiz') . 'fe_carrito_nombre', $usuario[0]['nombre']);
                      $this->session->set_userdata($this->config->item('raiz') . 'fe_carrito_correo', $usuario[0]['email']);
                      $this->session->set_userdata($this->config->item('raiz') . 'fe_carrito_foto', $foto);
                      $this->session->set_userdata($this->config->item('raiz') . 'fe_carrito_fecha_ingreso', $this->config->item('YmdHis'));
                      $this->session->set_userdata($this->config->item('raiz') . 'fe_carrito_session_id', $session_id);


                        $tabla_pre_orden = $this->Modelo->registro($this->config->item('raiz_bd') . 'ordenes', $this->session->userdata($this->config->item('raiz') . 'fe_codigo_referencia_pago'), 'codigo_referencia_pago');
                        if (count($tabla_pre_orden)) {
                            $datos = array(
                                'cliente' => $usuario[0]['id'],
                                'nombre' => $usuario[0]['nombre'],
                                'apellidos' => $usuario[0]['apellidos'],
                                'correo' => $usuario[0]['email'],
                                'codigo_referencia_pago' => $this->session->userdata($this->config->item('raiz') . 'fe_codigo_referencia_pago'),);
                            $idPreOrden = $this->Modelo->actualizar($this->config->item('raiz_bd') . 'ordenes', $datos, $this->session->userdata($this->config->item('raiz') . 'fe_codigo_referencia_pago'), 'codigo_referencia_pago');
                        } else {
                            $codigo = 0;
                            $tabla_ordenes_tipos = $this->Modelo->registro($this->config->item('raiz_bd') . 'ordenes_tipos', 2);
                            $codigo = $tabla_ordenes_tipos->secuencia_num + 1;
                            $datosOT = array(
                                'secuencia_num' => $codigo,
                                'fecha_secuencia' => $this->config->item('YmdHis'),
                            );
                            $this->Modelo->actualizar($this->config->item('raiz_bd') . 'ordenes_tipos', $datosOT, 2);
                            $datos = array(
                                'tipo_orden' => 2,
                                'codigo' => $codigo,
                                'cliente' => $usuario[0]['id'],
                                'nombre' => $usuario[0]['nombre'],
                                'apellidos' => $usuario[0]['apellidos'],
                                'correo' => $usuario[0]['email'],
                                'ip' => $this->input->ip_address(),
                                'estado' => 1006, 
                                'codigo_referencia_pago' => $this->session->userdata($this->config->item('raiz') . 'fe_codigo_referencia_pago'),);
                            $idPreOrden = $this->Modelo->insertar($this->config->item('raiz_bd') . 'ordenes', $datos);

                            if ($this->session->has_userdata($this->config->item('raiz') . 'fe_carrito')) {
                                foreach ($this->session->userdata($this->config->item('raiz') . 'fe_carrito') as $producto => $cantidad) {
                                    $precio = 0;
                                    $impuesto_valor = 0;
                                    $descuento = 0;
                                    $total = 0;
                                    $tabla_producto = $this->Modelo->registro($this->config->item('raiz_bd') . 'productos', $producto);
                                    if (count($tabla_producto)) {
                                        $precio = $tabla_producto->precio;
                                        $impuesto_valor = round($precio * $tabla_comercio->impuesto / 100);
                                        $total = round(($precio + $impuesto_valor) * $cantidad);
                                    }
                                    $datosOP = array(
                                        'orden' => $idPreOrden,
                                        'producto' => $producto,
                                        'cantidad' => $cantidad,
                                        'precio' => $precio,
                                        'impuesto_porcentaje' => $tabla_comercio->impuesto,
                                        'impuesto_valor' => $impuesto_valor,
                                        'descuento' => 0,
                                        'total' => $total,
                                    );
                                    $this->Modelo->insertar($this->config->item('raiz_bd') . 'ordenes_productos', $datosOP);
                                }
                            }

                            $descuento = 0;
                            $subtotal = 0;
                            $impuestos = 0;
                            $envio = 0;
                            $total = 0;

                            $tabla_ordenes_productos_calculos = $this->Modelo->registros($this->config->item('raiz_bd') . 'ordenes_productos', '', array('orden'=>$idPreOrden));
                            if (count($tabla_ordenes_productos_calculos)) {
                                foreach ($tabla_ordenes_productos_calculos as $registro_opc) {
                                    $descuento += $registro_opc['descuento'];
                                    $subtotal += $registro_opc['precio'] * $registro_opc['cantidad'];
                                    $impuestos += $registro_opc['impuesto_valor'] * $registro_opc['cantidad'];
                                    $total += $registro_opc['total'];
                                }
                            }

                            $datosAct = array(
                                'descuento' => $descuento,
                                'subtotal' => $subtotal,
                                'impuestos' => $impuestos,
                                'envio' => $envio,
                                'total' => $total,);
                            $this->Modelo->actualizar($this->config->item('raiz_bd') . 'ordenes', $datosAct, $idPreOrden);

                        }
                        // -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+


// ------------------------------------------------------------------------------------------------------------------------------------------------------

                    redirect('carrito/perfil');  
                }
            }

        } else {
            $this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No se recibieron datos.<br>'.$this->lang->line('be_please_try_again'));
            $this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
            redirect('carrito/correo');  
        }
    }



  public function credenciales($usuario) {

      // -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      $session_id = $this->utilities->code(20);
      $datos1['fecha_ultimo_ingreso'] = $this->config->item('YmdHis'); // date('Y-m-d H:i:s');
      $datos1['ultima_ip'] = $this->input->ip_address();
      $datos1['session_id'] = $session_id;
      $this->Modelo->actualizar($this->config->item('raiz_bd') . 'usuarios', $datos1, $usuario[0]['id']);

      $tabla_comercio = $this->Modelo->registro($this->config->item('raiz_bd') . 'comercio', '1');

      $foto = ( trim($usuario[0]['foto']) == '' ? 'cart.png' : trim($usuario[0]['foto']) );

      // fe_usuario
      $this->session->set_userdata($this->config->item('raiz') . 'fe_usuario_id', $usuario[0]['id']);
      $this->session->set_userdata($this->config->item('raiz') . 'fe_usuario_nombre', $usuario[0]['nombre']);
      $this->session->set_userdata($this->config->item('raiz') . 'fe_usuario_correo', $usuario[0]['email']);

      // fe_carrito
      $this->session->set_userdata($this->config->item('raiz') . 'fe_carrito_id', $usuario[0]['id']);
      $this->session->set_userdata($this->config->item('raiz') . 'fe_carrito_nombre', $usuario[0]['nombre']);
      $this->session->set_userdata($this->config->item('raiz') . 'fe_carrito_correo', $usuario[0]['email']);
      $this->session->set_userdata($this->config->item('raiz') . 'fe_carrito_foto', $foto);
      $this->session->set_userdata($this->config->item('raiz') . 'fe_carrito_fecha_ingreso', $this->config->item('YmdHis'));
      $this->session->set_userdata($this->config->item('raiz') . 'fe_carrito_session_id', $session_id);


        $tabla_pre_orden = $this->Modelo->registro($this->config->item('raiz_bd') . 'ordenes', $this->session->userdata($this->config->item('raiz') . 'fe_codigo_referencia_pago'), 'codigo_referencia_pago');
        if (count($tabla_pre_orden)) {
            $datos = array(
                'cliente' => $usuario[0]['id'],
                'nombre' => $usuario[0]['nombre'],
                'apellidos' => $usuario[0]['apellidos'],
                'correo' => $usuario[0]['email'],
                'codigo_referencia_pago' => $this->session->userdata($this->config->item('raiz') . 'fe_codigo_referencia_pago'),);
            $idPreOrden = $this->Modelo->actualizar($this->config->item('raiz_bd') . 'ordenes', $datos, $this->session->userdata($this->config->item('raiz') . 'fe_codigo_referencia_pago'), 'codigo_referencia_pago');
        } else {
            $codigo = 0;
            $tabla_ordenes_tipos = $this->Modelo->registro($this->config->item('raiz_bd') . 'ordenes_tipos', 2);
            $codigo = $tabla_ordenes_tipos->secuencia_num + 1;
            $datosOT = array(
                'secuencia_num' => $codigo,
                'fecha_secuencia' => $this->config->item('YmdHis'),
            );
            $this->Modelo->actualizar($this->config->item('raiz_bd') . 'ordenes_tipos', $datosOT, 2);
            $datos = array(
                'tipo_orden' => 2,
                'codigo' => $codigo,
                'cliente' => $usuario[0]['id'],
                'nombre' => $usuario[0]['nombre'],
                'apellidos' => $usuario[0]['apellidos'],
                'correo' => $usuario[0]['email'],
                'ip' => $this->input->ip_address(),
                'estado' => 1006, 
                'codigo_referencia_pago' => $this->session->userdata($this->config->item('raiz') . 'fe_codigo_referencia_pago'),);
            $idPreOrden = $this->Modelo->insertar($this->config->item('raiz_bd') . 'ordenes', $datos);

            if ($this->session->has_userdata($this->config->item('raiz') . 'fe_carrito')) {
                foreach ($this->session->userdata($this->config->item('raiz') . 'fe_carrito') as $producto => $cantidad) {
                    $precio = 0;
                    $impuesto_valor = 0;
                    $descuento = 0;
                    $total = 0;
                    $tabla_producto = $this->Modelo->registro($this->config->item('raiz_bd') . 'productos', $producto);
                    if (count($tabla_producto)) {
                        $precio = $tabla_producto->precio;
                        $impuesto_valor = round($precio * $tabla_comercio->impuesto / 100,2);
                        $total = round(($precio + $impuesto_valor) * $cantidad);
                    }
                    $datosOP = array(
                        'orden' => $idPreOrden,
                        'producto' => $producto,
                        'cantidad' => $cantidad,
                        'precio' => $precio,
                        'impuesto_porcentaje' => $tabla_comercio->impuesto,
                        'impuesto_valor' => $impuesto_valor,
                        'descuento' => 0,
                        'total' => $total,
                    );
                    $this->Modelo->insertar($this->config->item('raiz_bd') . 'ordenes_productos', $datosOP);
                }
            }

            $descuento = 0;
            $subtotal = 0;
            $impuestos = 0;
            $envio = 0;
            $total = 0;

            $tabla_ordenes_productos_calculos = $this->Modelo->registros($this->config->item('raiz_bd') . 'ordenes_productos', '', array('orden'=>$idPreOrden));
            if (count($tabla_ordenes_productos_calculos)) {
                foreach ($tabla_ordenes_productos_calculos as $registro_opc) {
                    $descuento += $registro_opc['descuento'];
                    $subtotal += $registro_opc['precio'] * $registro_opc['cantidad'];
                    $impuestos += $registro_opc['impuesto_valor'] * $registro_opc['cantidad'];
                    $total += $registro_opc['total'];
                }
            }

            $datosAct = array(
                'descuento' => $descuento,
                'subtotal' => $subtotal,
                'impuestos' => $impuestos,
                'envio' => $envio,
                'total' => $total,);
            $this->Modelo->actualizar($this->config->item('raiz_bd') . 'ordenes', $datosAct, $idPreOrden);

        }
        // -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

        redirect('carrito/perfil');  
     
  }

  public function perfil() {

//----------------------------

        $tabla_comercio = $this->Modelo->registro($this->config->item('raiz_bd') . 'comercio', '1');
        $this->parametros['tabla_comercio'] = $tabla_comercio;      

        $tabla_productos_multimedia = $this->Modelo->registros($this->config->item('raiz_bd') . 'productos_multimedia', '', array(), 'orden'); 
        $this->parametros['tabla_productos_multimedia'] = $tabla_productos_multimedia;   

        $proyVar = array(
            'base_url' => base_url(),
            'language' =>  $this->session->userdata($this->config->item('raiz') . 'be_lang_value'),
            'lang' =>  $this->session->userdata($this->config->item('raiz') . 'be_lang_code'),
        );
        $this->parametros['proyVar'] = '<script>var proyVar =' . json_encode($proyVar) . ';var proyVarS ={"sgctn":"'.$this->security->get_csrf_token_name().'","sgch":"'.$this->security->get_csrf_hash().'"};</script>';
      
        $carrito_compras = array();
        $productos = '';
        if ($this->session->has_userdata($this->config->item('raiz') . 'fe_carrito')) {
            foreach ($this->session->userdata($this->config->item('raiz') . 'fe_carrito') as $key => $value) {
                $carrito_compras[$key] = $value;
                $productos .= "$key,";
            }
            $productos .= "0";
        }
        $this->parametros['carrito_compras'] = $carrito_compras; 

        if ($productos != '') {
            $tabla_productos = $this->Modelo->registros($this->config->item('raiz_bd') . 'productos', '', "id IN ($productos)", 'id DESC');
        } else {
            $tabla_productos = array();
        }
        $this->parametros['tabla_productos'] = $tabla_productos;   


        $this->parametros['email_datos'] = '';
        if ($this->session->has_userdata($this->config->item('raiz') . 'fe_carrito_correo')) {
            $this->parametros['email_datos'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_correo');
        }
        $this->parametros['nombre_datos'] = '';
        if ($this->session->has_userdata($this->config->item('raiz') . 'fe_carrito_nombre')) {
            $this->parametros['nombre_datos'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_nombre');
        }
        $this->parametros['apellidos_datos'] = '';
        if ($this->session->has_userdata($this->config->item('raiz') . 'fe_carrito_apellidos')) {
            $this->parametros['apellidos_datos'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_apellidos');
        }        
        $this->parametros['cedula_datos'] = '';
        if ($this->session->has_userdata($this->config->item('raiz') . 'fe_carrito_cedula')) {
            $this->parametros['cedula_datos'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_cedula');
        }        
        $this->parametros['telefono_datos'] = '';
        if ($this->session->has_userdata($this->config->item('raiz') . 'fe_carrito_telefono')) {
            $this->parametros['telefono_datos'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_telefono');
        }        
        $this->parametros['razon_datos'] = '';
        if ($this->session->has_userdata($this->config->item('raiz') . 'fe_carrito_razon')) {
            $this->parametros['razon_datos'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_razon');
        }   
        $this->parametros['nit_datos'] = '';
        if ($this->session->has_userdata($this->config->item('raiz') . 'fe_carrito_nit')) {
            $this->parametros['nit_datos'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_nit');
        }        
        $this->parametros['rut_datos'] = '';
        if ($this->session->has_userdata($this->config->item('raiz') . 'fe_carrito_rut')) {
            $this->parametros['rut_datos'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_rut');
        } 

//----------------------------

        $vista_css = '';
        $this->parametros['vista_css'] = $vista_css;

        $vista_js = '<script src="'.base_url().'assets/js/front/tienda/carrito_perfil/vista.js?v='.$this->config->item('version').'"></script>';
        $this->parametros['vista_js'] = $vista_js;
      
        $this->parametros['title'] = 'Bittat - Carrito de Compras - Perfil';

        $this->load->view('tienda/cabezote_2', $this->parametros);
        $this->load->view('tienda/carrito_perfil', $this->parametros);
        $this->load->view('tienda/pie_2', $this->parametros);
        
    }

    public function perfil_validacion() {
        $post = $this->input->post(NULL, TRUE);
        if (!empty($post)) {
            foreach ($post as $key => $value) {
                $post[$key] = $this->security->xss_clean($value);
            }

            $this->form_validation->set_rules('email_datos', 'Correo Electrónico', 'required|min_length[10]|max_length[70]|valid_email|xss_clean');

            $this->form_validation->set_rules('nombre_datos', 'Nombre', 'required|min_length[2]|max_length[50]|xss_clean');

            $this->form_validation->set_rules('apellidos_datos', 'Apellidos', 'required|min_length[2]|max_length[50]|xss_clean');

            $this->form_validation->set_rules('cedula_datos', 'Documento de Identidad', 'required|min_length[7]|max_length[50]|xss_clean');

            $this->form_validation->set_rules('telefono_datos', 'Número Telefónico', 'required|min_length[7]|max_length[50]|xss_clean');

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.validation_errors().'<br><b>'.$this->lang->line('be_please_try_again').'</b>');
                $this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
                redirect('carrito/perfil');  
            } else {

                $this->session->set_userdata($this->config->item('raiz') . 'fe_carrito_nombre', $post['nombre_datos']);
                $this->session->set_userdata($this->config->item('raiz') . 'fe_carrito_apellidos', $post['apellidos_datos']);
                $this->session->set_userdata($this->config->item('raiz') . 'fe_carrito_cedula', $post['cedula_datos']);
                $this->session->set_userdata($this->config->item('raiz') . 'fe_carrito_telefono', $post['telefono_datos']);
                $this->session->set_userdata($this->config->item('raiz') . 'fe_carrito_razon', $post['razon_datos']);
                $this->session->set_userdata($this->config->item('raiz') . 'fe_carrito_nit', $post['nit_datos']);
                $this->session->set_userdata($this->config->item('raiz') . 'fe_carrito_rut', $post['rut_datos']);


                $datos1['nombre'] = $post['nombre_datos'];
                $datos1['apellidos'] = $post['apellidos_datos'];
                $datos1['documento_identidad'] = $post['cedula_datos'];
                $datos1['numero_telefonico'] = $post['telefono_datos'];
                $datos1['razon_social'] = $post['razon_datos'];
                if ($post['nit_datos'] != '') $datos1['entidad_identificacion'] = $post['nit_datos'];
                if ($post['rut_datos'] != '') $datos1['entidad_identificacion'] = $post['rut_datos'];
                $this->Modelo->actualizar($this->config->item('raiz_bd') . 'usuarios', $datos1, $post['email_datos'], 'email');

                $datos = array(
                    'nombre' => $post['nombre_datos'],
                    'apellidos' => $post['apellidos_datos'],);
                $this->Modelo->actualizar($this->config->item('raiz_bd') . 'ordenes', $datos, $this->session->userdata($this->config->item('raiz') . 'fe_codigo_referencia_pago'), 'codigo_referencia_pago');

                redirect('carrito/envio'); 

            }

        } else {
            $this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No se recibieron datos.<br>'.$this->lang->line('be_please_try_again'));
            $this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger

            redirect('carrito/perfil'); 
        }
    }



  public function envio() {

//----------------------------

        $tabla_comercio = $this->Modelo->registro($this->config->item('raiz_bd') . 'comercio', '1');
        $this->parametros['tabla_comercio'] = $tabla_comercio;      

        $tabla_productos_multimedia = $this->Modelo->registros($this->config->item('raiz_bd') . 'productos_multimedia', '', array(), 'orden'); 
        $this->parametros['tabla_productos_multimedia'] = $tabla_productos_multimedia;   

        $proyVar = array(
            'base_url' => base_url(),
            'language' =>  $this->session->userdata($this->config->item('raiz') . 'be_lang_value'),
            'lang' =>  $this->session->userdata($this->config->item('raiz') . 'be_lang_code'),
        );
        $this->parametros['proyVar'] = '<script>var proyVar =' . json_encode($proyVar) . ';var proyVarS ={"sgctn":"'.$this->security->get_csrf_token_name().'","sgch":"'.$this->security->get_csrf_hash().'"};</script>';
      
        $carrito_compras = array();
        $productos = '';
        if ($this->session->has_userdata($this->config->item('raiz') . 'fe_carrito')) {
            foreach ($this->session->userdata($this->config->item('raiz') . 'fe_carrito') as $key => $value) {
                $carrito_compras[$key] = $value;
                $productos .= "$key,";
            }
            $productos .= "0";
        }
        $this->parametros['carrito_compras'] = $carrito_compras; 

        if ($productos != '') {
            $tabla_productos = $this->Modelo->registros($this->config->item('raiz_bd') . 'productos', '', "id IN ($productos)", 'id DESC');
        } else {
            $tabla_productos = array();
        }
        $this->parametros['tabla_productos'] = $tabla_productos;   


        $this->parametros['email_datos'] = '';
        if ($this->session->has_userdata($this->config->item('raiz') . 'fe_carrito_correo')) {
            $this->parametros['email_datos'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_correo');
        }
        $this->parametros['nombre_datos'] = '';
        if ($this->session->has_userdata($this->config->item('raiz') . 'fe_carrito_nombre')) {
            $this->parametros['nombre_datos'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_nombre');
        }
        $this->parametros['apellidos_datos'] = '';
        if ($this->session->has_userdata($this->config->item('raiz') . 'fe_carrito_apellidos')) {
            $this->parametros['apellidos_datos'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_apellidos');
        }        
        $this->parametros['telefono_datos'] = '';
        if ($this->session->has_userdata($this->config->item('raiz') . 'fe_carrito_telefono')) {
            $this->parametros['telefono_datos'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_telefono');
        }        

        // ---

        $this->parametros['direccion_1_datos'] = '';
        if ($this->session->has_userdata($this->config->item('raiz') . 'fe_carrito_direccion_1')) {
            $this->parametros['direccion_1_datos'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_direccion_1');
        }
        $this->parametros['direccion_2_datos'] = '';
        if ($this->session->has_userdata($this->config->item('raiz') . 'fe_carrito_direccion_2')) {
            $this->parametros['direccion_2_datos'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_direccion_2');
        }
        $this->parametros['barrio_datos'] = '';
        if ($this->session->has_userdata($this->config->item('raiz') . 'fe_carrito_barrio')) {
            $this->parametros['barrio_datos'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_barrio');
        }        
        $this->parametros['departamento_datos'] = '';
        if ($this->session->has_userdata($this->config->item('raiz') . 'fe_carrito_departamento')) {
            $this->parametros['departamento_datos'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_departamento');
        }        
        $this->parametros['municipio_datos'] = '';
        if ($this->session->has_userdata($this->config->item('raiz') . 'fe_carrito_municipio')) {
            $this->parametros['municipio_datos'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_municipio');
        }        
        $this->parametros['persona_recibe_datos'] = '';
        if ($this->session->has_userdata($this->config->item('raiz') . 'fe_carrito_persona_recibe')) {
            $this->parametros['persona_recibe_datos'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_persona_recibe');
        }   

// ------------------------------------------------------------------------------------------------------------------------------------------------------
/*
              $datos1['cliente'] = $this->session->userdata($this->config->item('raiz') . 'fe_usuario_id');
              $datos1['nombre'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_nombre');
              $datos1['apellidos'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_apellidos');
              $datos1['compania'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_razon');

              $datos1['direccion_1'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_direccion_1');
              $datos1['direccion_2'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_direccion_2');
              $datos1['barrio'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_barrio');
              $datos1['departamento'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_departamento');
              $datos1['ciudad'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_municipio');
              $datos1['persona_recibe'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_persona_recibe');

            $tabla_direcciones = $this->Modelo->registro($this->config->item('raiz_bd') . 'direcciones', $this->session->userdata($this->config->item('raiz') . 'fe_codigo_referencia_pago'), 'codigo_referencia_pago');
            if (count($tabla_direcciones)) {
                    $this->Modelo->actualizar($this->config->item('raiz_bd') . 'direcciones', $datos1, $this->session->userdata($this->config->item('raiz') . 'fe_codigo_referencia_pago'), 'codigo_referencia_pago');
            } else {
                  $datos1['codigo_referencia_pago'] = $this->session->userdata($this->config->item('raiz') . 'fe_codigo_referencia_pago');
                    $this->Modelo->insertar($this->config->item('raiz_bd') . 'direcciones', $datos1);
            }
*/
// ------------------------------------------------------------------------------------------------------------------------------------------------------


        $vista_css = '';
        $this->parametros['vista_css'] = $vista_css;

        $vista_js = '<script src="'.base_url().'assets/js/front/tienda/carrito_envio/vista.js?v='.$this->config->item('version').'"></script>';
        $this->parametros['vista_js'] = $vista_js;
      
        $this->parametros['title'] = 'Bittat - Carrito de Compras - Envio';

        $this->load->view('tienda/cabezote_2', $this->parametros);
        $this->load->view('tienda/carrito_envio', $this->parametros);
        $this->load->view('tienda/pie_2', $this->parametros);
        
    }

    public function envio_validacion() {
        $post = $this->input->post(NULL, TRUE);
        if (!empty($post)) {
            foreach ($post as $key => $value) {
                $post[$key] = $this->security->xss_clean($value);
            }

            $this->form_validation->set_rules('direccion_1_datos', 'Dirección', 'required|min_length[4]|max_length[250]|xss_clean');
            $this->form_validation->set_rules('direccion_2_datos', 'Edificio, apartamento, torre o interior', 'min_length[2]|max_length[250]|xss_clean');
            $this->form_validation->set_rules('barrio_datos', 'Barrio', 'required|min_length[2]|max_length[50]|xss_clean');
            $this->form_validation->set_rules('departamento_datos', 'Departamento', 'required|min_length[2]|max_length[50]|xss_clean');
            $this->form_validation->set_rules('municipio_datos', 'Municipio', 'required|min_length[2]|max_length[50]|xss_clean');
            $this->form_validation->set_rules('persona_recibe_datos', 'Nombre de la persona que va a recibir', 'min_length[2]|max_length[50]|xss_clean');

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>'.validation_errors().'<br><b>'.$this->lang->line('be_please_try_again').'</b>');
                $this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger
                redirect('carrito/envio');  
            } else {

                $this->session->set_userdata($this->config->item('raiz') . 'fe_carrito_direccion_1', $post['direccion_1_datos']);
                $this->session->set_userdata($this->config->item('raiz') . 'fe_carrito_direccion_2', $post['direccion_2_datos']);
                $this->session->set_userdata($this->config->item('raiz') . 'fe_carrito_barrio', $post['barrio_datos']);
                $this->session->set_userdata($this->config->item('raiz') . 'fe_carrito_departamento', $post['departamento_datos']);
                $this->session->set_userdata($this->config->item('raiz') . 'fe_carrito_municipio', $post['municipio_datos']);
                $this->session->set_userdata($this->config->item('raiz') . 'fe_carrito_persona_recibe', $post['persona_recibe_datos']);

// Aqui puede actualizar la pre orden!!!! kilopayu

// ------------------------------------------------------------------------------------------------------------------------------------------------------
              $datos1['cliente'] = $this->session->userdata($this->config->item('raiz') . 'fe_usuario_id');
              $datos1['nombre'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_nombre');
              $datos1['apellidos'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_apellidos');
              $datos1['compania'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_razon');

              $datos1['direccion_1'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_direccion_1');
              $datos1['direccion_2'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_direccion_2');
              $datos1['barrio'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_barrio');
              $datos1['departamento'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_departamento');
              $datos1['ciudad'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_municipio');
              $datos1['persona_recibe'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_persona_recibe');

                $tabla_direcciones = $this->Modelo->registro($this->config->item('raiz_bd') . 'direcciones', $this->session->userdata($this->config->item('raiz') . 'fe_codigo_referencia_pago'), 'codigo_referencia_pago');
                if (count($tabla_direcciones)) {
                        $this->Modelo->actualizar($this->config->item('raiz_bd') . 'direcciones', $datos1, $this->session->userdata($this->config->item('raiz') . 'fe_codigo_referencia_pago'), 'codigo_referencia_pago');

//echo "Act dir";

                } else {
                      $datos1['codigo_referencia_pago'] = $this->session->userdata($this->config->item('raiz') . 'fe_codigo_referencia_pago');
                        $this->Modelo->insertar($this->config->item('raiz_bd') . 'direcciones', $datos1);

//echo "Ins dir";

                }
// ------------------------------------------------------------------------------------------------------------------------------------------------------



                $tabla_pre_orden = $this->Modelo->registro($this->config->item('raiz_bd') . 'ordenes', $this->session->userdata($this->config->item('raiz') . 'fe_codigo_referencia_pago'), 'codigo_referencia_pago');
                if (count($tabla_pre_orden)) {

                    $datosDir['envio_direccion1'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_direccion_1');
                    $datosDir['envio_direccion2'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_direccion_2');
                    $datosDir['envio_barrio'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_barrio');
                    $datosDir['envio_departamento'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_departamento');
                    $datosDir['envio_ciudad'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_municipio');
                    $datosDir['envio_persona_recibe'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_persona_recibe');

                    $idPreOrden = $this->Modelo->actualizar($this->config->item('raiz_bd') . 'ordenes', $datosDir, $this->session->userdata($this->config->item('raiz') . 'fe_codigo_referencia_pago'), 'codigo_referencia_pago');
                }

                redirect('carrito/pago'); 

            }

        } else {
            $this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-ban"></i> '.$this->lang->line('be_error').' </h4>No se recibieron datos.<br>'.$this->lang->line('be_please_try_again'));
            $this->session->set_flashdata('alertaTipo', 'danger'); // success, info, warning, danger

            redirect('carrito/envio'); 
        }
    }

  public function pago() {
        $tabla_comercio = $this->Modelo->registro($this->config->item('raiz_bd') . 'comercio', '1');
        $this->parametros['tabla_comercio'] = $tabla_comercio;      

        $tabla_productos_multimedia = $this->Modelo->registros($this->config->item('raiz_bd') . 'productos_multimedia', '', array(), 'orden'); 
        $this->parametros['tabla_productos_multimedia'] = $tabla_productos_multimedia;   

        $proyVar = array(
            'base_url' => base_url(),
            'language' =>  $this->session->userdata($this->config->item('raiz') . 'be_lang_value'),
            'lang' =>  $this->session->userdata($this->config->item('raiz') . 'be_lang_code'),
        );
        $this->parametros['proyVar'] = '<script>var proyVar =' . json_encode($proyVar) . ';var proyVarS ={"sgctn":"'.$this->security->get_csrf_token_name().'","sgch":"'.$this->security->get_csrf_hash().'"};</script>';
      
        $carrito_compras = array();
        $productos = '';
        if ($this->session->has_userdata($this->config->item('raiz') . 'fe_carrito')) {
            foreach ($this->session->userdata($this->config->item('raiz') . 'fe_carrito') as $key => $value) {
                $carrito_compras[$key] = $value;
                $productos .= "$key,";
            }
            $productos .= "0";
        }
        $this->parametros['carrito_compras'] = $carrito_compras; 

        if ($productos != '') {
            $tabla_productos = $this->Modelo->registros($this->config->item('raiz_bd') . 'productos', '', "id IN ($productos)", 'id DESC');
        } else {
            $tabla_productos = array();

        }
        $this->parametros['tabla_productos'] = $tabla_productos;   

// -----
        $this->parametros['email_datos'] = '';
        if ($this->session->has_userdata($this->config->item('raiz') . 'fe_carrito_correo')) {
            $this->parametros['email_datos'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_correo');
        }
        $this->parametros['nombre_datos'] = '';
        if ($this->session->has_userdata($this->config->item('raiz') . 'fe_carrito_nombre')) {
            $this->parametros['nombre_datos'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_nombre');
        }
        $this->parametros['apellidos_datos'] = '';
        if ($this->session->has_userdata($this->config->item('raiz') . 'fe_carrito_apellidos')) {
            $this->parametros['apellidos_datos'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_apellidos');
        }        
        $this->parametros['cedula_datos'] = '';
        if ($this->session->has_userdata($this->config->item('raiz') . 'fe_carrito_cedula')) {
            $this->parametros['cedula_datos'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_cedula');
        }        
        $this->parametros['telefono_datos'] = '';
        if ($this->session->has_userdata($this->config->item('raiz') . 'fe_carrito_telefono')) {
            $this->parametros['telefono_datos'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_telefono');
        }        
        $this->parametros['razon_datos'] = '';
        if ($this->session->has_userdata($this->config->item('raiz') . 'fe_carrito_razon')) {
            $this->parametros['razon_datos'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_razon');
        }   
        $this->parametros['nit_datos'] = '';
        if ($this->session->has_userdata($this->config->item('raiz') . 'fe_carrito_nit')) {
            $this->parametros['nit_datos'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_nit');
        }        
        $this->parametros['rut_datos'] = '';
        if ($this->session->has_userdata($this->config->item('raiz') . 'fe_carrito_rut')) {
            $this->parametros['rut_datos'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_rut');
        } 
        $this->parametros['direccion_1_datos'] = '';
        if ($this->session->has_userdata($this->config->item('raiz') . 'fe_carrito_direccion_1')) {
            $this->parametros['direccion_1_datos'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_direccion_1');
        }
        $this->parametros['direccion_2_datos'] = '';
        if ($this->session->has_userdata($this->config->item('raiz') . 'fe_carrito_direccion_2')) {
            $this->parametros['direccion_2_datos'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_direccion_2');
        }
        $this->parametros['barrio_datos'] = '';
        if ($this->session->has_userdata($this->config->item('raiz') . 'fe_carrito_barrio')) {
            $this->parametros['barrio_datos'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_barrio');
        }        
        $this->parametros['departamento_datos'] = '';
        if ($this->session->has_userdata($this->config->item('raiz') . 'fe_carrito_departamento')) {
            $this->parametros['departamento_datos'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_departamento');
        }        
        $this->parametros['municipio_datos'] = '';
        if ($this->session->has_userdata($this->config->item('raiz') . 'fe_carrito_municipio')) {
            $this->parametros['municipio_datos'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_municipio');
        }        
        $this->parametros['persona_recibe_datos'] = '';
        if ($this->session->has_userdata($this->config->item('raiz') . 'fe_carrito_persona_recibe')) {
            $this->parametros['persona_recibe_datos'] = $this->session->userdata($this->config->item('raiz') . 'fe_carrito_persona_recibe');
        }     
//------

        $vista_css = '';
        $this->parametros['vista_css'] = $vista_css;
        $vista_js = '
            <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
            <script src="'.base_url().'assets/js/front/tienda/carrito_pago/vista.js?v='.$this->config->item('version').'"></script>';
        $this->parametros['vista_js'] = $vista_js;
      
        $this->parametros['title'] = 'Bittat - Carrito de Compras - Pago';

// -----
$subtotal = 0;
$total = 0;
$impuesto_valor = 0;
$total_impuesto_valor = 0;
$total_base_impuesto_valor = 0;
$description = '';
if (count($carrito_compras)) {
    foreach ($carrito_compras as $key => $value) {
        $productoNombre = '';
        $productoPrecio = 0;
        $productoPrecioSinImpuesto = 0;
        if (count($tabla_productos)) {
            foreach ($tabla_productos as $regProducto) {
                if ($regProducto['id'] == $key) {
                    $productoNombre = $regProducto['nombre'];
                    $productoPrecio = $regProducto['precio'];
                    $productoPrecioSinImpuesto = $regProducto['precio'];
                    $impuesto_valor = $productoPrecio*($tabla_comercio->impuesto/100);
                    $productoPrecio = $productoPrecio+$impuesto_valor;
                    break;
                }
            }
        }
        $productoTotal = 0;
        $productoTotal = $productoPrecio * $value;
        $total_impuesto_valor += ($impuesto_valor * $value);
        $subtotal += $productoTotal;

        $description .= "($value) $productoNombre $" . number_format($productoPrecioSinImpuesto,0) . ", ";
    }
}
$total = round($subtotal);
$total_impuesto_valor = round($total_impuesto_valor);
$total_base_impuesto_valor = $total - $total_impuesto_valor;


$this->parametros['description'] = $description;
$this->parametros['total'] = $total;
$this->parametros['subtotal'] = $subtotal;
$this->parametros['total_impuesto_valor'] = $total_impuesto_valor;
$this->parametros['total_base_impuesto_valor'] = $total_base_impuesto_valor;


// -----
/* Datos de PayU de Prueba para Colombia
merchantId: 508029
API Login:  pRRXKOl8ikMmt9u
API Key:    4Vj8eK4rloUd272L48hsrarnUA
accountId:  512321
*/
/*
$ApiKey = "4Vj8eK4rloUd272L48hsrarnUA";
$merchantId = "508029"; // "750804"; 
$accountId = "512321";
*/

$ApiKey = "1C5rPZ7Xx1FCcsK3m03GUyiSzB";
$merchantId = "750804"; 
$accountId = "756620";



// $referenceCode = rand(1000000,100000000);
$referenceCode = $this->session->userdata($this->config->item('raiz') . 'fe_codigo_referencia_pago');

$currency = "COP";
$amount = $total;
$signature = "$ApiKey~$merchantId~$referenceCode~$amount~$currency";
$signature = md5($signature);
$test = 0; // 1 = Pruebas CC
$shippingCountry = "CO";
$shipmentValue = "0";
$displayShippingInformation = "YES";

$this->parametros['shippingCountry'] = $shippingCountry;
$this->parametros['shipmentValue'] = $shipmentValue;
$this->parametros['displayShippingInformation'] = $displayShippingInformation;
$this->parametros['currency'] = $currency;
$this->parametros['merchantId'] = $merchantId;
$this->parametros['accountId'] = $accountId;
$this->parametros['referenceCode'] = $referenceCode;
$this->parametros['signature'] = $signature;
$this->parametros['test'] = $test;
// -----

        $this->load->view('tienda/cabezote_2', $this->parametros);
        $this->load->view('tienda/carrito_pago', $this->parametros);
        $this->load->view('tienda/pie_2', $this->parametros);

  }


}  