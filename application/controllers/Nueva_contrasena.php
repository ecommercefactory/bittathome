<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nueva_contrasena extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('cookie');
        $this->session->set_userdata($this->config->item('raiz') . 'fe_sesion_tienda', 1);

        if (!$this->session->has_userdata($this->config->item('raiz') . 'fe_codigo_referencia_pago')) {
            $referenceCode = rand(1000000,100000000);
            $this->session->set_userdata($this->config->item('raiz') . 'fe_codigo_referencia_pago', $referenceCode);
        }
// ----
        $tabla_pre_orden = $this->Modelo->registro($this->config->item('raiz_bd') . 'ordenes', $this->session->userdata($this->config->item('raiz') . 'fe_codigo_referencia_pago'), 'codigo_referencia_pago');
        if (count($tabla_pre_orden)) {
            if ($tabla_pre_orden->proceso_referencia == 1) {
                // actualizar datos de la orden
                $codigo = 0;
                $tabla_ordenes_tipos = $this->Modelo->registro($this->config->item('raiz_bd') . 'ordenes_tipos', 1);
                $codigo = $tabla_ordenes_tipos->secuencia_num + 1;
                $datosOT = array(
                    'secuencia_num' => $codigo,
                    'fecha_secuencia' => $this->config->item('YmdHis'),
                );
                $this->Modelo->actualizar($this->config->item('raiz_bd') . 'ordenes_tipos', $datosOT, 2);
                $datos = array(
                    'tipo_orden' => 1,
                    'codigo' => $codigo,);
                $idPreOrden = $this->Modelo->actualizar($this->config->item('raiz_bd') . 'ordenes', $datos, $this->session->userdata($this->config->item('raiz') . 'fe_codigo_referencia_pago'), 'codigo_referencia_pago');

                // Ajuste de inventarios
                $tabla_ordenes_productos = $this->Modelo->registros($this->config->item('raiz_bd') . 'ordenes_productos', '', array('orden'=>$tabla_pre_orden->id));      
                if (count($tabla_ordenes_productos) > 0) {
                    foreach ($tabla_ordenes_productos as $reg_op) {
                        $inventario = 0;
                        $tabla_productos = $this->Modelo->registro($this->config->item('raiz_bd') . 'productos', $reg_op['producto']);
                        $inventario = $tabla_productos->inventario;
                        $inventario = $inventario - $reg_op['cantidad'];                        
                        $datos_p = array('inventario' => $inventario);
                        $this->Modelo->actualizar($this->config->item('raiz_bd') . 'productos', $datos_p, $reg_op['producto']);
                    }
                }

                // Enviar Correo a: servicioalcliente@bittathome.com y r.reyes@bittathome.com
                $datosCorreo['orden'] = $tabla_pre_orden->id;
                $vista = $this->load->view('tienda/correos/orden_venta_nueva', $datosCorreo, true);
                $this->utilities->sendEmail(
                    'servicioalcliente@bittathome.com', 
                    'Nueva Orden de Venta Creada', 
                    $vista, 
                    'r.reyes@bittathome.com');
                
                // refrescar datos
                $this->session->unset_userdata($this->config->item('raiz') . 'fe_carrito');
                $referenceCode = rand(1000000,100000000);
                $this->session->set_userdata($this->config->item('raiz') . 'fe_codigo_referencia_pago', $referenceCode);
            }
        }


    }

    function _remap($param) {
        $this->index($param);
    }  

    public function index($codigo) {

        $this->utilities->force_ssl();
    
        if ($codigo == '' or $codigo == 'index') {
            if ( $this->session->flashdata('alertaMensaje') ) {

            } else {
                redirect(base_url());    
            }
        }

        $this->parametros['codigo'] = $codigo;

        $error = '';

        $usuario = $this->Modelo->registro($this->config->item('raiz_bd') . 'usuarios', $codigo, 'codigo_clave_cambio');
        if (count($usuario)) {
            if ( $usuario->fecha_clave_cambio > $this->config->item('YmdHis') ) {
                $error = '';
            } else {
                $error = $this->lang->line('be_you_have_exceeded_the_time_limit_to_define_your_password').'<br>'.$this->lang->line('be_please_try_again');
            }
        } else {
            $error = $this->lang->line('be_user_not_found').'<br>'.$this->lang->line('be_please_try_again');
        }

        $this->parametros['error'] = $error;

        $tabla_categorias = $this->Modelo->registros($this->config->item('raiz_bd') . 'categorias', '', array(), 'nombre');      
        $this->parametros['tabla_categorias'] = $tabla_categorias;

        $tabla_subcategorias = $this->Modelo->registros($this->config->item('raiz_bd') . 'subcategorias', '', array(), 'nombre');      
        $this->parametros['tabla_subcategorias'] = $tabla_subcategorias;    

        $proyVar = array(
            'base_url' => base_url(),
            'language' =>  $this->session->userdata($this->config->item('raiz') . 'be_lang_value'),
            'lang' =>  $this->session->userdata($this->config->item('raiz') . 'be_lang_code'),
        );
        $this->parametros['proyVar'] = '<script>var proyVar =' . json_encode($proyVar) . ';var proyVarS ={"sgctn":"'.$this->security->get_csrf_token_name().'","sgch":"'.$this->security->get_csrf_hash().'"};</script>';
      
        $lista_deseos = 0;
        if ($this->session->has_userdata($this->config->item('raiz') . 'fe_lista_deseos')) {
            $lista_deseos = count ($this->session->userdata($this->config->item('raiz') . 'fe_lista_deseos'));
        }
        $this->parametros['lista_deseos'] = $lista_deseos;

        $carrito_cantidad = 0;
        if ($this->session->has_userdata($this->config->item('raiz') . 'fe_carrito')) {
            foreach ($this->session->userdata($this->config->item('raiz') . 'fe_carrito') as $key => $value) {
                // $carrito_compras[$key] = $value;
                $carrito_cantidad += $value;
            }
        }
        $this->parametros['carrito_cantidad'] = $carrito_cantidad;

        $vista_css = '';
        $this->parametros['vista_css'] = $vista_css;

        $vista_js = '';
        $this->parametros['vista_js'] = $vista_js;

        $this->parametros['title'] = 'Bittat - Nueva Contraseña';

        $tabla_cms_contenidos = $this->Modelo->registros($this->config->item('raiz_bd') . 'cms_contenidos', '', array('estado'=>1), 'orden');      
        $tabla_cms_contenidos_ctr = array();
        if (count($tabla_cms_contenidos)) {
            foreach ($tabla_cms_contenidos as $reg_cms_contenidos) {
                $control = true;
                if ($reg_cms_contenidos['desde'] != "0000-00-00") {
                    if ($reg_cms_contenidos['desde'] <= $this->config->item('YmdHis')) {
                        if ($reg_cms_contenidos['hasta'] != "0000-00-00") {
                            if ($reg_cms_contenidos['hasta'] >= $this->config->item('YmdHis')) {
                                $control = true;
                            } else {
                                $control = false;
                            }
                        } else {
                            $control = true;
                        }
                    } else {
                        $control = false;
                    }
                } else {
                    if ($reg_cms_contenidos['hasta'] != "0000-00-00") {
                        if ($reg_cms_contenidos['hasta'] >= $this->config->item('YmdHis')) {
                            $control = true;
                        } else {
                            $control = false;
                        }
                    } else {
                        $control = true;
                    }                
                }
                if ($control) {
                    $tabla_cms_contenidos_ctr[] = $reg_cms_contenidos;
                }
            }
        }
        $this->parametros['tabla_cms_contenidos'] = $tabla_cms_contenidos_ctr;      

        $this->load->view('tienda/cabezote_1', $this->parametros);
        $this->load->view('tienda/nueva_contrasena', $this->parametros);
        $this->load->view('tienda/pie_1', $this->parametros);

    }

}  