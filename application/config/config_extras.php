<?php
defined('BASEPATH') OR exit('No direct script access allowed');

date_default_timezone_set("America/Bogota");

// Version
$config['version'] = '0.2.1.0.0.7.2';


//$this->config->item('YmdHis')

// Date
$config['YmdHis']  = date('Y-m-d H:i:s');
$config['Ymd2His'] = date('Y-m-d H:i:s', strtotime('+2 hours'));
$config['Y6md']    = date('Y-m-d', strtotime('+6 months'));
$config['Ymd']     = date('Y-m-d');
$config['Y']       = date('Y');

// Time
$config['1minuto'] = 60;
$config['1hora']   = 60*60;
$config['1dia']    = 60*60*24;
$config['1semana'] = 60*60*24*7;
$config['1mes']    = 60*60*24*30;
$config['1anio']   = 60*60*24*365;

// Email
/*
$config['email_smtp_host'] = 'ssl://smtp.googlemail.com'; //  // 'ssl://smtp.gmail.com';
$config['email_smtp_user'] = 'servicioalcliente@bittathome.com';
$config['email_smtp_pass'] = 'Colombia01';
$config['email_smtp_port'] = '465';
*/
$config['email_smtp_host'] = 'smtp.sendgrid.net';
$config['email_smtp_user'] = 'apikey';
$config['email_smtp_pass'] = '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';
$config['email_smtp_port'] = '587';


$config['email_smtp_user_name'] = 'Bittat Group';

$config['correo_escribenos'] = 'serviciocliente@bittathome.com';

// Languages
$config['lang_en'] = "English";
$config['lang_es'] = "Español";
/*
$config['lang_zh'] = "中文";
$config['lang_ja'] = "日本語";
$config['lang_de'] = "Deutsche";
$config['lang_ru'] = "Pусский";
$config['lang_fr'] = "Français";
$config['lang_ar'] = "عربى";
$config['lang_hi'] = "हिंदी";
$config['lang_pt'] = "Português";
$config['lang_sv'] = "Svenska";
$config['lang_it'] = "Italiano";
*/
// Locations
$config['default_country'] = 43; // Colombia

// recaptcha
$config['recaptcha_sitekey'] = '6LdcanMUAAAAAIDzOOcriZOC2chDzqESgp1kCupL'; 
$config['recaptcha_secret'] = '6LdcanMUAAAAAFMwfbWgei2m2IFc7sJ0aLaIVa75'; 

// Email Nueva Membresia
$config['email_member'] = 'serviciocliente@bittathome.com';

// Datos logo admin
$config['logo_mini'] = 'BG';
$config['logo_lg'] = 'BittatGroup';


