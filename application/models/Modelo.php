<?php
defined('BASEPATH') OR exit('No esta permitido el acceso directo a este script.');

class Modelo extends MY_Model {

    public function __construct() {
        parent::__construct();
    }

    public function filasDatos($dato) {
      $this->db->where("dato = " . $dato);
		  return  $this->db->count_all_results($this->config->item('raiz_bd') . 'datos_valores');
    }  

    public function filasProyectosDatos($dato) {
      $this->db->where("dato = " . $dato);
		  return  $this->db->count_all_results($this->config->item('raiz_bd') . 'proyectos_datos_valores');
    }  

    public function filasEspecificacionesDatos($dato) {
        $this->db->where("especificacion = " . $dato);
        return  $this->db->count_all_results($this->config->item('raiz_bd') . 'especificaciones_datos');
    }  

/*	
    public function tablaCamposTipos($tabla) {
        return $this->db->select('c.*, dv.nombre AS tipo_campo_nombre')
                        ->join('datos_valores dv', 'dv.id = c.tipo_campo')
                        ->where('c.tabla', $tabla)
                        ->get('campos c')
                        ->result_array();
    }
*/
/*
// -------------------------------------------------------------------------	

		public function paraderosLista($id = 0) {
			$this->db->select('p.*, d.nombre AS departamento_nombre, m.nombre AS municipio_nombre, e.nombre AS sentido_nombre');
			$this->db->join($this->config->item('raiz_bd') . 'departamentos d', 'd.id = p.departamento');
			$this->db->join($this->config->item('raiz_bd') . 'municipios m', 'm.id = p.municipio');
			$this->db->join($this->config->item('raiz_bd') . 'estados e', 'e.id = p.sentido');
			if ($id > 0) $this->db->where("p.id", $id);
			$result = $this->db->get($this->config->item('raiz_bd') . 'paraderos p')->result_array();
			return $result;
		}

    public function registrosProyectos() {
				return $this->db->select('tp_pro_proyectos.*, COUNT(tp_pro_evaluaciones.id) AS evaluaciones')
												->join('tp_pro_evaluaciones', 'tp_pro_evaluaciones.proyecto = tp_pro_proyectos.id AND tp_pro_evaluaciones.activo = "1"', 'LEFT OUTER')
												->where('tp_pro_proyectos.activo', '1')
												->group_by('tp_pro_proyectos.id')
												->get('tp_pro_proyectos')
												->result_array();
    }

    public function registrosEvaluaciones($proyecto) {
        return $this->db->select('tp_pro_evaluaciones.*, tp_par_versiones.nombre AS nombreVersion')
                        ->join('tp_par_versiones', 'tp_par_versiones.id = tp_pro_evaluaciones.version')
                        ->where('tp_pro_evaluaciones.activo', '1')
                        ->where('tp_pro_evaluaciones.proyecto', $proyecto)
                        ->get('tp_pro_evaluaciones')
                        ->result_array();
    }

    public function registroEvaluacionVersion($evaluacion) {
        return $this->db->select('tp_pro_evaluaciones.*, tp_par_versiones.nombre AS nombreVersion')
                        ->join('tp_par_versiones', 'tp_par_versiones.id = tp_pro_evaluaciones.version')
                        ->where('tp_pro_evaluaciones.id', $evaluacion)
                        ->get('tp_pro_evaluaciones')
                        ->result_array();
    }

    public function registrosCategorias($version) {
        return $this->db->select('*')
                        ->where('version', $version)
                        ->where('activo', '1')
                        ->where('tipo IN ("1","2")')
                        ->order_by('orden', 'asc')
                        ->get('tp_par_categorias')
                        ->result_array();
    }

    public function registrosParametrosValores($evaluacion, $categoria, $categoriaDependencia) {
        return $this->db->select('tp_par_parametros.nombre, tp_par_parametros.corto, tp_pro_valores.id, tp_pro_valores.optimista, tp_pro_valores.masprobable, tp_pro_valores.pesimista, tp_pro_valores.calificacion, tp_pro_valores.pert, tp_pro_valores.desv, tp_pro_valores.var')
                        ->join('tp_pro_valores', 'tp_par_parametros.id = tp_pro_valores.parametro AND tp_pro_valores.activo = "1" and tp_pro_valores.evaluacion = ' . $evaluacion . ' AND tp_pro_valores.categoria = ' . $categoria)
                        ->where('tp_par_parametros.activo', '1')
                        ->where('tp_par_parametros.categoria', $categoriaDependencia)
                        ->order_by('tp_par_parametros.orden')
                        ->get('tp_par_parametros')
                        ->result_array();
    }

    public function registrosParametrosValoresCategorizaciones($evaluacion, $categoria, $categoriaDependencia) {
        return $this->db->select('cg.nombre, SUM(v.calificacion) AS calificacion')
                        ->join('tp_pro_valores AS v', 'p.id = v.parametro AND v.activo = "1" and v.evaluacion = ' . $evaluacion . ' AND v.categoria = ' . $categoria)
                        ->join('tp_par_aux_categorizaciones_grupo AS cg', 'p.grupo_categorizacion = cg.id AND cg.activo = "1"')
                        ->where('p.activo', '1')
                        ->where('p.categoria', $categoriaDependencia)
                        ->group_by('p.grupo_categorizacion')
                        ->get('tp_par_parametros AS p')
                        ->result_array();
    }

    public function insertaValores($datos) {
        $this->db->insert('tp_pro_valores', $datos);
    }

    public function actualizaEvaluaciones($datos, $id) {
        $this->db->where('id', $id)
                 ->update('tp_pro_evaluaciones', $datos); 
    }

    public function actualizaValores($datos, $id) {
        $this->db->where('id', $id)
                 ->update('tp_pro_valores', $datos);
    }

    public function registrosCategorizacion ($codigo_aux) {
        return $this->db->select('p.nombre, p.corto, cg.nombre AS cgNombre')
                        ->join('tp_par_aux_categorizaciones_grupo AS cg', 'p.grupo_categorizacion = cg.id AND cg.activo = "1"')
                        ->where('p.activo', '1')
                        ->where('p.codigo_aux', $codigo_aux)
                        ->get('tp_par_parametros AS p')
                        ->result_array();
    }

    // - Calculos Categoria ---------------------------------------------------------------------------------------------------
    public function actualizaValoresCalculosPert($categoria, $evaluacion, $unidad) {
        $this->db->set('pert', 'ROUND( ( ((optimista + pesimista + (4 * masprobable)) / 6) / ' . $unidad . '), 2)', FALSE);
        $this->db->where('categoria', $categoria)
                 ->where('evaluacion', $evaluacion)
                 ->update('tp_pro_valores');
    }

    public function actualizaValoresCalculosDesv($categoria, $evaluacion, $unidad) {
        $this->db->set('desv', 'ROUND( ( ((pesimista - optimista) / 6) / ' . $unidad . '), 2)', FALSE);
        $this->db->where('categoria', $categoria)
                 ->where('evaluacion', $evaluacion)
                 ->update('tp_pro_valores');
    }

    public function actualizaValoresCalculosVar($categoria, $evaluacion) {
        $this->db->set('var', 'POW(desv, 2)', FALSE);
        $this->db->where('categoria', $categoria)
                 ->where('evaluacion', $evaluacion)
                 ->update('tp_pro_valores');
    }

    // - Calculos Categoria Dependencia ---------------------------------------------------------------------------------------
    public function calculaPertDependencia($categoria, $evaluacion, $unidad) {
        return $this->db->select('x1.id, ( (((x1.optimista * x2.optimista) + (x1.pesimista * x2.pesimista) + (4*((x1.masprobable * x2.masprobable))))/6) / ' . $unidad . ') AS valor')
                        ->join('tp_pro_valores x2', 'x1.evaluacion = x2.evaluacion AND x1.parametro = x2.parametro AND x2.activo = "1" AND x1.id != x2.id')
                        ->where('x1.activo', '1')
                        ->where('x1.categoria', $categoria)
                        ->where('x1.evaluacion', $evaluacion)
                        ->get('tp_pro_valores x1')
                        ->result_array();

    }

    public function calculaDesvDependencia($categoria, $evaluacion, $unidad) {
        return $this->db->select('x1.id, ( (( (x1.pesimista * x2.pesimista) - (x1.optimista * x2.optimista) )/6) / ' . $unidad . ') AS valor')
                        ->join('tp_pro_valores x2', 'x1.evaluacion = x2.evaluacion AND x1.parametro = x2.parametro AND x2.activo = "1" AND x1.id != x2.id')
                        ->where('x1.activo', '1')
                        ->where('x1.categoria', $categoria)
                        ->where('x1.evaluacion', $evaluacion)
                        ->get('tp_pro_valores x1')
                        ->result_array();

    }
*/
}