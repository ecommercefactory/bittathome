<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//$base_url = 'http://www.solucionaticos.com/robot/';
$base_url = 'https://www.solucionaticos.com/ts02/';
?><!DOCTYPE html>
<html lang="en">
<head>
  <title>MiTienda. <?php echo $heading; ?></title>
	
  <link rel="apple-touch-icon" sizes="120x120" href="<?php echo $base_url; ?>assets/icos/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $base_url; ?>assets/icos/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $base_url; ?>assets/icos/favicon-16x16.png">
  <link rel="manifest" href="<?php echo $base_url; ?>assets/icos/manifest.json">
  <link rel="mask-icon" href="<?php echo $base_url; ?>assets/icos/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="theme-color" content="#ffffff">	
	
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	
<div class="container">
  <div class="error_caja">
		<div class="row">
			<div class="col-xs-3">
				<img src="<?php echo $base_url; ?>assets/imagenes/error.png" class="img-responsive">
			</div>
			<div class="col-xs-9">
				<h1><?php echo $heading; ?></h1> 
				<p><?php echo $message; ?></p> 
			</div>
		</div>
  </div>
</div>

</body>
</html>