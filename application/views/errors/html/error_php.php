<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div style="border:1px solid #333;padding-left:20px;margin:0 0 10px 0;">

  <div class="error_caja">
		<div class="row">
			<div class="col-xs-3">
				<img src="https://www.solucionaticos.com/ts02/assets/imagenes/error.png" class="img-responsive">
			</div>
			<div class="col-xs-9">

				<h2>A PHP Error was encountered</h2>

				<p>Severity: <?php echo $severity; ?></p>
				<p>Message:  <?php echo $message; ?></p>
				<p>Filename: <?php echo $filepath; ?></p>
				<p>Line Number: <?php echo $line; ?></p>

				<?php if (defined('SHOW_DEBUG_BACKTRACE') && SHOW_DEBUG_BACKTRACE === TRUE): ?>

					<p>Backtrace:</p>
					<?php foreach (debug_backtrace() as $error): ?>

						<?php if (isset($error['file']) && strpos($error['file'], realpath(BASEPATH)) !== 0): ?>

							<p style="margin-left:10px">
							File: <?php echo $error['file'] ?><br />
							Line: <?php echo $error['line'] ?><br />
							Function: <?php echo $error['function'] ?>
							</p>

						<?php endif ?>

					<?php endforeach ?>

				<?php endif ?>			
			
			</div>
		</div>
  </div>	

</div>