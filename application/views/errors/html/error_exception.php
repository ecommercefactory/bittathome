<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div style="border:1px solid #333;padding-left:20px;margin:0 0 10px 0;">

  <div class="error_caja">
		<div class="row">
			<div class="col-xs-3">
				<img src="<?php echo base_url(); ?>assets/imagenes/error.png" class="img-responsive">
			</div>
			<div class="col-xs-9">

				<h2>An uncaught Exception was encountered</h2>

				<p>Type: <?php echo get_class($exception); ?></p>
				<p>Message: <?php echo $message; ?></p>
				<p>Filename: <?php echo $exception->getFile(); ?></p>
				<p>Line Number: <?php echo $exception->getLine(); ?></p>

				<?php if (defined('SHOW_DEBUG_BACKTRACE') && SHOW_DEBUG_BACKTRACE === TRUE): ?>

					<p>Backtrace:</p>
					<?php foreach ($exception->getTrace() as $error): ?>

						<?php if (isset($error['file']) && strpos($error['file'], realpath(BASEPATH)) !== 0): ?>

							<p style="margin-left:10px">
							File: <?php echo $error['file']; ?><br />
							Line: <?php echo $error['line']; ?><br />
							Function: <?php echo $error['function']; ?>
							</p>
						<?php endif ?>

					<?php endforeach ?>

				<?php endif ?>

			</div>
		</div>
  </div>	
	
</div>