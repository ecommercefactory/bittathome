<?php defined('BASEPATH') OR exit('No esta permitido el acceso directo a este script.'); ?>

<?php 
/*
<script src="<?= base_url() ?>assets/plugins/jquery-cookie-master/src/jquery.cookie.js"></script>
<script>
$(document).ready(function() {
    var csrf_token= $.cookie('lntcs');
    $.ajaxSetup({
        data: {
            'slcnts' : csrf_token
        }
    });	
});
</script>
*/

foreach($css_files as $file): ?>
  <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($js_files as $file): ?>
  <script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>

  <!-- Area de Trabajo -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $titulo; ?> 
      </h1>
      <?php echo $migadepan; ?> 
    </section>

    <!-- Main content -->
    <section class="content">
      
  <?php
  // Bloque de codigo para presentar mensajes de alerta
  if ( $this->session->flashdata('alertaMensaje') ) {
  ?>
  <div class="alert alert-<?php echo $this->session->flashdata('alertaTipo'); ?> alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <?php echo $this->session->flashdata('alertaMensaje'); ?>
  </div>
  <?php
  }
  ?>    
      
      <!-- Default box -->
      <div class="box">
        <div class="box-body">
          <?php echo $tabs; ?>
          <?php echo $output; ?>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- /.Area de Trabajo -->
