<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="login-box">
<?php
if (count($languages) > 1) {
?>
  <div class="row">
    <div class="col-sm-12 text-center" style="margin:10px;">
<?php
      foreach ($languages as $record) {
        $class = '';
        if ( $this->session->userdata($this->config->item('raiz') . 'be_lang_code') == $record['code'] ) {
            $class = 'selLangActive';
        }
?>
      <a href='<?=base_url()?>lang/cambiarIdioma/<?=$record['code']?>' class="selLang <?=$class?>"><?=$this->config->item('lang_'.$record['code'])?></a> | 
<?php
      }
?>
    </div>
  </div>
<?php
}
?>
  <div class="login-box-body">
    <div class="login-logo">
      <a href="<?= base_url() ?>backend"><b><?=$this->lang->line('be_project_s_name')?></b></a>
    </div>

<?php
// Bloque de codigo para presentar mensajes de alerta
if ( $this->session->flashdata('alertaMensaje') ) {
?>
<div class="alert alert-<?php echo $this->session->flashdata('alertaTipo'); ?> alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <?php echo $this->session->flashdata('alertaMensaje'); ?>
</div>
<?php
}
?>

    <p class="login-box-msg"><?=$this->lang->line('be_type_your_data_to_create_a_new_membership')?></p>

<?php
$atributos = array('id' => 'forma');
echo form_open('backend/validacion_registro', $atributos);
?>
<?php
if (count($t_usuarios) == 1) {
?>
      <input type="hidden" name="tu_id" id="tu_id" value="<?=$t_usuarios[0]['auxiliar_2']?>">
<?php
} else {
?>
      <div class="form-group has-feedback">
        <select name="tu_id" id="tu_id" class="form-control">
<?php
foreach ($t_usuarios as $registro) {
?>
          <option value="<?=$registro['auxiliar_2']?>"><?=$registro['nombre']?></option>
<?php
}
?>
        </select>
      </div>
<?php
}
?>
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="<?=$this->lang->line('be_type_your_first_name')?>" name="nombre">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="<?=$this->lang->line('be_type_your_last_names')?>" name="apellidos">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="email" class="form-control" placeholder="<?=$this->lang->line('be_type_your_email')?>" name="correo" id="correo">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="<?=$this->lang->line('be_type_your_password')?>" name="clave" id="clave">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="<?=$this->lang->line('be_confirm_the_password')?>" name="clave_validacion">
        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
      </div>
    
      <div class="g-recaptcha" data-sitekey="<?php echo $this->config->item('recaptcha_sitekey');?>" data-theme="dark"></div><br>
    
      <div class="row">
        <div class="col-xs-7 recuerdame">
          <label>
            <input type="checkbox" name="terminos" id="terminos"> <a href="#"><?=$this->lang->line('be_i_accept_the_terms')?></a>
          </label>
        </div>
        <!-- /.col -->
        <div class="col-xs-5">
          <button type="submit" class="btn btn-primary btn-block btn-flat"><?=$this->lang->line('be_create_account')?></button>
        </div>
        <!-- /.col -->
      </div>   
      
    </form>
  </div>
  
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->