<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="login-box">
<?php
if (count($languages) > 1) {
?>
  <div class="row">
    <div class="col-sm-12 text-center" style="margin:10px;">
<?php
      foreach ($languages as $record) {
        $class = '';
        if ( $this->session->userdata($this->config->item('raiz') . 'be_lang_code') == $record['code'] ) {
            $class = 'selLangActive';
        }
?>
      <a href='<?=base_url()?>lang/cambiarIdioma/<?=$record['code']?>' class="selLang <?=$class?>"><?=$this->config->item('lang_'.$record['code'])?></a> | 
<?php
      }
?>
    </div>
  </div>
<?php
}
?>

  <div class="login-box-body">
    <div class="login-logo">
            <a href="<?= base_url() ?>backend"><img src="<?= base_url() ?>assets/tienda/images/home-bittat-group.png" class="img-responsive"></a>
    </div>
<?php
// Bloque de codigo para presentar mensajes de alerta
if ( $this->session->flashdata('alertaMensaje') ) {
?>
<div class="alert alert-<?php echo $this->session->flashdata('alertaTipo'); ?> alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <?php echo $this->session->flashdata('alertaMensaje'); ?>
</div>
<?php
}
?>

    <p class="login-box-msg"><?=$this->lang->line('be_type_your_email_to_define_your_password')?></p>

<?php
$atributos = array('id' => 'forma');
echo form_open('backend/validacion_olvido', $atributos);
?>
      <div class="form-group has-feedback">
        <input type="email" class="form-control" placeholder="<?=$this->lang->line('be_type_your_email')?>" name="correo">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
<?php
if (count($t_usuarios) == 1) {
?>
      <input type="hidden" name="tu_id" value="<?=$t_usuarios[0]['auxiliar_2']?>">
<?php
} else {
?>
      <div class="form-group has-feedback">
        <select name="tu_id" id="tu_id" class="form-control">
<?php
foreach ($t_usuarios as $registro) {
?>
          <option value="<?=$registro['auxiliar_2']?>"><?=$registro['nombre']?></option>
<?php
}
?>
        </select>
      </div>
<?php
}
?>      

<?php    
if ($_SERVER['SERVER_NAME'] == "bittathome.com") {
?>
      <div class="g-recaptcha" data-sitekey="<?php echo $this->config->item('recaptcha_sitekey');?>" data-theme="dark"></div><br>
    
<?php
}
?>
    
      <div class="row">
        <div class="col-xs-7">
          
        </div>
        <!-- /.col -->
        <div class="col-xs-5">
          <button type="submit" class="btn btn-primary btn-block btn-flat"><?=$this->lang->line('be_send')?></button>
        </div>
        <!-- /.col -->
      </div>
    </form>
  </div>
  
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->