<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="login-box">
<?php
if (count($languages) > 1) {
?>
  <div class="row">
    <div class="col-sm-12 text-center" style="margin:10px;">
<?php
      foreach ($languages as $record) {
        $class = '';
        if ( $this->session->userdata($this->config->item('raiz') . 'be_lang_code') == $record['code'] ) {
            $class = 'selLangActive';
        }
?>
      <a href='<?=base_url()?>lang/cambiarIdioma/<?=$record['code']?>' class="selLang <?=$class?>"><?=$this->config->item('lang_'.$record['code'])?></a> | 
<?php
      }
?>
    </div>
  </div>
<?php
}
?>

  <div class="login-box-body">
    <div class="login-logo">
      <a href="<?= base_url() ?>backend"><b><?=$this->lang->line('be_project_s_name')?></b></a>
    </div>
<?php
// Bloque de codigo para presentar mensajes de alerta
if ( $this->session->flashdata('alertaMensaje') ) {
?>
<div class="alert alert-<?php echo $this->session->flashdata('alertaTipo'); ?> alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <?php echo $this->session->flashdata('alertaMensaje'); ?>
</div>
<?php
}
?>

    <p class="login-box-msg"><?=$this->lang->line('be_define_your_new_password')?></p>

<?php
$atributos = array('id' => 'forma');
echo form_open('backend/validacion_nueva_clave', $atributos);
?>
      <input type="hidden" name="id" value="<?=$id?>">
      <input type="hidden" name="tu_id" value="<?=$tu_id?>">
      
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="<?=$this->lang->line('be_type_your_new_password')?>" name="clave" id="clave">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>      
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="<?=$this->lang->line('be_confirm_your_new_password')?>" name="clave_validacion">
        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
      </div>

<?php    
if ($_SERVER['SERVER_NAME'] == "bittathome.com") {
?>
      <div class="g-recaptcha" data-sitekey="<?php echo $this->config->item('recaptcha_sitekey');?>" data-theme="dark"></div><br>
<?php
}
?>
    
      <div class="row">
        <div class="col-xs-7">
          
        </div>
        <!-- /.col -->
        <div class="col-xs-5">
          <button type="submit" class="btn btn-primary btn-block btn-flat"><?=$this->lang->line('be_send')?></button>
        </div>
        <!-- /.col -->
      </div>
    </form>
  </div>
  
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->