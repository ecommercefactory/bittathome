<?php defined('BASEPATH') OR exit('No esta permitido el acceso directo a este script.'); ?>
<div class="container">
  <div class="jumbotron">
		<div class="row">
			<div class="col-xs-3">
				<img src="<?php echo base_url(); ?>assets/imagenes/error.png" class="img-responsive">
			</div>
			<div class="col-xs-9">
				<h1><?php echo $titulo; ?></h1> 
				<p><?php echo $informacion; ?></p> 
			</div>
		</div>
  </div>
</div>