<?php defined('BASEPATH') OR exit('No esta permitido el acceso directo a este script.'); ?>
<div class="login-box">
  <div class="login-logo">
    <a href="#"><b>Robot</b></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">

<?php
// Bloque de codigo para presentar mensajes de alerta
if ( $this->session->flashdata('alertaMensaje') ) {
?>
<div class="alert alert-<?php echo $this->session->flashdata('alertaTipo'); ?> alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <?php echo $this->session->flashdata('alertaMensaje'); ?>
</div>
<?php
}
?>

    <p class="login-box-msg">Ingrese sus datos para iniciar sesión</p>

    <form action="<?= base_url() ?>admin_login/validacion" method="post"> <!-- acá se llama a ejecutar el controlador Login y la función validación -->
      <div class="form-group has-feedback">
        <input type="email" class="form-control" placeholder="Ingrese su correo" name="correo">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Ingrese su clave" name="clave">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          
          <div class="checkbox icheck">
            <label>
              <input type="checkbox"> Recuérdame
            </label>
          </div>
          
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Ingresar</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
  </div>
  <div style="background: rgba(255, 255, 255, 0.5) !important;padding:10px 20px;">   
    <a href="#" style="color:black;">Olvidé mi contraseña</a><br>
    <a href="#" style="color:black;">Registrar una nueva membresía</a>    
  </div>  
  
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->