<?php defined('BASEPATH') OR exit('No esta permitido el acceso directo a este script.'); ?>

<!-- Alertas -------------------------------------------------------------------------------------- -->

<?php
if ( $this->session->flashdata('msgTipo') ) {
    switch ($this->session->flashdata('msgTipo')) {
        case 1: // Ok
?>
  <div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-check"></i> Proceso exitoso</h4>
    <?=$this->session->flashdata('msgTxt')?>
  </div>
<?php
            break;
        case 2: // Error
?>
  <div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-ban"></i> Error</h4>
    <?=$this->session->flashdata('msgTxt')?>
  </div>
<?php
            break;
    }
}
?>

<!-- Tabla -------------------------------------------------------------------------------------- -->

      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
                <button type="button" class="btn btn-success btn-md" id="btnNuevo" title="Nuevo"><i class="fa fa-plus"></i>&nbsp;&nbsp;Añadir Registro</button>
            </div>
            <div class="box-body table-responsive">
              <?php if ($tituloCaja != '') {?>
              <h3 class="box-title"><?=$tituloCaja?> <small><?=$subtituloCaja?></small></h3>
              <?php } ?>
              <table id="tablaDatos" class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th style="width:90px;">Acciones</th>
                        <th>Direccion</th>
                        <th>Departamento</th>
                        <th>Municipio</th>

                        <th>Barrio</th>
                        <th>Sentido</th>
                        <th>Hora</th>                     
                    </tr>
                </thead>
<?php if (count($regParaderos) > 0) { ?>
                <tbody>
<?php  foreach ($regParaderos as $registro) { ?>
                    <tr>

                        <td>
                          <table width="100%">
                            <tr>
                              <td>
                                <button type="button" class="btn btn-info btn-sm enlVer" cod="<?=$registro['id']?>" title="Ver Registro">
                                    <i class="fa fa-search"></i>
                                </button>                                
                              </td>
                              <td>
                                <button type="button" class="btn btn-primary btn-sm enlEditar" cod="<?=$registro['id']?>" title="Editar Registro">
                                    <i class="fa fa-pencil"></i>
                                </button>                                
                              </td>
                              <td>
                                <button type="button" class="btn btn-danger btn-sm enlEliminar" cod="<?=$registro['id']?>" title="Eliminar Registro">
                                    <i class="fa fa-minus-circle"></i>
                                </button>                                
                              </td>
                            </tr>
                          </table>
                        </td>
                      
                        <td><?=$registro['direccion']?></td>
                        <td><?=$registro['departamento_nombre']?></td>
                        <td><?=$registro['municipio_nombre']?></td>

                        <td><?=$registro['barrio']?></td>
                        <td><?=$registro['sentido_nombre']?></td>
                        <td><?=$registro['hora']?></td>
                    </tr>
<?php  } ?>
                </tbody>
<?php } ?>
              </table>
            </div>
          </div>
        </div>
      </div>

<!-- Modal - Nuevo -------------------------------------------------------------------------------------- -->
<div class="modal" id="ModNuevo">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Añadir Registro</h4>              
            </div>
            <div class="modal-body" style="overflow-y:auto;height:300px;" id="bodyN">
                <form class="form-horizontal" role="form" action="<?= base_url() ?>admin/rutas/paraderos/insertar" method="post" id="formaN">

                  <div class="form-group">
                    <label class="control-label col-sm-4" for="direccionN">Direccion:</label>
                    <div class="col-sm-5">
                        <textarea class="form-control" id="direccionN" name="direccion" placeholder="Ingrese la direccion"></textarea>
                    </div>
                  </div>                  

                  <div class="form-group">
                    <label class="control-label col-sm-4" for="departamentoN">Departamento:</label>
                    <div class="col-sm-5">
<?php if (count($regDepartamentos) > 0) { ?>
                      <select class="form-control select2" id="departamentoN" name="departamento" style="width: 100%;">
                        <option value="">Seleccione...</option>
<?php   foreach ($regDepartamentos as $registro) { ?>
                        <option value="<?=$registro['id']?>"><?=$registro['nombre']?></option>
<?php   } ?>
                      </select>
<?php } ?>
                    </div>
                  </div>

                  
                  <div class="form-group">
                    <label class="control-label col-sm-4" for="municipioN">Municipio:</label>
                    <div class="col-sm-5">
                      <select class="form-control select2" id="municipioN" name="municipio" style="width: 100%;">
                        <option value="">Seleccione...</option>
                      </select>
                    </div>
                  </div>                  

                  <!-- Sentido -->
                  <div class="form-group">
                    <label class="control-label col-sm-4" for="sentidoN">Sentido:</label>
                    <div class="col-sm-5">
<?php if (count($regSentidos) > 0) { ?>
                      <select class="form-control" id="sentidoN" name="sentido">
<?php   foreach ($regSentidos as $registro) { ?>
                        <option value="<?=$registro['id']?>"><?=$registro['nombre']?></option>
<?php   } ?>
                      </select>
<?php } ?>
                    </div>
                  </div>                   
                  
                <!-- time Picker -->
                  <div class="form-group">
                    <label class="control-label col-sm-4" for="horaN">Hora:</label>
                    <div class="col-sm-5">
                      <div class="bootstrap-timepicker" style="position:relative">
                        <div class="input-group">
                          <input type="text" class="form-control timepicker" id="horaN" name="hora">
                          <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- /.input group -->
                  </div>
                  <!-- /.form group -->
                  
                  <div class="form-group">
                    <label class="control-label col-sm-4" for="barrioN">Barrio:</label>
                    <div class="col-sm-5">
                      <input type="text" class="form-control" id="barrioN" name="barrio" placeholder="Ingrese el barrio" value="" maxlength="100">
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-sm-4" for="zonaN">Zona:</label>
                    <div class="col-sm-5">
                      <input type="text" class="form-control" id="zonaN" name="zona" placeholder="Ingrese la zona" value="" maxlength="100">
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="control-label col-sm-4" for="descripcionN">Descripcion:</label>
                    <div class="col-sm-5">
                        <textarea class="form-control" id="descripcionN" name="descripcion" placeholder="Ingrese la descripcion"></textarea>
                    </div>
                  </div>                      
                  
                 

                  <div class="form-group">
                    <label class="control-label col-sm-4" for="latitudN">Latitud:</label>
                    <div class="col-sm-5">
                      <input type="text" class="form-control" id="latitudN" name="latitud" placeholder="Ingrese la latitud" value="" maxlength="100">
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-sm-4" for="latitudN">Longitud:</label>
                    <div class="col-sm-5">
                      <input type="text" class="form-control" id="longitudN" name="longitud" placeholder="Ingrese la longitud" value="" maxlength="100">
                    </div>
                  </div>                  

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" id="btnNCancelar">
                    <i class="fa fa-close"></i>&nbsp;&nbsp;Cancelar
                </button>
                <button type="button" class="btn btn-success" id="btnNGuardar">
                    <i class="fa fa-save"></i>&nbsp;&nbsp;Guardar
                </button>
            </div>
        </div>
    </div>
</div>


<!-- Modal - Editar -------------------------------------------------------------------------------------- -->
<div class="modal" id="ModEditar">
    <div class="modal-dialog modal-lg">
        <div class="modal-content box">
          
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Editar Registro</h4>              
            </div>
            <div class="modal-body" style="overflow-y:auto;height:300px;" id="bodyE">
                <div id="msgErrorE"></div>
                <form class="form-horizontal" role="form" action="<?= base_url() ?>admin/rutas/paraderos/actualizar" method="post" id="formaE">
                  <input type="hidden" id="codigoE" name="codigo">
                  
                  <div class="form-group">
                    <label class="control-label col-sm-4" for="direccionE">Direccion:</label>
                    <div class="col-sm-5">
                        <textarea class="form-control" id="direccionE" name="direccion" placeholder="Ingrese la direccion"></textarea>
                    </div>
                  </div>                  

                  <div class="form-group">
                    <label class="control-label col-sm-4" for="departamentoE">Departamento:</label>
                    <div class="col-sm-5">
<?php if (count($regDepartamentos) > 0) { ?>
                      <input type="hidden" id="departamento_auxE" value="">
                      <select class="form-control select2" id="departamentoE" name="departamento" style="width: 100%;">
                        <option value="">Seleccione...</option>
<?php   foreach ($regDepartamentos as $registro) { ?>
                        <option value="<?=$registro['id']?>"><?=$registro['nombre']?></option>
<?php   } ?>
                      </select>
<?php } ?>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="control-label col-sm-4" for="municipioE">Municipio:</label>
                    <div class="col-sm-5">
                      <input type="hidden" id="municipio_auxE" value="">
                      <select class="form-control select2" id="municipioE" name="municipio" style="width: 100%;">
                        <option value="">Seleccione...</option>
                      </select>
                    </div>
                  </div>                  
                                    
                  <!-- Sentido -->
                  <div class="form-group">
                    <label class="control-label col-sm-4" for="sentidoE">Sentido:</label>
                    <div class="col-sm-5">
<?php if (count($regSentidos) > 0) { ?>
                      <select class="form-control" id="sentidoE" name="sentido">
<?php   foreach ($regSentidos as $registro) { ?>
                        <option value="<?=$registro['id']?>"><?=$registro['nombre']?></option>
<?php   } ?>
                      </select>
<?php } ?>
                    </div>
                  </div>   
                  
                <!-- time Picker -->
                  <div class="form-group">
                    <label class="control-label col-sm-4" for="horaE">Hora:</label>
                    <div class="col-sm-5">
                      <div class="bootstrap-timepicker" style="position:relative">
                        <div class="input-group">
                          <input type="text" class="form-control timepicker" id="horaE" name="hora">
                          <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- /.input group -->
                  </div>
                  <!-- /.form group -->

                  <div class="form-group">
                    <label class="control-label col-sm-4" for="barrioE">Barrio:</label>
                    <div class="col-sm-5">
                      <input type="text" class="form-control" id="barrioE" name="barrio" placeholder="Ingrese el barrio" value="" maxlength="100">
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-sm-4" for="zonaE">Zona:</label>
                    <div class="col-sm-5">
                      <input type="text" class="form-control" id="zonaE" name="zona" placeholder="Ingrese la zona" value="" maxlength="100">
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="control-label col-sm-4" for="descripcionE">Descripcion:</label>
                    <div class="col-sm-5">
                        <textarea class="form-control" id="descripcionE" name="descripcion" placeholder="Ingrese la descripcion"></textarea>
                    </div>
                  </div>                      
                  
                  <div class="form-group">
                    <label class="control-label col-sm-4" for="latitudE">Latitud:</label>
                    <div class="col-sm-5">
                      <input type="text" class="form-control" id="latitudE" name="latitud" placeholder="Ingrese la latitud" value="" maxlength="100">
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-sm-4" for="latitudE">Longitud:</label>
                    <div class="col-sm-5">
                      <input type="text" class="form-control" id="longitudE" name="longitud" placeholder="Ingrese la longitud" value="" maxlength="100">
                    </div>
                  </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" id="btnECancelar">
                    <i class="fa fa-close"></i>&nbsp;&nbsp;Cancelar
                </button>
                <button type="button" class="btn btn-success" id="btnEGuardar">
                    <i class="fa fa-save"></i>&nbsp;&nbsp;Actualizar
                </button>
            </div>
            <div class="overlay" id="cargandoE">
              <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>


<!-- Modal - Borrar -------------------------------------------------------------------------------------- -->
<div class="modal" id="ModEliminar">
    <div class="modal-dialog">
        <div class="modal-content">
          
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Eliminar Registro</h4>              
            </div>
          
            <div class="modal-body">
                <form class="form-horizontal" role="form" action="<?= base_url() ?>admin/rutas/paraderos/eliminar" method="post" id="formaB">
                    <input type="hidden" id="codigoB" name="codigo">
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <strong>Direccion: </strong><span id="direccionB"></span>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" id="btnBCancelar">
                    <i class="fa fa-close"></i>&nbsp;&nbsp;Cancelar
                </button>
                <button type="button" class="btn btn-danger" id="btnBEliminar">
                    <i class="fa fa-trash-o"></i>&nbsp;&nbsp;Eliminar
                </button>
            </div>
        </div>
    </div>
</div>

<!-- Modal - Ver -------------------------------------------------------------------------------------- -->
<div class="modal" id="ModVer">
    <div class="modal-dialog modal-lg">
        <div class="modal-content box">
          
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ver Registro</h4>              
            </div>
            <div class="modal-body" style="overflow-y:auto;height:300px;" id="bodyV">
                <div id="msgErrorV"></div>
                <form class="form-horizontal" role="form" id="formaV">
                  
                  <div class="form-group">
                    <label class="control-label col-sm-4">Direccion:</label>
                    <div class="col-sm-5">
                        <p class="form-control-static" id="direccionV"></p>
                    </div>
                  </div>                  

                  <div class="form-group">
                    <label class="control-label col-sm-4">Departamento:</label>
                    <div class="col-sm-5">
                        <p class="form-control-static" id="departamentoV"></p>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="control-label col-sm-4">Municipio:</label>
                    <div class="col-sm-5">
                        <p class="form-control-static" id="municipioV"></p>
                    </div>
                  </div>                  
                                    
                  <div class="form-group">
                    <label class="control-label col-sm-4">Sentido:</label>
                    <div class="col-sm-5">
                        <p class="form-control-static" id="sentidoV"></p>
                    </div>
                  </div>   
                  
                  <div class="form-group">
                    <label class="control-label col-sm-4">Hora:</label>
                    <div class="col-sm-5">
                        <p class="form-control-static" id="horaV"></p>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-sm-4">Barrio:</label>
                    <div class="col-sm-5">
                        <p class="form-control-static" id="barrioV"></p>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-sm-4">Zona:</label>
                    <div class="col-sm-5">
                        <p class="form-control-static" id="zonaV"></p>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="control-label col-sm-4">Descripcion:</label>
                    <div class="col-sm-5">
                        <p class="form-control-static" id="descripcionV"></p>
                    </div>
                  </div>                      
                  
                  <div class="form-group">
                    <label class="control-label col-sm-4">Latitud:</label>
                    <div class="col-sm-5">
                        <p class="form-control-static" id="latitudV"></p>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-sm-4">Longitud:</label>
                    <div class="col-sm-5">
                        <p class="form-control-static" id="longitudV"></p>
                    </div>
                  </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" id="btnVCancelar">
                    <i class="fa fa-close"></i>&nbsp;&nbsp;Cerrar
                </button>
            </div>
            <div class="overlay" id="cargandoV">
              <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>