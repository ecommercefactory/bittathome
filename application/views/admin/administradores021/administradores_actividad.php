<?php defined('BASEPATH') OR exit('No esta permitido el acceso directo a este script.'); ?>

<?php
// Bloque de codigo para presentar mensajes de alerta
if ( $this->session->flashdata('alertaMensaje') ) {
    ?>
    <div class="alert alert-<?php echo $this->session->flashdata('alertaTipo'); ?> alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?php echo $this->session->flashdata('alertaMensaje'); ?>
    </div>
    <?php
}
?>

<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Lista</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="btn-groupx" style="margin-bottom: 10px;">
                    <button type="button" class="btn btn-default text-green" id="btnNuevo"><span class="glyphicon glyphicon-plus"></span> Nuevo</button>
                    <button type="button" class="btn btn-default text-light-blue" id="btnEditar"><span class="glyphicon glyphicon-pencil"></span> Editar</button>
                    <button type="button" class="btn btn-default text-red" id="btnEliminar"><span class="glyphicon glyphicon-trash"></span> Eliminar</button>
                </div> 
                <div class="table-responsive">
                    <!-- dt-responsive -->
                    <table id="lista" class="table table-bordered table-striped" width="100%">
                        <thead>
                            <tr>
                                <th class="tdSeleccion"><input type="checkbox" id="seleccionarTodos"></th>
                                <th class="tdBotones"></th>
                                <th class="tdBotones"></th>  
                                <th>Usuario</th><th>Fecha Ingreso</th><th>Ip</th><th>Pais</th><th>Tipo Agente</th><th>Agente</th><th>Fecha Ultima Accion</th><th>Permanencia</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->

<!-- Modal Nuevo - Inicio -->
<div class="modal fade" id="modalNuevo" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: #008d4c !important;">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title" style="color:white;">Nuevo</h4>
            </div>
            <div class="modal-body">
                <?php echo form_open('admin/administradores021/administradores_actividad/ingresar', array('id' => 'forma_i', 'class' => 'form-horizontal')); ?>  

                    <input type='hidden' id='id_i' name='id' value='0'>
                    <div class='form-group'><label class='control-label col-sm-2' for='usuario_i'>Usuario:</label><div class='col-sm-10'><input type='text' class='form-control' id='usuario_i' name='usuario'></div></div><div class='form-group'><label class='control-label col-sm-2' for='fecha_ingreso_i'>Fecha Ingreso:</label><div class='col-sm-10'><input type='text' class='form-control' id='fecha_ingreso_i' name='fecha_ingreso'></div></div><div class='form-group'><label class='control-label col-sm-2' for='ip_i'>Ip:</label><div class='col-sm-10'><input type='text' class='form-control' id='ip_i' name='ip'></div></div><div class='form-group'><label class='control-label col-sm-2' for='pais_i'>Pais:</label><div class='col-sm-10'><select class='form-control' id='pais_i' name='pais'><?php foreach ($tabla_paises as $registro) { ?><option value="<?=$registro['id']?>"><?=$registro['nombre_en']?></option><?php } ?></select></div></div><div class='form-group'><label class='control-label col-sm-2' for='tipo_agente_i'>Tipo Agente:</label><div class='col-sm-10'><select class='form-control' id='tipo_agente_i' name='tipo_agente'><?php foreach ($tabla_datos_valores as $registro) { ?><option value="<?=$registro['id']?>"><?=$registro['nombre']?></option><?php } ?></select></div></div><div class='form-group'><label class='control-label col-sm-2' for='agente_i'>Agente:</label><div class='col-sm-10'><select class='form-control' id='agente_i' name='agente'><?php foreach ($tabla_datos_valores as $registro) { ?><option value="<?=$registro['id']?>"><?=$registro['nombre']?></option><?php } ?></select></div></div><div class='form-group'><label class='control-label col-sm-2' for='fecha_ultima_accion_i'>Fecha Ultima Accion:</label><div class='col-sm-10'><input type='text' class='form-control' id='fecha_ultima_accion_i' name='fecha_ultima_accion'></div></div><div class='form-group'><label class='control-label col-sm-2' for='permanencia_i'>Permanencia:</label><div class='col-sm-10'><input type='text' class='form-control' id='permanencia_i' name='permanencia'></div></div>

                    <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-success">Ingresar</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>  
<!-- Modal Nuevo - Fin -->

<!-- Modal Editar - Inicio -->
<div class="modal fade" id="modalEditar" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: #357ca5 !important;">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title" style="color:white;">Editar</h4>
            </div>
            <div class="modal-body">

                <?php echo form_open('admin/administradores021/administradores_actividad/guardar', array('id' => 'forma_g', 'class' => 'form-horizontal')); ?>              

                    <input type='hidden' id='id_g' name='id'>
                    <div class='form-group'><label class='control-label col-sm-2' for='usuario_g'>Usuario:</label><div class='col-sm-10'><input type='text' class='form-control' id='usuario_g' name='usuario'></div></div><div class='form-group'><label class='control-label col-sm-2' for='fecha_ingreso_g'>Fecha Ingreso:</label><div class='col-sm-10'><input type='text' class='form-control' id='fecha_ingreso_g' name='fecha_ingreso'></div></div><div class='form-group'><label class='control-label col-sm-2' for='ip_g'>Ip:</label><div class='col-sm-10'><input type='text' class='form-control' id='ip_g' name='ip'></div></div><div class='form-group'><label class='control-label col-sm-2' for='pais_g'>Pais:</label><div class='col-sm-10'><select class='form-control' id='pais_g' name='pais'><?php foreach ($tabla_paises as $registro) { ?><option value="<?=$registro['id']?>"><?=$registro['nombre_en']?></option><?php } ?></select></div></div><div class='form-group'><label class='control-label col-sm-2' for='tipo_agente_g'>Tipo Agente:</label><div class='col-sm-10'><select class='form-control' id='tipo_agente_g' name='tipo_agente'><?php foreach ($tabla_datos_valores as $registro) { ?><option value="<?=$registro['id']?>"><?=$registro['nombre']?></option><?php } ?></select></div></div><div class='form-group'><label class='control-label col-sm-2' for='agente_g'>Agente:</label><div class='col-sm-10'><select class='form-control' id='agente_g' name='agente'><?php foreach ($tabla_datos_valores as $registro) { ?><option value="<?=$registro['id']?>"><?=$registro['nombre']?></option><?php } ?></select></div></div><div class='form-group'><label class='control-label col-sm-2' for='fecha_ultima_accion_g'>Fecha Ultima Accion:</label><div class='col-sm-10'><input type='text' class='form-control' id='fecha_ultima_accion_g' name='fecha_ultima_accion'></div></div><div class='form-group'><label class='control-label col-sm-2' for='permanencia_g'>Permanencia:</label><div class='col-sm-10'><input type='text' class='form-control' id='permanencia_g' name='permanencia'></div></div>

                    <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>  
<!-- Modal Editar - Fin -->

<!-- Modal Eliminar - Inicio -->
<div class="modal fade" id="modalEliminar" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: #d33724 !important;">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title" style="color:white">Confirmación de Eliminación</h4>
            </div>
            <div class="modal-body">
                <?php echo form_open('admin/administradores021/administradores_actividad/eliminar', array('id' => 'forma_e', 'class' => 'form-horizontal')); ?> 
                    <input type='hidden' id='id_e' name='id'>          
                    <p class="lead text-danger text-center">Esta seguro de eliminar el(los) registro(s)?<br><br><button type="button" class="btn btn-lg btn-danger">Si, eliminar el(los) registro(s)</button></p>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>  
<!-- Modal Eliminar - Fin -->


