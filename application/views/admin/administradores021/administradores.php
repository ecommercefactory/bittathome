<?php defined('BASEPATH') OR exit('No esta permitido el acceso directo a este script.'); ?>

<?php
// Bloque de codigo para presentar mensajes de alerta
if ( $this->session->flashdata('alertaMensaje') ) {
    ?>
    <div class="alert alert-<?php echo $this->session->flashdata('alertaTipo'); ?> alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?php echo $this->session->flashdata('alertaMensaje'); ?>
    </div>
    <?php
}
?>

<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Lista</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="btn-groupx" style="margin-bottom: 10px;">
                    <button type="button" class="btn btn-default text-green" id="btnNuevo"><span class="glyphicon glyphicon-plus"></span> Nuevo</button>
                    <button type="button" class="btn btn-default text-light-blue" id="btnEditar"><span class="glyphicon glyphicon-pencil"></span> Editar</button>
                    <button type="button" class="btn btn-default text-red" id="btnEliminar"><span class="glyphicon glyphicon-trash"></span> Eliminar</button>
                </div> 
                <div class="table-responsive">
                    <!-- dt-responsive -->
                    <table id="lista" class="table table-bordered table-striped" width="100%">
                        <thead>
                            <tr>
                                <th class="tdSeleccion"><input type="checkbox" id="seleccionarTodos"></th>
                                <th class="tdBotones"></th>
                                <th class="tdBotones"></th>  
                                <th>Nombre</th><th>Apellidos</th><th>Perfil</th><th>Correo</th><th>Foto</th><th>Language</th><th>Fecha Nacimiento</th><th>Pais</th><th>Departamento</th><th>Ciudad</th><th>Telefono</th><th>Facebook</th><th>Whatsapp</th><th>Skype</th><th>Instagram</th><th>Google</th><th>Linkdn</th><th>Estado</th><th>Fecha Ingreso</th><th>Fecha Ultimo Ingreso</th><th>Ultima Ip</th><th>Fecha Clave Cambio</th><th>Fecha Activacion</th><th>Fecha Cambio Clave</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->

<!-- Modal Nuevo - Inicio -->
<div class="modal fade" id="modalNuevo" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: #008d4c !important;">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title" style="color:white;">Nuevo</h4>
            </div>
            <div class="modal-body">
                <?php echo form_open_multipart('admin/administradores021/administradores/ingresar', array('id' => 'forma_i', 'class' => 'form-horizontal')); ?>  

                    <input type='hidden' id='id_i' name='id' value='0'>
                    <div class='form-group'><label class='control-label col-sm-2' for='nombre_i'>Nombre:</label><div class='col-sm-10'><input type='text' class='form-control' id='nombre_i' name='nombre'></div></div><div class='form-group'><label class='control-label col-sm-2' for='apellidos_i'>Apellidos:</label><div class='col-sm-10'><input type='text' class='form-control' id='apellidos_i' name='apellidos'></div></div><div class='form-group'><label class='control-label col-sm-2' for='perfil_i'>Perfil:</label><div class='col-sm-10'><select class='form-control' id='perfil_i' name='perfil'><?php foreach ($tabla_datos_valores_perfil as $registro) { ?><option value="<?=$registro['id']?>"><?=$registro['nombre']?></option><?php } ?></select></div></div><div class='form-group'><label class='control-label col-sm-2' for='correo_i'>Correo:</label><div class='col-sm-10'><input type='email' class='form-control' id='correo_i' name='correo'></div></div>
              
              <div class='form-group'><label class='control-label col-sm-2' for='foto_i'>Foto:</label><div class='col-sm-10'><input type='file' class='form-control' id='foto_i' name='foto'></div></div><div class='form-group'><label class='control-label col-sm-2' for='language_i'>Language:</label><div class='col-sm-10'><select class='form-control' id='language_i' name='language'><?php foreach ($tabla_languages as $registro) { ?><option value="<?=$registro['id']?>"><?=$registro['name']?></option><?php } ?></select></div></div><div class='form-group'><label class='control-label col-sm-2' for='fecha_nacimiento_i'>Fecha Nacimiento:</label><div class='col-sm-10'><input type='text' class='form-control datepicker' id='fecha_nacimiento_i' name='fecha_nacimiento'></div></div><div class='form-group'><label class='control-label col-sm-2' for='pais_i'>Pais:</label><div class='col-sm-10'><select class='form-control' id='pais_i' name='pais'><?php foreach ($tabla_paises as $registro) { ?><option value="<?=$registro['id']?>"><?=$registro['nombre_en']?></option><?php } ?></select></div></div><div class='form-group'><label class='control-label col-sm-2' for='departamento_i'>Departamento:</label><div class='col-sm-10'><input type='text' class='form-control' id='departamento_i' name='departamento'></div></div><div class='form-group'><label class='control-label col-sm-2' for='ciudad_i'>Ciudad:</label><div class='col-sm-10'><input type='text' class='form-control' id='ciudad_i' name='ciudad'></div></div><div class='form-group'><label class='control-label col-sm-2' for='telefono_i'>Telefono:</label><div class='col-sm-10'><input type='text' class='form-control' id='telefono_i' name='telefono'></div></div><div class='form-group'><label class='control-label col-sm-2' for='facebook_i'>Facebook:</label><div class='col-sm-10'><input type='text' class='form-control' id='facebook_i' name='facebook'></div></div><div class='form-group'><label class='control-label col-sm-2' for='whatsapp_i'>Whatsapp:</label><div class='col-sm-10'><input type='text' class='form-control' id='whatsapp_i' name='whatsapp'></div></div><div class='form-group'><label class='control-label col-sm-2' for='skype_i'>Skype:</label><div class='col-sm-10'><input type='text' class='form-control' id='skype_i' name='skype'></div></div><div class='form-group'><label class='control-label col-sm-2' for='instagram_i'>Instagram:</label><div class='col-sm-10'><input type='text' class='form-control' id='instagram_i' name='instagram'></div></div><div class='form-group'><label class='control-label col-sm-2' for='google_i'>Google:</label><div class='col-sm-10'><input type='text' class='form-control' id='google_i' name='google'></div></div><div class='form-group'><label class='control-label col-sm-2' for='linkdn_i'>Linkdn:</label><div class='col-sm-10'><input type='text' class='form-control' id='linkdn_i' name='linkdn'></div></div>
              <div class='form-group'><label class='control-label col-sm-2' for='estado_i'>Estado:</label><div class='col-sm-10'><select class='form-control' id='estado_i' name='estado'><?php foreach ($tabla_datos_valores_estado as $registro) { ?><option value="<?=$registro['id']?>"><?=$registro['nombre']?></option><?php } ?></select></div></div>


                    <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-success">Ingresar</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>  
<!-- Modal Nuevo - Fin -->

<!-- Modal Editar - Inicio -->
<div class="modal fade" id="modalEditar" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: #357ca5 !important;">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title" style="color:white;">Editar</h4>
            </div>
            <div class="modal-body">

                <?php echo form_open_multipart('admin/administradores021/administradores/guardar', array('id' => 'forma_g', 'class' => 'form-horizontal')); ?>              

                    <input type='hidden' id='id_g' name='id'>
                    <div class='form-group'><label class='control-label col-sm-2' for='nombre_g'>Nombre:</label><div class='col-sm-10'><input type='text' class='form-control' id='nombre_g' name='nombre'></div></div><div class='form-group'><label class='control-label col-sm-2' for='apellidos_g'>Apellidos:</label><div class='col-sm-10'><input type='text' class='form-control' id='apellidos_g' name='apellidos'></div></div><div class='form-group'><label class='control-label col-sm-2' for='perfil_g'>Perfil:</label><div class='col-sm-10'><select class='form-control' id='perfil_g' name='perfil'><?php foreach ($tabla_datos_valores_perfil as $registro) { ?><option value="<?=$registro['id']?>"><?=$registro['nombre']?></option><?php } ?></select></div></div><div class='form-group'><label class='control-label col-sm-2' for='correo_g'>Correo:</label><div class='col-sm-10'><input type='email' class='form-control' id='correo_g' name='correo'></div></div>
              
              <div class='form-group'><label class='control-label col-sm-2' for='foto_g'>Foto:</label><div class='col-sm-10'><div id='foto_g_ver'></div><input type='file' class='form-control' id='foto_g' name='foto'></div></div><div class='form-group'><label class='control-label col-sm-2' for='language_g'>Language:</label><div class='col-sm-10'><select class='form-control' id='language_g' name='language'><?php foreach ($tabla_languages as $registro) { ?><option value="<?=$registro['id']?>"><?=$registro['name']?></option><?php } ?></select></div></div><div class='form-group'><label class='control-label col-sm-2' for='fecha_nacimiento_g'>Fecha Nacimiento:</label><div class='col-sm-10'><input type='text' class='form-control datepicker' id='fecha_nacimiento_g' name='fecha_nacimiento'></div></div><div class='form-group'><label class='control-label col-sm-2' for='pais_g'>Pais:</label><div class='col-sm-10'><select class='form-control' id='pais_g' name='pais'><?php foreach ($tabla_paises as $registro) { ?><option value="<?=$registro['id']?>"><?=$registro['nombre_en']?></option><?php } ?></select></div></div><div class='form-group'><label class='control-label col-sm-2' for='departamento_g'>Departamento:</label><div class='col-sm-10'><input type='text' class='form-control' id='departamento_g' name='departamento'></div></div><div class='form-group'><label class='control-label col-sm-2' for='ciudad_g'>Ciudad:</label><div class='col-sm-10'><input type='text' class='form-control' id='ciudad_g' name='ciudad'></div></div><div class='form-group'><label class='control-label col-sm-2' for='telefono_g'>Telefono:</label><div class='col-sm-10'><input type='text' class='form-control' id='telefono_g' name='telefono'></div></div><div class='form-group'><label class='control-label col-sm-2' for='facebook_g'>Facebook:</label><div class='col-sm-10'><input type='text' class='form-control' id='facebook_g' name='facebook'></div></div><div class='form-group'><label class='control-label col-sm-2' for='whatsapp_g'>Whatsapp:</label><div class='col-sm-10'><input type='text' class='form-control' id='whatsapp_g' name='whatsapp'></div></div><div class='form-group'><label class='control-label col-sm-2' for='skype_g'>Skype:</label><div class='col-sm-10'><input type='text' class='form-control' id='skype_g' name='skype'></div></div><div class='form-group'><label class='control-label col-sm-2' for='instagram_g'>Instagram:</label><div class='col-sm-10'><input type='text' class='form-control' id='instagram_g' name='instagram'></div></div><div class='form-group'><label class='control-label col-sm-2' for='google_g'>Google:</label><div class='col-sm-10'><input type='text' class='form-control' id='google_g' name='google'></div></div><div class='form-group'><label class='control-label col-sm-2' for='linkdn_g'>Linkdn:</label><div class='col-sm-10'><input type='text' class='form-control' id='linkdn_g' name='linkdn'></div></div>
              
              
              <div class='form-group'><label class='control-label col-sm-2' for='estado_g'>Estado:</label><div class='col-sm-10'><select class='form-control' id='estado_g' name='estado'><?php foreach ($tabla_datos_valores_estado as $registro) { ?><option value="<?=$registro['id']?>"><?=$registro['nombre']?></option><?php } ?></select></div></div>


                    <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>  
<!-- Modal Editar - Fin -->

<!-- Modal Eliminar - Inicio -->
<div class="modal fade" id="modalEliminar" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: #d33724 !important;">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title" style="color:white">Confirmación de Eliminación</h4>
            </div>
            <div class="modal-body">
                <?php echo form_open('admin/administradores021/administradores/eliminar', array('id' => 'forma_e', 'class' => 'form-horizontal')); ?> 
                    <input type='hidden' id='id_e' name='id'>          
                    <p class="lead text-danger text-center">Esta seguro de eliminar el(los) registro(s)?<br><br><button type="button" class="btn btn-lg btn-danger">Si, eliminar el(los) registro(s)</button></p>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>  
<!-- Modal Eliminar - Fin -->


