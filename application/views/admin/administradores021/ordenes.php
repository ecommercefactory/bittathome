<?php defined('BASEPATH') OR exit('No esta permitido el acceso directo a este script.'); ?>

<?php
// Bloque de codigo para presentar mensajes de alerta
if ( $this->session->flashdata('alertaMensaje') ) {
    ?>
    <div class="alert alert-<?php echo $this->session->flashdata('alertaTipo'); ?> alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?php echo $this->session->flashdata('alertaMensaje'); ?>
    </div>
    <?php
}
?>

<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Lista</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="btn-groupx" style="margin-bottom: 10px;">
                    <button type="button" class="btn btn-default text-green" id="btnNuevo"><span class="glyphicon glyphicon-plus"></span> Nuevo</button>
                    <button type="button" class="btn btn-default text-light-blue" id="btnEditar"><span class="glyphicon glyphicon-pencil"></span> Editar</button>
                    <button type="button" class="btn btn-default text-red" id="btnEliminar"><span class="glyphicon glyphicon-trash"></span> Eliminar</button>
                </div> 
                <div class="table-responsive">
                    <!-- dt-responsive -->
                    <table id="lista" class="table table-bordered table-striped" width="100%">
                        <thead>
                            <tr>
                                <th class="tdSeleccion"><input type="checkbox" id="seleccionarTodos"></th>
                                <th class="tdBotones"></th>
                                <th class="tdBotones"></th>  
                                <th></th>  
                                <th>Cliente</th><th>Nombre</th><th>Apellidos</th><th>Correo</th><th>Pais</th><th>Empresa</th><th>Cargo</th><th>Direccion 1</th><th>Direccion 2</th><th>Departamento</th><th>Ciudad</th><th>Zip</th><th>Telefono</th><th>Envio Apellidos</th><th>Envio Nombre</th><th>Envio Correo</th><th>Envio Pais</th><th>Envio Empresa</th><th>Envio Cargo</th><th>Envio Direccion 1</th><th>Envio Direccion 2</th><th>Envio Departamento</th><th>Envio Ciudad</th><th>Envio Zip</th><th>Envio Telefono 1</th><th>Envio Telefono 2</th><th>Ip</th><th>Estado</th><th>Estado Fecha</th><th>Estado Usuario</th><th>Codigo Descuento</th><th>Descuento</th><th>Subtotal</th><th>Impuestos</th><th>Envio</th><th>Moneda</th><th>Total</th><th>Entrega Fecha</th><th>Entrega Hora</th><th>Fecha</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->

<!-- Modal Nuevo - Inicio -->
<div class="modal fade" id="modalNuevo" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: #008d4c !important;">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title" style="color:white;">Nuevo</h4>
            </div>
            <div class="modal-body">
                <?php echo form_open('admin/administradores021/ordenes/ingresar', array('id' => 'forma_i', 'class' => 'form-horizontal')); ?>  

                    <div class='form-group'><label class='control-label col-sm-2' for='cliente_i'>Cliente:</label><div class='col-sm-10'><select class='form-control' id='cliente_i' name='cliente'><option value='0'></option><?php foreach ($tabla_clientes as $registro) { ?><option value="<?=$registro['id']?>"><?=$registro['nombre']?></option><?php } ?></select></div></div>
              
                    <input type='hidden' id='id_i' name='id' value='0'>
                    <div class='form-group'><label class='control-label col-sm-2' for='nombre_i'>Nombre:</label><div class='col-sm-10'><input type='text' class='form-control' id='nombre_i' name='nombre'></div></div>
                    <div class='form-group'><label class='control-label col-sm-2' for='apellidos_i'>Apellidos:</label><div class='col-sm-10'><input type='text' class='form-control' id='apellidos_i' name='apellidos'></div></div>
                    <div class='form-group'><label class='control-label col-sm-2' for='correo_i'>Correo:</label><div class='col-sm-10'><input type='text' class='form-control' id='correo_i' name='correo'></div></div><div class='form-group'><label class='control-label col-sm-2' for='pais_i'>Pais:</label><div class='col-sm-10'><select class='form-control' id='pais_i' name='pais'><?php foreach ($tabla_paises as $registro) { ?><option value="<?=$registro['id']?>"><?=$registro['nombre_es']?></option><?php } ?></select></div></div><div class='form-group'><label class='control-label col-sm-2' for='empresa_i'>Empresa:</label><div class='col-sm-10'><input type='text' class='form-control' id='empresa_i' name='empresa'></div></div><div class='form-group'><label class='control-label col-sm-2' for='cargo_i'>Cargo:</label><div class='col-sm-10'><input type='text' class='form-control' id='cargo_i' name='cargo'></div></div><div class='form-group'><label class='control-label col-sm-2' for='direccion1_i'>Direccion 1:</label><div class='col-sm-10'><input type='text' class='form-control' id='direccion1_i' name='direccion1'></div></div><div class='form-group'><label class='control-label col-sm-2' for='direccion2_i'>Direccion 2:</label><div class='col-sm-10'><input type='text' class='form-control' id='direccion2_i' name='direccion2'></div></div><div class='form-group'><label class='control-label col-sm-2' for='departamento_i'>Departamento:</label><div class='col-sm-10'><input type='text' class='form-control' id='departamento_i' name='departamento'></div></div><div class='form-group'><label class='control-label col-sm-2' for='ciudad_i'>Ciudad:</label><div class='col-sm-10'><input type='text' class='form-control' id='ciudad_i' name='ciudad'></div></div><div class='form-group'><label class='control-label col-sm-2' for='zip_i'>Zip:</label><div class='col-sm-10'><input type='text' class='form-control' id='zip_i' name='zip'></div></div><div class='form-group'><label class='control-label col-sm-2' for='telefono_i'>Telefono:</label><div class='col-sm-10'><input type='text' class='form-control' id='telefono_i' name='telefono'></div></div><div class='form-group'><label class='control-label col-sm-2' for='envio_apellidos_i'>Envio Apellidos:</label><div class='col-sm-10'><input type='text' class='form-control' id='envio_apellidos_i' name='envio_apellidos'></div></div><div class='form-group'><label class='control-label col-sm-2' for='envio_nombre_i'>Envio Nombre:</label><div class='col-sm-10'><input type='text' class='form-control' id='envio_nombre_i' name='envio_nombre'></div></div><div class='form-group'><label class='control-label col-sm-2' for='envio_correo_i'>Envio Correo:</label><div class='col-sm-10'><input type='text' class='form-control' id='envio_correo_i' name='envio_correo'></div></div><div class='form-group'><label class='control-label col-sm-2' for='envio_pais_i'>Envio Pais:</label><div class='col-sm-10'><select class='form-control' id='envio_pais_i' name='envio_pais'><?php foreach ($tabla_paises as $registro) { ?><option value="<?=$registro['id']?>"><?=$registro['nombre_es']?></option><?php } ?></select></div></div><div class='form-group'><label class='control-label col-sm-2' for='envio_empresa_i'>Envio Empresa:</label><div class='col-sm-10'><input type='text' class='form-control' id='envio_empresa_i' name='envio_empresa'></div></div><div class='form-group'><label class='control-label col-sm-2' for='envio_cargo_i'>Envio Cargo:</label><div class='col-sm-10'><input type='text' class='form-control' id='envio_cargo_i' name='envio_cargo'></div></div><div class='form-group'><label class='control-label col-sm-2' for='envio_direccion1_i'>Envio Direccion 1:</label><div class='col-sm-10'><input type='text' class='form-control' id='envio_direccion1_i' name='envio_direccion1'></div></div><div class='form-group'><label class='control-label col-sm-2' for='envio_direccion2_i'>Envio Direccion 2:</label><div class='col-sm-10'><input type='text' class='form-control' id='envio_direccion2_i' name='envio_direccion2'></div></div><div class='form-group'><label class='control-label col-sm-2' for='envio_departamento_i'>Envio Departamento:</label><div class='col-sm-10'><input type='text' class='form-control' id='envio_departamento_i' name='envio_departamento'></div></div><div class='form-group'><label class='control-label col-sm-2' for='envio_ciudad_i'>Envio Ciudad:</label><div class='col-sm-10'><input type='text' class='form-control' id='envio_ciudad_i' name='envio_ciudad'></div></div><div class='form-group'><label class='control-label col-sm-2' for='envio_zip_i'>Envio Zip:</label><div class='col-sm-10'><input type='text' class='form-control' id='envio_zip_i' name='envio_zip'></div></div><div class='form-group'><label class='control-label col-sm-2' for='envio_telefono1_i'>Envio Telefono 1:</label><div class='col-sm-10'><input type='text' class='form-control' id='envio_telefono1_i' name='envio_telefono1'></div></div><div class='form-group'><label class='control-label col-sm-2' for='envio_telefono2_i'>Envio Telefono 2:</label><div class='col-sm-10'><input type='text' class='form-control' id='envio_telefono2_i' name='envio_telefono2'></div></div><div class='form-group'><label class='control-label col-sm-2' for='ip_i'>Ip:</label><div class='col-sm-10'><input type='text' class='form-control' id='ip_i' name='ip'></div></div><div class='form-group'><label class='control-label col-sm-2' for='estado_i'>Estado:</label><div class='col-sm-10'><select class='form-control' id='estado_i' name='estado'><?php foreach ($tabla_datos_valores_estado as $registro) { ?><option value="<?=$registro['id']?>"><?=$registro['nombre']?></option><?php } ?></select></div></div>

              <div class='form-group'><label class='control-label col-sm-2' for='estado_fecha_i'>Estado Fecha:</label><div class='col-sm-10'><input type='text' class='form-control datepicker' id='estado_fecha_i' name='estado_fecha' disabled></div></div>

              <div class='form-group'><label class='control-label col-sm-2' for='estado_usuario_i'>Usuario:</label><div class='col-sm-10'><select class='form-control' id='estado_usuario_i' name='estado_usuario'><option value='0'></option><?php foreach ($tabla_administradores as $registro) { ?><option value="<?=$registro['id']?>"><?=$registro['nombre']?></option><?php } ?></select></div></div><div class='form-group'><label class='control-label col-sm-2' for='codigo_descuento_i'>Codigo Descuento:</label><div class='col-sm-10'><input type='text' class='form-control' id='codigo_descuento_i' name='codigo_descuento'></div></div><div class='form-group'><label class='control-label col-sm-2' for='descuento_i'>Descuento:</label><div class='col-sm-10'><input type='text' class='form-control' id='descuento_i' name='descuento'></div></div><div class='form-group'><label class='control-label col-sm-2' for='subtotal_i'>Subtotal:</label><div class='col-sm-10'><input type='text' class='form-control' id='subtotal_i' name='subtotal'></div></div><div class='form-group'><label class='control-label col-sm-2' for='impuestos_i'>Impuestos:</label><div class='col-sm-10'><input type='text' class='form-control' id='impuestos_i' name='impuestos'></div></div><div class='form-group'><label class='control-label col-sm-2' for='envio_i'>Envio:</label><div class='col-sm-10'><input type='text' class='form-control' id='envio_i' name='envio'></div></div><div class='form-group'><label class='control-label col-sm-2' for='moneda_i'>Moneda:</label><div class='col-sm-10'><select class='form-control' id='moneda_i' name='moneda'><?php foreach ($tabla_monedas as $registro) { ?><option value="<?=$registro['id']?>"><?=$registro['nombre']?></option><?php } ?></select></div></div><div class='form-group'><label class='control-label col-sm-2' for='total_i'>Total:</label><div class='col-sm-10'><input type='text' class='form-control' id='total_i' name='total'></div></div><div class='form-group'><label class='control-label col-sm-2' for='entrega_fecha_i'>Entrega Fecha:</label><div class='col-sm-10'><input type='text' class='form-control datepicker' id='entrega_fecha_i' name='entrega_fecha'></div></div>
              
<div class="form-group">
  <div class="bootstrap-timepicker">
		<label class='control-label col-sm-2' for='entrega_hora_i'>Entrega Hora:</label>
		<div class="col-sm-10">
			<input type="text" class="form-control timepicker" id='entrega_hora_i' name='entrega_hora'>
		</div>
	</div>
</div>              
              
                    <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-success">Ingresar</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>  
<!-- Modal Nuevo - Fin -->

<!-- Modal Editar - Inicio -->
<div class="modal fade" id="modalEditar" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: #357ca5 !important;">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title" style="color:white;">Editar</h4>
            </div>
            <div class="modal-body">

                <?php echo form_open('admin/administradores021/ordenes/guardar', array('id' => 'forma_g', 'class' => 'form-horizontal')); ?>              

                    <input type='hidden' id='id_g' name='id'>
              
                    <div class='form-group'><label class='control-label col-sm-2' for='cliente_g'>Cliente:</label><div class='col-sm-10'><select class='form-control' id='cliente_g' name='cliente'><option value='0'></option><?php foreach ($tabla_clientes as $registro) { ?><option value="<?=$registro['id']?>"><?=$registro['nombre']?></option><?php } ?></select></div></div>
              
              <div class='form-group'><label class='control-label col-sm-2' for='nombre_g'>Nombre:</label><div class='col-sm-10'><input type='text' class='form-control' id='nombre_g' name='nombre'></div></div>
              <div class='form-group'><label class='control-label col-sm-2' for='apellidos_g'>Apellidos:</label><div class='col-sm-10'><input type='text' class='form-control' id='apellidos_g' name='apellidos'></div></div>
              
                    <div class='form-group'><label class='control-label col-sm-2' for='correo_g'>Correo:</label><div class='col-sm-10'><input type='text' class='form-control' id='correo_g' name='correo'></div></div><div class='form-group'><label class='control-label col-sm-2' for='pais_g'>Pais:</label><div class='col-sm-10'><select class='form-control' id='pais_g' name='pais'><?php foreach ($tabla_paises as $registro) { ?><option value="<?=$registro['id']?>"><?=$registro['nombre_es']?></option><?php } ?></select></div></div><div class='form-group'><label class='control-label col-sm-2' for='empresa_g'>Empresa:</label><div class='col-sm-10'><input type='text' class='form-control' id='empresa_g' name='empresa'></div></div><div class='form-group'><label class='control-label col-sm-2' for='cargo_g'>Cargo:</label><div class='col-sm-10'><input type='text' class='form-control' id='cargo_g' name='cargo'></div></div><div class='form-group'><label class='control-label col-sm-2' for='direccion1_g'>Direccion 1:</label><div class='col-sm-10'><input type='text' class='form-control' id='direccion1_g' name='direccion1'></div></div><div class='form-group'><label class='control-label col-sm-2' for='direccion2_g'>Direccion 2:</label><div class='col-sm-10'><input type='text' class='form-control' id='direccion2_g' name='direccion2'></div></div><div class='form-group'><label class='control-label col-sm-2' for='departamento_g'>Departamento:</label><div class='col-sm-10'><input type='text' class='form-control' id='departamento_g' name='departamento'></div></div><div class='form-group'><label class='control-label col-sm-2' for='ciudad_g'>Ciudad:</label><div class='col-sm-10'><input type='text' class='form-control' id='ciudad_g' name='ciudad'></div></div><div class='form-group'><label class='control-label col-sm-2' for='zip_g'>Zip:</label><div class='col-sm-10'><input type='text' class='form-control' id='zip_g' name='zip'></div></div><div class='form-group'><label class='control-label col-sm-2' for='telefono_g'>Telefono:</label><div class='col-sm-10'><input type='text' class='form-control' id='telefono_g' name='telefono'></div></div><div class='form-group'><label class='control-label col-sm-2' for='envio_apellidos_g'>Envio Apellidos:</label><div class='col-sm-10'><input type='text' class='form-control' id='envio_apellidos_g' name='envio_apellidos'></div></div><div class='form-group'><label class='control-label col-sm-2' for='envio_nombre_g'>Envio Nombre:</label><div class='col-sm-10'><input type='text' class='form-control' id='envio_nombre_g' name='envio_nombre'></div></div><div class='form-group'><label class='control-label col-sm-2' for='envio_correo_g'>Envio Correo:</label><div class='col-sm-10'><input type='text' class='form-control' id='envio_correo_g' name='envio_correo'></div></div><div class='form-group'><label class='control-label col-sm-2' for='envio_pais_g'>Envio Pais:</label><div class='col-sm-10'><select class='form-control' id='envio_pais_g' name='envio_pais'><?php foreach ($tabla_paises as $registro) { ?><option value="<?=$registro['id']?>"><?=$registro['nombre_es']?></option><?php } ?></select></div></div><div class='form-group'><label class='control-label col-sm-2' for='envio_empresa_g'>Envio Empresa:</label><div class='col-sm-10'><input type='text' class='form-control' id='envio_empresa_g' name='envio_empresa'></div></div><div class='form-group'><label class='control-label col-sm-2' for='envio_cargo_g'>Envio Cargo:</label><div class='col-sm-10'><input type='text' class='form-control' id='envio_cargo_g' name='envio_cargo'></div></div><div class='form-group'><label class='control-label col-sm-2' for='envio_direccion1_g'>Envio Direccion 1:</label><div class='col-sm-10'><input type='text' class='form-control' id='envio_direccion1_g' name='envio_direccion1'></div></div><div class='form-group'><label class='control-label col-sm-2' for='envio_direccion2_g'>Envio Direccion 2:</label><div class='col-sm-10'><input type='text' class='form-control' id='envio_direccion2_g' name='envio_direccion2'></div></div><div class='form-group'><label class='control-label col-sm-2' for='envio_departamento_g'>Envio Departamento:</label><div class='col-sm-10'><input type='text' class='form-control' id='envio_departamento_g' name='envio_departamento'></div></div><div class='form-group'><label class='control-label col-sm-2' for='envio_ciudad_g'>Envio Ciudad:</label><div class='col-sm-10'><input type='text' class='form-control' id='envio_ciudad_g' name='envio_ciudad'></div></div><div class='form-group'><label class='control-label col-sm-2' for='envio_zip_g'>Envio Zip:</label><div class='col-sm-10'><input type='text' class='form-control' id='envio_zip_g' name='envio_zip'></div></div><div class='form-group'><label class='control-label col-sm-2' for='envio_telefono1_g'>Envio Telefono 1:</label><div class='col-sm-10'><input type='text' class='form-control' id='envio_telefono1_g' name='envio_telefono1'></div></div><div class='form-group'><label class='control-label col-sm-2' for='envio_telefono2_g'>Envio Telefono 2:</label><div class='col-sm-10'><input type='text' class='form-control' id='envio_telefono2_g' name='envio_telefono2'></div></div><div class='form-group'><label class='control-label col-sm-2' for='ip_g'>Ip:</label><div class='col-sm-10'><input type='text' class='form-control' id='ip_g' name='ip'></div></div><div class='form-group'><label class='control-label col-sm-2' for='estado_g'>Estado:</label><div class='col-sm-10'><select class='form-control' id='estado_g' name='estado'><?php foreach ($tabla_datos_valores_estado as $registro) { ?><option value="<?=$registro['id']?>"><?=$registro['nombre']?></option><?php } ?></select></div></div>

              <div class='form-group'><label class='control-label col-sm-2' for='estado_fecha_g'>Estado Fecha:</label><div class='col-sm-10'><input type='text' class='form-control datepicker' id='estado_fecha_g' name='estado_fecha' disabled></div></div>

              <div class='form-group'><label class='control-label col-sm-2' for='estado_usuario_g'>Usuario:</label><div class='col-sm-10'><select class='form-control' id='estado_usuario_g' name='estado_usuario'><option value='0'></option><?php foreach ($tabla_administradores as $registro) { ?><option value="<?=$registro['id']?>"><?=$registro['nombre']?></option><?php } ?></select></div></div><div class='form-group'><label class='control-label col-sm-2' for='codigo_descuento_g'>Codigo Descuento:</label><div class='col-sm-10'><input type='text' class='form-control' id='codigo_descuento_g' name='codigo_descuento'></div></div><div class='form-group'><label class='control-label col-sm-2' for='descuento_g'>Descuento:</label><div class='col-sm-10'><input type='text' class='form-control' id='descuento_g' name='descuento'></div></div><div class='form-group'><label class='control-label col-sm-2' for='subtotal_g'>Subtotal:</label><div class='col-sm-10'><input type='text' class='form-control' id='subtotal_g' name='subtotal'></div></div><div class='form-group'><label class='control-label col-sm-2' for='impuestos_g'>Impuestos:</label><div class='col-sm-10'><input type='text' class='form-control' id='impuestos_g' name='impuestos'></div></div><div class='form-group'><label class='control-label col-sm-2' for='envio_g'>Envio:</label><div class='col-sm-10'><input type='text' class='form-control' id='envio_g' name='envio'></div></div><div class='form-group'><label class='control-label col-sm-2' for='moneda_g'>Moneda:</label><div class='col-sm-10'><select class='form-control' id='moneda_g' name='moneda'><?php foreach ($tabla_monedas as $registro) { ?><option value="<?=$registro['id']?>"><?=$registro['nombre']?></option><?php } ?></select></div></div><div class='form-group'><label class='control-label col-sm-2' for='total_g'>Total:</label><div class='col-sm-10'><input type='text' class='form-control' id='total_g' name='total'></div></div><div class='form-group'><label class='control-label col-sm-2' for='entrega_fecha_g'>Entrega Fecha:</label><div class='col-sm-10'><input type='text' class='form-control datepicker' id='entrega_fecha_g' name='entrega_fecha'></div></div>
              
<div class="form-group">
  <div class="bootstrap-timepicker">
		<label class='control-label col-sm-2' for='entrega_hora_g'>Entrega Hora:</label>
		<div class="col-sm-10">
			<input type="text" class="form-control timepicker" id='entrega_hora_g' name='entrega_hora'>
		</div>
	</div>
</div>                

                    <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>  
<!-- Modal Editar - Fin -->

<!-- Modal Eliminar - Inicio -->
<div class="modal fade" id="modalEliminar" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: #d33724 !important;">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title" style="color:white">Confirmación de Eliminación</h4>
            </div>
            <div class="modal-body">
                <?php echo form_open('admin/administradores021/ordenes/eliminar', array('id' => 'forma_e', 'class' => 'form-horizontal')); ?> 
                    <input type='hidden' id='id_e' name='id'>          
                    <p class="lead text-danger text-center">Esta seguro de eliminar el(los) registro(s)?<br><br><button type="button" class="btn btn-lg btn-danger">Si, eliminar el(los) registro(s)</button></p>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>  
<!-- Modal Eliminar - Fin -->


