<?php defined('BASEPATH') OR exit('No esta permitido el acceso directo a este script.'); ?>

<?php
// Bloque de codigo para presentar mensajes de alerta
if ( $this->session->flashdata('alertaMensaje') ) {
    ?>
    <div class="alert alert-<?php echo $this->session->flashdata('alertaTipo'); ?> alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?php echo $this->session->flashdata('alertaMensaje'); ?>
    </div>
    <?php
}
?>

<input type="hidden" id="plantilla" value="<?php echo $plantilla;?>">

<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Lista</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="btn-groupx" style="margin-bottom: 10px;">
                    <button type="button" class="btn btn-default text-green" id="btnNuevo"><span class="glyphicon glyphicon-plus"></span> Nuevo</button>
                    <button type="button" class="btn btn-default text-light-blue" id="btnEditar"><span class="glyphicon glyphicon-pencil"></span> Editar</button>
                    <button type="button" class="btn btn-default text-red" id="btnEliminar"><span class="glyphicon glyphicon-trash"></span> Eliminar</button>
                </div> 
                <div class="table-responsive">
                    <!-- dt-responsive -->
                    <table id="lista" class="table table-bordered table-striped" width="100%">
                        <thead>
                            <tr>
                                <th class="tdSeleccion"><input type="checkbox" id="seleccionarTodos"></th>
                                <th class="tdBotones"></th>
                                <th class="tdBotones"></th>  
                                <th>Nombre</th><th>URL</th><th>Codigo</th><th>Logo</th><th>Color</th><th>Orden</th><th>Fecha</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->

<!-- Modal Nuevo - Inicio -->
<div class="modal fade" id="modalNuevo" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: #008d4c !important;">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title" style="color:white;">Nuevo</h4>
            </div>
            <div class="modal-body">
                <?php echo form_open_multipart('admin/administradores021/plantilla_redes_sociales_funciones/ingresar', array('id' => 'forma_i', 'class' => 'form-horizontal')); ?>  

                    <input type='hidden' id='id_i' name='id' value='0'>
                    <input type="hidden" name="plantilla" value="<?php echo $plantilla;?>">

              <div class='form-group'><label class='control-label col-sm-2' for='nombre_i'>Nombre:</label><div class='col-sm-10'><input type='text' class='form-control' id='nombre_i' name='nombre'></div></div><div class='form-group'><label class='control-label col-sm-2' for='url_i'>URL:</label><div class='col-sm-10'><input type='text' class='form-control' id='url_i' name='url'></div></div><div class='form-group'><label class='control-label col-sm-2' for='codigo_i'>Codigo:</label><div class='col-sm-10'><input type='text' class='form-control' id='codigo_i' name='codigo'></div></div><div class='form-group'><label class='control-label col-sm-2' for='logo_i'>Logo:</label><div class='col-sm-10'><input type='file' class='form-control' id='logo_i' name='logo'></div></div><div class='form-group'><label class='control-label col-sm-2' for='color_i'>Color:</label><div class='col-sm-10'><input type='text' class='form-control color' id='color_i' name='color'></div></div>
              <div class='form-group'><label class='control-label col-sm-2' for='orden_i'>Orden:</label><div class='col-sm-10'><input type='text' class='form-control' id='orden_i' name='orden'></div></div>

                    <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-success">Ingresar</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>  
<!-- Modal Nuevo - Fin -->

<!-- Modal Editar - Inicio -->
<div class="modal fade" id="modalEditar" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: #357ca5 !important;">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title" style="color:white;">Editar</h4>
            </div>
            <div class="modal-body">

                <?php echo form_open_multipart('admin/administradores021/plantilla_redes_sociales_funciones/guardar', array('id' => 'forma_g', 'class' => 'form-horizontal')); ?>              

                    <input type='hidden' id='id_g' name='id'>
                    <input type="hidden" name="plantilla" value="<?php echo $plantilla;?>">
                    <div class='form-group'><label class='control-label col-sm-2' for='nombre_g'>Nombre:</label><div class='col-sm-10'><input type='text' class='form-control' id='nombre_g' name='nombre'></div></div><div class='form-group'><label class='control-label col-sm-2' for='url_g'>URL:</label><div class='col-sm-10'><input type='text' class='form-control' id='url_g' name='url'></div></div><div class='form-group'><label class='control-label col-sm-2' for='codigo_g'>Codigo:</label><div class='col-sm-10'><input type='text' class='form-control' id='codigo_g' name='codigo'></div></div><div class='form-group'><label class='control-label col-sm-2' for='logo_g'>Logo:</label><div class='col-sm-10'><input type='file' class='form-control' id='logo_g' name='logo'></div></div>
              <div class='form-group'><label class='control-label col-sm-2' for='color_g'>Color:</label><div class='col-sm-10'><input type='text' class='form-control color' id='color_g' name='color'></div></div>
              <div class='form-group'><label class='control-label col-sm-2' for='orden_g'>Orden:</label><div class='col-sm-10'><input type='text' class='form-control' id='orden_g' name='orden'></div></div>

                    <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>  
<!-- Modal Editar - Fin -->

<!-- Modal Eliminar - Inicio -->
<div class="modal fade" id="modalEliminar" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: #d33724 !important;">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title" style="color:white">Confirmación de Eliminación</h4>
            </div>
            <div class="modal-body">
                <?php echo form_open('admin/administradores021/plantilla_redes_sociales_funciones/eliminar', array('id' => 'forma_e', 'class' => 'form-horizontal')); ?> 
                    <input type='hidden' id='id_e' name='id'>          
                    <input type="hidden" name="plantilla" value="<?php echo $plantilla;?>">
                    <p class="lead text-danger text-center">Esta seguro de eliminar el(los) registro(s)?<br><br><button type="button" class="btn btn-lg btn-danger">Si, eliminar el(los) registro(s)</button></p>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>  
<!-- Modal Eliminar - Fin -->


