<?php defined('BASEPATH') OR exit('No esta permitido el acceso directo a este script.'); ?>

<?php
// Bloque de codigo para presentar mensajes de alerta
if ( $this->session->flashdata('alertaMensaje') ) {
    ?>
    <div class="alert alert-<?php echo $this->session->flashdata('alertaTipo'); ?> alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?php echo $this->session->flashdata('alertaMensaje'); ?>
    </div>
    <?php
}
?>





        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: #357ca5 !important;">
                <h4 class="modal-title" style="color:white;">Editar</h4>
            </div>
            <div class="modal-body">

                <?php echo form_open('admin/administradores021/comercio/guardar', array('id' => 'forma_g', 'class' => 'form-horizontal')); ?>              

                    <input type='hidden' id='id_g' name='id'>


                    <fieldset>
                        <legend>Generales</legend>
                    
                            <div class='form-group'><label class='control-label col-sm-2' for='impuesto_g'>Impuesto:</label>
                              <div class="col-sm-10">
                              <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-percent"></i></span>
                                <input type='text' class='form-control' id='impuesto_g' name='impuesto'>
                              </div>                
                              </div>                
                            </div>
                            
                            <div class='form-group'><label class='control-label col-sm-2' for='envioNacional_g'>Envio Nacional:</label>
                              <div class="col-sm-10">
                              <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                                <input type='text' class='form-control' id='envioNacional_g' name='envioNacional'>
                              </div>                  
                              </div>                  
                            </div>

              <!--
                            <div class='form-group'><label class='control-label col-sm-2' for='envioInternacional_g'>Envio Internacional:</label><div class='col-sm-10'><input type='text' class='form-control' id='envioInternacional_g' name='envioInternacional'></div></div><div class='form-group'><label class='control-label col-sm-2' for='tasaMinimaNal_g'>Tasa Minima Nacional:</label><div class='col-sm-10'><input type='text' class='form-control' id='tasaMinimaNal_g' name='tasaMinimaNal'></div></div><div class='form-group'><label class='control-label col-sm-2' for='tasaMinimaInt_g'>Tasa Minima Internacional:</label><div class='col-sm-10'><input type='text' class='form-control' id='tasaMinimaInt_g' name='tasaMinimaInt'></div></div>
              -->
                            
                            <div class='form-group'><label class='control-label col-sm-2' for='pais_g'>Pais:</label><div class='col-sm-10'><select class='form-control' id='pais_g' name='pais'><?php foreach ($tabla_paises as $registro) { ?><option value="<?=$registro['id']?>"><?=$registro['nombre_en']?></option><?php } ?></select></div></div>

                    </fieldset>


                    <fieldset>
                        <legend>Paypal</legend>

                        <div class='form-group'><label class='control-label col-sm-2' for='modoPaypal_g'>Modo Paypal:</label><div class='col-sm-10'><select class='form-control' id='modoPaypal_g' name='modoPaypal'><?php foreach ($tabla_datos_valores_paypal_modo as $registro) { ?><option value="<?=$registro['id']?>"><?=$registro['nombre']?></option><?php } ?></select></div></div>
                        
                        <div class='form-group'><label class='control-label col-sm-2' for='clienteIdPaypal_g'>Cliente Id Paypal:</label><div class='col-sm-10'><textarea class='form-control' rows='5' id='clienteIdPaypal_g' name='clienteIdPaypal'></textarea></div></div>

                        <div class='form-group'><label class='control-label col-sm-2' for='llaveSecretaPaypal_g'>Llave Secreta Paypal:</label><div class='col-sm-10'><textarea class='form-control' rows='5' id='llaveSecretaPaypal_g' name='llaveSecretaPaypal'></textarea></div></div>

                    </fieldset>



                    <fieldset>
                        <legend>PayU</legend>

                            <div class='form-group'><label class='control-label col-sm-2' for='modoPayu_g'>Modo PayU:</label><div class='col-sm-10'><select class='form-control' id='modoPayu_g' name='modoPayu'><?php foreach ($tabla_datos_valores_payu_modo as $registro) { ?><option value="<?=$registro['id']?>"><?=$registro['nombre']?></option><?php } ?></select></div></div>
                            
                            <div class='form-group'><label class='control-label col-sm-2' for='merchantIdPayu_g'>Merchant Id PayU:</label><div class='col-sm-10'><input type='text' class='form-control' id='merchantIdPayu_g' name='merchantIdPayu'></div></div>

                            <div class='form-group'><label class='control-label col-sm-2' for='accountIdPayu_g'>Account Id PayU:</label><div class='col-sm-10'><input type='text' class='form-control' id='accountIdPayu_g' name='accountIdPayu'></div></div>

                            <div class='form-group'><label class='control-label col-sm-2' for='apiKeyPayu_g'>Api Key PayU:</label><div class='col-sm-10'><input type='text' class='form-control' id='apiKeyPayu_g' name='apiKeyPayu'></div></div>

                    </fieldset>

                    <fieldset>
                        <legend>Pago Contra Entrega</legend>

                        <div class='form-group'>
                          <label class='control-label col-sm-2' for='activar_pago_contra_entrega'>
                            Activar Pago Contra Entrega
                          </label>
                          <div class='col-sm-10'>
                            <select class='form-control' id='activar_pago_contra_entrega' name='activar_pago_contra_entrega'>
                              <option>Si</option>
                              <option>No</option>
                            </select>
                          </div>
                        </div>

                        <div class='form-group'>
                          <label class='control-label col-sm-2' for='monto_minimo'>
                            Monto Mínimo
                          </label>
                          <div class='col-sm-10'>
                            <input type='text' class='form-control' id='monto_minimo' name='monto_minimo' value="20000">
                          </div>
                        </div>

                        <div class='form-group'>
                          <label class='control-label col-sm-2' for='solo_para_ciudades_donde_hay_bodegas'>
                            Solo para Ciudades donde hay Bodegas
                          </label>
                          <div class='col-sm-10'>
                            <select class='form-control' id='solo_para_ciudades_donde_hay_bodegas' name='solo_para_ciudades_donde_hay_bodegas'>
                              <option>Si</option>
                              <option>No</option>
                            </select>
                          </div>
                        </div>

                    </fieldset>




                    <fieldset>
                        <legend>MercadoLibre</legend>

 
<div class='form-group'>
   <label class='control-label col-sm-2' for='xxx'>
      ID de Tienda Oficial
   </label>
   <div class='col-sm-10'>
      <input type="text" name="mktp_mlibre_official_store_id" id="mktp_mlibre_official_store_id" value="12513" class='form-control'>
   </div>
</div>

<div class='form-group'>
   <label class='control-label col-sm-2' for='xxx'>
      Enlace 1
   </label>
   <div class='col-sm-10'>
      <a href="https://www.mercadolibre.com/jms/mco/lgz/msl/login" target="_blank"><span>Conectar con MercadoLibre</span></a>
   </div>

</div>

<div class='form-group'>
   <label class='control-label col-sm-2' for='xxx'>
      Enlace 2
   </label>
   <div class='col-sm-10'>
      <a href="#"><span>Mapeo de Categorías</span></a>
   </div>
</div>

<div class='form-group'>
   <label class='control-label col-sm-2' for='xxx'>
      Enlace 3
   </label>
   <div class='col-sm-10'>
      <a href="#"><span>Mapeo de Especificaciones</span></a>
   </div>
</div>
      
<div class='form-group'>
   <label class='control-label col-sm-2' for='xxx'>
      Mapear por producto
   </label>
   <div class='col-sm-10'>
      <input type="checkbox" name="mktp_mlibre_cats_by_product" id="mktp_mlibre_cats_by_product">
   </div>
</div>


<div class='form-group'>
   <label class='control-label col-sm-2' for='xxx'>
      Solo Grupos Seleccionados
   </label>
   <div class='col-sm-10'>
      <input type="checkbox" name="mktp_mlibre_by_group" id="mktp_mlibre_by_group" checked="checked">
   </div>
</div>

<div class='form-group'>
   <label class='control-label col-sm-2' for='xxx'>
      Lista de Precios
   </label>
   <div class='col-sm-10'>
      <select class='form-control' name="mktp_mlibre_price_list" id="mktp_mlibre_price_list">
      <option value="3" selected="selected">MercadoLibre</option>
      </select>
   </div>
</div>



<div class='form-group'>
   <label class='control-label col-sm-2' for='xxx'>
      Código de Descuento
   </label>
   <div class='col-sm-10'>
      <select class='form-control' name="mktp_mlibre_discount_code" id="mktp_mlibre_discount_code">
      <option value="0" selected="selected">Ninguno</option>
      </select>
   </div>
</div>

<div class='form-group'>
   <label class='control-label col-sm-2' for='xxx'>
      Tipo de Cuenta
   </label>
   <div class='col-sm-10'>
      <select class='form-control' name="mktp_mlibre_listing_type" id="mktp_mlibre_listing_type">
      <option value="free">Free</option>
      <option value="bronze">Bronze</option>
      <option value="silver">Silver</option>
      <option value="gold">Gold</option>
      <option value="gold_special" selected="selected">Gold Special</option>
      <option value="gold_pro">Gold Pro</option>
      </select>
   </div>
</div>

<div class='form-group'>
   <label class='control-label col-sm-2' for='xxx'>
      Tipo de Envío
   </label>
   <div class='col-sm-10'>
      <select class='form-control' name="mktp_mlibre_shipping_preferences" id="mktp_mlibre_shipping_preferences">
      <option value="free_shipping">Envío Gratis</option>
      <option value="me2">MercadoEnvío</option>
      <option value="custom">Custom</option>
      <option value="me2_no_free" selected="selected">MercadoEnvíoNF</option>
      </select>
   </div>
</div>

<div class='form-group'>
   <label class='control-label col-sm-2' for='xxx'>
      Mostrar Stock
   </label>
   <div class='col-sm-10'>
      <select class='form-control' name="mktp_mlibre_read_stock_from" id="mktp_mlibre_read_stock_from">
      <option value="-1"> Nivel de Stock</option>
      <option value="-2"> Stock Ficticio</option>
      <option value="11">Centro de Distribución</option>
      </select>
   </div>
</div>

<div class='form-group'>
   <label class='control-label col-sm-2' for='xxx'>
      Activo
   </label>
   <div class='col-sm-10'>
      <input type="checkbox" name="mktp_mlibre_active" id="mktp_mlibre_active" checked="checked">
   </div>
</div>






                    </fieldset>




                    <hr>

                    <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>


