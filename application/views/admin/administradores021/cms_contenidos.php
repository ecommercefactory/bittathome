<?php defined('BASEPATH') OR exit('No esta permitido el acceso directo a este script.'); ?>

<?php
// Bloque de codigo para presentar mensajes de alerta
if ( $this->session->flashdata('alertaMensaje') ) {
    ?>
    <div class="alert alert-<?php echo $this->session->flashdata('alertaTipo'); ?> alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?php echo $this->session->flashdata('alertaMensaje'); ?>
    </div>
    <?php
}
?>

<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Lista</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="btn-groupx" style="margin-bottom: 10px;">
                    <button type="button" class="btn btn-default text-green" id="btnNuevo"><span class="glyphicon glyphicon-plus"></span> Nuevo</button>
                    <button type="button" class="btn btn-default text-light-blue" id="btnEditar"><span class="glyphicon glyphicon-pencil"></span> Editar</button>
                    <button type="button" class="btn btn-default text-red" id="btnEliminar"><span class="glyphicon glyphicon-trash"></span> Eliminar</button>
                </div> 
                <div class="table-responsive">
                    <!-- dt-responsive -->
                    <table id="lista" class="table table-bordered table-striped" width="100%">
                        <thead>
                            <tr>
                                <th class="tdSeleccion"><input type="checkbox" id="seleccionarTodos"></th>
                                <th class="tdBotones"></th>
                                <th class="tdBotones"></th>  
                                <th>Titulo</th>
                                <th>Ruta</th>
                                <th>Estado</th>
                                <th>Desde</th>
                                <th>Hasta</th>
                                <th>Visitas</th>
                                <th>Orden</th>
                                <th>Fecha</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->

<!-- Modal Nuevo - Inicio -->
<div class="modal fade" id="modalNuevo" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: #008d4c !important;">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title" style="color:white;">Nuevo</h4>
            </div>
            <div class="modal-body">
                <?php echo form_open_multipart('admin/administradores021/cms_contenidos/ingresar', array('id' => 'forma_i', 'class' => 'form-horizontal')); ?>  

                    <input type='hidden' id='id_i' name='id' value='0'>
                    <div class='form-group'><label class='control-label col-sm-2' for='titulo_i'>Titulo:</label><div class='col-sm-10'><input type='text' class='form-control' id='titulo_i' name='titulo'></div></div>
                    <div class='form-group'><label class='control-label col-sm-2' for='ruta_i'>Ruta:</label><div class='col-sm-10'><input type='text' class='form-control' id='ruta_i' name='ruta'></div></div>
                    <div class='form-group'><label class='control-label col-sm-2' for='contenido_i'>Contenido:</label><div class='col-sm-10'><textarea class='form-control textarea' rows='5' id='contenido_i' name='contenido'></textarea></div></div>
                    <div class='form-group'><label class='control-label col-sm-2' for='estado_i'>Estado:</label><div class='col-sm-10'><select class='form-control' id='estado_i' name='estado'><?php foreach ($tabla_datos_valores_estado as $registro) { ?><option value="<?=$registro['id']?>"><?=$registro['nombre']?></option><?php } ?></select></div></div>
                    <div class='form-group'><label class='control-label col-sm-2' for='desde_i'>Desde:</label><div class='col-sm-10'><input type='text' class='form-control datepicker' id='desde_i' name='desde'></div></div>
                    <div class='form-group'><label class='control-label col-sm-2' for='hasta_i'>Hasta:</label><div class='col-sm-10'><input type='text' class='form-control datepicker' id='hasta_i' name='hasta'></div></div>
                    <div class='form-group'><label class='control-label col-sm-2' for='orden_i'>Orden:</label><div class='col-sm-10'><input type='text' class='form-control' id='orden_i' name='orden'></div></div>

                    <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-success">Ingresar</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>  
<!-- Modal Nuevo - Fin -->

<!-- Modal Editar - Inicio -->
<div class="modal fade" id="modalEditar" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: #357ca5 !important;">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title" style="color:white;">Editar</h4>
            </div>
            <div class="modal-body">

                <?php echo form_open_multipart('admin/administradores021/cms_contenidos/guardar', array('id' => 'forma_g', 'class' => 'form-horizontal')); ?>              

                    <input type='hidden' id='id_g' name='id'>
                    <div class='form-group'><label class='control-label col-sm-2' for='titulo_g'>Titulo:</label><div class='col-sm-10'><input type='text' class='form-control' id='titulo_g' name='titulo'></div></div>
                    <div class='form-group'><label class='control-label col-sm-2' for='ruta_g'>Ruta:</label><div class='col-sm-10'><input type='text' class='form-control' id='ruta_g' name='ruta'></div></div>

                    <div class='form-group'><label class='control-label col-sm-2' for='contenido_g'>Contenido:</label><div class='col-sm-10'><textarea class='form-control textarea' rows='5' id='contenido_g' name='contenido'></textarea></div></div>

                    <div class='form-group'><label class='control-label col-sm-2' for='estado_g'>Estado:</label><div class='col-sm-10'><select class='form-control' id='estado_g' name='estado'><?php foreach ($tabla_datos_valores_estado as $registro) { ?><option value="<?=$registro['id']?>"><?=$registro['nombre']?></option><?php } ?></select></div></div>

                    <div class='form-group'><label class='control-label col-sm-2' for='desde_g'>Desde:</label><div class='col-sm-10'><input type='text' class='form-control datepicker' id='desde_g' name='desde'></div></div>
                    <div class='form-group'><label class='control-label col-sm-2' for='hasta_g'>Hasta:</label><div class='col-sm-10'><input type='text' class='form-control datepicker' id='hasta_g' name='hasta'></div></div>

                    <div class='form-group'><label class='control-label col-sm-2' for='orden_g'>Orden:</label><div class='col-sm-10'><input type='text' class='form-control' id='orden_g' name='orden'></div></div>

                    <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>  
<!-- Modal Editar - Fin -->

<!-- Modal Eliminar - Inicio -->
<div class="modal fade" id="modalEliminar" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: #d33724 !important;">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title" style="color:white">Confirmación de Eliminación</h4>
            </div>
            <div class="modal-body">
                <?php echo form_open('admin/administradores021/cms_contenidos/eliminar', array('id' => 'forma_e', 'class' => 'form-horizontal')); ?> 
                    <input type='hidden' id='id_e' name='id'>          
                    <p class="lead text-danger text-center">Esta seguro de eliminar el(los) registro(s)?<br><br><button type="button" class="btn btn-lg btn-danger">Si, eliminar el(los) registro(s)</button></p>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>  
<!-- Modal Eliminar - Fin -->


