<?php defined('BASEPATH') OR exit('No esta permitido el acceso directo a este script.'); ?>

<?php
// Bloque de codigo para presentar mensajes de alerta
if ( $this->session->flashdata('alertaMensaje') ) {
    ?>
    <div class="alert alert-<?php echo $this->session->flashdata('alertaTipo'); ?> alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?php echo $this->session->flashdata('alertaMensaje'); ?>
    </div>
    <?php
}
?>

<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Lista</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="btn-groupx" style="margin-bottom: 10px;">
                    <button type="button" class="btn btn-default text-green" id="btnNuevo"><span class="glyphicon glyphicon-plus"></span> Nuevo</button>
                    <button type="button" class="btn btn-default text-light-blue" id="btnEditar"><span class="glyphicon glyphicon-pencil"></span> Editar</button>
                    <button type="button" class="btn btn-default text-red" id="btnEliminar"><span class="glyphicon glyphicon-trash"></span> Eliminar</button>
                </div> 
                <div class="table-responsive">
                    <!-- dt-responsive -->
                    <table id="lista" class="table table-bordered table-striped" width="100%">
                        <thead>
                            <tr>
                                <th class="tdSeleccion"><input type="checkbox" id="seleccionarTodos"></th>
                                <th class="tdBotones"></th>
                                <th class="tdBotones"></th>  
                                <th>Codigo Descuento</th><th>Cliente</th><th>Desde</th><th>Hasta</th><th>Descuento</th><th>Descuento Porcentaje</th><th>Envio Gratis</th><th>Limite Usos</th><th>Categoria</th><th>Subcategoria</th><th>Entrega Desde</th><th>Entrega Hasta</th><th>Subtotal Minimo</th><th>Fecha</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->

<!-- Modal Nuevo - Inicio -->
<div class="modal fade" id="modalNuevo" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: #008d4c !important;">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title" style="color:white;">Nuevo</h4>
            </div>
            <div class="modal-body">
                <?php echo form_open('admin/administradores021/descuentos/ingresar', array('id' => 'forma_i', 'class' => 'form-horizontal')); ?>  

                    <input type='hidden' id='id_i' name='id' value='0'>
                    <div class='form-group'><label class='control-label col-sm-2' for='codigo_descuento_i'>Codigo Descuento:</label><div class='col-sm-10'><input type='text' class='form-control' id='codigo_descuento_i' name='codigo_descuento'></div></div><div class='form-group'><label class='control-label col-sm-2' for='cliente_i'>Cliente:</label><div class='col-sm-10'>
              <select class='form-control' id='cliente_i' name='cliente'>
                <option value="0"></option>
                <?php foreach ($tabla_usuarios as $registro) { ?><option value="<?=$registro['id']?>"><?=$registro['nombre']?></option><?php } ?>
              </select>
              </div></div><div class='form-group'><label class='control-label col-sm-2' for='desde_i'>Desde:</label><div class='col-sm-10'><input type='text' class='form-control datepicker' id='desde_i' name='desde'></div></div><div class='form-group'><label class='control-label col-sm-2' for='hasta_i'>Hasta:</label><div class='col-sm-10'><input type='text' class='form-control datepicker' id='hasta_i' name='hasta'></div></div><div class='form-group'><label class='control-label col-sm-2' for='descuento_i'>Descuento:</label><div class='col-sm-10'><input type='text' class='form-control' id='descuento_i' name='descuento'></div></div><div class='form-group'><label class='control-label col-sm-2' for='descuento_porcentaje_i'>Descuento Porcentaje:</label><div class='col-sm-10'><input type='text' class='form-control' id='descuento_porcentaje_i' name='descuento_porcentaje'></div></div><div class='form-group'><label class='control-label col-sm-2' for='envio_gratis_i'>Envio Gratis:</label><div class='col-sm-10'><select class='form-control' id='envio_gratis_i' name='envio_gratis'><?php foreach ($tabla_datos_valores_envio_gratis as $registro) { ?><option value="<?=$registro['id']?>"><?=$registro['nombre']?></option><?php } ?></select></div></div><div class='form-group'><label class='control-label col-sm-2' for='limite_usos_i'>Limite Usos:</label><div class='col-sm-10'><input type='text' class='form-control' id='limite_usos_i' name='limite_usos'></div></div><div class='form-group'><label class='control-label col-sm-2' for='categoria_i'>Categoria:</label><div class='col-sm-10'>
              
              <select class='form-control' id='categoria_i' name='categoria'>
                <option value="0"></option>
                <?php foreach ($tabla_categorias as $registro) { ?><option value="<?=$registro['id']?>"><?=$registro['nombre']?></option><?php } ?>
              </select>
              
              </div></div><div class='form-group'><label class='control-label col-sm-2' for='subcategoria_i'>Subcategoria:</label><div class='col-sm-10'>
              
              <select class='form-control' id='subcategoria_i' name='subcategoria'>
                <option value="0"></option>
                <?php foreach ($tabla_subcategorias as $registro) { ?><option value="<?=$registro['id']?>"><?=$registro['nombre']?></option><?php } ?>
              </select>
              
              </div></div><div class='form-group'><label class='control-label col-sm-2' for='entrega_desde_i'>Entrega Desde:</label><div class='col-sm-10'><input type='text' class='form-control datepicker' id='entrega_desde_i' name='entrega_desde'></div></div><div class='form-group'><label class='control-label col-sm-2' for='entrega_hasta_i'>Entrega Hasta:</label><div class='col-sm-10'><input type='text' class='form-control datepicker' id='entrega_hasta_i' name='entrega_hasta'></div></div><div class='form-group'><label class='control-label col-sm-2' for='subtotal_minimo_i'>Subtotal Minimo:</label><div class='col-sm-10'><input type='text' class='form-control' id='subtotal_minimo_i' name='subtotal_minimo'></div></div>

                    <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-success">Ingresar</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>  
<!-- Modal Nuevo - Fin -->

<!-- Modal Editar - Inicio -->
<div class="modal fade" id="modalEditar" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: #357ca5 !important;">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title" style="color:white;">Editar</h4>
            </div>
            <div class="modal-body">

                <?php echo form_open('admin/administradores021/descuentos/guardar', array('id' => 'forma_g', 'class' => 'form-horizontal')); ?>              

                    <input type='hidden' id='id_g' name='id'>
                    <div class='form-group'><label class='control-label col-sm-2' for='codigo_descuento_g'>Codigo Descuento:</label><div class='col-sm-10'><input type='text' class='form-control' id='codigo_descuento_g' name='codigo_descuento'></div></div><div class='form-group'><label class='control-label col-sm-2' for='cliente_g'>Cliente:</label><div class='col-sm-10'>
              <select class='form-control' id='cliente_g' name='cliente'>
                <option value="0"></option>
                <?php foreach ($tabla_usuarios as $registro) { ?><option value="<?=$registro['id']?>"><?=$registro['nombre']?></option><?php } ?>
              </select>
              </div></div><div class='form-group'><label class='control-label col-sm-2' for='desde_g'>Desde:</label><div class='col-sm-10'><input type='text' class='form-control datepicker' id='desde_g' name='desde'></div></div><div class='form-group'><label class='control-label col-sm-2' for='hasta_g'>Hasta:</label><div class='col-sm-10'><input type='text' class='form-control datepicker' id='hasta_g' name='hasta'></div></div><div class='form-group'><label class='control-label col-sm-2' for='descuento_g'>Descuento:</label><div class='col-sm-10'><input type='text' class='form-control' id='descuento_g' name='descuento'></div></div><div class='form-group'><label class='control-label col-sm-2' for='descuento_porcentaje_g'>Descuento Porcentaje:</label><div class='col-sm-10'><input type='text' class='form-control' id='descuento_porcentaje_g' name='descuento_porcentaje'></div></div><div class='form-group'><label class='control-label col-sm-2' for='envio_gratis_g'>Envio Gratis:</label><div class='col-sm-10'><select class='form-control' id='envio_gratis_g' name='envio_gratis'><?php foreach ($tabla_datos_valores_envio_gratis as $registro) { ?><option value="<?=$registro['id']?>"><?=$registro['nombre']?></option><?php } ?></select></div></div><div class='form-group'><label class='control-label col-sm-2' for='limite_usos_g'>Limite Usos:</label><div class='col-sm-10'><input type='text' class='form-control' id='limite_usos_g' name='limite_usos'></div></div><div class='form-group'><label class='control-label col-sm-2' for='categoria_g'>Categoria:</label><div class='col-sm-10'>
              
              <select class='form-control' id='categoria_g' name='categoria'>
                <option value="0"></option>
                <?php foreach ($tabla_categorias as $registro) { ?><option value="<?=$registro['id']?>"><?=$registro['nombre']?></option><?php } ?>
              </select>
              
              </div></div><div class='form-group'><label class='control-label col-sm-2' for='subcategoria_g'>Subcategoria:</label><div class='col-sm-10'>
              <select class='form-control' id='subcategoria_g' name='subcategoria'>
                <option value="0"></option>
                <?php foreach ($tabla_subcategorias as $registro) { ?><option value="<?=$registro['id']?>"><?=$registro['nombre']?></option><?php } ?>
              </select>
              
              </div></div><div class='form-group'><label class='control-label col-sm-2' for='entrega_desde_g'>Entrega Desde:</label><div class='col-sm-10'><input type='text' class='form-control datepicker' id='entrega_desde_g' name='entrega_desde'></div></div><div class='form-group'><label class='control-label col-sm-2' for='entrega_hasta_g'>Entrega Hasta:</label><div class='col-sm-10'><input type='text' class='form-control datepicker' id='entrega_hasta_g' name='entrega_hasta'></div></div><div class='form-group'><label class='control-label col-sm-2' for='subtotal_minimo_g'>Subtotal Minimo:</label><div class='col-sm-10'><input type='text' class='form-control' id='subtotal_minimo_g' name='subtotal_minimo'></div></div>

                    <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>  
<!-- Modal Editar - Fin -->

<!-- Modal Eliminar - Inicio -->
<div class="modal fade" id="modalEliminar" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: #d33724 !important;">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title" style="color:white">Confirmación de Eliminación</h4>
            </div>
            <div class="modal-body">
                <?php echo form_open('admin/administradores021/descuentos/eliminar', array('id' => 'forma_e', 'class' => 'form-horizontal')); ?> 
                    <input type='hidden' id='id_e' name='id'>          
                    <p class="lead text-danger text-center">Esta seguro de eliminar el(los) registro(s)?<br><br><button type="button" class="btn btn-lg btn-danger">Si, eliminar el(los) registro(s)</button></p>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>  
<!-- Modal Eliminar - Fin -->


