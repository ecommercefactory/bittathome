<?php defined('BASEPATH') OR exit('No esta permitido el acceso directo a este script.'); ?>

<?php
// Bloque de codigo para presentar mensajes de alerta
if ( $this->session->flashdata('alertaMensaje') ) {
    ?>
    <div class="alert alert-<?php echo $this->session->flashdata('alertaTipo'); ?> alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?php echo $this->session->flashdata('alertaMensaje'); ?>
    </div>
    <?php
}
?>

<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Lista</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="btn-groupx" style="margin-bottom: 10px;">
                    <button type="button" class="btn btn-default text-green" id="btnNuevo"><span class="glyphicon glyphicon-plus"></span> Nuevo</button>
                    <button type="button" class="btn btn-default text-light-blue" id="btnEditar"><span class="glyphicon glyphicon-pencil"></span> Editar</button>
                    <button type="button" class="btn btn-default text-red" id="btnEliminar"><span class="glyphicon glyphicon-trash"></span> Eliminar</button>
                </div> 
                <div class="table-responsive">
                    <!-- dt-responsive -->
                    <table id="lista" class="table table-bordered table-striped" width="100%">
                        <thead>
                            <tr>
                                <th class="tdSeleccion"><input type="checkbox" id="seleccionarTodos"></th>
                                <th class="tdBotones"></th>
                                <th class="tdBotones"></th>  
                                <th>Tipo</th><th>Ruta</th><th>Imagen Desktop</th><th>Imagen Movil</th><th>Fecha</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->

<!-- Modal Nuevo - Inicio -->
<div class="modal fade" id="modalNuevo" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: #008d4c !important;">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title" style="color:white;">Nuevo</h4>
            </div>
            <div class="modal-body">
                <?php echo form_open_multipart('admin/administradores021/banner/ingresar', array('id' => 'forma_i', 'class' => 'form-horizontal')); ?>  

                    <input type='hidden' id='id_i' name='id' value='0'>

<div class='form-group'><label class='control-label col-sm-2' for='tipo_i'>Tipo:</label><div class='col-sm-10'><select class='form-control' id='tipo_i' name='tipo'><?php foreach ($tabla_datos_valores_tipo as $registro) { ?><option value="<?=$registro['id']?>"><?=$registro['nombre']?></option><?php } ?></select></div></div>              
              
<div class='form-group'><label class='control-label col-sm-2' for='ruta_i'>Ruta:</label>
  <div class='col-sm-10' id='select_ruta_i'>
  <select class='form-control' id='ruta_i' name='ruta'>
    <option value='0'>Inicio</option>
  </select>
  </div>
</div> 
              
              
              <div class='form-group'><label class='control-label col-sm-2' for='imgDesktop_i'>Imagen Desktop:</label><div class='col-sm-10'><input type='file' class='form-control' id='imgDesktop_i' name='imgDesktop'></div></div><div class='form-group'><label class='control-label col-sm-2' for='imgMovil_i'>Imagen Movil:</label><div class='col-sm-10'><input type='file' class='form-control' id='imgMovil_i' name='imgMovil'></div></div>

                    <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-success">Ingresar</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>  
<!-- Modal Nuevo - Fin -->

<!-- Modal Editar - Inicio -->
<div class="modal fade" id="modalEditar" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: #357ca5 !important;">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title" style="color:white;">Editar</h4>
            </div>
            <div class="modal-body">

                <?php echo form_open_multipart('admin/administradores021/banner/guardar', array('id' => 'forma_g', 'class' => 'form-horizontal')); ?>              

                    <input type='hidden' id='id_g' name='id'>

<div class='form-group'><label class='control-label col-sm-2' for='tipo_g'>Tipo:</label><div class='col-sm-10'><select class='form-control' id='tipo_g' name='tipo'><?php foreach ($tabla_datos_valores_tipo as $registro) { ?><option value="<?=$registro['id']?>"><?=$registro['nombre']?></option><?php } ?></select></div></div>  
              
<div class='form-group'><label class='control-label col-sm-2' for='ruta_g'>Ruta:</label><div class='col-sm-10' id='select_ruta_g'><select class='form-control' id='ruta_g' name='ruta'><option></option></select></div></div>
              
              <div class='form-group'><label class='control-label col-sm-2' for='imgDesktop_g'>Imagen Desktop:</label><div class='col-sm-10'><div id='imgDesktop_g_ver'></div><input type='file' class='form-control' id='imgDesktop_g' name='imgDesktop'></div></div>
              
              <div class='form-group'><label class='control-label col-sm-2' for='imgMovil_g'>Imagen Movil:</label><div class='col-sm-10'><div id='imgMovil_g_ver'></div><input type='file' class='form-control' id='imgMovil_g' name='imgMovil'></div></div>
              
              

                    <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>  
<!-- Modal Editar - Fin -->

<!-- Modal Eliminar - Inicio -->
<div class="modal fade" id="modalEliminar" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: #d33724 !important;">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title" style="color:white">Confirmación de Eliminación</h4>
            </div>
            <div class="modal-body">
                <?php echo form_open('admin/administradores021/banner/eliminar', array('id' => 'forma_e', 'class' => 'form-horizontal')); ?> 
                    <input type='hidden' id='id_e' name='id'>          
                    <p class="lead text-danger text-center">Esta seguro de eliminar el(los) registro(s)?<br><br><button type="button" class="btn btn-lg btn-danger">Si, eliminar el(los) registro(s)</button></p>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>  
<!-- Modal Eliminar - Fin -->


