<?php defined('BASEPATH') OR exit('No esta permitido el acceso directo a este script.'); ?>

<?php
// Bloque de codigo para presentar mensajes de alerta
if ( $this->session->flashdata('alertaMensaje') ) {
    ?>
    <div class="alert alert-<?php echo $this->session->flashdata('alertaTipo'); ?> alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?php echo $this->session->flashdata('alertaMensaje'); ?>
    </div>
    <?php
}
?>

<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Lista</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="btn-groupx" style="margin-bottom: 10px;">
                    <button type="button" class="btn btn-default text-green" id="btnNuevo"><span class="glyphicon glyphicon-plus"></span> Nuevo</button>
                    <button type="button" class="btn btn-default text-light-blue" id="btnEditar"><span class="glyphicon glyphicon-pencil"></span> Editar</button>
                    <button type="button" class="btn btn-default text-red" id="btnEliminar"><span class="glyphicon glyphicon-trash"></span> Eliminar</button>
                </div> 
                <div class="table-responsive">
                    <!-- dt-responsive -->
                    <table id="lista" class="table table-bordered table-striped" width="100%">
                        <thead>
                            <tr>
                                <th class="tdSeleccion"><input type="checkbox" id="seleccionarTodos"></th>
                                <th class="tdBotones"></th>
                                <th class="tdBotones"></th>  
                                <th>Codigo</th><th>Nombre_En</th><th>Nombre_Es</th><th>Nombre_Fr</th><th>Nombre_It</th><th>Nombre_Pt</th><th>Nombre_De</th><th>Nombre_Cn</th><th>Nombre_Ru</th><th>Codigo_Telefono</th><th>Moneda</th><th>Gmt</th><th>Lang</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->

<!-- Modal Nuevo - Inicio -->
<div class="modal fade" id="modalNuevo" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: #008d4c !important;">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title" style="color:white;">Nuevo</h4>
            </div>
            <div class="modal-body">
                <?php echo form_open('admin/administradores021/paises/ingresar', array('id' => 'forma_i', 'class' => 'form-horizontal')); ?>  

                    <input type='hidden' id='id_i' name='id' value='0'>
                    <div class='form-group'><label class='control-label col-sm-2' for='codigo_i'>Codigo:</label><div class='col-sm-10'><input type='text' class='form-control' id='codigo_i' name='codigo'></div></div><div class='form-group'><label class='control-label col-sm-2' for='nombre_en_i'>Nombre_En:</label><div class='col-sm-10'><input type='text' class='form-control' id='nombre_en_i' name='nombre_en'></div></div><div class='form-group'><label class='control-label col-sm-2' for='nombre_es_i'>Nombre_Es:</label><div class='col-sm-10'><input type='text' class='form-control' id='nombre_es_i' name='nombre_es'></div></div><div class='form-group'><label class='control-label col-sm-2' for='nombre_fr_i'>Nombre_Fr:</label><div class='col-sm-10'><input type='text' class='form-control' id='nombre_fr_i' name='nombre_fr'></div></div><div class='form-group'><label class='control-label col-sm-2' for='nombre_it_i'>Nombre_It:</label><div class='col-sm-10'><input type='text' class='form-control' id='nombre_it_i' name='nombre_it'></div></div><div class='form-group'><label class='control-label col-sm-2' for='nombre_pt_i'>Nombre_Pt:</label><div class='col-sm-10'><input type='text' class='form-control' id='nombre_pt_i' name='nombre_pt'></div></div><div class='form-group'><label class='control-label col-sm-2' for='nombre_de_i'>Nombre_De:</label><div class='col-sm-10'><input type='text' class='form-control' id='nombre_de_i' name='nombre_de'></div></div><div class='form-group'><label class='control-label col-sm-2' for='nombre_cn_i'>Nombre_Cn:</label><div class='col-sm-10'><input type='text' class='form-control' id='nombre_cn_i' name='nombre_cn'></div></div><div class='form-group'><label class='control-label col-sm-2' for='nombre_ru_i'>Nombre_Ru:</label><div class='col-sm-10'><input type='text' class='form-control' id='nombre_ru_i' name='nombre_ru'></div></div><div class='form-group'><label class='control-label col-sm-2' for='codigo_telefono_i'>Codigo_Telefono:</label><div class='col-sm-10'><input type='text' class='form-control' id='codigo_telefono_i' name='codigo_telefono'></div></div><div class='form-group'><label class='control-label col-sm-2' for='moneda_i'>Moneda:</label><div class='col-sm-10'><select class='form-control' id='moneda_i' name='moneda'><?php foreach ($tabla_datos_valores as $registro) { ?><option value="<?=$registro['id']?>"><?=$registro['nombre']?></option><?php } ?></select></div></div><div class='form-group'><label class='control-label col-sm-2' for='gmt_i'>Gmt:</label><div class='col-sm-10'><input type='text' class='form-control' id='gmt_i' name='gmt'></div></div><div class='form-group'><label class='control-label col-sm-2' for='lang_i'>Lang:</label><div class='col-sm-10'><input type='text' class='form-control' id='lang_i' name='lang'></div></div>

                    <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-success">Ingresar</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>  
<!-- Modal Nuevo - Fin -->

<!-- Modal Editar - Inicio -->
<div class="modal fade" id="modalEditar" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: #357ca5 !important;">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title" style="color:white;">Editar</h4>
            </div>
            <div class="modal-body">

                <?php echo form_open('admin/administradores021/paises/guardar', array('id' => 'forma_g', 'class' => 'form-horizontal')); ?>              

                    <input type='hidden' id='id_g' name='id'>
                    <div class='form-group'><label class='control-label col-sm-2' for='codigo_g'>Codigo:</label><div class='col-sm-10'><input type='text' class='form-control' id='codigo_g' name='codigo'></div></div><div class='form-group'><label class='control-label col-sm-2' for='nombre_en_g'>Nombre_En:</label><div class='col-sm-10'><input type='text' class='form-control' id='nombre_en_g' name='nombre_en'></div></div><div class='form-group'><label class='control-label col-sm-2' for='nombre_es_g'>Nombre_Es:</label><div class='col-sm-10'><input type='text' class='form-control' id='nombre_es_g' name='nombre_es'></div></div><div class='form-group'><label class='control-label col-sm-2' for='nombre_fr_g'>Nombre_Fr:</label><div class='col-sm-10'><input type='text' class='form-control' id='nombre_fr_g' name='nombre_fr'></div></div><div class='form-group'><label class='control-label col-sm-2' for='nombre_it_g'>Nombre_It:</label><div class='col-sm-10'><input type='text' class='form-control' id='nombre_it_g' name='nombre_it'></div></div><div class='form-group'><label class='control-label col-sm-2' for='nombre_pt_g'>Nombre_Pt:</label><div class='col-sm-10'><input type='text' class='form-control' id='nombre_pt_g' name='nombre_pt'></div></div><div class='form-group'><label class='control-label col-sm-2' for='nombre_de_g'>Nombre_De:</label><div class='col-sm-10'><input type='text' class='form-control' id='nombre_de_g' name='nombre_de'></div></div><div class='form-group'><label class='control-label col-sm-2' for='nombre_cn_g'>Nombre_Cn:</label><div class='col-sm-10'><input type='text' class='form-control' id='nombre_cn_g' name='nombre_cn'></div></div><div class='form-group'><label class='control-label col-sm-2' for='nombre_ru_g'>Nombre_Ru:</label><div class='col-sm-10'><input type='text' class='form-control' id='nombre_ru_g' name='nombre_ru'></div></div><div class='form-group'><label class='control-label col-sm-2' for='codigo_telefono_g'>Codigo_Telefono:</label><div class='col-sm-10'><input type='text' class='form-control' id='codigo_telefono_g' name='codigo_telefono'></div></div><div class='form-group'><label class='control-label col-sm-2' for='moneda_g'>Moneda:</label><div class='col-sm-10'><select class='form-control' id='moneda_g' name='moneda'><?php foreach ($tabla_datos_valores as $registro) { ?><option value="<?=$registro['id']?>"><?=$registro['nombre']?></option><?php } ?></select></div></div><div class='form-group'><label class='control-label col-sm-2' for='gmt_g'>Gmt:</label><div class='col-sm-10'><input type='text' class='form-control' id='gmt_g' name='gmt'></div></div><div class='form-group'><label class='control-label col-sm-2' for='lang_g'>Lang:</label><div class='col-sm-10'><input type='text' class='form-control' id='lang_g' name='lang'></div></div>

                    <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>  
<!-- Modal Editar - Fin -->

<!-- Modal Eliminar - Inicio -->
<div class="modal fade" id="modalEliminar" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: #d33724 !important;">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title" style="color:white">Confirmación de Eliminación</h4>
            </div>
            <div class="modal-body">
                <?php echo form_open('admin/administradores021/paises/eliminar', array('id' => 'forma_e', 'class' => 'form-horizontal')); ?> 
                    <input type='hidden' id='id_e' name='id'>          
                    <p class="lead text-danger text-center">Esta seguro de eliminar el(los) registro(s)?<br><br><button type="button" class="btn btn-lg btn-danger">Si, eliminar el(los) registro(s)</button></p>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>  
<!-- Modal Eliminar - Fin -->


