<?php defined('BASEPATH') OR exit('No esta permitido el acceso directo a este script.'); ?>

<?php
// Bloque de codigo para presentar mensajes de alerta
if ( $this->session->flashdata('alertaMensaje') ) {
    ?>
    <div class="alert alert-<?php echo $this->session->flashdata('alertaTipo'); ?> alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?php echo $this->session->flashdata('alertaMensaje'); ?>
    </div>
    <?php
}
?>


        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: #357ca5 !important;">
                <h4 class="modal-title" style="color:white;">Editar</h4>
            </div>
            <div class="modal-body">

                <?php echo form_open_multipart('admin/administradores021/plantilla/guardar', array('id' => 'forma_g', 'class' => 'form-horizontal')); ?>              

                    <input type='hidden' id='id_g' name='id'>

                    <fieldset>
                        <legend>Colores</legend>

                            <div class='form-group'><label class='control-label col-sm-2' for='barraSuperior_g'>Color Barra Superior:</label><div class='col-sm-10'><input type='text' class='form-control color' id='barraSuperior_g' name='barraSuperior'></div></div>
                      <div class='form-group'><label class='control-label col-sm-2' for='textoSuperior_g'>Color Texto Superior:</label><div class='col-sm-10'><input type='text' class='form-control color' id='textoSuperior_g' name='textoSuperior'></div></div>
                      <div class='form-group'><label class='control-label col-sm-2' for='colorFondo_g'>Color Fondo:</label><div class='col-sm-10'><input type='text' class='form-control color' id='colorFondo_g' name='colorFondo'></div></div>
                      <div class='form-group'><label class='control-label col-sm-2' for='colorTexto_g'>ColorTexto:</label><div class='col-sm-10'><input type='text' class='form-control color' id='colorTexto_g' name='colorTexto'></div></div>

                    </fieldset>
              

                    <fieldset>
                        <legend>Logo + Icono</legend>

                           <div class='form-group'><label class='control-label col-sm-2' for='logo_g'>Logo:</label><div class='col-sm-10'><div id='logo_g_ver'></div><input type='file' class='form-control' id='logo_g' name='logo'></div></div>
                            
                            <div class='form-group'><label class='control-label col-sm-2' for='icono_g'>Icono:</label><div class='col-sm-10'><div id='icono_g_ver'></div><input type='file' class='form-control' id='icono_g' name='icono'></div></div>

                    </fieldset>

                    <fieldset>
                        <legend>Facebook</legend>

                        <div class='form-group'><label class='control-label col-sm-2' for='apiFacebook_g'>Api Facebook:</label>
                          <div class='col-sm-10'>
                            <textarea class="form-control" rows="5" id='apiFacebook_g' name='apiFacebook' disabled></textarea>
                          </div>
                        </div>
                        <div class='form-group'><label class='control-label col-sm-2' for='pixelFacebook_g'>Pixel Facebook:</label>
                          <div class='col-sm-10'>
                            <textarea class="form-control" rows="5" id='pixelFacebook_g' name='pixelFacebook' disabled></textarea>
                          </div>
                        </div>

                    </fieldset>

          

                    <fieldset>
                        <legend>Google</legend>

                        <div class='form-group'><label class='control-label col-sm-2' for='googleAnalytics_g'>Google Analytics:</label>
                          <div class='col-sm-10'>
                            <textarea class="form-control" rows="5" id='googleAnalytics_g' name='googleAnalytics' disabled></textarea>
                          </div>
                        </div>

                    </fieldset>




                    <fieldset>
                        <legend>SmartLook</legend>

                        <div class='form-group'><label class='control-label col-sm-2' for='googleAnalytics_g'>SmartLook:</label>
                          <div class='col-sm-10'>
                            <textarea class="form-control" rows="5" id='googleAnalytics_g' name='googleAnalytics' disabled><script type="text/javascript">
      window.smartlook||(function(d) {
      var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
      var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
      c.charset='utf-8';c.src='https://rec.smartlook.com/recorder.js';h.appendChild(c);
      })(document);
      smartlook('init', '0800f69490a75cb5de70895196af6058a57feca5');
  </script></textarea>
                          </div>
                        </div>
                        
                    </fieldset>

                    <fieldset>
                        <legend>MailChimp</legend>

                        <div class='form-group'>
                          <label class='control-label col-sm-2' for='mailchimp_api_key'>
                            API Key
                          </label>
                          <div class='col-sm-10'>
                            <input type='text' class='form-control' id='mailchimp_api_key' name='mailchimp_api_key' value="MTM0NS0zNDE4LWVhc3ljb191c3I1">
                          </div>
                        </div>

                        <div class='form-group'>
                          <label class='control-label col-sm-2' for='mailchimp_profile_key'>
                            Profile Key
                          </label>
                          <div class='col-sm-10'>
                            <input type='text' class='form-control' id='mailchimp_profile_key' name='mailchimp_profile_key' value="MjM0NjEz0">
                          </div>
                        </div>                        

                    </fieldset>



                    <hr>

                    <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-primary">Guardar</button>
                            
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <a href="<?php echo base_url(); ?>admin/administradores021/plantilla_redes_sociales/1" class="btn btn-success">Redes Sociales</a>
            </div>
        </div>

