<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<ul class="sidebar-menu">
	<li class="header"><?=$this->lang->line('be_menu_menu')?></li>
	<li class="treeview">
		<a href="#">
			<i class="fa fa-user"></i> <span><?=$this->lang->line('be_menu_my_data')?></span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo base_url(); ?>admin/mis_datos/perfil/index/edit/<?=$this->session->userdata($this->config->item('raiz') . 'be_usuario_id')?>"><i class="fa fa-circle-o"></i> <?=$this->lang->line('be_menu_profile')?></a></li>
			<li><a href="<?php echo base_url(); ?>admin/mis_datos/cambiar_clave"><i class="fa fa-circle-o"></i> <?=$this->lang->line('be_menu_change_password')?></a></li>
		</ul>
	</li>
</ul>