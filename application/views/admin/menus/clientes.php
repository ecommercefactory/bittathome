<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<ul class="sidebar-menu">
	<li class="header"><?=$this->lang->line('be_menu_menu')?></li>


	<li class="treeview">
		<a href="#">
			<i class="fa fa-home"></i> <span>Inicio</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href="#"><i class="fa fa-circle-o"></i> Lista de Tiendas</a></li>
			<li><a href="#"><i class="fa fa-circle-o"></i> Nueva Tienda</a></li>
			<li><a href="#"><i class="fa fa-circle-o"></i> SEO</a></li>
			<li><a href="#"><i class="fa fa-circle-o"></i> Registro</a></li>
			<li><a href="#"><i class="fa fa-circle-o"></i> Mi Cuenta</a></li>
			<li><a href="#"><i class="fa fa-circle-o"></i> Dominio</a></li>
			<li><a href="#"><i class="fa fa-circle-o"></i> Diseño</a></li>
			<li><a href="#"><i class="fa fa-circle-o"></i> Medios de Pago</a></li>
			<li><a href="#"><i class="fa fa-circle-o"></i> Medios de Envio</a></li>
			<li><a href="#"><i class="fa fa-circle-o"></i> Productos</a></li>
		</ul>
	</li>


	<li class="treeview">
		<a href="#">
			<i class="fa fa-user"></i> <span>Mi Cuenta</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href="#"><i class="fa fa-circle-o"></i> Plan</a></li>
			<li><a href="#"><i class="fa fa-circle-o"></i> Facturacion</a></li>
			<li><a href="#"><i class="fa fa-circle-o"></i> Medios de Pago</a></li>
		</ul>
	</li>

	<li class="treeview">
		<a href="#">
			<i class="fa fa-shopping-cart"></i> <span>Ventas</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href="#"><i class="fa fa-circle-o"></i> Lista de Pedidos</a></li>
			<li><a href="#"><i class="fa fa-circle-o"></i> Nuevo Pedido</a></li>
			<li><a href="#"><i class="fa fa-circle-o"></i> Carritos de Compra</a></li>
		</ul>
	</li>

	<li class="treeview">
		<a href="#">
			<i class="fa fa-group"></i> <span>Clientes</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href="#"><i class="fa fa-circle-o"></i> Lista de Clientes</a></li>
			<li><a href="#"><i class="fa fa-circle-o"></i> Nuevo Cliente</a></li>
		</ul>
	</li>

	<li class="treeview">
		<a href="#">
			<i class="fa fa-money"></i> <span>Descuentos</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href="#"><i class="fa fa-circle-o"></i> Lista de Descuentos</a></li>
			<li><a href="#"><i class="fa fa-circle-o"></i> Nuevo Descuento</a></li>
		</ul>
	</li>

	<li class="treeview">
		<a href="#">
			<i class="fa fa-bolt"></i> <span>Mercadolibre</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href="#"><i class="fa fa-circle-o"></i> Configuracion</a></li>
		</ul>
	</li>

	<li class="treeview">
		<a href="#">
			<i class="fa fa-cubes"></i> <span>Catálogo</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href="#"><i class="fa fa-circle-o"></i> Lista de Productos</a></li>
			<li><a href="#"><i class="fa fa-circle-o"></i> Nuevo Producto</a></li>
			<li><a href="#"><i class="fa fa-circle-o"></i> Inventarios</a></li>
			<li><a href="#"><i class="fa fa-circle-o"></i> Categorias</a></li>
			<li><a href="#"><i class="fa fa-circle-o"></i> Caracteristicas</a></li>
			<li><a href="#"><i class="fa fa-circle-o"></i> SEO</a></li>
		</ul>
	</li>

	<li class="treeview">
		<a href="#">
			<i class="fa fa-bar-chart"></i> <span>Analítica</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href="#"><i class="fa fa-circle-o"></i> Reportes</a></li>
			<li><a href="#"><i class="fa fa-circle-o"></i> Indicadores</a></li>
		</ul>
	</li>

	<li class="treeview">
		<a href="#">
			<i class="fa fa-file-picture-o"></i> <span>Diseño</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href="#"><i class="fa fa-circle-o"></i> Lista de Plantillas</a></li>
			<li><a href="#"><i class="fa fa-circle-o"></i> Plantilla</a></li>
			<li><a href="#"><i class="fa fa-circle-o"></i> Contenidos</a></li>
			<li><a href="#"><i class="fa fa-circle-o"></i> Multimedia</a></li>
		</ul>
	</li>

	<li class="treeview">
		<a href="#">
			<i class="fa fa-cloud"></i> <span>Aplicaciones</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href="#"><i class="fa fa-circle-o"></i> Google</a></li>
			<li><a href="#"><i class="fa fa-circle-o"></i> Google Analytics</a></li>
			<li><a href="#"><i class="fa fa-circle-o"></i> Google AdWords</a></li>
			<li><a href="#"><i class="fa fa-circle-o"></i> Facebook Pixel</a></li>
		</ul>
	</li>

	<li class="treeview">
		<a href="#">
			<i class="fa fa-cog"></i> <span>Configuración</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href="#"><i class="fa fa-circle-o"></i> General</a></li>
			<li><a href="#"><i class="fa fa-circle-o"></i> Medios de Pago</a></li>
			<li><a href="#"><i class="fa fa-circle-o"></i> Medios de Envio</a></li>
			<li><a href="#"><i class="fa fa-circle-o"></i> Dominios y Correos</a></li>
		</ul>
	</li>

	<li class="treeview">
		<a href="#">
			<i class="fa fa-life-saver"></i> <span>Ayuda</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href="#"><i class="fa fa-circle-o"></i> Preguntas Frecuentes</a></li>
			<li><a href="#"><i class="fa fa-circle-o"></i> Manual </a></li>
			<li><a href="#"><i class="fa fa-circle-o"></i> Tutorial</a></li>
			<li><a href="#"><i class="fa fa-circle-o"></i> Tiquetes</a></li>
		</ul>
	</li>

	<li class="treeview">
		<a href="#">
			<i class="fa fa-user"></i> <span><?=$this->lang->line('be_menu_my_data')?></span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo base_url(); ?>admin/mis_datos/perfil/index/edit/<?=$this->session->userdata($this->config->item('raiz') . 'be_usuario_id')?>"><i class="fa fa-circle-o"></i> <?=$this->lang->line('be_menu_profile')?></a></li>
			<li><a href="<?php echo base_url(); ?>admin/mis_datos/cambiar_clave"><i class="fa fa-circle-o"></i> <?=$this->lang->line('be_menu_change_password')?></a></li>
		</ul>
	</li>

</ul>