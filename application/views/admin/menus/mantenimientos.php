<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<ul class="sidebar-menu">
	<li class="header"><?=$this->lang->line('be_menu_menu')?></li>

	<li class="treeview">
		<a href="#">
			<i class="fa fa-code"></i> <span>Mantenimiento</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo base_url(); ?>admin/mantenimientos/datos"><i class="fa fa-circle-o"></i> <?=$this->lang->line('be_menu_data')?></a></li>
			<li><a href="<?php echo base_url(); ?>admin/mantenimientos/languages"><i class="fa fa-circle-o"></i> <?=$this->lang->line('be_menu_languages')?></a></li>
			<li><a href="<?php echo base_url(); ?>admin/mantenimientos/variables"><i class="fa fa-circle-o"></i> <?=$this->lang->line('be_menu_variables')?></a></li>

			<li><a href="<?php echo base_url(); ?>admin/mantenimientos/monedas"><i class="fa fa-circle-o"></i> Monedas</a></li>
      
      
      <li><a href="<?php echo base_url(); ?>admin/mantenimientos/configuracion"><i class="fa fa-circle-o"></i> Tablas y CRUDs</a></li>

			<li><a href="<?php echo base_url(); ?>admin/proyectos/proyectos"><i class="fa fa-circle-o"></i> Proyectos</a></li>
			<li><a href="<?php echo base_url(); ?>admin/administracion/usuarios"><i class="fa fa-circle-o"></i> Usuarios</a></li>
<!--
<li><a href="#"><i class="fa fa-circle-o"></i> Archivos</a></li>
-->
		</ul>
	</li>

	<li class="treeview">
		<a href="#">
			<i class="fa fa-road"></i> <span>Versiones</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li>
				<a href="#"><i class="fa fa-circle-o"></i> V.0.1
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li><a href="<?php echo base_url(); ?>admin/mantenimientos/front01" target="_blank"><i class="fa fa-circle-o"></i> Front</a></li>
					<li><a href="<?php echo base_url(); ?>admin/mantenimientos/back01" target="_blank"><i class="fa fa-circle-o"></i> Back</a></li>
				</ul>
			</li>
		</ul>
	</li>

	<li class="treeview">
		<a href="#">
			<i class="fa fa-user"></i> <span><?=$this->lang->line('be_menu_my_data')?></span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo base_url(); ?>admin/mis_datos/perfil/index/edit/<?=$this->session->userdata($this->config->item('raiz') . 'be_usuario_id')?>"><i class="fa fa-circle-o"></i> <?=$this->lang->line('be_menu_profile')?></a></li>
			<li><a href="<?php echo base_url(); ?>admin/mis_datos/cambiar_clave"><i class="fa fa-circle-o"></i> <?=$this->lang->line('be_menu_change_password')?></a></li>
		</ul>
	</li>
</ul>