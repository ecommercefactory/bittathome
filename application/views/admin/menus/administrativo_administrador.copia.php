<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!--=====================================
MENU
======================================-->	

<ul class="sidebar-menu">

	<li class="active"><a href="<?php echo base_url();?>admin/panelcontrol"><i class="fa fa-home"></i> <span>Inicio*</span></a></li>

	<li><a href="<?php echo base_url();?>admin/administradores02/comercio"><i class="fa fa-files-o"></i> <span>Gestor Comercio</span></a></li>

	<li><a href="<?php echo base_url();?>admin/administradores02/carrusel"><i class="fa fa-edit"></i> <span>Carrusel</span></a></li>

	<li class="treeview">

		<a href="#">
			<i class="fa fa-th"></i>
			<span>Gestor Categorías</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>

		<ul class="treeview-menu">

			<li><a href="<?php echo base_url();?>admin/administradores02/categorias"><i class="fa fa-circle-o"></i> Categorías</a></li>
			<li><a href="<?php echo base_url();?>admin/administradores02/subcategorias"><i class="fa fa-circle-o"></i> Subcategorías</a></li>

		</ul>

	</li>

	<li class="treeview">

		<a href="#">
			<i class="fa fa-product-hunt"></i>
			<span>Gestor Productos</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>

		<ul class="treeview-menu">

			<li><a href="<?php echo base_url();?>admin/administradores02/productos"><i class="fa fa-circle-o"></i> Productos</a></li>
			<li><a href="<?php echo base_url();?>admin/administradores02/especificaciones"><i class="fa fa-circle-o"></i> Especificaciones</a></li>

		</ul>

	</li>

	<li><a href="<?php echo base_url();?>admin/administradores02/banner"><i class="fa fa-map-o"></i> <span>Gestor Banner</span></a></li>

	<li><a href="<?php echo base_url();?>admin/administradores02/ordenes"><i class="fa fa-shopping-cart"></i> <span>Gestor Ventas</span></a></li>

	<li><a href="<?php echo base_url();?>admin/administradores02/visitas_paises"><i class="fa fa-map-marker"></i> <span>Gestor Visitas Paises</span></a></li>
	<li><a href="<?php echo base_url();?>admin/administradores02/visitas_personas"><i class="fa fa-map-marker"></i> <span>Gestor Visitas Personas</span></a></li>  

	<li><a href="<?php echo base_url();?>admin/administradores02/usuarios"><i class="fa fa-users"></i> <span>Gestor Usuarios</span></a></li>

	<li><a href="<?php echo base_url();?>admin/administradores/perfiles"><i class="fa fa-key"></i> <span>Gestor Perfiles *</span></a></li>

</ul>	

<?php
/*
<ul class="sidebar-menu">
	<li class="header"><?=$this->lang->line('be_menu_menu')?></li>


	<li><a href="<?php echo base_url(); ?>admin/proyectos/proyectos"><i class="fa fa-industry"></i> <span><?=$this->lang->line('be_menu_projects')?></span></a></li>


<!-- ** Clientes ****************************************************** -->
	<li class="treeview">
		<a href="#">
			<i class="fa fa-shopping-cart"></i> <span>Tiendas</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href="#"><i class="fa fa-circle-o"></i> Tiendas</a></li>
		</ul>
	</li>

<!-- ** Clientes ****************************************************** -->
	<li class="treeview">
		<a href="#">
			<i class="fa fa-users"></i> <span>Clientes</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo base_url(); ?>admin/clientes/usuarios"><i class="fa fa-circle-o"></i> Usuarios</a></li>
		</ul>
	</li>
<!-- ** Comerciales ****************************************************** -->
	<li class="treeview">
		<a href="#">
			<i class="fa fa-suitcase"></i> <span>Comerciales</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo base_url(); ?>admin/comerciales/usuarios"><i class="fa fa-circle-o"></i> Usuarios</a></li>
		</ul>
	</li>
<!-- ** Soporte ****************************************************** -->
	<li class="treeview">
		<a href="#">
			<i class="fa fa-support"></i> <span>Soporte</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo base_url(); ?>admin/soportes/usuarios"><i class="fa fa-circle-o"></i> Usuarios</a></li>
		</ul>
	</li>
<!-- ** Mantenimiento ****************************************************** -->
	<li class="treeview">
		<a href="#">
			<i class="fa fa-code"></i> <span>Mantenimiento</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo base_url(); ?>admin/mantenimientos/usuarios"><i class="fa fa-circle-o"></i> Usuarios</a></li>
		</ul>
	</li>
<!-- ** Diseñadores ****************************************************** -->
	<li class="treeview">
		<a href="#">
			<i class="fa fa-picture-o"></i> <span>Diseñadores</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo base_url(); ?>admin/disenos/usuarios"><i class="fa fa-circle-o"></i> Usuarios</a></li>
		</ul>
	</li>
<!-- ** Cuentas ****************************************************** -->
	<li class="treeview">
		<a href="#">
			<i class="fa fa-calculator"></i> <span>Cuentas</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo base_url(); ?>admin/cuentas/usuarios"><i class="fa fa-circle-o"></i> Usuarios</a></li>
		</ul>
	</li>

<!-- ** Administracion ****************************************************** -->
	<li class="treeview">
		<a href="#">
			<i class="fa fa-cog"></i> <span><?=$this->lang->line('be_menu_administration')?></span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo base_url(); ?>admin/administracion/usuarios"><i class="fa fa-circle-o"></i> <?=$this->lang->line('be_menu_users')?></a></li>
			<li><a href="<?php echo base_url(); ?>admin/administracion/paises"><i class="fa fa-circle-o"></i> <?=$this->lang->line('be_menu_countries')?></a></li>
			<li><a href="<?php echo base_url(); ?>admin/administracion/ips_bloqueadas"><i class="fa fa-circle-o"></i> <?=$this->lang->line('be_menu_blocked_ips')?></a></li>
		</ul>
	</li>

<!-- ** Mis datos ****************************************************** -->
	<li class="treeview">
		<a href="#">
			<i class="fa fa-user"></i> <span><?=$this->lang->line('be_menu_my_data')?></span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo base_url(); ?>admin/mis_datos/perfil/index/edit/<?=$this->session->userdata($this->config->item('raiz') . 'be_usuario_id')?>"><i class="fa fa-circle-o"></i> <?=$this->lang->line('be_menu_profile')?></a></li>
			<li><a href="<?php echo base_url(); ?>admin/mis_datos/cambiar_clave"><i class="fa fa-circle-o"></i> <?=$this->lang->line('be_menu_change_password')?></a></li>
		</ul>
	</li>
</ul>
*/
?>