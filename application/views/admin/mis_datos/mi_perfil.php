<?php defined('BASEPATH') OR exit('No esta permitido el acceso directo a este script.'); ?>

<?php
// Bloque de codigo para presentar mensajes de alerta
if ( $this->session->flashdata('alertaMensaje') ) {
    ?>
    <div class="alert alert-<?php echo $this->session->flashdata('alertaTipo'); ?> alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?php echo $this->session->flashdata('alertaMensaje'); ?>
    </div>
    <?php
}
?>

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: #357ca5 !important;">
                <h4 class="modal-title" style="color:white;">Editar</h4>
            </div>
            <div class="modal-body">

                <?php echo form_open_multipart('admin/mis_datos/mi_perfil/guardar', array('id' => 'forma_g', 'class' => 'form-horizontal')); ?>              

                    <input type='hidden' id='id_g' name='id'>
                    <div class='form-group'><label class='control-label col-sm-2' for='nombre_g'>Nombre:</label><div class='col-sm-10'><input type='text' class='form-control' id='nombre_g' name='nombre'></div></div><div class='form-group'><label class='control-label col-sm-2' for='apellidos_g'>Apellidos:</label><div class='col-sm-10'><input type='text' class='form-control' id='apellidos_g' name='apellidos'></div></div><div class='form-group'><label class='control-label col-sm-2' for='perfil_g'>Perfil:</label><div class='col-sm-10'><select class='form-control' id='perfil_g' name='perfil' disabled><?php foreach ($tabla_datos_valores_perfil as $registro) { ?><option value="<?=$registro['id']?>"><?=$registro['nombre']?></option><?php } ?></select></div></div><div class='form-group'><label class='control-label col-sm-2' for='correo_g'>Correo:</label><div class='col-sm-10'><input type='email' class='form-control' id='correo_g' name='correo' disabled></div></div>
              
              <div class='form-group'><label class='control-label col-sm-2' for='foto_g'>Foto:</label><div class='col-sm-10'><div id='foto_g_ver'></div><input type='file' class='form-control' id='foto_g' name='foto'></div></div><div class='form-group'><label class='control-label col-sm-2' for='language_g'>Language:</label><div class='col-sm-10'><select class='form-control' id='language_g' name='language'><?php foreach ($tabla_languages as $registro) { ?><option value="<?=$registro['id']?>"><?=$registro['name']?></option><?php } ?></select></div></div><div class='form-group'><label class='control-label col-sm-2' for='fecha_nacimiento_g'>Fecha Nacimiento:</label><div class='col-sm-10'><input type='text' class='form-control datepicker' id='fecha_nacimiento_g' name='fecha_nacimiento'></div></div><div class='form-group'><label class='control-label col-sm-2' for='pais_g'>Pais:</label><div class='col-sm-10'><select class='form-control' id='pais_g' name='pais'><?php foreach ($tabla_paises as $registro) { ?><option value="<?=$registro['id']?>"><?=$registro['nombre_en']?></option><?php } ?></select></div></div><div class='form-group'><label class='control-label col-sm-2' for='departamento_g'>Departamento:</label><div class='col-sm-10'><input type='text' class='form-control' id='departamento_g' name='departamento'></div></div><div class='form-group'><label class='control-label col-sm-2' for='ciudad_g'>Ciudad:</label><div class='col-sm-10'><input type='text' class='form-control' id='ciudad_g' name='ciudad'></div></div><div class='form-group'><label class='control-label col-sm-2' for='telefono_g'>Telefono:</label><div class='col-sm-10'><input type='text' class='form-control' id='telefono_g' name='telefono'></div></div><div class='form-group'><label class='control-label col-sm-2' for='facebook_g'>Facebook:</label><div class='col-sm-10'><input type='text' class='form-control' id='facebook_g' name='facebook'></div></div><div class='form-group'><label class='control-label col-sm-2' for='whatsapp_g'>Whatsapp:</label><div class='col-sm-10'><input type='text' class='form-control' id='whatsapp_g' name='whatsapp'></div></div><div class='form-group'><label class='control-label col-sm-2' for='skype_g'>Skype:</label><div class='col-sm-10'><input type='text' class='form-control' id='skype_g' name='skype'></div></div><div class='form-group'><label class='control-label col-sm-2' for='instagram_g'>Instagram:</label><div class='col-sm-10'><input type='text' class='form-control' id='instagram_g' name='instagram'></div></div><div class='form-group'><label class='control-label col-sm-2' for='google_g'>Google:</label><div class='col-sm-10'><input type='text' class='form-control' id='google_g' name='google'></div></div><div class='form-group'><label class='control-label col-sm-2' for='linkdn_g'>Linkdn:</label><div class='col-sm-10'><input type='text' class='form-control' id='linkdn_g' name='linkdn'></div></div>
              
              
              <div class='form-group'><label class='control-label col-sm-2' for='estado_g'>Estado:</label><div class='col-sm-10'><select class='form-control' id='estado_g' name='estado' disabled><?php foreach ($tabla_datos_valores_estado as $registro) { ?><option value="<?=$registro['id']?>"><?=$registro['nombre']?></option><?php } ?></select></div></div>


                    <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>

