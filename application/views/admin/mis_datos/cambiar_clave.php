  <?php defined('BASEPATH') OR exit('No esta permitido el acceso directo a este script.'); ?>

  <?php
  // Bloque de codigo para presentar mensajes de alerta
  if ( $this->session->flashdata('alertaMensaje') ) {
  ?>
  <div class="alert alert-<?php echo $this->session->flashdata('alertaTipo'); ?> alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <?php echo $this->session->flashdata('alertaMensaje'); ?>
  </div>
  <?php
  }
  ?>

<div class="row">
  <div class="col-xs-12 col-sm-6">
    <div class="box box-default color-palette-box">
      <div class="box-header with-border">
        <h3 class="box-title">Formulario para Cambiar la Clave</h3>
      </div>
      <div class="box-body">
<?php
$atributos = array('id' => 'forma');
echo form_open('admin/mis_datos/cambiar_clave/proceso', $atributos);
?>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="Ingresa tu clave actual" name="clave_actual" id="clave_actual">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>      
          <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="Ingresa tu nueva clave" name="clave" id="clave">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>      
          <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="Confirma tu nueva clave" name="clave_validacion">
            <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-8">

            </div>
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Actualizar</button>
            </div>
          </div>
        </form>              
      </div>
    </div>    
  </div>

  <div class="col-xs-12 col-sm-6">
    <div class="box box-default color-palette-box">
      <div class="box-header with-border">
        <h3 class="box-title">Consejos para definir una nueva clave</h3>
      </div>
      <div class="box-body">              
        <p>
          <strong>La seguridad es siempre relativa, pero una clave segura debería cumplir la mayoría de los requisitos siguientes:</strong>
        </p>
        <ul>
          <li>Una longitud mínima de siete caracteres.</li>
          <li>Letras mayúsculas y minúsculas.</li>
          <li>Incluye números.</li>
          <li>Incluye caracteres no estándar: ñ, $, % ë â…</li>
          <li>Cambia tu clave periódicamente.</li>
        </ul>              
      </div>
    </div>
  </div>

</div>