	<section id="content">
		<!-- Contenido plano -->
		<div class="contenido-plano">
			<div class="titulos">
				<h2>NUEVA CONTRASEÑA</h2>
			</div>

			<div class="cont-plano">

<?php
if ( $this->session->flashdata('alertaMensaje') ) {
?>
				<div class="alert alert-<?php echo $this->session->flashdata('alertaTipo'); ?> alert-dismissible">
				  <?php echo $this->session->flashdata('alertaMensaje'); ?>
				</div>
<?php
} else {
	if ($error != '') {
?>
				<div class="errorPagina errorPaginaMargen20"><center><?php echo $error; ?><center></div>
<?php
	}
}
?>

<?php
$atributos = array('id' => 'forma-nueva-clave');
echo form_open('nueva_contrasena_validacion', $atributos);
?>
					<input type="hidden" name="codigo" value="<?php echo $codigo; ?>">
					<div class="registro-contrasena input-ventana">
						<i class="fas fa-unlock-alt"></i>
						<input type="password" placeholder="Contraseña" required name="password" id="password_nc" minlength="7" maxlength="20">
					</div>
					<div class="registro-contrasena input-ventana">
						<i class="fas fa-unlock-alt"></i>
						<input type="password" placeholder="Repetir Contraseña" required equalTo="#password_nc" name="password2" id="password2_nc" minlength="7" maxlength="20">
					</div>				
<?php 
if ($_SERVER['SERVER_NAME'] == "bittathome.com") {
?>
					<center>
						<div class="g-recaptcha" data-sitekey="<?php echo $this->config->item('recaptcha_sitekey');?>"></div><br>
					</center>
<?php
}
?>

					<div class="cols2">
						<button type="submit" class="btn-ingresar btn-producto">ENVIAR</button>
					</div>
				</form>


			</div>
		</div>
		<!-- Contenido plano fin -->
	</section>