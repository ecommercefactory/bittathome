	<section id="content-checkout">
		<div class="titulo-checkout">
			<h3>FINALIZAR COMPRA</h3>
		</div>

		<div class="datos-finales">
			<div class="contenido-datos">
				<div class="datos-comprador">
					<div class="tus-datos">
						<h6 id="paso-1-titulo">PASO 1. TUS DATOS</h6>
						<p>Solicitamos únicamente la información esencial para la finalización de la compra</p>


<?php
if ( $this->session->flashdata('alertaMensaje') ) {
?>
							<div class="alert alert-<?php echo $this->session->flashdata('alertaTipo'); ?> alert-dismissible">
							  <?php echo $this->session->flashdata('alertaMensaje'); ?>
							</div>
<?php
}
?>

<?php
$atributos = array('id' => 'formulario-perfil');
echo form_open('carrito/perfil_validacion', $atributos);
?>
							<div class="c-correo">
								<label for="">Correo Electrónico</label>
								<input type="email" id="email_datos" name="email_datos" value="<?php echo $email_datos; ?>" required minlength="10" maxlength="70" readonly>
							</div>

							<div class="c-nombre w-50">
								<label for="">Nombre</label>
								<input type="text" id="nombre_datos" name="nombre_datos" value="<?php echo $nombre_datos; ?>" required minlength="2" maxlength="50">
							</div>

							<div class="c-apellido w-50">
								<label for="">Apellidos</label>
								<input type="text" id="apellidos_datos" name="apellidos_datos" value="<?php echo $apellidos_datos; ?>" required minlength="2" maxlength="50">
							</div>

							<div class="c-cedula w-50">
								<label for="">Documento de Identidad</label>
								<input type="text" id="cedula_datos" name="cedula_datos" value="<?php echo $cedula_datos; ?>" required minlength="6" maxlength="20">
							</div>

							<div class="c-telefono w-50">
								<label for="">Número Telefónico</label>
								<input type="text" id="telefono_datos" name="telefono_datos" value="<?php echo $telefono_datos; ?>" required minlength="7" maxlength="50">
							</div>

							<a class="btn-agregar-empresa">Agregar datos de empresa (opcional)</a>

							<div class="datos-empresa">
								<div class="c-razon">
									<label for="">Razon social</label>
									<input type="text" id="razon_datos" name="razon_datos" value="<?php echo $razon_datos; ?>" >
								</div>

								<div class="c-nit w-50">
									<label for="">NIT</label>
									<input type="text" id="nit_datos" name="nit_datos" value="<?php echo $nit_datos; ?>" >
								</div>

								<div class="c-rut w-50">
									<label for="">RUT</label>
									<input type="text" id="rut_datos" name="rut_datos" value="<?php echo $rut_datos; ?>" >
								</div>
							</div>

							<div class="datos">
								<input type="checkbox" checked id="terminos_datos" name="terminos_datos">
								<p>Acepto <a href="<?php echo base_url(); ?>info/terminos-y-condiciones" target="_blank">los Términos y condiciones y politicas de envíos</a></p>
							</div>

							<div class="datos">
								<input type="checkbox" checked id="politicas" name="politicas">
								<p>Acepto <a href="<?php echo base_url(); ?>info/politica-de-tratamiento-de-datos-personales" target="_blank">la Política de Tratamiento de Datos Personales</a></p>
							</div>

							<div class="cont-btn-check">
								<button class="btn-producto btn-finalizar-compra" type="submit">
								    <span>CONTINUAR</span>
								</button>
							</div>
						</form>
					</div>

					<div class="direccion-envio">
						<h6 class="texto-gris" id="paso-2-titulo">PASO 2. DIRECCIÓN DE ENVÍO</h6>

						<div id="paso-2-1">
							<p>Diligencia los campos.</p>
							<span class="btn-gris btn btnSpan" style="">Editar</span>
						</div>

					</div>

					<div class="medio-pago">
						<h6 class="texto-gris" id="paso-3-titulo">PASO 3. PAGAR</h6>
<!--
						<div id="paso-3-1">
							<p>Diligencia los campos.</p>
							<span class="btn-gris btn btnSpan">Editar</span>
						</div>
-->
					</div>	
				</div>

				<div class="resumen-compra">
					<h6>RESUMEN DE LA COMPRA</h6>

					<br>
		        	< 
		        	<a href="<?php echo base_url(); ?>carrito" style="text-decoration:underline;font-weight: bold;font-size: 12px;">Regresar al carrito</a>
					<br><br>
<!-- ******************************************************************* -->
<?php

$productosCarrito = 0;
$subtotal = 0;
$total = 0;

if (count($carrito_compras)) {
	foreach ($carrito_compras as $key => $value) {
		$imagen = '';
		if (count($tabla_productos_multimedia)) {
			foreach ($tabla_productos_multimedia as $regImagen) {
				if ($regImagen['producto'] == $key) {
					$imagen = $regImagen['imagen'];
					break;
				}
			}
		}

		$productoNombre = '';
		$productoRuta = '';
		$productoPrecio = 0;
		if (count($tabla_productos)) {
			foreach ($tabla_productos as $regProducto) {
				if ($regProducto['id'] == $key) {
					$productoNombre = $regProducto['nombre'];
					$productoRuta = $regProducto['ruta'];
					$productoPrecio = $regProducto['precio'];
					$productoPrecio = ($productoPrecio+($productoPrecio*($tabla_comercio->impuesto/100)));
					break;
				}
			}
		}

?>
<!-- ******************************************************************* -->
					<div class="cont-carrito-producto">
						<div class="producto-carrito">

<?php
		if ($imagen != '') {
?>
					<img src="<?php echo base_url(); ?>imagenes/producto/multimedia/<?php echo $imagen; ?>" width="82" height="82" alt="<?php echo $productoNombre; ?>" />
<?php
		}
?>

							<a href="<?php echo base_url() . 'p/'. $productoRuta; ?>"><?php echo $productoNombre; ?></a>
						</div>

						<div class="total-carrito">
							<p>$<?php
$productoTotal = 0;
$productoTotal = $productoPrecio * $value;
echo number_format($productoTotal,0);
$subtotal += $productoTotal;
					?></p>
						</div>
					</div>
<!-- ******************************************************************* -->
<?php
		$productosCarrito += $value;
	}
}
$total = $subtotal;
?> 
<!-- ******************************************************************* -->
					<div class="total">
						<div class="total-cont">
							<div class="sub-total">
								<span class="txt-sub">Subtotal</span>
								<span class="valor-sub">$<?php echo number_format($subtotal,0); ?></span>	
							</div>

							<div class="total-carrito">
								<span class="txt-total">Total</span>
								<span class="valor-total">$<?php echo number_format($total,0); ?></span>
							</div>
						</div>

						<div class="cont-btn-check oculto">
								<button class="btn-producto btn-finalizar-compra">
						            <span>PAGAR</span>
						        </button>
						</div>
					</div>
<!-- ******************************************************************* -->
				</div>
			</div>

		</div>
	
	</section>