	<section id="content">
		<div class="detalle-producto">
			<div class="producto-zoom">
                <section class="slider">
                    <div id="slider" class="flexslider ampliar">
                        <ul class="slides">
<?php
        if (count($tabla_productos_multimedia)) {
        	$imgCtr = false; 
			foreach ($tabla_productos_multimedia as $registro) {
				$imgCtr = true;
				if (trim($registro['imagen']) == '') {
?>
							<li>
    							<img src="<?php echo base_url(); ?>imagenes/producto/sin-imagen.png?v=<?php echo $this->config->item('version');?>" />
    						</li>
<?php

				} else {
?>
          					<li>
          						<img src="<?php echo base_url(); ?>imagenes/producto/multimedia/grande/<?php echo $registro['grande'] ?>">
          					</li>
<?php

				}
			}
			if (!$imgCtr) {
?>
							<li>
    							<img src="<?php echo base_url(); ?>imagenes/producto/sin-imagen.png?v=<?php echo $this->config->item('version');?>" />
    						</li>
<?php
			}
	    } else {
?>
    						<li>
	    						<img src="<?php echo base_url(); ?>imagenes/producto/sin-imagen.png?v=<?php echo $this->config->item('version');?>" /> 
	    					</li>
<?php
    	}
?>  
                        </ul>
                    </div>

					<div id="carousel-producto" class="flexslider">
						<ul class="slides">            
<?php
        if (count($tabla_productos_multimedia)) {
        	$imgCtr = false; 
			foreach ($tabla_productos_multimedia as $registro) {
				$imgCtr = true;

				if (trim($registro['imagen']) == '') {
?>
<!--
							<li>
    							<img src="<?php echo base_url(); ?>imagenes/producto/sin-imagen.png" width="243" height="243" />
    						</li>
-->
<?php

				} else {
?>
          					<li>
          						<img src="<?php echo base_url(); ?>imagenes/producto/multimedia/pequena/<?php echo $registro['pequena'] ?>?v=<?php echo $this->config->item('version');?>">
          					</li>
<?php
				}
			}
			if (!$imgCtr) {
?>
					<!--
							<li>
    							<img src="<?php echo base_url(); ?>imagenes/producto/sin-imagen.png" width="243" height="243" />
    						</li>
    				-->
<?php
			}
	    } else {
?>
    				<!--	
    						<li>	
    							<img src="<?php echo base_url(); ?>imagenes/producto/sin-imagen.png" width="243" height="243" /> 
    						</li>
    				-->
<?php
    	}
?>  


						</ul>
					</div>
				</section>
			</div>

			<div class="producto-info">
				<h2 class="nombre-producto-detalle"><?php echo $titulo; ?></h2>

				<div class="cont-precios">
		            <h4 class="precio-dcto">$<?php echo number_format($precio+($precio*($tabla_comercio->impuesto/100)),0); ?></h4>
<!--
					<h4 class="precio-dcto">$2.915.958</h4>
		            <h4 class="precio-real">$4.049.900</h4>
-->
		        </div>

		        <div class="descripcion-producto">

		        <p><?php echo nl2br($descripcion); ?></p>
              
		        </div>

		        <div class="atributos">

	              <div class="color">
<!--
			        	<select name="" id="selector-color">
			        		<option value="">rojo</option>
			        		<option value="">Amarillo</option>
			        		<option value="">Verde</option>
			        	</select>
-->
		        	</div>


		        	<div class="selector-cantidad">
		        		<button type="button" class="btn-cantidad menos-cantidad" data-quantity="minus">
		        			<i class="fas fa-minus"></i>
		        		</button>
		        		
		        		<input class="input-cantidad allownumericwithoutdecimal" type="number" name="quantity_to_add" id="quantity_to_add" value="1" min="1" max="10" step="1">

		        		<button type="button" class="btn-cantidad mas-cantidad" data-quantity="plus">
		        			<i class="fas fa-plus"></i>
		        		</button>
		        	</div>
		        </div>

		        <div class="cont-btn-carrito">
		           	<button class="btn-producto add-carrito" producto="<?php echo $producto_id; ?>">
		            	<span>ADICIONAR AL CARRITO</span>
		           	</button>
		        </div>



			</div>
		</div>

		<!-- Productos relacionados -->
		<div class="productos-relacionados">
			<div class="titulos">
				<h2>RELACIONADOS</h2>
			</div>

			<div class="carousel">
		        <ul class="slides">

<?php
    foreach ($tabla_productos_relacionados as $registro) {
?>              
		          <li class="cont-producto">
		            	<a href="<?php echo base_url(); ?>p/<?php echo $registro['ruta']; ?>" class="nombre-producto"><?php echo $registro['nombre']; ?></a>

		            	<a href="<?php echo base_url(); ?>p/<?php echo $registro['ruta']; ?>" class="cont-img-producto">
<?php
        if (count($tabla_productos_multimedia_relacionados)) {
        	$imgCtr = false; 

	        foreach ($tabla_productos_multimedia_relacionados as $registro2) {
	            if ($registro2['producto'] == $registro['id']) {
	            	$imgCtr = true;
					if (trim($registro2['imagen']) == '') {
?>
    							<img src="<?php echo base_url(); ?>imagenes/producto/sin-imagen.png?v=<?php echo $this->config->item('version');?>" width="243" height="243" />
<?php

					} else {
?>
          						<img src="<?php echo base_url(); ?>imagenes/producto/multimedia/mediana/<?php echo $registro2['mediana'] ?>?v=<?php echo $this->config->item('version');?>" width="243" height="243" >
<?php
					}
	                break;
	            }
	        }
	        if (!$imgCtr) {
?>
    						<img src="<?php echo base_url(); ?>imagenes/producto/sin-imagen.png?v=<?php echo $this->config->item('version');?>" width="243" height="243" />
<?php
	        }
	    } else {
?>
    						<img src="<?php echo base_url(); ?>imagenes/producto/sin-imagen.png?v=<?php echo $this->config->item('version');?>" width="243" height="243" />
<?php
    	}
?>                      
		            	</a>

		            	<div class="flag-dcto" style="display:none;">
		            		<span>28</span>
		            	</div>

		            	<div class="flag-nuevo">
		            		<img src="<?php echo base_url(); ?>assets/tienda/images/flag-nuevo.png" alt="flag nuevo">
		            	</div>

<?php
		$class_deseos = 'deseos';
		if ($this->session->has_userdata($this->config->item('raiz') . 'fe_lista_deseos')) {
		    foreach ($this->session->userdata($this->config->item('raiz') . 'fe_lista_deseos') as $key => $value) {
		        if ($registro['id'] == $key) {
					$class_deseos = 'deseos_sel';
		        	break;
		        }
		    }
		}
?>

		            	<button type="button" class="<?php echo $class_deseos; ?>" producto="<?php echo $registro['id']; ?>">
							<i class="fas fa-heart"></i>
						</button>

		            	<div class="cont-precios">
		            		<h4 class="precio-dcto">$<?php echo number_format($registro['precio']+($registro['precio']*($tabla_comercio->impuesto/100)),0); ?></h4>
<!--
		            		<h4 class="precio-real">$4.049.900</h4>
		            		<h4 class="precio-dcto">$2.915.958</h4>
-->
		            	</div>

		            	<div class="cont-btn-producto">
		            		<button class="btn-producto">
                      <a href="<?php echo base_url(); ?>p/<?php echo $registro['ruta']; ?>">
		            			<span>VER PRODUCTO</span>
                      </a>
		            		</button>
		            	</div>   
		  	    	</li>
<?php
    }
?>                
         
		        </ul>
	        </div>
		</div>
		

		<!-- Productos relacionados Fin-->
	</section>
