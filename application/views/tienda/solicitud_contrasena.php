	<section id="content">
		<!-- Contenido plano -->
		<div class="contenido-plano">
			<div class="titulos">
				<h2>SOLICITUD DE NUEVA CONTRASEÑA</h2>
			</div>

			<div class="cont-plano">

<?php
if ( $this->session->flashdata('alertaMensaje') ) {
?>
				<div class="alert alert-<?php echo $this->session->flashdata('alertaTipo'); ?> alert-dismissible">
				  <?php echo $this->session->flashdata('alertaMensaje'); ?>
				</div>
<?php
}
?>

<?php
$atributos = array('id' => 'forma-olvido-clave2');
echo form_open('solicitud_contrasena/validacion', $atributos);
?>
				<div class="registro-correo input-ventana">
					<i class="fas fa-envelope"></i>
					<input type="email" placeholder="Correo Electrónico" required name="email" id="email_sc2" minlength="10" maxlength="70">
				</div>			
<?php 
if ($_SERVER['SERVER_NAME'] == "bittathome.com") {
?>
					<center>
						<div class="g-recaptcha" data-sitekey="<?php echo $this->config->item('recaptcha_sitekey');?>"></div><br>
					</center>
<?php
}
?>
					<div class="cols2">
						<button type="submit" class="btn-ingresar btn-producto">ENVIAR</button>
					</div>
				</form>

				<br>

				<div style="text-align: right;padding:0px 10px 10px 10px;">
					<hr style="border: 1px solid #e4e4e4;">
					<p style="color:grey;">¿No tienes una cuenta registrada? | <a href="<?php echo base_url(); ?>registro" style="color:#3cb39d;">Crear Cuenta</a></p>
				</div>


			</div>
		</div>
		<!-- Contenido plano fin -->
	</section>