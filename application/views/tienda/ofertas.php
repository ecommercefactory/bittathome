	<section id="content">
		<div id="perfil">
			<div class="tabbers">
				<ul>
					<li data-target="mis-compras" class="tab">
						<a href="<?php echo base_url()?>mis_compras">
							<i class="fas fa-cart-arrow-down"></i>
							<span>MIS COMPRAS</span>
						</a>
					</li>
					<li data-target="mis-listas" class="tab">
						<a href="<?php echo base_url()?>mis_favoritos">
							<i class="fas fa-grin-hearts"></i>
							<span>MIS FAVORITOS</span>
						</a>
					</li>
					<li data-target="mi-perfil" class="tab">
						<a href="<?php echo base_url()?>mi_perfil">
							<i class="fas fa-user-circle"></i>
							<span>EDITAR PERFIL</span>
						</a>
					</li>
					<li data-target="ofertas" class="tab tab-active">
						<a href="<?php echo base_url()?>ofertas">
							<i class="fas fa-star"></i>
							<span>VER OFERTAS</span>
						</a>
					</li>
				</ul>

				<div class="tabs-cont">

					<div id="ofertas" class="tab-c">

						<!-- miga de pan / Ordenar producto -->
						<div class="miga-ordenar">
<!--
							<div class="ordenar-productos">
								<select name="" id="ordenar">
									<option value="">Ordenar productos</option>
									<option value="">Más reciente</option>
									<option value="">Más antiguo</option>
								</select>
							</div>
-->
						</div>
						<!-- miga de pan / Ordenar producto Fin -->

						<div class="cols4 listado-producto">
							<div style='color:#e59200;font-size: 16px;margin-bottom:200px;'>No se ha encontrado ningún producto en oferta.</div>
						</div>

					</div>
					
				</div>
			</div>
		</div>
	</section>
