<?php
$atributos = array('id' => 'formulario-pago');
// echo form_open('https://sandbox.checkout.payulatam.com/ppp-web-gateway-payu', $atributos);
echo form_open('https://checkout.payulatam.com/ppp-web-gateway-payu', $atributos);
?>
	<section id="content-checkout">
		<div class="titulo-checkout">
			<h3>FINALIZAR COMPRA</h3>
		</div>

		<div class="datos-finales">
			<div class="contenido-datos">


				<div class="datos-comprador">
					<div class="tus-datos">
						<h6 class="texto-gris" id="paso-1-titulo">PASO 1. TUS DATOS</h6>

<p><?php echo $email_datos; ?></p>
<p>-</p>
<p><?php echo $nombre_datos; ?></p>
<p><?php echo $apellidos_datos; ?></p>
<p><?php echo $telefono_datos; ?></p>
<a href="<?php echo base_url() . "carrito/perfil" ?>" class="btn-gris btn">Editar</a>

					</div>

					<div class="direccion-envio">
						<h6 class="texto-gris" id="paso-2-titulo">PASO 2. DIRECCIÓN DE ENVÍO</h6>

<p><?php echo $direccion_1_datos; ?></p>
<p><?php echo $direccion_2_datos; ?></p>
<p><?php echo $barrio_datos; ?></p>
<p><?php echo $departamento_datos; ?></p>
<p><?php echo $municipio_datos; ?></p>
<p>Colombia</p>
<a href="<?php echo base_url() . "carrito/envio" ?>" class="btn-gris btn">Editar</a>

					</div>

					<div class="medio-pago">
						<h6 id="paso-3-titulo">PASO 3. PAGAR</h6>

						<div class="cont-btn-check">
								<button class="btn-producto btn-finalizar-compra" type="submit">
						            <span>PAGAR</span>
						        </button>
						</div>

					</div>	
				</div>

				<div class="resumen-compra">
					<h6>RESUMEN DE LA COMPRA</h6>
					<br>
		        	< 
		        	<a href="<?php echo base_url(); ?>carrito" style="text-decoration:underline;font-weight: bold;font-size: 12px;">Regresar al carrito</a>
					<br><br>
<!-- ******************************************************************* -->
<?php
if (count($carrito_compras)) {
	foreach ($carrito_compras as $key => $value) {
		$imagen = '';
		if (count($tabla_productos_multimedia)) {
			foreach ($tabla_productos_multimedia as $regImagen) {
				if ($regImagen['producto'] == $key) {
					$imagen = $regImagen['imagen'];
					break;
				}
			}
		}
		$productoNombre = '';
		$productoRuta = '';
		$productoPrecio = 0;
		if (count($tabla_productos)) {
			foreach ($tabla_productos as $regProducto) {
				if ($regProducto['id'] == $key) {
					$productoNombre = $regProducto['nombre'];
					$productoRuta = $regProducto['ruta'];
					$productoPrecio = $regProducto['precio'];
					$productoPrecio = ($productoPrecio+($productoPrecio*($tabla_comercio->impuesto/100)));
					break;
				}
			}
		}
		$productoTotal = $productoPrecio * $value;
?>
					<div class="cont-carrito-producto">
						<div class="producto-carrito">
<?php   if ($imagen != '') { ?>
							<img src="<?php echo base_url(); ?>imagenes/producto/multimedia/<?php echo $imagen; ?>" width="82" height="82" alt="<?php echo $productoNombre; ?>" />
<?php   } ?>
							<a href="<?php echo base_url() . 'p/'. $productoRuta; ?>"><?php echo $productoNombre; ?></a>
						</div>
						<div class="total-carrito">
							<p>$<?php echo number_format($productoTotal,0); ?></p>
						</div>
					</div>
<?php
	}
}
?> 
					<div class="total">
						<div class="total-cont">
							<div class="sub-total">
								<span class="txt-sub">Subtotal</span>
								<span class="valor-sub">$<?php echo number_format($subtotal,0); ?></span>	
							</div>

							<div class="total-carrito">
								<span class="txt-total">Total</span>
								<span class="valor-total">$<?php echo number_format($total,0); ?></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>


	<input name="merchantId" type="hidden" value="<?php echo $merchantId; ?>">
	<input name="accountId" type="hidden" value="<?php echo $accountId; ?>">
	<input name="referenceCode" type="hidden" value="<?php echo $referenceCode; ?>">	

	<input name="buyerEmail" type="hidden" value="<?php echo $email_datos; ?>" >	
	<input name="description" type="hidden" value="<?php echo $description; ?>">

	<input name="amount" type="hidden" value="<?php echo $total; ?>">
	<input name="tax" type="hidden" value="<?php echo $total_impuesto_valor; ?>">
	<input name="taxReturnBase" type="hidden" value="<?php echo $total_base_impuesto_valor; ?>">

	<input name="currency" type="hidden" value="<?php echo $currency; ?>">
	<input name="test" type="hidden" value="<?php echo $test; ?>">

	<input name="displayShippingInformation" type="hidden" value="<?php echo $displayShippingInformation; ?>">
	<input name="shippingAddress" type="hidden" value="<?php echo $direccion_1_datos . ', ' . $direccion_1_datos . ', ' . $barrio_datos; ?>" > 
	<input name="shippingCity" type="hidden" value="<?php echo $municipio_datos . ' (' . $departamento_datos . ')'; ?>" > 
	<input name="shippingCountry" type="hidden" value="<?php echo $shippingCountry; ?>" > 
	<input name="shipmentValue" type="hidden" value="<?php echo $shipmentValue; ?>">

	<input name="responseUrl" type="hidden"  value="<?php echo base_url(); ?>payulatam/respuesta_prueba.php">
	<input name="confirmationUrl" type="hidden"  value="<?php echo base_url(); ?>payulatam/confirmacion_prueba.php">
	<input name="declinedResponseUrl" type="hidden" value="<?php echo base_url(); ?>payulatam/rechazado_prueba.php">

	<input name="signature" type="hidden" value="<?php echo $signature; ?>">


	<input name="lng" type="hidden" value="es">


</form>