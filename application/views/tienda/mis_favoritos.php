	<section id="content">
		<div id="perfil">
			<div class="tabbers">
				<ul>
					<li data-target="mis-compras" class="tab">
						<a href="<?php echo base_url()?>mis_compras">
							<i class="fas fa-cart-arrow-down"></i>
							<span>MIS COMPRAS</span>
						</a>
					</li>
					<li data-target="mis-listas" class="tab tab-active">
						<a href="<?php echo base_url()?>mis_favoritos">
							<i class="fas fa-grin-hearts"></i>
							<span>MIS FAVORITOS</span>
						</a>
					</li>
					<li data-target="mi-perfil" class="tab">
						<a href="<?php echo base_url()?>mi_perfil">
							<i class="fas fa-user-circle"></i>
							<span>EDITAR PERFIL</span>
						</a>
					</li>
					<li data-target="ofertas" class="tab">
						<a href="<?php echo base_url()?>ofertas">
							<i class="fas fa-star"></i>
							<span>VER OFERTAS</span>
						</a>
					</li>
				</ul>

				<div class="tabs-cont">

					<div id="mis-listas" class="tab-c">
<!-- *********** -->

		<!-- miga de pan / Ordenar producto -->
		<div class="miga-ordenar">
<!--
			<div class="ordenar-productos">
				<select name="" id="ordenar">
					<option value="">Ordenar productos</option>
					<option value="">Más reciente</option>
					<option value="">Más antiguo</option>
				</select>
			</div>
-->
		</div>
		<!-- miga de pan / Ordenar producto Fin -->




		<div class="cols4 listado-producto">
      
    <?php
    if (count($tabla_productos)) {
    foreach ($tabla_productos as $registro) {
    ?>
			<div class="col">
				<div class="cont-producto">
		            <a href="<?php echo base_url(); ?>p/<?php echo $registro['ruta']; ?>" class="nombre-producto"><?php echo $registro['nombre']; ?></a>

		            <a href="<?php echo base_url(); ?>p/<?php echo $registro['ruta']; ?>" class="cont-img-producto">
        <?php
        foreach ($tabla_productos_multimedia as $registro2) {
            if ($registro2['producto'] == $registro['id']) {
        ?>
                     <img src="<?php echo base_url(); ?>imagenes/producto/multimedia/<?php echo $registro2['imagen'] ?>" width="243" height="243" />
        <?php
                break;
            }
        }
        ?>                   
		            </a>

		            <div class="flag-dcto" style="display:none;">
		            	<span>28</span>
		            </div>

		            <div class="flag-nuevo">
		            	<img src="<?php echo base_url(); ?>assets/tienda/images/flag-nuevo.png" alt="flag nuevo">
		            </div>

<?php
$class_deseos = 'deseos';
if ($this->session->has_userdata($this->config->item('raiz') . 'fe_lista_deseos')) {
    foreach ($this->session->userdata($this->config->item('raiz') . 'fe_lista_deseos') as $key => $value) {
        if ($registro['id'] == $key) {
			$class_deseos = 'deseos_sel';
        	break;
        }
    }
}
?>

		            <button type="button" class="<?php echo $class_deseos; ?>" producto="<?php echo $registro['id']; ?>">
	                  <i class="fas fa-heart"></i>
	                </button>

		            <div class="cont-precios">

		            	<h4 class="precio-dcto">$<?php echo number_format($registro['precio'],0); ?></h4>
<!--
		            	<h4 class="precio-real">$4.049.900</h4>
		            	<h4 class="precio-dcto">$2.915.958</h4>
-->		            	
		            </div>

		            <div class="cont-btn-producto">
                  <a href="<?php echo base_url(); ?>p/<?php echo $registro['ruta']; ?>">
		            	<button class="btn-producto">
		            		<span>VER PRODUCTO</span>
		            	</button>
                  </a>
		            </div>   
		  	    </div>
			</div>
      
      
      
    <?php
    }
    } else {
	?>
			<div style='color:#e59200;font-size: 16px;margin-bottom:200px;'>No se ha encontrado ningún producto favorito.</div>

	<?php    	
    }
    ?>          
      
		</div>
		<!-- Lista de productos a 4 columnas -->

		<br><br><br>


<!-- *********** -->
					</div>

					
				</div>
			</div>
		</div>
	</section>
