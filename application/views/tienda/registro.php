	<section id="content">
		<!-- Contenido plano -->
		<div class="contenido-plano">
			<div class="titulos">
				<h2>REGISTRARSE</h2>
			</div>

			<div class="cont-plano">

<?php
if ( $this->session->flashdata('alertaMensaje') ) {
?>
				<div class="alert alert-<?php echo $this->session->flashdata('alertaTipo'); ?> alert-dismissible">
				  <?php echo $this->session->flashdata('alertaMensaje'); ?>
				</div>
<?php
}
?>

<?php
$atributos = array('id' => 'forma-registro2');
echo form_open('registro/validacion', $atributos);
?>
<!--
					<div class="cols2 registro-redes">
						<div class="col">
							<button class="btn-inicio-facebook">
								<i class="fab fa-facebook-f"></i>
								<span>Ingresar con Facebook</span>
							</button>
						</div>

						<div class="col">
							<button class="btn-inicio-google">
								<i class="fab fa-google-plus-g"></i>
								<span>Ingresar con Google</span>
							</button>
						</div>
					</div>
-->
					<div class="registro-nombre input-ventana">
						<i class="fas fa-user"></i>
						<input type="text" placeholder="Nombre Completo" required name="nombre" id="nombre_r2" minlength="6" maxlength="70">
					</div>
					<div class="registro-correo input-ventana">
						<i class="fas fa-envelope"></i>
						<input type="email" placeholder="Correo Electrónico" required name="email" id="email_r2" minlength="10" maxlength="70">
					</div>
					<div class="registro-contrasena input-ventana">
						<i class="fas fa-unlock-alt"></i>
						<input type="password" placeholder="Contraseña" required name="password" id="password_r2" minlength="7" maxlength="20">
					</div>
					<div class="checked-terminos-registro">
						<input type="checkbox" checked required name="terminos" id="terminos_r2">
						<p>Al registrarse. usted acepta nuestras condiciones de uso de privacidad <a href="#"> Leer más</a></p>
					</div>
					<div class="checked-terminos-registro">
						<input type="checkbox" checked required name="datos" id="datos_r2">
						<p>Al registrarse. usted acepta nuestras condiciones de habeas data <a href="#"> Leer más</a></p>
					</div>
<?php 
if ($_SERVER['SERVER_NAME'] == "bittathome.com") {
?>
					<center>
						<div class="g-recaptcha" data-sitekey="<?php echo $this->config->item('recaptcha_sitekey');?>"></div><br>
					</center>
<?php
}
?>

					<div class="cols2">
						<button type="submit" class="crear-cuenta btn-producto">REGISTRARME</button>
					</div>

				</form>

				<br>

				<div style="text-align: right;padding:0px 10px 10px 10px;">
					<hr style="border: 1px solid #e4e4e4;">
					<p style="color:grey;">¿Ya tienes una cuenta registrada? | <a href="<?php echo base_url(); ?>ingreso" style="color:#3cb39d;">Ingresar</a></p>
				</div>

			</div>
		</div>
		<!-- Contenido plano fin -->
	</section>