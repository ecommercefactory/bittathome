	<section id="content">
		<div id="perfil">
			<div class="tabbers">
				<ul>
					<li data-target="mis-compras" class="tab">
						<a href="<?php echo base_url()?>mis_compras">
							<i class="fas fa-cart-arrow-down"></i>
							<span>MIS COMPRAS</span>
						</a>
					</li>
					<li data-target="mis-listas" class="tab">
						<a href="<?php echo base_url()?>mis_favoritos">
							<i class="fas fa-grin-hearts"></i>
							<span>MIS FAVORITOS</span>
						</a>
					</li>
					<li data-target="mi-perfil" class="tab tab-active">
						<a href="<?php echo base_url()?>mi_perfil">
							<i class="fas fa-user-circle"></i>
							<span>EDITAR PERFIL</span>
						</a>
					</li>
					<li data-target="ofertas" class="tab">
						<a href="<?php echo base_url()?>ofertas">
							<i class="fas fa-star"></i>
							<span>VER OFERTAS</span>
						</a>
					</li>
				</ul>

				<div class="tabs-cont">

					<div id="mi-perfil" class="tab-c">
						<div class="perfil-cont">
							<div class="img-perfil">
								<div class="cont-img-perfil">
									<img src="<?php echo base_url(); ?>assets/tienda/images/img-perfil.png" alt="imagen perfil">
								</div>
							</div>

							<div class="editar-info-perfil">
								<p>NOMBRE</p>
								<div class="registro-nombre input-ventana">
									<i class="fas fa-user"></i>
									<input type="text" placeholder="Nombre" value="<?php echo $usuario->nombre; ?>">
								</div>

								<p>APELLIDOS</p>
								<div class="registro-nombre input-ventana">
									<i class="fas fa-user"></i>
									<input type="text" placeholder="Apellidos" value="<?php echo $usuario->apellidos; ?>">
								</div>

								<p>CORREO ELECTRÓNICO</p>
								<div class="registro-nombre input-ventana">
									<i class="fas fa-envelope"></i>
									<input type="text" placeholder="Correo Electrónico" value="<?php echo $usuario->email; ?>" readonly>
								</div>

								<p>DOCUMENTO DE IDENTIDAD</p>
								<div class="registro-nombre input-ventana">
									<i class="fas fa-address-card"></i>
									<input type="text" placeholder="Documento de Identidad" value="<?php echo $usuario->documento_identidad; ?>">
								</div>

								<p>NÚMERO TELEFÓNICO</p>
								<div class="registro-nombre input-ventana">
									<i class="fas fa-phone"></i>
									<input type="text" placeholder="Número Telefónico" value="<?php echo $usuario->numero_telefonico; ?>">
								</div>

								<p>RAZON SOCIAL</p>
								<div class="registro-nombre input-ventana">
									<i class="fas fa-industry"></i>
									<input type="text" placeholder="Razon social" value="<?php echo $usuario->razon_social; ?>">
								</div>

								<p>NIT/RUT</p>
								<div class="registro-nombre input-ventana">
									<i class="fas fa-address-card"></i>
									<input type="text" placeholder="NIT/RUT" value="<?php echo $usuario->entidad_identificacion; ?>">
								</div>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</section>
