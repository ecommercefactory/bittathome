	<section id="content">
		<div id="perfil">
			<div class="tabbers">
				<ul>
					<li data-target="mis-compras" class="tab tab-active">
						<a href="<?php echo base_url()?>mis_compras">
							<i class="fas fa-cart-arrow-down"></i>
							<span>MIS COMPRAS</span>
						</a>
					</li>
					<li data-target="mis-listas" class="tab">
						<a href="<?php echo base_url()?>mis_favoritos">
							<i class="fas fa-grin-hearts"></i>
							<span>MIS FAVORITOS</span>
						</a>
					</li>
					<li data-target="mi-perfil" class="tab">
						<a href="<?php echo base_url()?>mi_perfil">
							<i class="fas fa-user-circle"></i>
							<span>EDITAR PERFIL</span>
						</a>
					</li>
					<li data-target="ofertas" class="tab">
						<a href="<?php echo base_url()?>ofertas">
							<i class="fas fa-star"></i>
							<span>VER OFERTAS</span>
						</a>
					</li>
				</ul>

				<div class="tabs-cont">
					<div id="mis-compras" class="tab-c">

<?php
if (count($tabla_ordenes)) {
	echo "<table style='width:100%;'>";
	foreach ($tabla_ordenes as $registro) {
		$estado = '';
		foreach ($tabla_datos_valores_estado as $registro2) {
			if ($registro["estado"] == $registro2["id"]) {
				$estado = $registro2["nombre"];
				break;
			}
		}

		echo "<tr>";
			echo "<td style='width:20%;font-size: 14px;text-align:right;padding-right: 30px;' valign='top'>";
			// Aqui va la orden
				// orden detalle			

					echo "<span style='color: #71458b;font-size: 18px;margin-bottom: 10px;'>N.º pedido:</span>";
					echo "<br>";
					echo "<div style='margin-top: 10px;'><b style='font-size:36px;color:#47b49b;text-align:right;'>" . $registro["id"] . "</b></div>";
					echo "<br>";
					echo "<span style='color: #71458b;'>Hora y fecha de pedido:</span>";
					echo "<br>";
					echo "<b style='font-size:16px;color:#47b49b;'>" . $registro["fecha"] . "</b>";
					echo "<br>";
					echo "<br>";
					echo "<span style='color: #71458b;'>Estado:</span>";
					echo "<br>";
					echo "<b style='font-size:16px;color:#47b49b;'>" . $estado . "</b>";
/*					
					echo "<br>";
					echo "<br>";
					echo "Total:";
					echo "<br>";
					echo "<b style='font-size:16px;color:#47b49b;'>" . "$" . number_format($registro["total"], 0) . "</b>";
*/

				echo "<br><br><br>";
			echo "</td>";
			echo "<td style='width:80%;' valign='top'>";
			// Aqui va el detalle
				// productos detalle		


					$filasProductos = '';
					$filaCtr = 0;
					foreach ($tabla_ordenes_productos as $registro3) {
						if ($registro["id"] == $registro3["orden"]) {
							$productoPrecio = 0;
							$productoPrecio = $registro3['precio'];
							$productoPrecio = ($productoPrecio+($productoPrecio*($tabla_comercio->impuesto/100)));

							$imagen = '';
							if (count($tabla_productos_multimedia)) {
								foreach ($tabla_productos_multimedia as $regImagen) {
									if ($regImagen['producto'] == $registro3["producto"]) {
										$imagen = $regImagen['imagen'];
										break;
									}
								}
							}

							$productoNombre = '';
							$productoDescripcion = '';
							$productoRuta = '';
							if (count($tabla_productos)) {
								foreach ($tabla_productos as $regProducto) {
									if ($regProducto['id'] == $registro3["producto"]) {
										$productoNombre = $regProducto['nombre'];
										$productoDescripcion = $regProducto['descripcionCorta'];;
										$productoRuta = $regProducto['ruta'];
										break;
									}
								}
							}

/*
							$filasProductos .= '<tr valign="top">';

							$filasProductos .= '<td style="width:100px;">';
							if ($imagen != '') {
								$filasProductos .= '<a href="'.base_url().'p/'.$productoRuta.'"><img src="' . base_url() . 'imagenes/producto/multimedia/' .  $imagen . '" style="height: 82px;width: 82px;" alt="' . $productoNombre . '" border="0"/></a>';
							}
							$filasProductos .= '</td>';

							$filasProductos .= '<td>';
							$filasProductos .= '<a href="'.base_url().'p/'.$productoRuta.'" style="color:#663b7e;"><b>' . $productoNombre . '</b><br>' . $productoDescripcion . '</a><br><br>';
							$filasProductos .= '</td>';

							$filasProductos .= '<td style="width:15%;text-align:right;">';
							$filasProductos .= '<b>' . '$' . number_format($productoPrecio, 0) . ' x ' .  $registro3["cantidad"] . '</b>';
							$filasProductos .= '</td>';

							$filasProductos .= '<td style="width:15%;text-align:right;">';
							$filasProductos .= '<b>' . '$' . number_format(($productoPrecio*$registro3["cantidad"]), 0) . '</b>';
							$filasProductos .= '</td>';

							$filasProductos .= '</tr>';

*/
							$colorFila = '#f9f9f9';
							if ($filaCtr == 1) {
								$colorFila = '#ffffff';
								$filaCtr = 0;
							} else {
								$filaCtr = 1;
							}

							$filasProductos .= '<tr valign="top" style="background: '.$colorFila.';">';

							$filasProductos .= '<td style="text-align:center;">';
							$filasProductos .= $registro3["cantidad"];
							$filasProductos .= '</td>';

							$filasProductos .= '<td>';
							$filasProductos .= $productoNombre;
							$filasProductos .= '</td>';

							$filasProductos .= '<td style="text-align:right;">';
							$filasProductos .= '$' . number_format($registro3['precio'], 0);
							$filasProductos .= '</td>';

							$filasProductos .= '<td style="text-align:right;">';
							$filasProductos .= '$' . number_format($registro3['precio']*$registro3["cantidad"], 0);
							$filasProductos .= '</td>';

							$filasProductos .= '</tr>';							

						}

					}

/*
					echo "<table style='width:100%;font-size: 13px;'>";
					echo "<tr>";
					echo "<td colspan=2 style='text-align:center;'><b style='font-size:16px;color:#47b49b;'>";
					echo "Producto";
					echo "</b></td>";
					echo "<td style='text-align:right;'><b style='font-size:16px;color:#47b49b;'>";
					echo "Precio x Unidades";
					echo "</b></td>";
					echo "<td style='text-align:right;'><b style='font-size:16px;color:#47b49b;'>";
					echo "Total";
					echo "</b></td>";
					echo "</tr>";
					echo $filasProductos;
					echo "</table>";
*/

$mis_compras = '
<table style="width:100%;border: 1px solid #ececec;">
	<thead>
	<tr style="height: 70px;">
	  <th width="10%"><b style="font-size:16px;color:#47b49b;">CANTIDAD</b></th>
	  <th width="60%"><b style="font-size:16px;color:#47b49b;">PRODUCTO</b></th>
	  <th width="15%"><b style="font-size:16px;color:#47b49b;">PRECIO UNITARIO</b></th>
	  <th width="15%"><b style="font-size:16px;color:#47b49b;">VALOR VENTA</b></th>
	</tr>
	</thead>
	<tbody>
	'.$filasProductos.'
	</tbody>
	<tfoot>
	  <tr style="height: 35px;">
	    <td></td>  
	    <td></td>  
	    <td style="text-align:right;border-top: 1px solid grey;"><b style="font-size:16px;color:#47b49b;">SUBTOTAL</b></td>  
	    <td style="text-align:right;border-top: 1px solid grey;">'.'$' . number_format($registro["subtotal"], 0).'</td>  
	  </tr>
	  <tr style="height: 35px;">
	    <td></td>  
	    <td></td>  
	    <td style="text-align:right;"><b style="font-size:16px;color:#47b49b;">IVA</b></td>  
	    <td style="text-align:right;">'.'$' . number_format($registro["impuestos"], 0).'</td>  
	  </tr>
	  <tr style="height: 35px;">
	    <td></td>  
	    <td></td>  
	    <td style="text-align:right;"><b style="font-size:16px;color:#47b49b;">ENVIO</b></td>  
	    <td style="text-align:right;">'.'$' . number_format(0, 0).'</td>  
	  </tr>
	  <tr style="height: 35px;">
	    <td></td>  
	    <td></td>  
	    <td style="text-align:right;"><b style="font-size:16px;color:#47b49b;">TOTAL</b></td>  
	    <td style="text-align:right;"><b>'.'$' . number_format($registro["total"], 0) .'</b></td>  
	  </tr>
	</tfoot>
</table>';

echo $mis_compras;


				echo "<br><br><br>";
			echo "</td>";
		echo "</tr>";

		echo "<tr>";
		echo "<td colspan='2'><br><hr><br></td>";
		echo "</tr>";


	}
	echo "</table>";





/*
echo '
<br>
<div class="cont-mis-compras">
	<div class="numero-pedido">
		<h6>N.º pedido</h6>
		<h2>01</h2>
		<h6>Fecha y Hora</h6>
		<p> 2018/11/15 <strong>03:06:27</strong></p>
	</div>

	<div class="estado-pedido">
		<h6>Estado</h6>
		<p>Confirmada</p>
	</div>

	<div class="producto-carrito">
		<h6>Producto</h6>
		<img src="images/prod-1.jpg" alt="nombre producto">
		<a href="#">Interruptor Inteligente</a>
	</div>

	<div class="precio-carrito">
		<h6>Precio X Unidades</h6>
		<p>$00.000</p>
	</div>

	<div class="total-compras">
		<h6>Total</h6>
		<p>$150.000</p>
	</div>
</div>
<br>

';
*/




} else {
?>
<div style='color:#e59200;font-size: 16px;margin-bottom:200px;'>No se ha encontrado ninguna orden.</div>
<?php	
}
?>  


					</div>

				</div>
			</div>
		</div>
	</section>

	<div class="ventana-calificacion ventana-emergente" style="display: none;">
		<div class="ventana-emergente-c">
			<div class="title-ventana bg-color-modificado">
				<h3>CALIFICA ESTE PRODUCTO</h3>
				<a href="#" class="btn-cerrar">X</a>
			</div>

			<form action="">
				<div class="calificacion-promedio">
				    <i class="fas fa-star"></i>
				    <i class="fas fa-star"></i>
				    <i class="fas fa-star"></i>
				    <i class="fas fa-star"></i>
				    <i class="fas fa-star"></i>
				</div>

				<div class="calificacion">
					<div>
						<input type="radio" id="uno" name="drone" value="uno" checked />
            			<label for="uno">1</label>
					</div>
					<div>
						<input type="radio" id="dos" name="drone" value="dos" checked />
            			<label for="dos">2</label>
					</div>
					<div>
						<input type="radio" id="tres" name="drone" value="tres" checked />
            			<label for="tres">3</label>
					</div>
					<div>
						<input type="radio" id="cuatro" name="drone" value="cuatro" checked />
            			<label for="cuatro">4</label>
					</div>
					<div>
						<input type="radio" id="cinco" name="drone" value="cinco" checked />
            			<label for="cinco">5</label>
					</div>
				</div>

				<div class="mensaje-calificaion">
					<p>Tú opinión acerca de este producto (máximo 300 caracteres)</p>
					<textarea  id="mensaje-calificaion"></textarea>
				</div>

				<div class="cont-btn-calificar">
					<button class="btn-ingresar btn-producto">CALIFICAR</button>
				</div>
			</form>
		</div>		
	</div>