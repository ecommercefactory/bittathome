	<section id="content">
		<!-- Banner Categoria -->
		<div class="banner-categoria">
			<img src="<?php echo base_url(); ?>assets/tienda/images/banner-categoria.jpg" alt="banner categoria">
		</div>
		<!-- Banner Categoria fin -->

		<!-- miga de pan / Ordenar producto -->
		<div class="miga-ordenar">
			<ul class="miga-pan">
				<li>
					<a href="<?php echo base_url(); ?>">Inicio</a>
				</li>
<?php
if ($tituloSubcategoria == '') {
?>
				<li>
					<span><?php echo $tituloCategoria; ?></span>
				</li>
<?php
} else {
?>
				<li>
					<a href="<?php echo base_url() . 'c/' . $rutaCategoria; ?>"><?php echo $tituloCategoria; ?></a>
				</li>
				<li>
					<span><?php echo $tituloSubcategoria; ?></span>
				</li>
<?php
}
?>
			</ul>
<!--
			<div class="ordenar-productos">
				<select name="" id="ordenar">
					<option value="0">Ordenar productos</option>
					<option value="1">Más reciente</option>
					<option value="2">Más antiguo</option>
				</select>
			</div>
-->
		</div>
		<!-- miga de pan / Ordenar producto Fin -->

		<!-- Lista de productos a 4 columnas -->
		<div class="cols4 listado-producto">
      
    <?php
    foreach ($tabla_productos as $registro) {
    ?>
			<div class="col">
				<div class="cont-producto">
		            <a href="<?php echo base_url(); ?>p/<?php echo $registro['ruta']; ?>" class="nombre-producto"><?php echo $registro['nombre']; ?></a>

		            <a href="<?php echo base_url(); ?>p/<?php echo $registro['ruta']; ?>" class="cont-img-producto">
        <?php
        if (count($tabla_productos_multimedia)) {
        	$imgCtr = false; 
	        foreach ($tabla_productos_multimedia as $registro2) {
	            if ($registro2['producto'] == $registro['id']) {
	            	$imgCtr = true;
	            	if (trim($registro2['imagen']) == '') {
        ?>
	                     <img src="<?php echo base_url(); ?>imagenes/producto/sin-imagen.png?v=<?php echo $this->config->item('version');?>" width="243" height="243" />
        <?php

	            	} else {
        ?>
	                     <img src="<?php echo base_url(); ?>imagenes/producto/multimedia/mediana/<?php echo $registro2['mediana'] ?>?v=<?php echo $this->config->item('version');?>" width="243" height="243" />
        <?php

	            	}
	                break;
	            }
	        }
	        if (!$imgCtr) {
        ?>
    				<img src="<?php echo base_url(); ?>imagenes/producto/sin-imagen.png?v=<?php echo $this->config->item('version');?>" width="243" height="243" />
        <?php
	        }
        } else {
        ?>
    				<img src="<?php echo base_url(); ?>imagenes/producto/sin-imagen.png?v=<?php echo $this->config->item('version');?>" width="243" height="243" />
        <?php
        }
        ?>
		            </a>

		            <div class="flag-dcto" style="display:none;">
		            	<span>28</span>
		            </div>

		            <div class="flag-nuevo">
		            	<img src="<?php echo base_url(); ?>assets/tienda/images/flag-nuevo.png" alt="flag nuevo">
		            </div>

<?php
$class_deseos = 'deseos';
if ($this->session->has_userdata($this->config->item('raiz') . 'fe_lista_deseos')) {
    foreach ($this->session->userdata($this->config->item('raiz') . 'fe_lista_deseos') as $key => $value) {
        if ($registro['id'] == $key) {
			$class_deseos = 'deseos_sel';
        	break;
        }
    }
}
?>

		            <button type="button" class="<?php echo $class_deseos; ?>" producto="<?php echo $registro['id']; ?>">
	                  <i class="fas fa-heart"></i>
	                </button>

		            <div class="cont-precios">

		            	<h4 class="precio-dcto">$<?php echo number_format($registro['precio']+($registro['precio']*($tabla_comercio->impuesto/100)),0); ?></h4>
<!--
		            	<h4 class="precio-real">$4.049.900</h4>
		            	<h4 class="precio-dcto">$2.915.958</h4>
-->		            	
		            </div>

		            <div class="cont-btn-producto">
                  <a href="<?php echo base_url(); ?>p/<?php echo $registro['ruta']; ?>">
		            	<button class="btn-producto">
		            		<span>VER PRODUCTO</span>
		            	</button>
                  </a>
		            </div>   
		  	    </div>
			</div>
      
      
      
    <?php
    }
    ?>          
      
		</div>
		<!-- Lista de productos a 4 columnas -->

<br><br><br>

		<!-- paginador 
		<div class="paginador">
			<ul>
				<li>
					<a href="#">1</a>
				</li>
				<li class="pagina-activa">
					<a href="#">2</a>
				</li>
				<li>
					<a href="#">3</a>
				</li>
				<li>
					<a href="#">4</a>
				</li>
			</ul>
		</div>
		 paginador fin -->
	</section>
