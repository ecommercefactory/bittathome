	<section id="content-checkout">
		<div class="titulo-checkout">
			<h3>FINALIZAR COMPRA</h3>
		</div>

        <div style="margin-top: 15px;">
        	< 
        	<a href="<?php echo base_url(); ?>carrito" style="text-decoration:underline;font-weight: bold;font-size: 14px;">Regresar al carrito</a>
        </div>

		<div class="cont-finalizar-mail">
			<p>Ingresa tu e-mail para continuar la compra. <span>Rápido, fácil y seguro.</span></p>
		</div>

		<div class="cont-plano">

<?php
if ( $this->session->flashdata('alertaMensaje') ) {
?>
			<div class="alert alert-<?php echo $this->session->flashdata('alertaTipo'); ?> alert-dismissible">
			  <?php echo $this->session->flashdata('alertaMensaje'); ?>
			</div>
<?php
}
?>

<?php
$atributos = array('id' => 'forma-carrito-correo');
echo form_open('carrito/correo_validacion', $atributos);
?>

				<input type="hidden" id="emailCtr" value="<?php echo $email; ?>">

				<div class="registro-correo input-ventana">
					<i class="fas fa-envelope"></i>

					<input type="email" placeholder="Correo Electrónico" name="email" id="email" required minlength="10" maxlength="70" value="<?php echo $email; ?>"> 

				</div>
<?php
if ($email != '') {
?>
				<div class="registro-contrasena input-ventana">
					<i class="fas fa-unlock-alt"></i>
					<input type="password" placeholder="Contraseña" required name="password" id="password" minlength="7" maxlength="20">
				</div>

<?php
}
?>
				<div class="cols2">
					<button class="btn-producto btn-finalizar-compra" type="submit">
					    <span>CONTINUAR</span>
					</button>
				</div>

			</form>

			<br>

			<center>
				<p><a href="<?php echo base_url(); ?>/solicitud_contrasena" style="color:#3cb39d;">¿Olvidaste tu contraseña?</a></p>
			</center>
			<div style="text-align: right;padding:0px 10px 10px 10px;">
				<hr style="border: 1px solid #e4e4e4;">
				<p style="color:grey;">¿No tienes una cuenta registrada? | <a href="<?php echo base_url(); ?>registro" style="color:#3cb39d;">Crear Cuenta</a></p>
			</div>


		</div>

		
	</section>