	<section id="content">
		<!-- Banner Categoria -->
		<div class="banner-categoria">
			<img src="<?php echo base_url(); ?>assets/tienda/images/banner-categoria.jpg" alt="banner categoria">
		</div>
		<!-- Banner Categoria fin -->

		<!-- miga de pan / Ordenar producto -->
		<div class="miga-ordenar">
			<ul class="miga-pan">
				<li>
					<a href="<?php echo base_url(); ?>">Inicio</a>
				</li>
				<li>
					<span>Mis Favoritos</span>
				</li>
			</ul>
<!--
			<div class="ordenar-productos">
				<select name="" id="ordenar">
					<option value="">Ordenar productos</option>
					<option value="">Más reciente</option>
					<option value="">Más antiguo</option>
				</select>
			</div>
-->
		</div>
		<!-- miga de pan / Ordenar producto Fin -->

		<!-- Lista de productos a 4 columnas -->
		<div class="cols4 listado-producto">
      
    <?php
    if (count($tabla_productos)) {
    foreach ($tabla_productos as $registro) {
    ?>
			<div class="col">
				<div class="cont-producto">
		            <a href="<?php echo base_url(); ?>p/<?php echo $registro['ruta']; ?>" class="nombre-producto"><?php echo $registro['nombre']; ?></a>

		            <a href="<?php echo base_url(); ?>p/<?php echo $registro['ruta']; ?>" class="cont-img-producto">
        <?php
        foreach ($tabla_productos_multimedia as $registro2) {
            if ($registro2['producto'] == $registro['id']) {
        ?>
                     <img src="<?php echo base_url(); ?>imagenes/producto/multimedia/<?php echo $registro2['imagen'] ?>" width="243" height="243" />
        <?php
                break;
            }
        }
        ?>                   
		            </a>

		            <div class="flag-dcto" style="display:none;">
		            	<span>28</span>
		            </div>

		            <div class="flag-nuevo">
		            	<img src="<?php echo base_url(); ?>assets/tienda/images/flag-nuevo.png" alt="flag nuevo">
		            </div>

<?php
$class_deseos = 'deseos';
if ($this->session->has_userdata($this->config->item('raiz') . 'fe_lista_deseos')) {
    foreach ($this->session->userdata($this->config->item('raiz') . 'fe_lista_deseos') as $key => $value) {
        if ($registro['id'] == $key) {
			$class_deseos = 'deseos_sel';
        	break;
        }
    }
}
?>

		            <button type="button" class="<?php echo $class_deseos; ?>" producto="<?php echo $registro['id']; ?>">
	                  <i class="fas fa-heart"></i>
	                </button>

		            <div class="cont-precios">

		            	<h4 class="precio-dcto">$<?php echo number_format($registro['precio'],0); ?></h4>
<!--
		            	<h4 class="precio-real">$4.049.900</h4>
		            	<h4 class="precio-dcto">$2.915.958</h4>
-->		            	
		            </div>

		            <div class="cont-btn-producto">
                  <a href="<?php echo base_url(); ?>p/<?php echo $registro['ruta']; ?>">
		            	<button class="btn-producto">
		            		<span>VER PRODUCTO</span>
		            	</button>
                  </a>
		            </div>   
		  	    </div>
			</div>
      
      
      
    <?php
    }
    } else {
	?>
			<div style='color:#e59200;font-size: 16px;margin-bottom:200px;'>No se ha encontrado ningún producto favorito.</div>

	<?php    	
    }
    ?>          
      
		</div>
		<!-- Lista de productos a 4 columnas -->

		<br><br><br>

		<!-- paginador 
		<div class="paginador">
			<ul>
				<li>
					<a href="#">1</a>
				</li>
				<li class="pagina-activa">
					<a href="#">2</a>
				</li>
				<li>
					<a href="#">3</a>
				</li>
				<li>
					<a href="#">4</a>
				</li>
			</ul>
		</div>
		 paginador fin -->
	</section>
