<!DOCTYPE html>
<html lang="es">
<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
  
		<title><?php echo $title; ?></title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="<?php echo base_url(); ?>assets/tienda/css/layout.css?v=<?php echo $this->config->item('version');?>" type="text/css" rel="stylesheet">
		<link href="<?php echo base_url(); ?>assets/tienda/css/layout_fix.css?v=<?php echo $this->config->item('version');?>" type="text/css" rel="stylesheet">
		<link href="<?php echo base_url(); ?>assets/tienda/css/header.css?v=<?php echo $this->config->item('version');?>" type="text/css" rel="stylesheet">
		<link href="<?php echo base_url(); ?>assets/tienda/css/footer.css?v=<?php echo $this->config->item('version');?>" type="text/css" rel="stylesheet">
		<link href="<?php echo base_url(); ?>assets/tienda/css/mobile.css?v=<?php echo $this->config->item('version');?>" type="text/css" rel="stylesheet" media="only screen and (max-width: 1024px)">

		<link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url(); ?>assets/tienda/favicon/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url(); ?>assets/tienda/favicon/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url(); ?>assets/tienda/favicon/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>assets/tienda/favicon/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url(); ?>assets/tienda/favicon/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url(); ?>assets/tienda/favicon/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url(); ?>assets/tienda/favicon/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url(); ?>assets/tienda/favicon/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url(); ?>assets/tienda/favicon/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url(); ?>assets/tienda/favicon/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url(); ?>assets/tienda/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url(); ?>assets/tienda/favicon/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>assets/tienda/favicon/favicon-16x16.png">

		<!-- css fontawesome -->
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css?v=<?php echo $this->config->item('version');?>" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
		<!-- css fontawesome fin -->

		<!-- css Slider -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/tienda/css/flexslider.css?v=<?php echo $this->config->item('version');?>" type="text/css" media="screen" />
		<!-- css Slider fin -->
		
		<!-- Modernizr -->
  		<script src="<?php echo base_url(); ?>assets/tienda/js/modernizr.js?v=<?php echo $this->config->item('version');?>"></script>
  
		<!-- vista css -->
		<?php echo $vista_css; ?>


		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-111141148-1"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'UA-111141148-1');
		</script>

</head>
<body>

	<header class="bg-header" id="header">
		<!-- Cabezote logo *->
		<div id="logo">
			<img src="<?php echo base_url(); ?>assets/tienda/images/logo.jpg" alt="logo">
		</div>
		<!-* Cabezote logo fin -->	

		<div class="cont-logo">
			<div id="logo">
				<a href="<?php echo base_url(); ?>">
					<img src="<?php echo base_url(); ?>assets/tienda/images/home-bittat-group.png" alt="bittat logo">
				</a>
			</div>

			<div class="whatsapp-header">
				<a href="https://api.whatsapp.com/send?phone=573229440083" target="_blank">
					<i class="fab fa-whatsapp"></i>
					<span>322 944 0083</span>
				</a>
			</div>
		</div>

	</header>
