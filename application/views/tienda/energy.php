	<section id="content">
		<!-- Energy -->
		<div class="banner-energy">
			<img src="<?php echo base_url(); ?>assets/tienda/images/banner-panel-desde.jpg" alt="panel desde $300.000">
		</div>

		<div class="servicios-energy cols2">
			<div class="col">
				<div class="logo-marca">
					<img src="<?php echo base_url(); ?>assets/tienda/images/logo-bittat-energy.png" alt="bittat energy">
				</div>

				<h2>CONOCE NUESTROS <strong>SERVICIOS</strong></h2>
			</div>

			<div class="col">
				<div class="cont-servicio">
					<a href="#">
						<span class="icon icon60 icon-paneles"></span>
						<h3>Paneles solares</h3>
					</a>
				</div>

				<div class="cont-servicio">
					<a href="<?php echo base_url(); ?>energy_servicios">
						<span class="icon icon60 icon-servicios"></span>
						<h3>Nuestros Servicios</h3>
					</a>
				</div>
			</div>
		</div>

		<div class="beneficios">
			<div class="descrip-beneficios">
				<h2>BENEFICIOS</h2>
				<p>¿Conoces los beneficios de usar paneles solares?</p>
			</div>

			<div class="cols3">
				<div class="col">
					<div class="cont-servicio">
						<span class="icon icon60 icon-ahorro"></span>
						<div>
							<h6>AHORRO</h6>
							<p>Ahorros en el pago de la factura mensual.</p>
						</div>
					</div>
				</div>

				<div class="col">
					<div class="cont-servicio">
						<span class="icon icon60 icon-gratis"></span>
						<div>
							<h6>GRATIS</h6>
							<p>La energía del sol es gratuita.</p>
						</div>
					</div>
				</div>

				<div class="col">
					<div class="cont-servicio">
						<span class="icon icon60 icon-generacion"></span>
						<div>
							<h6>GENERACIÓN DE ENERGIA</h6>
							<p>Generación de energía en todo momento del día.</p>
						</div>
					</div>
				</div>

				<div class="col">
					<div class="cont-servicio">
						<span class="icon icon60 icon-protege"></span>
						<div>
							<h6>PROTEGE</h6>
							<p>Se protege contra incrementos del costo de energía pública.</p>
						</div>
					</div>
				</div>

				<div class="col">
					<div class="cont-servicio">
						<span class="icon icon60 icon-durabilidad"></span>
						<div>
							<h6>DURABILIDAD</h6>
							<p>Durabilidad de 20 años.</p>
						</div>
					</div>
				</div>

				<div class="col">
					<div class="cont-servicio">
						<span class="icon icon60 icon-instalacion"></span>
						<div>
							<h6>INSTALACIÓN</h6>
							<p>Instalación en diferentes espacios.</p>
						</div>
					</div>
				</div>

				<div class="col">
					<div class="cont-servicio">
						<span class="icon icon60 icon-sitio-ideal"></span>
						<div>
							<h6>SITIO IDEAL</h6>
							<p>Ideal para fincas y empresas.</p>
						</div>
					</div>
				</div>

				<div class="col">
					<div class="cont-servicio">
						<span class="icon icon60 icon-impuestos"></span>
						<div>
							<h6>IMPUESTOS</h6>
							<p>Descuentos en impuestos.</p>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="asesor-contacto">
			<div class="asesor">
				<h2>Conviértase en distribuidor y obtenga lista de precios preferente.</h2>
			</div>

			<div class="contacto">
				<div class="cont-contacto">
					<p>Contacto: 322-9440083</p>
					<p>Email: servicioalcliente@bittathome.com</p>
					<p>Compra en línea: www.bittathome.com</p>
				</div>
			</div>
		</div>
		<!-- Energy fin -->
	</section>