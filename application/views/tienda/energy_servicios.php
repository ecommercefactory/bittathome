	<section id="content">
		<!-- Energy -->
		<div class="banner-energy">
			<img src="<?php echo base_url(); ?>assets/tienda/images/banner-servicios.png" alt="Servicios bittat energy">
		</div>
		
		<div class="servicios-interna">
			<div class="cont-servicio-interna">
				<span class="icon icon60 icon-servicios"></span>
				<div>
					<h6>Consultoria</h6>
					<p>En la consecución de licencias ante las entidades de control como la UPME, AMLA, CRGE para obtener los beneficios especiales tributarios.</p>
				</div>
			</div>

			<div class="cont-servicio-interna">
				<span class="icon icon60 icon-diseno"></span>
				<div>
					<h6>Diseño</h6>
					<p>Diseño arquitectónico y técnico del sistema fotovoltaico para cada espacio requerido.</p>
				</div>
			</div>

			<div class="cont-servicio-interna">
				<span class="icon icon60 icon-asesoria"></span>
				<div>
					<h6>Asesoria</h6>
					<p>Asesoría de cómo optimizar los beneficios obtenidos por la inversión en energías limpias para las empresas.</p>
				</div>
			</div>

			<div class="cont-servicio-interna">
				<span class="icon icon60 icon-consultoria"></span>
				<div>
					<h6>Consultoria</h6>
					<p>Consultoría en materia contable internacional para soportar el proyecto en los estados financieros.</p>
				</div>
			</div>

			<div class="cont-servicio-interna">
				<span class="icon icon60 icon-revision"></span>
				<div>
					<h6>Revisión</h6>
					<p>Revisión de cumplimiento de normas técnicas dentro del marco eléctrico (RETIE).</p>
				</div>
			</div>

			<div class="cont-servicio-interna">
				<span class="icon icon60 icon-mantenimiento"></span>
				<div>
					<h6>Mantenimiento</h6>
					<p>Mantenimiento y ajustes a equipos solares existentes.</p>
				</div>
			</div>

			<div class="cont-servicio-interna">
				<span class="icon icon60 icon-suministros"></span>
				<div>
					<h6>Suministros</h6>
					<p>Suministro, instalación y montaje de paneles solares y equipos periféricos.</p>
				</div>
			</div>

		</div>

		<!-- Energy fin -->
	</section>
