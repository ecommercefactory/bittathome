	<section id="content">
		<!-- Contenido plano -->
		<div class="contenido-plano">
			<div class="titulos">
				<h2>INGRESAR</h2>
			</div>

			<div class="cont-plano">

<?php
if ( $this->session->flashdata('alertaMensaje') ) {
?>
				<div class="alert alert-<?php echo $this->session->flashdata('alertaTipo'); ?> alert-dismissible">
				  <?php echo $this->session->flashdata('alertaMensaje'); ?>
				</div>
<?php
}
?>

<?php
$atributos = array('id' => 'forma-ingreso2');
echo form_open('ingreso/validacion', $atributos);
?>
<!--
					<div class="cols2 registro-redes">
						<div class="col">
							<button class="btn-inicio-facebook">
								<i class="fab fa-facebook-f"></i>
								<span>Ingresar con Facebook</span>
							</button>
						</div>

						<div class="col">
							<button class="btn-inicio-google">
								<i class="fab fa-google-plus-g"></i>
								<span>Ingresar con Google</span>
							</button>
						</div>
					</div>
-->
					<div class="registro-correo input-ventana">
						<i class="fas fa-envelope"></i>
						<input type="email" placeholder="Correo Electrónico" required name="email" id="email_i2" minlength="10" maxlength="70">
					</div>
					<div class="registro-contrasena input-ventana">
						<i class="fas fa-unlock-alt"></i>
						<input type="password" placeholder="Contraseña" required name="password" id="password_i2" minlength="7" maxlength="20">
					</div>
<?php 
if ($_SERVER['SERVER_NAME'] == "bittathome.com") {
?>
					<center>
						<div class="g-recaptcha" data-sitekey="<?php echo $this->config->item('recaptcha_sitekey');?>"></div><br>
					</center>
<?php
}
?>
					<div class="cols2">
						<button type="submit" class="btn-ingresar btn-producto">INGRESAR</button>
					</div>

				</form>

				<br>

				<center>
					<p><a href="<?php echo base_url(); ?>/solicitud_contrasena" style="color:#3cb39d;">¿Olvidaste tu contraseña?</a></p>
				</center>
				<div style="text-align: right;padding:0px 10px 10px 10px;">
					<hr style="border: 1px solid #e4e4e4;">
					<p style="color:grey;">¿No tienes una cuenta registrada? | <a href="<?php echo base_url(); ?>registro" style="color:#3cb39d;">Crear Cuenta</a></p>
				</div>



			</div>
		</div>
		<!-- Contenido plano fin -->
	</section>