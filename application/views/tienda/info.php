	<section id="content">
		<!-- Contenido plano -->
		<div class="contenido-plano">
			<div class="titulos">
				<h2><?php echo $cms_titulo; ?></h2>
			</div>

			<div class="cont-plano-grande">
				<?php echo $cms_contenido; ?>
			</div>
		</div>
		<!-- Contenido plano fin -->
	</section>