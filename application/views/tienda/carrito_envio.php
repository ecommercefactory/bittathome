	<section id="content-checkout">
		<div class="titulo-checkout">
			<h3>FINALIZAR COMPRA</h3>
		</div>

		<div class="datos-finales">
			<div class="contenido-datos">
				<div class="datos-comprador">
					<div class="tus-datos">
						<h6 class="texto-gris" id="paso-1-titulo">PASO 1. TUS DATOS</h6>

						<p><?php echo $email_datos; ?></p>
						<p>-</p>
						<p><?php echo $nombre_datos; ?></p>
						<p><?php echo $apellidos_datos; ?></p>
						<p><?php echo $telefono_datos; ?></p>
						<a href="<?php echo base_url() . "carrito/perfil" ?>" class="btn-gris btn">Editar</a>

					</div>

					<div class="direccion-envio">
						<h6 id="paso-2-titulo">PASO 2. DIRECCIÓN DE ENVÍO</h6>

						<div id="paso-2-1" class="oculto">
							<p>Diligencia los campos.</p>
							<a href="" class="btn-gris btn">Editar</a>
						</div>

<?php
if ( $this->session->flashdata('alertaMensaje') ) {
?>
						<div class="alert alert-<?php echo $this->session->flashdata('alertaTipo'); ?> alert-dismissible">
						  <?php echo $this->session->flashdata('alertaMensaje'); ?>
						</div>
<?php
}
?>
					

<?php
$atributos = array('id' => 'formulario-envio');
echo form_open('carrito/envio_validacion', $atributos);
?>
							<br>
							<div class="c-direccion">
								<label for="">Dirección</label>
								<input type="text" id="direccion_1_datos" name="direccion_1_datos" value="<?php echo $direccion_1_datos; ?>" required minlength="4" maxlength="250">
							</div>

							<div class="c-piso">
								<label for="">Edificio, apartamento, torre o interior</label>
								<input type="text" id="direccion_2_datos" name="direccion_2_datos" value="<?php echo $direccion_2_datos; ?>" minlength="2" maxlength="250">
							</div>

							<div class="c-barrio">
								<label for="">Barrio</label>
								<input type="text" id="barrio_datos" name="barrio_datos" value="<?php echo $barrio_datos; ?>" required minlength="2" maxlength="50">
							</div>

							<div class="c-departamento">
								<label for="">Departamento</label>
								<input type="text" id="departamento_datos" name="departamento_datos" value="Cundinamarca" required minlength="2" maxlength="50" readonly>
							</div>

							<div class="c-municipio">
								<label for="">Municipio</label>
								<input type="text" id="municipio_datos" name="municipio_datos" value="Bogotá" required minlength="2" maxlength="50" readonly>
							</div>

							<div class="c-persona-recibe">
								<label for="">Nombre de la persona que va a recibir</label>
								<input type="text" id="persona_recibe_datos" name="persona_recibe_datos" value="<?php echo $persona_recibe_datos; ?>" minlength="2" maxlength="50">
							</div>

							<div class="cont-btn-check">
								<button class="btn-producto btn-finalizar-compra">
								    <span>CONTINUAR</span>
								</button>
							</div>
						</form>
					</div>

					<div class="medio-pago">
						<h6 class="texto-gris" id="paso-3-titulo">PASO 3. PAGAR</h6>
<!--
						<div id="paso-3-1">
							<p>Diligencia los campos.</p>
							<span class="btn-gris btn btnSpan">Editar</span>
						</div>
-->

					</div>	
				</div>

				<div class="resumen-compra">
					<h6>RESUMEN DE LA COMPRA</h6>

					<br>
		        	< 
		        	<a href="<?php echo base_url(); ?>carrito" style="text-decoration:underline;font-weight: bold;font-size: 12px;">Regresar al carrito</a>
					<br><br>
<!-- ******************************************************************* -->
<?php

$productosCarrito = 0;
$subtotal = 0;
$total = 0;

if (count($carrito_compras)) {
	foreach ($carrito_compras as $key => $value) {

		$imagen = '';
		if (count($tabla_productos_multimedia)) {
			foreach ($tabla_productos_multimedia as $regImagen) {
				if ($regImagen['producto'] == $key) {
					$imagen = $regImagen['imagen'];
					break;
				}
			}
		}

		$productoNombre = '';
		$productoRuta = '';
		$productoPrecio = 0;
		if (count($tabla_productos)) {
			foreach ($tabla_productos as $regProducto) {
				if ($regProducto['id'] == $key) {
					$productoNombre = $regProducto['nombre'];
					$productoRuta = $regProducto['ruta'];
					$productoPrecio = $regProducto['precio'];
					$productoPrecio = ($productoPrecio+($productoPrecio*($tabla_comercio->impuesto/100)));
					break;
				}
			}
		}

?>
<!-- ******************************************************************* -->
					<div class="cont-carrito-producto">
						<div class="producto-carrito">

<?php
		if ($imagen != '') {
?>
					<img src="<?php echo base_url(); ?>imagenes/producto/multimedia/<?php echo $imagen; ?>" width="82" height="82" alt="<?php echo $productoNombre; ?>" />
<?php
		}
?>

							<a href="<?php echo base_url() . 'p/'. $productoRuta; ?>"><?php echo $productoNombre; ?></a>
						</div>

						<div class="total-carrito">
							<p>$<?php
$productoTotal = 0;
$productoTotal = $productoPrecio * $value;
echo number_format($productoTotal,0);
$subtotal += $productoTotal;
					?></p>
						</div>
					</div>
<!-- ******************************************************************* -->
<?php
		$productosCarrito += $value;
	}
}
$total = $subtotal;
?> 
<!-- ******************************************************************* -->
					<div class="total">
						<div class="total-cont">
							<div class="sub-total">
								<span class="txt-sub">Subtotal</span>
								<span class="valor-sub">$<?php echo number_format($subtotal,0); ?></span>	
							</div>

							<div class="total-carrito">
								<span class="txt-total">Total</span>
								<span class="valor-total">$<?php echo number_format($total,0); ?></span>
							</div>
						</div>

						<div class="cont-btn-check oculto">
								<button class="btn-producto btn-finalizar-compra">
						            <span>PAGAR</span>
						        </button>
						</div>
					</div>
<!-- ******************************************************************* -->
				</div>
			</div>

		</div>
	
	</section>