
	<footer class="footer-checkout">
		<div class="datos-contacto">
			<ul>
				<li><h6>DATOS DE CONTACTO</h6></li>
				<li><i class="fas fa-phone"></i>(+57 1) 2851914</li>
				<li><i class="fas fa-mobile-alt"></i>(+57) 322 944 0083</li>
				<li><i class="fas fa-envelope-open"></i>servicioalcliente@bittathome.com</li>
				<li><i class="fas fa-home"></i>Cra 15 No 78-33 Local 2270 </li>
				<li><i class="fas fa-globe-asia"></i>Bogotá - Colombia</li>
			</ul>
				<ul class="logo-footer">
					<li>
					 	<img src="<?php echo base_url(); ?>assets/tienda/images/home-bittat-group.png" alt="bittat logo">
					</li>
				</ul>
			</ul>
		</div>

		<div class="copy-footer">
			<p>©2018 Todos los derechos reservados a Bittat Group</p>
		</div>
	</footer>


	 <!-- jQuery -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="<?php echo base_url(); ?>assets/tienda/js/libs/jquery-1.7.min.js">\x3C/script>')</script>
	 
	<!-- sweetalert2 -->
	<script src="<?= base_url() ?>assets/plugins/sweetalert2/sweetalert2.all.js?v=<?php echo $this->config->item('version');?>"></script>
	 
	<!-- Eventos JS -->
	<script defer src="<?php echo base_url(); ?>assets/tienda/js/eventos.js?v=<?php echo $this->config->item('version');?>"></script>
	<!-- Eventos JS -->

	<!-- FlexSlider -->
	<script defer src="<?php echo base_url(); ?>assets/tienda/js/jquery.flexslider.js?v=<?php echo $this->config->item('version');?>"></script>
	<!-- FlexSlider fin -->

	<script defer src="<?php echo base_url(); ?>assets/tienda/js/slide.js?v=<?php echo $this->config->item('version');?>"></script>

	<!-- jQuery Validation Plugin -->
	<script src="<?= base_url() ?>assets/plugins/jquery-validation/jquery.validate.min.js?v=<?php echo $this->config->item('version');?>"></script>

	<?php echo $proyVar; ?>

	<!-- vista js -->
	<?php echo $vista_js; ?>

</body>
</html>