	<section id="content-checkout">
		<div class="titulo-checkout">
			<h3>CARRITO DE COMPRAS</h3>
		</div>

		<div class="carrito-compras">


<?php

$productosCarrito = 0;
$subtotal = 0;
$total = 0;

if (count($carrito_compras)) {
?>

			<div class="titulos-carrito">
				<ul>
					<li>Producto</li>

					<li>Envío</li>

					<li>Precio</li>
					<li>Cantidad</li>
					<li>Total</li>
				</ul>
			</div>

<?php
	foreach ($carrito_compras as $key => $value) {

		$imagen = '';
		if (count($tabla_productos_multimedia)) {
			foreach ($tabla_productos_multimedia as $regImagen) {
				if ($regImagen['producto'] == $key) {
					$imagen = $regImagen['imagen'];
					break;
				}
			}
		}

		$productoNombre = '';
		$productoRuta = '';
		$productoPrecio = 0;
		if (count($tabla_productos)) {
			foreach ($tabla_productos as $regProducto) {
				if ($regProducto['id'] == $key) {
					$productoNombre = $regProducto['nombre'];
					$productoRuta = $regProducto['ruta'];
					$productoPrecio = $regProducto['precio'];
					$productoPrecio = ($productoPrecio+($productoPrecio*($tabla_comercio->impuesto/100)));
					break;
				}
			}
		}
?>
			<div class="cont-carrito-producto" id="producto-registro_<?php echo $key; ?>">
				<div class="producto-carrito">
					
<?php
		if ($imagen != '') {
?>
					<img src="<?php echo base_url(); ?>imagenes/producto/multimedia/<?php echo $imagen; ?>" width="82" height="82" alt="<?php echo $productoNombre; ?>" />
<?php
		}
?>


					<a href="<?php echo base_url() . 'p/'. $productoRuta; ?>"><?php echo $productoNombre; ?></a>
				</div>


				<div class="envio-carrito">
					<p>Gratis</p>
				</div>


				<div class="precio-carrito">
					<p class="precio-real-carrito">$<?php echo number_format($productoPrecio,0); ?></p>
					<!--
					<p class="precio-real-carrito">De $180.000</p>
					<p class="precio-dcto-carrito">Por <span>$150.000</span></p>
					-->
				</div>

				<div class="cantidad-carrito">
					<button type="button" class="btn-cantidad menos-cantidad" data-quantity="minus" producto="<?php echo $key; ?>">
		        			<i class="fas fa-minus"></i>
		        	</button>
		        		
		        	<input class="input-cantidad allownumericwithoutdecimal" type="number" name="" producto="<?php echo $key; ?>" id="cantidad_<?php echo $key; ?>" value="<?php echo $value; ?>" min="1" max="10" step="1" precio="<?php echo $productoPrecio; ?>">

		        	<button type="button" class="btn-cantidad mas-cantidad" data-quantity="plus" producto="<?php echo $key; ?>">
		        			<i class="fas fa-plus"></i>
		        	</button>
				</div>

				<div class="total-carrito">
					<p class="productoTotal" id="productoTotal_<?php echo $key; ?>">$<?php
		$productoTotal = 0;
		$productoTotal = $productoPrecio * $value;
		echo number_format($productoTotal,0);
		$subtotal += $productoTotal;
					?></p>
				</div>

				<span class="eliminar-producto-carrito" style="cursor:pointer;" alt="Retirar" title="Retirar Producto del Carrito de Compras" producto="<?php echo $key; ?>">X</span>
			</div>
<?php
		$productosCarrito += $value;
	}
}

$total = $subtotal;

$productosCarritoVacioStyle = "block";
$productosCarritoTotalesStyle = "none";
if ($productosCarrito > 0) {
	$productosCarritoVacioStyle = "none";
	$productosCarritoTotalesStyle = "block";
}
?> 

			<div id="msgNoCarrito" style="margin-top:40px;display:<?php echo $productosCarritoVacioStyle; ?>;">
				<p style="color:#000;font-size: 24px;margin-bottom:20px;">
			Tu carrito está vacio
				</p>
				<p style="font-size: 16px;margin-bottom:20px;">Para seguir comprando, navega por las categorías en el sitio.</p>
				<a href="<?php echo base_url(); ?>" class="btn-morado btn" style="margin-bottom:200px;">ELEGIR PRODUCTOS</a>
			</div>

			<div class="total-cupon" style="display:<?php echo $productosCarritoTotalesStyle; ?>;">

<!--
				<div class="cupon">
					<p>Sumar cupón de descuento</p>
					<form action="">
						<input type="text" id="cupon">
						<button type="button" class="btn-cupon">
							<span>Sumar cupón</span>
		        		</button>
					</form>
				</div>
-->
				<div class="total">
					<div class="total-cont">
						<div class="sub-total">
							<span class="txt-sub">Subtotal</span>
							<span class="valor-sub">$<?php echo number_format($subtotal,0); ?></span>	
						</div>

						<div class="total-carrito">
							<span class="txt-total">Total</span>
							<span class="valor-total">$<?php echo number_format($total,0); ?></span>
						</div>
					</div>

					<button class="btn-producto btn-finalizar-compra" type="button">
			            <span>FINALIZAR COMPRA</span>
			        </button>

			        <div style="margin-top: 15px;">
			        	<a href="<?php echo base_url(); ?>" style="text-decoration:underline;font-weight: bold;font-size: 14px;">Agregar más productos</a>
			        </div>
			        
				</div>
				
			</div>
		</div>
	</section>
