
	<!-- ir al inicio -->
	<a href="#header" id="top">
		<span class="icon icon40 icon-top"></span>
	</a>
	<!-- ir al inicio fin -->


	<footer>
		<div class="cont-footer">
			<div class="menu-foot">
        
<?php
foreach ($tabla_categorias as $registro) {
?>
				<ul>
					<li>
            			<h6><a href="<?php echo base_url(); ?>c/<?php echo $registro['ruta']; ?>"><?php echo $registro['nombre']; ?></a></h6>
					</li>
<?php
    foreach ($tabla_subcategorias as $registro2) {
        if ( $registro['id'] == $registro2['categoria']  ) {
?>
					<li>
            			<a href="<?php echo base_url(); ?>c/<?php echo $registro['ruta']; ?>/<?php echo $registro2['ruta']; ?>"><?php echo $registro2['nombre']; ?></a>
					</li>
<?php
        }
    }
?>           
				</ul>
<?php
}
?>           

<?php
if (isset($tabla_cms_contenidos)) {
	if (count($tabla_cms_contenidos)) {
?>
				<ul>
					<li>
            			<h6>Enlaces de Interés</h6>
					</li>
<?php
	    foreach ($tabla_cms_contenidos as $registro3) {
?>
					<li>
            			<a href="<?php echo base_url(); ?>info/<?php echo $registro3['ruta']; ?>"><?php echo $registro3['titulo']; ?></a>
					</li>
<?php
	   	}
?>
				</ul>

<?php
	}
}
?>


				<div class="datos-contacto">
					<ul>
						<li><h6>DATOS DE CONTACTO</h6></li>
						<li><i class="fas fa-phone"></i>(+57 1) 2851914</li>
						<li><i class="fas fa-mobile-alt"></i>(+57) 322 944 0083</li>
						<li><i class="fas fa-envelope-open"></i>servicioalcliente@bittathome.com</li>
						<li><i class="fas fa-home"></i>Cra 15 No 78-33 Local 2270 </li>
						<li><i class="fas fa-globe-asia"></i>Bogotá - Colombia</li>
					</ul>
					 <!-- <ul class="logo-footer">
					 	<li>
					 		<img src="<?php echo base_url(); ?>assets/tienda/images/home-bittat-group.png" alt="bittat logo">
					 	</li>
					 </ul> -->
					 <ul class="mapa-google">
					 	<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15906.40252229757!2d-74.058062!3d4.665077!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x35ffe0403608d714!2sBittat!5e0!3m2!1ses-419!2sco!4v1537979484163" width="100%" height="200" frameborder="0" style="border:0" allowfullscreen=""></iframe>
					 </ul>
				</div>
			</div>

			<div class="formulario-foot">
				<h6>ESCRIBENOS</h6>
				<form action="">
					<input type="text" id="nombres" placeholder="Nombres">
					<input type="email" id="email" placeholder="E-mail">
					<input type="tel" id="telefono" placeholder="Teléfono">
					<textarea id="mensaje" placeholder="Mensaje"></textarea>
					<input type="button" value="Enviar" id="enviar">
				</form>
			</div>
		</div>

		<div class="copy-footer">
			<p>©2018 Todos los derechos reservados a Bittat Group</p>


			<ul class="redes-sociales">
				<li>
					<a href="https://www.facebook.com/bittathome/" target="_blank">
						<i class="fab fa-facebook-f"></i>
					</a>
				</li>
				<li>
					<a href="https://www.instagram.com/bittathome/" target="_blank">
						<i class="fab fa-instagram"></i>
					</a>
				</li>
				<li>
					<a href="https://www.youtube.com/channel/UC0I5BwkrgFrm9K4ln3GAIhw/featured" target="_blank">
						<i class="fab fa-youtube"></i>
					</a>
				</li>

				<li class="marcas">
					<a href="#" target="_blank">
						<img src="<?php echo base_url(); ?>assets/tienda/images/logo-TS.png" alt="tiendishops">
					</a>

					<div class="e-factory">
						<p>Powered By</p>
						<a href="https://ecommercefactory.co/" target="_blank">
							<img src="<?php echo base_url(); ?>assets/tienda/images/logo-EF.png" alt="e-commerce factory">
						</a>
					</div>
				</li>

			</ul>


		</div>
	</footer>
	

	<!-- - ventana emergente registro - inicio ------------------------ -->	
	<div class="ventana-registro ventana-emergente" style="display: none;">
		<div class="ventana-emergente-c">
			<div class="title-ventana bg-color-modificado">
				<h3>REGISTRARSE</h3>
				<a href="" class="btn-cerrar cerrarMdlRegistro">X</a>
			</div>
<?php
$atributos = array('id' => 'forma-registro');
echo form_open('registro/validacion', $atributos);
?>
<!--
				<div class="cols2 registro-redes">
					<div class="col">
						<button class="btn-inicio-facebook">
							<i class="fab fa-facebook-f"></i>
							<span>Registro con Facebook</span>
						</button>
					</div>

					<div class="col">
						<button class="btn-inicio-google">
							<i class="fab fa-google-plus-g"></i>
							<span>Registro con Google</span>
						</button>
					</div>
				</div>
-->
				<div class="registro-nombre input-ventana">
					<i class="fas fa-user"></i>
					<input type="text" placeholder="Nombre Completo" required name="nombre" id="nombre_r" minlength="6" maxlength="70">
				</div>
				<div class="registro-correo input-ventana">
					<i class="fas fa-envelope"></i>
					<input type="email" placeholder="Correo Electrónico" required name="email" id="email_r" minlength="10" maxlength="70">
				</div>
				<div class="registro-contrasena input-ventana">
					<i class="fas fa-unlock-alt"></i>
					<input type="password" placeholder="Contraseña" required name="password" id="password_r" minlength="7" maxlength="20">
				</div>
				<div class="checked-terminos-registro">
					<input type="checkbox" checked required name="terminos" id="terminos_r">
					<p>Al registrarse. usted acepta nuestras condiciones de uso de privacidad <a href="<?php echo base_url(); ?>info/terminos-y-condiciones" target="_blank"> Leer más</a></p>
				</div>
				<div class="checked-terminos-registro">
					<input type="checkbox" checked required name="datos" id="datos_r">
					<p>Al registrarse. usted acepta nuestras condiciones de habeas data <a href="<?php echo base_url(); ?>info/politica-de-tratamiento-de-datos-personales" target="_blank"> Leer más</a></p>
				</div>
<?php 
if ($_SERVER['SERVER_NAME'] == "bittathome.com") {
?>
				<center>
					<div class="g-recaptcha" data-sitekey="<?php echo $this->config->item('recaptcha_sitekey');?>"></div><br>
				</center>
<?php
}
?>

				<div class="cols2">
					<button type="submit" class="crear-cuenta btn-producto">REGISTRARME</button>
				</div>
			</form>

			<div style="text-align: right;padding:0px 10px 10px 10px;">
			<hr style="border: 1px solid #e4e4e4;">
			<p style="color:grey;">¿Ya tienes una cuenta registrada? | <a href="" id="mdlIngreso2" style="color:#3cb39d;">Ingresar</a></p>
			</div>

		</div>		
	</div>
	<!-- - ventana emergente registro - fin ------------------------ -->	

	<!-- - ventana emergente ingreso - inicio ---------------------- -->	
	<div class="ventana-ingreso ventana-emergente" style="display: none;">
		<div class="ventana-emergente-c">
			<div class="title-ventana bg-color-modificado">
				<h3>INGRESAR</h3>
				<a href="" class="btn-cerrar cerrarMdlIngreso">X</a>
			</div>
<?php
$atributos = array('id' => 'forma-ingreso');
echo form_open('ingreso/validacion', $atributos);
?>
<!--
				<div class="cols2 registro-redes">
					<div class="col">
						<button class="btn-inicio-facebook">
							<i class="fab fa-facebook-f"></i>
							<span>Ingresar con Facebook</span>
						</button>
					</div>

					<div class="col">
						<button class="btn-inicio-google">
							<i class="fab fa-google-plus-g"></i>
							<span>Ingresar con Google</span>
						</button>
					</div>
				</div>
-->
				<div class="registro-correo input-ventana">
					<i class="fas fa-envelope"></i>
					<input type="email" placeholder="Correo Electrónico" required name="email" id="email_i" minlength="10" maxlength="70">
				</div>
				<div class="registro-contrasena input-ventana">
					<i class="fas fa-unlock-alt"></i>
					<input type="password" placeholder="Contraseña" required name="password" id="password_i" minlength="7" maxlength="20">
				</div>
<?php 
if ($_SERVER['SERVER_NAME'] == "bittathome.com") {
?>
				<center>
					<div class="g-recaptcha" data-sitekey="<?php echo $this->config->item('recaptcha_sitekey');?>"></div><br>
				</center>
<?php
}
?>


				<div class="cols2">
					<button type="submit" class="btn-ingresar btn-producto">INGRESAR</button>
				</div>
			</form>

			<center>
				<a href="" id="mdlOlvido2" style="color:#3cb39d;">¿Olvidaste tu contraseña?</a>
			</center>
			<div style="text-align: right;padding:0px 10px 10px 10px;">
			<hr style="border: 1px solid #e4e4e4;">
			<p style="color:grey;">¿No tienes una cuenta registrada? | <a href="" id="mdlRegistro2" style="color:#3cb39d;">Crear Cuenta</a></p>
			</div>

		</div>		
	</div> 
	<!-- - ventana emergente ingreso - fin ------------------------- -->	

	<!-- - ventana emergente olvido clave - inicio ----------------- -->	
	<div class="ventana-olvido-clave ventana-emergente" style="display: none;">
		<div class="ventana-emergente-c">
			<div class="title-ventana bg-color-modificado">
				<h3>SOLICITUD DE NUEVA CONTRASEÑA</h3>
				<a href="" class="btn-cerrar cerrarMdlOlvidoClave">X</a>
			</div>
<?php
$atributos = array('id' => 'forma-olvido-clave');
echo form_open('solicitud_contrasena/validacion', $atributos);
?>
				<div class="registro-correo input-ventana">
					<i class="fas fa-envelope"></i>
					<input type="email" placeholder="Correo Electrónico" required name="email" id="email_sc" minlength="10" maxlength="70">
				</div>
<?php 
if ($_SERVER['SERVER_NAME'] == "bittathome.com") {
?>
				<center>
					<div class="g-recaptcha" data-sitekey="<?php echo $this->config->item('recaptcha_sitekey');?>"></div><br>
				</center>
<?php
}
?>

				<div class="cols2">
					<button type="submit" class="btn-nueva-contrasena btn-producto">ENVIAR</button>
				</div>
			</form>

			<div style="text-align: right;padding:0px 10px 10px 10px;">
			<hr style="border: 1px solid #e4e4e4;">
			<p style="color:grey;">¿No tienes una cuenta registrada? | <a href="" id="mdlRegistro3" style="color:#3cb39d;">Crear Cuenta</a></p>
			</div>

		</div>		
	</div> 
	<!-- - ventana emergente olvido clave - fin -------------------- -->	

	<!-- jQuery -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="<?php echo base_url(); ?>assets/tienda/js/libs/jquery-1.7.min.js">\x3C/script>')</script>

	<!-- sweetalert2 -->
	<script src="<?= base_url() ?>assets/plugins/sweetalert2/sweetalert2.all.js?v=<?php echo $this->config->item('version');?>"></script>

	<!-- Eventos JS -->
	<script defer src="<?php echo base_url(); ?>assets/tienda/js/eventos.js?v=<?php echo $this->config->item('version');?>"></script>
	<!-- Eventos JS -->

	<!-- FlexSlider -->
	<script defer src="<?php echo base_url(); ?>assets/tienda/js/jquery.flexslider.js?v=<?php echo $this->config->item('version');?>"></script>
	<!-- FlexSlider fin -->

	<script defer src="<?php echo base_url(); ?>assets/tienda/js/slide.js?v=<?php echo $this->config->item('version');?>"></script>

	<!-- jQuery Validation Plugin -->
	<script src="<?= base_url() ?>assets/plugins/jquery-validation/jquery.validate.min.js?v=<?php echo $this->config->item('version');?>"></script>

	<?php echo $proyVar; ?>

	<script defer src="<?php echo base_url(); ?>assets/js/front/tienda/vista.js?v=<?php echo $this->config->item('version');?>"></script>

	<!-- vista js -->
	<?php echo $vista_js; ?>

</body>
</html>