	<section id="content">
		<!-- Sección marcas Bittat -->
		<div class="cols2 marcas-bittat">
			<div class="col">
				<div class="col-c borde-marca">
					<div class="logo-marca">
						<img src="<?php echo base_url(); ?>assets/tienda/images/logo-bittat-home.png" alt="bittat home">
					</div>

					<div class="cont-marca">
						<p>Automatiza tu hogar u oficina con lo último en equipos de domótica, controlando todo desde tu smartphone o Tablet todo a un click</p>

						<a href="<?php echo base_url(); ?>info/nosotros" class="btn-morado btn">VER MÁS</a>
					</div>
				</div>
			</div>

			<div class="col">
				<div class="col-c">
					<div class="logo-marca">
						<img src="<?php echo base_url(); ?>assets/tienda/images/logo-bittat-energy.png" alt="bittat energy">
					</div>

					<div class="cont-marca">
						<p>¿Buscas ahorrar energía?...  La energía solar es la solución, gracias a paneles solares y equipos de alta tecnología podrás economizar dinero en su factura…</p>

						<a href="<?php echo base_url(); ?>energy" class="btn-naranja btn">VER MÁS</a>
					</div>
				</div>
			</div>
		</div>
		<!-- Sección marcas Bittat fin -->

		<!-- Slider principal
		<div class="flexslider">
		    <ul class="slides">
		        <li>
		  	    	<img src="<?php echo base_url(); ?>assets/tienda/images/slide-1.png" />
		  	    </li>
		  	    <li>
		  	    	<img src="<?php echo base_url(); ?>assets/tienda/images/slide-2.png" />
		  	    </li>
		  	    <li>
		  	    	<img src="<?php echo base_url(); ?>assets/tienda/images/slide-3.png" />
		  	    </li>
		  	    <li>
		  	    	<img src="<?php echo base_url(); ?>assets/tienda/images/slide-4.png" />
		  	    </li>
		    </ul>
		</div>
		Slider principal Fin-->

		<!-- Slider principal -->
		<div class="flexslider slider-principal">
		    <ul class="slides">
		        <li>
		  	    	<img src="<?php echo base_url(); ?>assets/tienda/images/slide-1.png" />
		  	    </li>
		  	    <li>
		  	    	<img src="<?php echo base_url(); ?>assets/tienda/images/slide-2.png" />
		  	    </li>
		  	    <li>
		  	    	<img src="<?php echo base_url(); ?>assets/tienda/images/slide-3.png" />
		  	    </li>
		  	    <li>
		  	    	<img src="<?php echo base_url(); ?>assets/tienda/images/slide-4.png" />
		  	    </li>
		    </ul>
		</div>
		<!-- Slider principal Fin-->

		<!-- Slider principal movil-->
		<div class="flexslider slider-movil">
		    <ul class="slides">
		        <li>
		  	    	<img src="<?php echo base_url(); ?>assets/tienda/images/slide-movil-1.png" />
		  	    </li>
		  	    <li>
		  	    	<img src="<?php echo base_url(); ?>assets/tienda/images/slide-movil-2.png" />
		  	    </li>
		  	    <li>
		  	    	<img src="<?php echo base_url(); ?>assets/tienda/images/slide-movil-3.png" />
		  	    </li>
		  	    <li>
		  	    	<img src="<?php echo base_url(); ?>assets/tienda/images/slide-movil-4.png" />
		  	    </li>
		    </ul>
		</div>
		<!-- Slider principal movil Fin -->




		
		<!-- Slider productos -->
		<div class="vitrina-productos-home">
			<div class="titulos">
				<h2>LO MÁS VENDIDO</h2>

				<div class="cont-btn-todo">
					<a href="#" class="btn-todo">VER TODO</a>
				</div>
			</div>

			<div class="carousel">
		        <ul class="slides">
              
    <?php
    foreach ($tabla_productos_mas_vendido as $registro) {
    ?>              
		          <li class="cont-producto">
		            	<a href="<?php echo base_url(); ?>p/<?php echo $registro['ruta']; ?>" class="nombre-producto"><?php echo $registro['nombre']; ?></a>

		            	<a href="<?php echo base_url(); ?>p/<?php echo $registro['ruta']; ?>" class="cont-img-producto">
        <?php
        $imgCtr = false;
        foreach ($tabla_productos_multimedia as $registro2) {
            if ($registro2['producto'] == $registro['id']) {
	        	$imgCtr = true;
        ?>
                     <img src="<?php echo base_url(); ?>imagenes/producto/multimedia/mediana/<?php echo $registro2['mediana'] ?>?v=<?php echo $this->config->item('version');?>" width="243" height="243" />
        <?php
                break;
            }
        }
        if (!$imgCtr) {
        ?>
					<img src="<?php echo base_url(); ?>imagenes/producto/sin-imagen.png?v=<?php echo $this->config->item('version');?>" width="243" height="243" />        
        <?php        
        }
        ?>                      
		            	</a>

		            	<div class="flag-dcto" style="display:none;">
		            		<span>28</span>
		            	</div>

		            	<div class="flag-nuevo">
		            		<img src="<?php echo base_url(); ?>assets/tienda/images/flag-nuevo.png" alt="flag nuevo">
		            	</div>


<?php
$class_deseos = 'deseos';
if ($this->session->has_userdata($this->config->item('raiz') . 'fe_lista_deseos')) {
    foreach ($this->session->userdata($this->config->item('raiz') . 'fe_lista_deseos') as $key => $value) {
        if ($registro['id'] == $key) {
			$class_deseos = 'deseos_sel';
        	break;
        }
    }
}
?>

		            	<button type="button" class="<?php echo $class_deseos; ?>" producto="<?php echo $registro['id']; ?>">
							<i class="fas fa-heart"></i>
						</button>

		            	<div class="cont-precios">
		            		<h4 class="precio-dcto">$<?php echo number_format($registro['precio']+($registro['precio']*($tabla_comercio->impuesto/100)),0); ?></h4>
		            		<!-- <h4 class="precio-real">$<?php echo $registro['precio']; ?></h4> -->
		            		<!-- <h4 class="precio-dcto">$2.915.958</h4> -->
		            	</div>

		            	<div class="cont-btn-producto">
		            		<button class="btn-producto">
                      <a href="<?php echo base_url(); ?>p/<?php echo $registro['ruta']; ?>">
		            			<span>VER PRODUCTO</span>
                      </a>
		            		</button>
		            	</div>   
		  	    	</li>
    <?php
    }
    ?>  

		        </ul>
	        </div>
		</div>
        <!-- Slider productos fin-->

        <!-- Banner transversales -->
		<div class="banner-transversales">
			<div class="cols3">
				<div class="col">
					<div class="col-c">
						<span class="icon icon80 icon-envio"></span>
						<h3>VISITA TÉCNICA GRATIS</h3>
						<p>Se realiza visita sin costo en Bogotá y alrededores asesorando en el diseño optimo para cada espacio.</p>
					</div>
				</div>

				<div class="col">
					<div class="col-c">
						<span class="icon icon80 icon-horario"></span>
						<h3>HORARIO DE LA TIENDA</h3>
						<p>Atendemos de lunes a sábado de 10:30 a.m. a 6:30 p.m.</p>
					</div>
				</div>

				<div class="col">
					<div class="col-c">
						<span class="icon icon80 icon-garantia"></span>
						<h3>GARANTÍA DE COMPRA</h3>
						<p>Todo producto cuenta con garantía directa con nuestra compañía.</p>
					</div>
				</div>
			</div>
		</div>
        <!-- Banner transversales fin -->

        <!-- Slider productos -->
		<div class="vitrina-productos-home">
			<div class="titulos">
				<h2>LO MÁS VISTO</h2>

				<div class="cont-btn-todo">
					<a href="#" class="btn-todo">VER TODO</a>
				</div>
			</div>

			<div class="carousel">
		        <ul class="slides">
    <?php
    foreach ($tabla_productos_mas_visto as $registro) {
    ?>              
		          <li class="cont-producto">
		            	<a href="<?php echo base_url(); ?>p/<?php echo $registro['ruta']; ?>" class="nombre-producto"><?php echo $registro['nombre']; ?></a>

		            	<a href="<?php echo base_url(); ?>p/<?php echo $registro['ruta']; ?>" class="cont-img-producto">
        <?php
        $imgCtr = false;        
        foreach ($tabla_productos_multimedia as $registro2) {        	
            if ($registro2['producto'] == $registro['id']) {
		        $imgCtr = true;
        ?>
                     <img src="<?php echo base_url(); ?>imagenes/producto/multimedia/mediana/<?php echo $registro2['mediana'] ?>?v=<?php echo $this->config->item('version');?>" width="243" height="243" />
        <?php
                break;
            }
        }
        if (!$imgCtr) {
        ?>
					<img src="<?php echo base_url(); ?>imagenes/producto/sin-imagen.png?v=<?php echo $this->config->item('version');?>" width="243" height="243" />        
        <?php        
		}
        ?>                      
		            	</a>

		            	<div class="flag-dcto" style="display:none;">
		            		<span>28</span>
		            	</div>

		            	<div class="flag-nuevo">
		            		<img src="<?php echo base_url(); ?>assets/tienda/images/flag-nuevo.png" alt="flag nuevo">
		            	</div>


<?php
$class_deseos = 'deseos';
if ($this->session->has_userdata($this->config->item('raiz') . 'fe_lista_deseos')) {
    foreach ($this->session->userdata($this->config->item('raiz') . 'fe_lista_deseos') as $key => $value) {
        if ($registro['id'] == $key) {
			$class_deseos = 'deseos_sel';
        	break;
        }
    }
}
?>


		            	<button type="button" class="<?php echo $class_deseos; ?>" producto="<?php echo $registro['id']; ?>">
							<i class="fas fa-heart"></i>
						</button>

		            	<div class="cont-precios">
		            		<h4 class="precio-dcto">$<?php echo number_format($registro['precio']+($registro['precio']*($tabla_comercio->impuesto/100)),0); ?></h4>
		            		<!-- <h4 class="precio-real">$<?php echo $registro['precio']; ?></h4> -->
		            		<!-- <h4 class="precio-dcto">$2.915.958</h4> -->
		            	</div>

		            	<div class="cont-btn-producto">
		            		<button class="btn-producto">
                      <a href="<?php echo base_url(); ?>p/<?php echo $registro['ruta']; ?>">
		            			<span>VER PRODUCTO</span>
                      </a>
		            		</button>
		            	</div>   
		  	    	</li>
    <?php
    }
    ?>  
		        </ul>
	        </div>
		</div>
        <!-- Slider productos fin-->
	</section>