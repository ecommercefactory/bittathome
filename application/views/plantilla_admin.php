<?php
defined('BASEPATH') OR exit('No esta permitido el acceso directo a este script.');
/**
 * @autor Ing. David Amador - r.reyes@bittathome.com
 * @nombre plantilla.php
 * @fecha 24 Agosto 2017
 * @descripcion: 
 *              Manejo de footer (cabeza) y header (pie) en la pagina archivos correspondientes a la 
 *              pagina desplegada se crean el nombre de la carpeta igual al nombre de la 
 *              vista con el js o css dentro de la caperta segun corresponda con el 
 *              nombre "vista.css" o "vista.js" segun como se necesite.
 */
if (!isset($datos)) {
    $datos = null;
}
if (!isset($vista)) {
    $datos = null;
    die('falta Vista');
}

$js_ruta = $vista;
if (isset($ruta_js)) {
	$css_ruta = $ruta_js;
}

$css_ruta = $vista;
if (isset($ruta_css)) {
	$css_ruta = $ruta_css;
}

$parametros['title'] = $parametros['titulo'] = $parametros['intro'] = $parametros['subtitulo'] = $parametros['breadcrumb'] = '';
if (isset($datos['titulo'])) {
	$parametros['titulo'] = $datos['titulo'];
	$parametros['title'] = $datos['titulo'];;
}
if (isset($datos['intro'])) {
	$parametros['intro'] = $datos['intro'];
}
if (isset($datos['subtitulo'])) {
  $parametros['subtitulo'] = $datos['subtitulo'];
}
if (isset($datos['breadcrumb'])) {
  $parametros['breadcrumb'] = $datos['breadcrumb'];
}

$parametros['tituloPagina'] = '';
if (trim($parametros['titulo']) != '' or trim($parametros['subtitulo']) != '') {
  $parametros['tituloPagina'] = '
    <section class="content-header">
      <h1>
        '.trim($parametros['titulo']).' <small>'.trim($parametros['subtitulo']).'</small>
      </h1>
      '.$parametros['breadcrumb'].'
    </section>';
}
$proyVar = array(
	'base_url' => base_url(),
	'language' =>  $this->session->userdata($this->config->item('raiz') . 'be_lang_value'),
	'lang' =>  $this->session->userdata($this->config->item('raiz') . 'be_lang_code'),
);
$parametros['proyVar'] = '<script>var proyVar =' . json_encode($proyVar) . ';var proyVarS ={"sgctn":"'.$this->security->get_csrf_token_name().'","sgch":"'.$this->security->get_csrf_hash().'"};</script>';
$parametros['txtVar'] = '<script>var txtVar =' .  json_encode($this->lang->language) . ';</script>';

$parametros['script'] = '';

$parametros['estilo'] = '<link rel="stylesheet" href="' . base_url() . 'assets/css/admin/global.css?v=' . $this->config->item('version') . '">';
$js = 'assets/js/' . $js_ruta . '/vista.js';
$css = 'assets/css/' . $css_ruta . '/vista.css';
if (file_exists($js)) {
    $parametros['script'] = '<script src="' . base_url() . $js . '?v=' . $this->config->item('version') . '"></script>';
}
if (file_exists($css)) {
    $parametros['estilo'] .= "\n" . '  <link rel="stylesheet" href="' . base_url() . $css . '?v=' . $this->config->item('version') . '">';
}

if (!isset($plantilla)) {
  $cabeza = '/basicoCabeza';
  $pie = '/basicoPie';
} else {
  $cabeza = "/{$plantilla}Cabeza";
  $pie = "/{$plantilla}Pie";
}

$parametros['sidebar_collapse'] = '';
if ($vista != 'admin/panel') {
	if (!isset($datos['sidebar_collapse'])) {
		$parametros['sidebar_collapse'] = 'sidebar-collapse';
	}
}

$this->load->view('plantilla/admin' . $cabeza, $parametros);
$this->load->view($vista, $datos);
$this->load->view('plantilla/admin' . $pie, $parametros);
