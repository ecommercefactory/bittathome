<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?=$this->session->userdata($this->config->item('raiz') . 'be_plantilla_footer');?>
  <?=$this->session->userdata($this->config->item('raiz') . 'be_plantilla_sidebar');?>
</div>
<!-- ./wrapper -->

<div id="img-out" style="display:none;"></div>

<!-- Variables -->
<?= $proyVar ?>

<?= $txtVar ?>





<!-- jQuery 2.2.3 -->
<script src="<?= base_url() ?>assets/admin/adminlte/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?= base_url() ?>assets/admin/adminlte/bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?= base_url() ?>assets/admin/adminlte/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url() ?>assets/admin/adminlte/dist/js/app.min.js"></script>
<!-- Sparkline -->
<script src="<?= base_url() ?>assets/admin/adminlte/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?= base_url() ?>assets/admin/adminlte/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?= base_url() ?>assets/admin/adminlte/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="<?= base_url() ?>assets/admin/adminlte/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS 1.0.1 -->
<script src="<?= base_url() ?>assets/admin/adminlte/plugins/chartjs/Chart.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?= base_url() ?>assets/admin/adminlte/dist/js/pages/dashboard2.js"></script>


<!-- page scripts -->


<?= $script ?>

 
<script>
$(function(){

    $("#fotoCanvas").click(function() { 
        html2canvas($("#areaTrabajo"), {
            onrendered: function(canvas) {
                theCanvas = canvas;
                document.body.appendChild(canvas);
                $("#img-out").append(canvas);
                var href=canvas.toDataURL("image/png");
                var windowtab=window.open('about:blank','_blank');
                windowtab.document.write("<img src='"+href+"'/>");                
            }
        });
        return false;
    });

});
</script>	

</body>
</html>

