<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
	<?=$this->session->userdata($this->config->item('raiz') . 'be_plantilla_footer');?>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 
Nota: 
	Elimanamos esta linea porque genera conflicto con grocerycrud
<script src="<?php echo base_url(); ?>assets/admin/adminlte/plugins/jQuery/jquery-2.2.3.min.js"></script>
-->
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url(); ?>assets/admin/adminlte/bootstrap/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url(); ?>assets/admin/adminlte/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url(); ?>assets/admin/adminlte/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assets/admin/adminlte/dist/js/app.min.js"></script>

<!-- page scripts -->


<?= $script ?>



</body>
</html>