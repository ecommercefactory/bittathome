<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?=$this->session->userdata($this->config->item('raiz') . 'be_plantilla_footer');?>
  <?=$this->session->userdata($this->config->item('raiz') . 'be_plantilla_sidebar'); ?>
</div>
<!-- ./wrapper -->

<div id="img-out" style="display:none;"></div>

<!-- Variables -->
<?= $proyVar ?>

<?= $txtVar ?>

 


<!-- page scripts -->


<?= $script ?>
 

<script>
$(function(){

    $("#fotoCanvas").click(function() { 
        html2canvas($("#areaTrabajo"), {
            onrendered: function(canvas) {
                theCanvas = canvas;
                document.body.appendChild(canvas);
                $("#img-out").append(canvas);
                var href=canvas.toDataURL("image/png");
                var windowtab=window.open('about:blank','_blank');
                windowtab.document.write("<img src='"+href+"'/>");                
            }
        });
        return false;
    });

});
</script>	

</body>
</html>