<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?=$this->session->userdata($this->config->item('raiz') . 'be_plantilla_footer');?>
  <?=$this->session->userdata($this->config->item('raiz') . 'be_plantilla_sidebar');?>
</div>
<!-- ./wrapper -->

<!-- Variables -->
<?= $proyVar ?>

<?= $txtVar ?>

<!-- jQuery 2.2.3 -->
<script src="<?= base_url() ?>assets/admin/adminlte/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?= base_url() ?>assets/admin/adminlte/bootstrap/js/bootstrap.min.js"></script>
  
<!-- DataTables -->
<script src="<?= base_url() ?>assets/admin/adminlte/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>assets/admin/adminlte/plugins/datatables/dataTables.bootstrap.min.js"></script>
  
<!-- Select2 -->
<script src="<?= base_url() ?>assets/admin/adminlte/plugins/select2/select2.full.min.js"></script>
<!-- InputMask -->
<script src="<?= base_url() ?>assets/admin/adminlte/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?= base_url() ?>assets/admin/adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?= base_url() ?>assets/admin/adminlte/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?= base_url() ?>assets/admin/adminlte/plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="<?= base_url() ?>assets/admin/adminlte/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- bootstrap color picker -->
<script src="<?= base_url() ?>assets/admin/adminlte/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="<?= base_url() ?>assets/admin/adminlte/plugins/timepicker/bootstrap-timepicker.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="<?= base_url() ?>assets/admin/adminlte/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="<?= base_url() ?>assets/admin/adminlte/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="<?= base_url() ?>assets/admin/adminlte/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url() ?>assets/admin/adminlte/dist/js/app.min.js"></script>
<!-- jQuery Validation Plugin -->
<script src="<?= base_url() ?>assets/plugins/jquery-validation/jquery.validate.min.js"></script>

<!-- page scripts -->


<?= $script ?>


</body>
</html>