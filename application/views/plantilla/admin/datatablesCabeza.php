<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?=$this->lang->line('be_project_s_name')?>. <?=$title?></title>

  <!-- https://realfavicongenerator.net/ 
  <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url(); ?>assets/icos/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url(); ?>assets/icos/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>assets/icos/favicon-16x16.png">
  <link rel="manifest" href="<?php echo base_url(); ?>assets/icos/manifest.json">
  <link rel="mask-icon" href="<?php echo base_url(); ?>assets/icos/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="theme-color" content="#ffffff">
  -->

  <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url(); ?>assets/tienda/favicon/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url(); ?>assets/tienda/favicon/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url(); ?>assets/tienda/favicon/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>assets/tienda/favicon/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url(); ?>assets/tienda/favicon/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url(); ?>assets/tienda/favicon/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url(); ?>assets/tienda/favicon/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url(); ?>assets/tienda/favicon/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url(); ?>assets/tienda/favicon/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url(); ?>assets/tienda/favicon/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url(); ?>assets/tienda/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url(); ?>assets/tienda/favicon/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>assets/tienda/favicon/favicon-16x16.png">



  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/admin/adminlte/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">

  <!-- DataTables 
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/plugins/datatables/datatables.min.css">
  -->

  
  <link rel="stylesheet" href="<?= base_url() ?>assets/admin/adminlte/plugins/datatables/dataTables.bootstrap.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/admin/adminlte/plugins/datatables/responsive.bootstrap.min.css">  
  
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/admin/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <!-- daterange picker -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/admin/adminlte/plugins/daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/admin/adminlte/plugins/datepicker/datepicker3.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/admin/adminlte/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/admin/adminlte/plugins/colorpicker/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/admin/adminlte/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/admin/adminlte/plugins/select2/select2.min.css">

  
  
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/admin/adminlte/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/admin/adminlte/dist/css/skins/_all-skins.min.css">

<!-- sweetalert2 
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/sweetalert2/sweetalert2.css">
-->
  
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->  
  
  <!-- style scripts -->
  <?= $estilo ?>  

  
<!-- jQuery 2.2.3 -->
<script src="<?= base_url() ?>assets/admin/adminlte/plugins/jQuery/jquery-2.2.3.min.js"></script>
  
<!-- Bootstrap 3.3.6 -->
<script src="<?= base_url() ?>assets/admin/adminlte/bootstrap/js/bootstrap.min.js"></script>

<!-- DataTables 

<script src="<?= base_url() ?>assets/admin/adminlte/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>assets/admin/adminlte/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?= base_url() ?>assets/admin/adminlte/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="<?= base_url() ?>assets/admin/adminlte/plugins/datatables/responsive.bootstrap.min.js"></script>


-->


  
<script type="text/javascript" src="<?= base_url() ?>assets/plugins/datatables/datatables.min.js"></script>    
  
  
<!-- sweetalert2 -->
<script src="<?= base_url() ?>assets/plugins/sweetalert2/sweetalert2.all.js"></script>

<!-- Select2 -->
<script src="<?= base_url() ?>assets/admin/adminlte/plugins/select2/select2.full.min.js"></script>
<!-- InputMask -->
<script src="<?= base_url() ?>assets/admin/adminlte/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?= base_url() ?>assets/admin/adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?= base_url() ?>assets/admin/adminlte/plugins/input-mask/jquery.inputmask.extensions.js"></script>

<!-- date-range-picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?= base_url() ?>assets/admin/adminlte/plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="<?= base_url() ?>assets/admin/adminlte/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- bootstrap color picker -->
<script src="<?= base_url() ?>assets/admin/adminlte/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="<?= base_url() ?>assets/admin/adminlte/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="<?= base_url() ?>assets/admin/adminlte/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="<?= base_url() ?>assets/admin/adminlte/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="<?= base_url() ?>assets/admin/adminlte/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url() ?>assets/admin/adminlte/dist/js/app.min.js"></script>
<!-- jQuery Validation Plugin -->
<script src="<?= base_url() ?>assets/plugins/jquery-validation/jquery.validate.min.js"></script>
  
<!-- html2canvas -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"></script>  

<!-- Bootstrap WYSIHTML5 -->
<script src="<?= base_url() ?>assets/admin/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>

    
</head>
<body class="hold-transition skin-blue sidebar-mini <?=$sidebar_collapse?>">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo base_url(); ?>admin/panelcontrol" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b><?php echo $this->config->item('logo_mini'); ?></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b><?php echo $this->config->item('logo_lg'); ?></b></span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">

          <li>
            <a href="" data-toggle="control-sidebar" id="fotoCanvas"><i class="fa fa-camera"></i></a>
          </li> 
          

          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li> 

        </ul>
      </div>
      
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
         <img src="<?php echo base_url(); ?>imagenes/<?php echo $this->session->userdata($this->config->item('raiz') . 'be_usuario_tabla'); ?>/<?php echo $this->session->userdata($this->config->item('raiz') . 'be_usuario_foto');?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>
<?php
echo $this->session->userdata($this->config->item('raiz') . 'be_usuario_nombre') . " " . $this->session->userdata($this->config->item('raiz') . 'be_usuario_apellidos');
?>
          </p>
          <a href="#"><i class="fa fa-circle text-success"></i> <?=$this->lang->line('be_connected')?></a>
        </div>
      </div>
      <!-- search form 
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
       /.search form -->
        <?php echo $this->session->userdata($this->config->item('raiz') . 'be_plantilla_menu');?>
       
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" id="areaTrabajo">
    <!-- Content Header (Page header) -->
    <?=$tituloPagina?>
    <!-- Main content -->
    <section class="content">