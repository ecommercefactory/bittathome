<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">

    <div>
      <br>
      <div class="text-center">
        <img src="<?php echo base_url(); ?>imagenes/<?php echo $this->session->userdata($this->config->item('raiz') . 'be_usuario_tabla'); ?>/<?php echo $this->session->userdata($this->config->item('raiz') . 'be_usuario_foto');?>" class="img-circle" alt="User Image" style="width:50%;">
        <p>
          <?php echo $this->session->userdata($this->config->item('raiz') . 'be_usuario_nombre') . " " . $this->session->userdata($this->config->item('raiz') . 'be_usuario_apellidos'); ?><br>
          <small><?=$this->lang->line('be_member_since')?> <?php echo date_format(date_create($this->session->userdata($this->config->item('raiz') . 'be_usuario_fecha_ingreso')), 'M, Y'); ?></small>
        </p>

        <div>
          <br>
          <a href="<?php echo base_url(); ?>admin/mis_datos/perfil/index/edit/<?=$this->session->userdata($this->config->item('raiz') . 'be_usuario_id')?>" class="btn btn-primary btn-sm"><?=$this->lang->line('be_profile')?></a>
          <br>
        </div>
        
        <div>
          <br>
          <a href="<?php echo base_url(); ?>admin/mis_datos/cambiar_clave" class="btn btn-info btn-sm"><?=$this->lang->line('be_menu_change_password')?></a>
          <br>
        </div>        
        
        <div>
          <br>
          <a href="<?php echo base_url(); ?>backend/salir" class="btn btn-danger btn-sm"><?=$this->lang->line('be_log_out')?></a>
          <br>
        </div>
      
      </div>
    </div>
    
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>