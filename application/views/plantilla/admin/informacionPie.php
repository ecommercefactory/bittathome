<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?=$this->session->userdata($this->config->item('raiz') . 'be_plantilla_footer');?>
  <?=$this->session->userdata($this->config->item('raiz') . 'be_plantilla_sidebar');?>
</div>
<!-- ./wrapper -->

<!-- Variables -->
<?= $proyVar ?>

<?= $txtVar ?>

<!-- jQuery 2.2.3 -->
<script src="<?= base_url() ?>assets/admin/adminlte/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?= base_url() ?>assets/admin/adminlte/bootstrap/js/bootstrap.min.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="<?= base_url() ?>assets/admin/adminlte/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?= base_url() ?>assets/admin/adminlte/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url() ?>assets/admin/adminlte/dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?= base_url() ?>assets/admin/adminlte/dist/js/demo.js"></script>

<!-- page scripts -->


<?= $script ?>


</body>
</html>