<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<footer class="main-footer">
	<div class="pull-right hidden-xs">
		<b><?=$this->lang->line('be_version')?></b> 0.2.0
	</div>
	<strong><?=$this->lang->line('be_copyright')?> &copy; <?=date('Y')?> <a href="https://ecommercefactory.co/">E-Commerce Factory</a>.</strong> <?=$this->lang->line('be_all_rights_reserved')?>
	<br>
<?php
if (count($languages) > 1) {
?>
	<div class="row">
		<div class="col-sm-12 text-right" style="margin:10px;">
<?php
      foreach ($languages as $record) {
        $class = '';
        if ( $this->session->userdata($this->config->item('raiz') . 'be_lang_code') == $record['code'] ) {
            $class = 'selLangBEActive';
        }
?>
			<a href='<?=base_url()?>lang/changeLanguageBE/<?=$record['code']?>' class="selLangBE <?=$class?>"><?=$this->config->item('lang_'.$record['code'])?></a> | 
<?php
      }
?>
		</div>
	</div>
<?php
}
?>
</footer>