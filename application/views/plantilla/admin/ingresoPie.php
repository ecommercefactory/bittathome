<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!-- Variables -->
<?= $proyVar ?>

<?= $txtVar ?>

<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url(); ?>assets/admin/adminlte/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url(); ?>assets/admin/adminlte/bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck
<script src="<?php echo base_url(); ?>assets/admin/adminlte/plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
 -->
<!-- jQuery Validation Plugin -->
<script src="<?= base_url() ?>assets/plugins/jquery-validation/jquery.validate.min.js"></script>

<!-- page scripts -->


<?= $script ?>


</body>
</html>