<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
// http://www.tutorials.kode-blog.com/codeigniter-model

class MY_Model extends CI_Model {

    public $table = 'Undefined_Table';
    public $key = 'id';

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function registro($table = '', $id, $key = '') {
        if ($table == '') {
            $table = $this->table;
        }
        if ($key == '') {
            $key = $this->key;
        }
        return $this->db->get_where($table, array($key => $id))->row();
    }

    public function registros($table = '', $fields = '', $where = array(), $order_by = '', $group_by = '', $having = array(), $limit = '') {
        // https://codeigniter.com/user_guide/database/query_builder.html
        $data = array();
      
        if ($table != '') {
            $this->table = $table;
        }
      
        if ($fields != '') {
            $this->db->select($fields);
        }

        if (count($where)) {
            $this->db->where($where);
        }

        if ($order_by != '') {
            $this->db->order_by($order_by);
        }

        if ($group_by != '') {
            $this->db->group_by($group_by);
        }

        if (count($having)) {
            $this->db->having($having);
        }
      
        if ($limit != '') {
            $this->db->limit($limit);
        }
      
        $Q = $this->db->get($this->table);

        if ($Q->num_rows() > 0) {
            foreach ($Q->result_array() as $row) {
                $data[] = $row;
            }
        }
        $Q->free_result();

        return $data;
    }

    public function insertar($table = '', $data) {
        // $data['fecha_creado'] = $data['fecha_actualizado'] = date('Y-m-d H:i:s');
        // $data['creado_ip'] = $data['actualizado_ip'] = $this->input->ip_address();
        if ($table != '') {
            $this->table = $table;
        }
        $success = $this->db->insert($this->table, $data);
        if ($success) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }

    public function actualizar($table = '', $data, $id = '', $key = '', $where = array()) {
        // $data['fecha_actualizado'] = date('Y-m-d H:i:s');
        // $data['actualizado_ip'] = $this->input->ip_address();
        if ($table == '') {
            $table = $this->table;
        }
        if ($key == '') {
            $key = $this->key;
        }
        if (count($where)) {
            $this->db->where($where);
        } else {
            $this->db->where($key, $id);
        }
        return $this->db->update($table, $data);
    }

    public function eliminar($table = '', $id = '', $key = '', $where = array()) {
        if ($table == '') {
            $table = $this->table;
        }
        if ($key == '') {
            $key = $this->key;
        }
        if (count($where)) {
            $this->db->where($where);
        } else {
            $this->db->where($key, $id);
        }
        return $this->db->delete($table);
    }
  
/*
    public function eliminar($table = '', $id = '', $llave = "", $where = array()) {
        if ($table != '') {
            $this->tabla = $table;
        }
        if ( $llave == "" ) {
            $llave = $this->llave;            
        }
        if ( $id != '' ) {
            $this->db->where($this->llave, $id);
        } else {
          if (count($where)) {
              $this->db->where($where);
          }      
        }
      
        $this->db->delete($this->tabla);
        return true; //$this->db->affected_rows();
    }
*/
  
    public function records($table = '', $id, $key = '') {
        if ($table == '') {
            $table = $this->table;
        }
        if ($key == '') {
            $key = $this->key;
        }
        $this->db->where($key, $id);
        return  $this->db->count_all_results($table);
    }  

}

