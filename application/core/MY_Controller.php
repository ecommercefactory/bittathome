<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
      
        // Bloqueo de IPs
        $ip = $this->input->ip_address();
        $regIPBloqueo = $this->Modelo->registro($this->config->item('raiz_bd') . 'ips_bloqueadas', $ip, 'ip');
        if ($regIPBloqueo) {
          redirect ('/no_autorizado');
        }

        // Idioma Backend
        if (!$this->session->has_userdata($this->config->item('raiz') . 'be_lang_value')) {
          //$this->load->library('utilities');	
          $this->utilities->setLanguage();
        }

        $this->lang->load('backend', $this->session->userdata($this->config->item('raiz') . 'be_lang_value'));
        $this->config->set_item('language', $this->session->userdata($this->config->item('raiz') . 'be_lang_value') );
      
        // Detalle de la actividad del usuario
        $usuario_actividad_id = 0;
        if ( $this->session->has_userdata($this->config->item('raiz') . 'be_usuario_actividad_id') ) {
            $usuario_actividad_id = $this->session->userdata($this->config->item('raiz') . 'be_usuario_actividad_id');
        }
        $tabla_usuarios = '';
        if ( $this->session->has_userdata($this->config->item('raiz') . 'be_usuario_tabla') ) {
            $tabla_usuarios = $this->session->userdata($this->config->item('raiz') . 'be_usuario_tabla');
        }
        $usuario = 0;
        if ( $this->session->has_userdata($this->config->item('raiz') . 'be_usuario_id') ) {
            $usuario = $this->session->userdata($this->config->item('raiz') . 'be_usuario_id');
        }
        $usuario_fecha_ingreso = '';
        if ( $this->session->has_userdata($this->config->item('raiz') . 'be_usuario_fecha_ingreso') ) {
            $usuario_fecha_ingreso = $this->session->userdata($this->config->item('raiz') . 'be_usuario_fecha_ingreso');
        }

/* --------------------------------------- */
/* --------------------------------------- */
/* --------------------------------------- */

        if ($usuario_actividad_id > 0 and $tabla_usuarios != '' and $usuario > 0) {
            $datos1['usuario'] = $usuario;
            $datos1['actividad'] = $usuario_actividad_id;
            $datos1['fecha'] = $this->config->item('YmdHis');
          
            $ruta_id = 0;
            $ruta  = $_SERVER['REQUEST_URI'];

            $dato_valor_ruta = $this->Modelo->registros('rutas', 'id', array('nombre'=>$ruta));
            if (count($dato_valor_ruta)) {
                $ruta_id = $dato_valor_ruta[0]['id'];
            } else {
                $datosR['nombre'] = $ruta;
                $ruta_id = $this->Modelo->insertar('rutas', $datosR);
            }

            $datos1['ruta'] = $ruta_id;

            $this->Modelo->insertar($this->config->item('raiz_bd') . $tabla_usuarios . '_actividad_detalle', $datos1);

            $datos2['fecha_ultima_accion'] = $this->config->item('YmdHis');
          
            $date1=date_create($this->config->item('YmdHis'));
            $date2=date_create($usuario_fecha_ingreso);
            $diff=date_diff($date1,$date2);
            $h = $diff->format("%h");
            $i = $diff->format("%i");
            $s = $diff->format("%s");
            $permanencia = $h*60*60 + $i*60 + $s*1;        
            $datos2['permanencia'] = $permanencia;

            $this->Modelo->actualizar($this->config->item('raiz_bd') . $tabla_usuarios . '_actividad', $datos2, $usuario_actividad_id);
        }

/* --------------------------------------- */
/* --------------------------------------- */
/* --------------------------------------- */
      
    }
  
    public function ctrSegAdmin() {

        if ( !$this->session->has_userdata($this->config->item('raiz') . 'be_usuario_id') ) {
            $this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-warning"></i> '.$this->lang->line('be_warning').'</h4>'.$this->lang->line('be_you_must_authenticate_to_enter_the_application'));
            $this->session->set_flashdata('alertaTipo', 'warning'); // success, info, warning, danger
            redirect ('/backend');
        } else {
          $regUsuario = $this->Modelo->registro(
            $this->config->item('raiz_bd') . $this->session->userdata($this->config->item('raiz') . 'be_usuario_tabla'), 
            $this->session->userdata($this->config->item('raiz') . 'be_usuario_id') );
          if ($this->session->userdata($this->config->item('raiz') . 'be_usuario_session_id') != $regUsuario->session_id) {
            
            $this->load->helper('cookie');
            if (get_cookie($this->config->item('raiz') . 'ctrai1') and (get_cookie($this->config->item('raiz') . 'ctrai2'))) {
                delete_cookie($this->config->item('raiz') . 'ctrai1');
                delete_cookie($this->config->item('raiz') . 'ctrai2');   
            }    
            
            $this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-warning"></i> '.$this->lang->line('be_warning').'</h4>'.$this->lang->line('be_your_session_is_over__you_must_authenticate_again_to_enter_the_application'));
            $this->session->set_flashdata('alertaTipo', 'warning'); // success, info, warning, danger

            redirect ('/backend');
          }
        }

        if ($this->session->userdata($this->config->item('raiz') . 'be_usuario_fecha_cambio_clave') != '0000-00-00') {
          if ( !(base_url() . 'admin/mis_datos/cambiar_clave' == current_url() or base_url() . 'admin/mis_datos/cambiar_clave/proceso' == current_url()) ) {
            if ($this->session->userdata($this->config->item('raiz') . 'be_usuario_fecha_cambio_clave') <= $this->config->item('Ymd')) {
              if ( $this->session->flashdata('alertaMensaje') ) {
              } else {
                  $this->session->set_flashdata('alertaMensaje', '<h4><i class="icon fa fa-warning"></i> '.$this->lang->line('be_warning').'</h4>'.$this->lang->line('be_you_must_change_your_password'));
                  $this->session->set_flashdata('alertaTipo', 'info'); // success, info, warning, danger
              }
              redirect ('/admin/mis_datos/cambiar_clave');
            }
          }
        }
    }
  
    public function crudver($crudTable, $title, $breadCrumbs = '', $css = '', $script = '', $tabs = '') {
        $headData['titulo'] = $title;
        $headData['migadepan'] = $breadCrumbs;
        $headData['css'] = $css;
        $footData['script'] = $script;

        $crudTableData['css_files'] = $crudTable->css_files;
        $crudTableData['js_files'] = $crudTable->js_files;
        $crudTableData['tabs'] = $tabs;
        $crudTableData['output'] = $crudTable->output;

        $this->load->view('plantilla/admin/crudCabeza', $headData);
        $this->load->view('admin/crud',$crudTableData);
        $this->load->view('plantilla/admin/crudPie', $footData);
    }

}
