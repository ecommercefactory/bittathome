<?php
if ( $_GET['transactionState'] == 6 ) {
	$respuesta = '<section id="content">
	<br><br><br><br>
	<center>
		<h4>Su transacción ha sido rechazada</h4>
		<p>Si lo deseas, puedes intentar de nuevo.</p>
		<br><br>
		<a href="https://www.bittathome.com/carrito/" class="btn-todo">Intentar de Nuevo</a>
	</center>
	<br><br><br><br><br><br>
</section>';
} else {
	$respuesta = '<section id="content">
	<br><br><br><br>
	<center>
		<h4>Gracias por tu orden</h4>
		<p>Recibirás un correo electrónico de confirmación tan pronto como la transacción sea aprobada.</p>
		<br><br>
		<a href="https://www.bittathome.com/" class="btn-todo">Volver a la página principal</a>
	</center>
	<br><br><br><br><br><br>
</section>';
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
  
		<title>Bittat - Carrito de Compras</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="https://www.bittathome.com/assets/tienda/css/layout.css?v=0.2.1.0.0.3.6" type="text/css" rel="stylesheet">
		<link href="https://www.bittathome.com/assets/tienda/css/layout_fix.css?v=0.2.1.0.0.3.6" type="text/css" rel="stylesheet">
		<link href="https://www.bittathome.com/assets/tienda/css/header.css?v=0.2.1.0.0.3.6" type="text/css" rel="stylesheet">
		<link href="https://www.bittathome.com/assets/tienda/css/footer.css?v=0.2.1.0.0.3.6" type="text/css" rel="stylesheet">
		<link href="https://www.bittathome.com/assets/tienda/css/mobile.css?v=0.2.1.0.0.3.6" type="text/css" rel="stylesheet" media="only screen and (max-width: 1024px)">

		<link rel="apple-touch-icon" sizes="57x57" href="https://www.bittathome.com/assets/tienda/favicon/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="https://www.bittathome.com/assets/tienda/favicon/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="https://www.bittathome.com/assets/tienda/favicon/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="https://www.bittathome.com/assets/tienda/favicon/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="https://www.bittathome.com/assets/tienda/favicon/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="https://www.bittathome.com/assets/tienda/favicon/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="https://www.bittathome.com/assets/tienda/favicon/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="https://www.bittathome.com/assets/tienda/favicon/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="https://www.bittathome.com/assets/tienda/favicon/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="https://www.bittathome.com/assets/tienda/favicon/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="https://www.bittathome.com/assets/tienda/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="https://www.bittathome.com/assets/tienda/favicon/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="https://www.bittathome.com/assets/tienda/favicon/favicon-16x16.png">

		<!-- css fontawesome -->
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css?v=0.2.1.0.0.3.6" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
		<!-- css fontawesome fin -->
</head>
<body>

	<header class="bg-header" id="header">
		<div class="cont-logo">
			<div id="logo">
				<a href="https://www.bittathome.com/">
					<img src="https://www.bittathome.com/assets/tienda/images/home-bittat-group.png" alt="bittat logo">
				</a>
			</div>

			<div class="whatsapp-header">
				<a href="https://api.whatsapp.com/send?phone=573229440083" target="_blank">
					<i class="fab fa-whatsapp"></i>
					<span>322 944 0083</span>
				</a>
			</div>
		</div>

	</header>
  
<?php
echo $respuesta;
?>
  
	<!-- ir al inicio -->
	<a href="#header" id="top">
		<span class="icon icon40 icon-top"></span>
	</a>
	<!-- ir al inicio fin -->

	<footer class="footer-checkout">
		<div class="datos-contacto">
			<ul>
				<li><h6>DATOS DE CONTACTO</h6></li>
				<li><i class="fas fa-phone"></i>(+57 1) 2851914</li>
				<li><i class="fas fa-mobile-alt"></i>(+57) 322 944 0083</li>
				<li><i class="fas fa-envelope-open"></i>servicioalcliente@solucionaticos.com</li>
				<li><i class="fas fa-home"></i>Cra 15 No 78-33 Local 2270 </li>
				<li><i class="fas fa-globe-asia"></i>Bogotá - Colombia</li>
			</ul>
				<ul class="logo-footer">
					<li>
					 	<img src="https://www.bittathome.com/assets/tienda/images/home-bittat-group.png" alt="bittat logo">
					</li>
				</ul>
			</ul>
		</div>

		<div class="copy-footer">
			<p>©2018 Todos los derechos reservados a Domotica</p>
		</div>
	</footer>

</body>
</html>