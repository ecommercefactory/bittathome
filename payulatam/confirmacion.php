<?php
function echo_vars_pol (&$item, $key) {
	global $var_list_pol;
	$var_list_pol .= $key . " => " . $item . "\n";
}

$var_list_pol = "GET:\n";

array_walk_recursive($_GET, 'echo_vars_pol');

$var_list_pol .= "POST:\n";

array_walk_recursive($_POST, 'echo_vars_pol');

/* Configuraciones
    Credenciales Produccion: 
        merchantId:     571258
        accountId:      573945
        apiKey:         5MwpVqeUW5gPRuu1Md32SzH654
        API Login:      33EV2z7WMII8fqn
    Credenciales Pruebas: 
        merchantId:     508029
        accountId:      512321
        apiKey:         4Vj8eK4rloUd272L48hsrarnUA
        API Login:      pRRXKOl8ikMmt9u
    URL WEBCheckOut:
        Pruebas:        https://sandbox.checkout.payulatam.com/ppp-web-gateway-payu/
        Produccion:     https://checkout.payulatam.com/ppp-web-gateway-payu/
    URL API:
        Pruebas:
                        Consultas:      https://sandbox.api.payulatam.com/reports-api/4.0/service.cgi
                        Pagos:          https://sandbox.api.payulatam.com/payments-api/4.0/service.cgi
        Produccion:
                        Consultas:      https://api.payulatam.com/reports-api/4.0/service.cgi
                        Pagos:          https://api.payulatam.com/payments-api/4.0/service.cgi 
*/


$mat_dias['Sunday'] = 1;
$mat_dias['Monday'] = 2;
$mat_dias['Tuesday'] = 3;
$mat_dias['Wednesday'] = 4;
$mat_dias['Thursday'] = 5;
$mat_dias['Friday'] = 6;
$mat_dias['Saturday'] = 7;
$hoy = date('Y-m-d');

//mail ('developers@totalcode.com', 'PagosOnline Confirmacion - daFlores', $var_list_pol, 'From: <mailer@'.$_SERVER['SERVER_NAME'].'>', '-f mailer@'.$_SERVER['SERVER_NAME']);

//Para hacer pruebas por GET (DEBUG)
/*
$_POST['reference_sale'] = $_GET['reference_sale'];
$_POST['payment_request_state'] = $_GET['payment_request_state'];
$_POST['state_pol'] = $_GET['state_pol'];
$_POST['value'] = $_GET['value'];
$_POST['reference_pol'] = $_GET['reference_pol'];
$_POST['sign'] = $_GET['sign'];
$_POST['merchant_id'] = $_GET['merchant_id'];
$_POST['payment_method_id'] = $_GET['payment_method_id'];
$_POST['franchise'] = $_GET['franchise'];
$_POST['ip'] = $_GET['ip'];
$_POST['currency'] = $_GET['currency'];
*/
/*
$pagosonline_url = 'https://gatewaylap.pagosonline.net/ppp-web-gateway/';
$conf_pagosonline_tdc_user = '500230';
$conf_pagosonline_tdc_pass = '30g28raha8tp7stnpb912vpcqn';
*/
          
// $_POST['merchant_id'] = "508029"; ---> Debe venir con este valor merchantId de pruebas       
$conf_pagosonline_tdc_pass = "4Vj8eK4rloUd272L48hsrarnUA"; // apiKey de pruebas         
         
$pagosonline_pass = $conf_pagosonline_tdc_pass;


$signature = $pagosonline_pass.'~'.$_POST['merchant_id'].'~'.$_POST['reference_sale'].'~'.substr($_POST['value'],0,-1).'~'.$_POST['currency'].'~'.$_POST['state_pol'];
$enc_signature = md5($signature);

/*
$_POST['sign'] = strtoupper($enc_signature);
print_r($_POST);
echo '<br>';
*/

if ( isset($_POST['reference_sale']) && strtoupper($enc_signature) == strtoupper($_POST['sign'])) {

	$pre_order_num = preg_replace("/[^0-9]/", "", $_POST['reference_sale']);
	if ( $_POST['payment_request_state'] == 'A' && strlen($pre_order_num) > 0 ) {

		$sql = 'SELECT * FROM ordenes WHERE pre_orden="'.$pre_order_num.'" AND fecha_orden = "0000-00-00 00:00:00"';
		$pre_abandoned = mysqli_query($conn, $sql);
		if ( mysqli_num_rows($pre_abandoned) ) {
	
			$abandoned = mysqli_fetch_assoc($pre_abandoned);
	
			$_SESSION['pre_order_num'] = $abandoned['pre_orden'];
			$_SESSION['cust_code'] = $abandoned['cliente'];
			$_SESSION['ip_country'] = $abandoned['ip_pais'];

      $_SESSION['currency'] = $_POST['currency']; // COP
      $_SESSION['local_currency'] = '_local';
      $_SESSION['decimal_places'] = 0;

			$_SESSION['shipping_address_num'] = $abandoned['shipping_address_num']; // Hay que crear una tabla de addresses
			
			$pre_citi_code = mysqli_query($conn, "SELECT city_code FROM addresses WHERE address_num='".$_SESSION['shipping_address_num']."'");
			$city_code = fx_mysqli_result($pre_citi_code,0);
	
			$abandoned_products = explode(',', $abandoned['product_list']); // esto es para sacar la lista de productos pero yo tengo la tabla ordenes productos
			if ( count($abandoned_products) ) {
				unset($_SESSION['product_num']);
				unset($_SESSION['product_quantity']);
				unset($_SESSION['price']);
				$loc_products = 0;
				do {
					$pre_product_prices = mysqli_query($conn, 'SELECT price FROM product_prices WHERE product_num="'.current($abandoned_products).'" AND site="'.$abandoned['site'].'" AND active="1"');
					if ( mysqli_num_rows($pre_product_prices) ) {
						$_SESSION['product_num'][] = current($abandoned_products);
						$_SESSION['product_quantity'][] = 1;
						$_SESSION['price'][] = fx_mysqli_result($pre_product_prices,0,0);
						$subtotal = $subtotal + fx_mysqli_result($pre_product_prices,0,0);
						$id_product[$loc_products] = current($abandoned_products);
						$price_product[$loc_products] = fx_mysqli_result($pre_product_prices,0,0);
						$qty_products[$loc_products] = 1;
						$loc_products++;
						if ($bgcolorsel == 1) {
							$bgcolor = "#eeeeee";
							$bgcolorsel = 0;
						} else {
							$bgcolor = "#ffffff";
							$bgcolorsel = 1;
						}
						$pre_products = mysqli_query($conn, "SELECT p.sku, pl.name, p.category, p.option_of FROM products p, products_lang pl WHERE pl.lang ='".$_SESSION['lang']."' AND p.product_num=pl.product_num AND p.product_num='".current($abandoned_products)."'");
						$products = mysqli_fetch_assoc($pre_products);
						$itemprice = fx_mysqli_result($pre_product_prices,0,0);
						$listitems .= "\t\t\t\t\t\t\t<tr bgcolor=\"".$bgcolor."\"><td>".$products['sku']."</td><td>".$products['name']."</td><td align=center>".$_SESSION['product_quantity'][$key]."</td><td align=right>\$".number_format($itemprice,$_SESSION['decimal_places'])."</td></tr>\n";
						$plainlist .= "1 ".$products['name']." (".$products['sku']."): \$".number_format($itemprice,$_SESSION['decimal_places'])."\n";
						$pre_prod_vendors = mysqli_query($conn, 'SELECT DISTINCT cv.vendor_code FROM prod_vendors pv, cities_vendors cv WHERE cv.vendor_code=pv.vendor_code AND cv.active="1" AND pv.active="1" AND cv.city_code="'.$city_code.'" AND pv.product_num="'.current($abandoned_products).'"');
						if ( mysqli_num_rows($pre_prod_vendors) ) {
							while ( $prod_vendors = mysqli_fetch_assoc($pre_prod_vendors) ) {
								$valid_prod_vendors .= $prod_vendors['vendor_code'].',';
							}
							$vendor_filter .= ' AND v.vendor_code IN ('.substr($valid_prod_vendors,0,-1).')';
						}
					}
				} while ( next($abandoned_products) );
			}
			
			$initial_date = mktime(0,0,0,substr($abandoned['deliver_date'],5,2),substr($abandoned['deliver_date'],8,2),substr($abandoned['deliver_date'],0,4));
			
			// Determina si la ciudad de entrega tiene lead-time
			$pre_cities = mysqli_query($conn, "SELECT ship_days, country_code FROM cities WHERE city_code='".$city_code."'");
			if ( mysqli_num_rows($pre_cities) ) {
				$cities = mysqli_fetch_assoc($pre_cities);
				$delay_city_ship = $cities['ship_days'];
			}
		
			// Determina si la categoria de algun producto tiene lead-time (por ciudad)
			$delay_category_ship = 0;
			reset($_SESSION['product_num']);
			do {
				$pre_delay_category_city_ship = mysqli_query($conn, "SELECT cc.ship_days FROM products p, category_cities cc WHERE p.category=cc.category_num AND cc.city_code='".$city_code."' AND p.product_num='".current($_SESSION['product_num'])."'");
				if ( mysqli_num_rows($pre_delay_category_city_ship) ) {
					if ( fx_mysqli_result($pre_delay_category_city_ship,0) > $delay_category_ship ) {
						$delay_category_ship = fx_mysqli_result($pre_delay_category_city_ship,0);
					}
				}
			} while ( next($_SESSION['product_num']) );
			
			// Determina la diferencia en horas entre hora local y hora en el pais de entrega.
			$pre_country = mysqli_query($conn, "SELECT c.gmt_country FROM countries c, sites s WHERE s.country_code=c.country_code AND s.site_num='".$cities['country_code']."'");
			$gmt_country_code = fx_mysqli_result($pre_country,0,0);
	//		$time_ship_country = $gmt_country_code + 5; // Se suma 5 por ser la diferencia entre GMT y la hora del servidor
			$time_ship_country = $gmt_country_code - (date('Z')/3600); // Se calcula la diferencia con GTM porque hora del servidor cambia con daylight saving
		
			// Trae el cut-off time para el pais de entrega
			$cut_off_time = 12; // Cut-off time por defecto
			$pre_cut_off_time = mysqli_query($conn, "SELECT cut_off_time_days FROM sites WHERE site_num='".$cities['country_code']."'");
			if ( mysqli_num_rows($pre_cut_off_time) ) {
				$cut_off_time_sites = fx_mysqli_result($pre_cut_off_time,0);

				$mat_cut_off_time_days_sites = explode(';', $cut_off_time_sites);
				$cut_off_time = $mat_cut_off_time_days_sites[$mat_dias[date('l', strtotime($hoy))]];
			}
		
			// Si ya pasó el cut-off time, sumar un dia
			if ( ((date("H") + $time_ship_country) >= $cut_off_time) ) {
				$delay_cutoff = 1;
			}
			
			$min_date = mktime(0,0,0,date("m"),date("d"),date("Y")) + (($delay_cutoff+$delay_city_ship+$delay_category_ship)*86400); 
			if ( $initial_date < $min_date ) {
				$initial_date = $min_date;
			}
			$pre_country_code = mysqli_query($conn, 'SELECT country_code FROM sites WHERE site_num="'.$_SESSION['site'].'"');
			$country_code = fx_mysqli_result($pre_country_code,0,0);
			$initial_date = $initial_date - 86400; // Por lo que se va a sumar durante el WHILE
			$valid_date = 0;
			while ( $valid_date == 0 ) {
				$initial_date = $initial_date + 86400; // Suma un dia.
				// Determina si es una fecha de entrega no permitida para el pais
				$pre_delivery_date_rules = mysqli_query($conn, "SELECT unique_id FROM delivery_date_rules WHERE country_code='".$country_code."' AND date='".date("Y", $initial_date)."-".date("m", $initial_date)."-".date("d", $initial_date)."' AND active='1' AND allow_delivery='0'");
				if ( mysqli_num_rows($pre_delivery_date_rules) > 0 ) {
					//			echo "Entrega no permitida el ".date("Y-m-d",$initial_date)." segun tabla delivery_date_rules.<br>";
					continue;
				}
				// Determina si es Domingo (generalmente no se entrega). Si es Domingo, determina si está en la lista de fechas permitidas para el pais
				if ( date("w",$initial_date) == 0 ) {
					$pre_delivery_date_rules = mysqli_query($conn, "SELECT unique_id FROM delivery_date_rules WHERE country_code='".$country_code."' AND date='".date("Y", $initial_date)."-".date("m", $initial_date)."-".date("d", $initial_date)."' AND active='1' AND allow_delivery='1'");
					if ( mysqli_num_rows($pre_delivery_date_rules) == 0 ) {
						//				echo "Entrega no permitida el ".date("Y-m-d",$initial_date)." por ser domingo.<br>";
						continue;
					}
				}
				$valid_date = 1;
			}
			
			$_SESSION['delivery_month'] = intval(date("m", $initial_date));
			$_SESSION['delivery_day'] = intval(date("d", $initial_date));
			$_SESSION['delivery_year'] = intval(date("Y", $initial_date));

//////////////

			if ( $_POST['payment_method_id'] == 2 ) {
				$cctype = strtolower($_POST['franchise']);
			}
			if ( $_POST['payment_method_id'] == 4 )  {
				$cctype = 'pse';
			}
			if ( $_POST['payment_method_id'] == 7 )  {
				$cctype = 'baloto';
			}

			if ( strlen($abandoned['discount_code']) ) {
				$_SESSION['discount_code'] = $abandoned['discount_code'];
			}

			if ( $abandoned['discount'] > 0 ) {
				$_POST['discount'] = $abandoned['discount'];
				$subtotal = $subtotal - $abandoned['discount'];
			}

			$cust_code = $_SESSION['cust_code'];
			$pre_customers = mysqli_query($conn, 'SELECT * FROM customers WHERE cust_code='.$cust_code);
			$customers = mysqli_fetch_assoc($pre_customers);
			$pre_addresses = mysqli_query($conn, 'SELECT * FROM addresses WHERE address_num='.$_SESSION['shipping_address_num']);
			$addresses = mysqli_fetch_assoc ($pre_addresses);
			$pre_cities = mysqli_query($conn, "SELECT area_code, shipping AS shipping FROM cities WHERE city_code=".$addresses["city_code"]);
			$cities = mysqli_fetch_assoc($pre_cities);
			$ship = $cities['shipping'];
			$total = $subtotal + $tax + $ship;
			$total_usd = 0; //convert($_POST['currency'], $total, 'USD');

			include ("language_".$_SESSION['lang'].".phtml");

			$drop_ship = 0;
			$denominator = 100;
			
			$deliverydate = $_SESSION['delivery_year'].'-'.str_pad($_SESSION['delivery_month'], 2, '0', STR_PAD_LEFT).'-'.str_pad($_SESSION['delivery_day'], 2, '0', STR_PAD_LEFT);
			$querydeliverydate = ", deliver_date='".$deliverydate."'";
			$emaildeliverydate = $txt_delivery_date.": ".$deliverydate."<br>";
			$delivertimestamp = mktime (0,0,0,$_SESSION['delivery_month'],$_SESSION['delivery_day'],$_SESSION['delivery_year']);
			
			if ( date("w",$delivertimestamp) == 0 ) { 
				$pre_delivery_date_rules = mysqli_query($conn, 'SELECT unique_id FROM delivery_date_rules WHERE country_code="'.$addresses['country'].'" AND date="'.$deliverydate.'" AND active=1 AND allow_delivery=1'); // Si el Domingo está activo para todo el país, actuar como si no fuera domingo (cualquier proveedor puede entregar)
				if ( mysqli_num_rows($pre_delivery_date_rules) == 0 ) {
					$sunday_filter = ' AND v.sunday = "1"';
				}
			}
				
			// Este query se hace porque puede que la suma de porcentajes no de 100 cuando se filtra por vendors que entreguen en Domingo

			$sql_denominator = 'SELECT
				SUM(cv.percent) AS denominator
			FROM
				vendors v, cities_vendors cv
			WHERE
				v.vendor_code = cv.vendor_code
				AND v.active = "1"
				AND cv.city_code = "'.$addresses['city_code'].'" 
				AND cv.active = "1"'.$sunday_filter.$vendor_filter;
		
			$sql_denominator_proc = mysqli_query($conn, $sql_denominator);
		
			$denominator = fx_mysqli_result ($sql_denominator_proc,0,0);
			
			if ( $denominator > 0 ) {
			
				$sql_vendors = 'SELECT
					v.vendor_code,
					cv.percent
				FROM
					vendors v, cities_vendors cv
				WHERE
					v.vendor_code = cv.vendor_code
					AND v.active = "1"
					AND cv.city_code = "'.$addresses['city_code'].'" 
					AND cv.active = "1"'.$sunday_filter.$vendor_filter.'
				ORDER BY cv.cities_vendors_code ASC';
				
				$sql_vendors_proc = mysqli_query($conn, $sql_vendors);
				
				$sum = 0;
				$rand = mt_rand(1,$denominator);//obtine numero aleatorio 
					
				while ( $r_sql_vendors_porc = mysqli_fetch_assoc($sql_vendors_proc) ) {
					$sum = $sum + $r_sql_vendors_porc['percent']; // Asigno valor porcentaje y los va sumando
					if ( $rand <= $sum && $drop_ship == 0 ) { // Valida para realizar la asignacion de vendedor
						$drop_ship = $r_sql_vendors_porc['vendor_code'];
					}
				}
			}

			if ( $drop_ship > 0 ) {
				$pre_currency = mysqli_query($conn, "SELECT currency FROM vendors WHERE vendor_code='".$drop_ship."'");
				if ( mysqli_num_rows($pre_currency) ) {
					$currency = fx_mysqli_result($pre_currency,0,0);
				}
			}

			reset ($_SESSION['product_num']);
			$id_product[0]=current($_SESSION['product_num']);
			//NEW CHANGES 10-09-2008
			$pre_qty_combos=mysqli_query($conn, "SELECT COUNT(product_num)AS count FROM combos WHERE combo_num=".$id_product[0]." AND active=1");
			$qty_combos=mysqli_fetch_assoc($pre_qty_combos);
			$qty_combo=$qty_combos['count'];
			$pre_combos=mysqli_query($conn, "SELECT * FROM combos WHERE combo_num=".$id_product[0]." AND active=1 ORDER BY inter_rec");
			$sh_year=$_SESSION['delivery_year'];
			$sh_month=$_SESSION['delivery_month'];
			$sh_day=$_SESSION['delivery_day'];
			$medida_rec=$prod_combo['medida_rec'];
			$nueva_fecha_combo=$_SESSION['delivery_day']."-".$_SESSION['delivery_month']."-".$_SESSION['delivery_year'];
			$code_country=$addresses["country"];
			$cx=1;
			while($prod_combo= mysqli_fetch_assoc ($pre_combos)){
				list($dia,$mes,$año)=explode("-",$nueva_fecha_combo);
				if($medida_rec=='M' && $cx>1){
					$dia=$_SESSION['delivery_day'];
					$mes=$mes+$prod_combo['inter_rec'];
					if($mes==1){
						$año=$año+1;
					}
				}else{
					if($cx>1){
						$dia=$_SESSION['delivery_day']+$prod_combo['inter_rec'];
					}else{
						$dia=$_SESSION['delivery_day'];
					}
				}
				$nueva_fecha_combo=validate_fechas($dia, $mes, $año, $code_country);
				list($dias,$mess,$años)=explode("-",$nueva_fecha_combo);
				$fechas_combos[$cx]=$años."-".$mess."-".$dias;
				$cx++;
			}
			$parcial_order=$price_product[0];
			for($m=1;$m<$loc_products;$m++){
				$sum_total_order=number_format(($sum_total_order+$price_product[$m]),2);
			}
			$ref_combo_num=time();
			$verify_number=1;
			$insert_products=1;
			$ax=0;
			$pre_combo=mysqli_query($conn, "SELECT product_num FROM combos WHERE combo_num=".$id_product[0]." AND active=1 ORDER BY inter_rec");
			if(mysqli_num_rows($pre_combo)){
				while($combo_insert=mysqli_fetch_assoc($pre_combo)){
					$pre_products_insert=mysqli_query($conn, "SELECT * FROM products WHERE product_num=".$combo_insert['product_num']);
					$sub_total_first=number_format(($price_product[0]/$qty_combo),2);
					$product_total=$sub_total_first;
					while($products_insert=mysqli_fetch_assoc($pre_products_insert)){
						$ax++;
						/*				$price_one="price_".$_SESSION['site'];
						 $price_product=$products_insert[$price_one];
						 ESTA OPCION DA EL PRECIO REAL DEL PRODUCTO
						 */
						if($verify_number==1){
							$verif_total=$sub_total_first*$qty_combo;
							$diferencia=($verif_total+$sum_total_order)-$subtotal;
							$sub_total_first=$sub_total_first+$sum_total_order-$diferencia;
							$verify_number=2;
						}
						$discount_combo=$_POST['discount']/$qty_combo;
						$sub_total_combo=$sub_total_first-$discount_combo;
						$shipping = $shipping + $ship;
						$total_combo = $sub_total_combo + $tax + $ship;
						$total_combo_usd = 0; //convert($_POST['currency'], $total_combo, 'USD');
						mysqli_query($conn, "INSERT INTO orders SET ref_num='".$ref_combo_num."', cust_code='".$cust_code."', prefix='".$customers["prefix"]."',last_name='".addslashes($customers["last_name"])."', first_name='".addslashes($customers["first_name"])."', email='".$customers["email"]."', country='".$customers["country"]."', company='".addslashes($customers["company"])."', position='".addslashes($customers["position"])."', address1='".addslashes($customers["address1"])."', address2='".addslashes($customers["address2"])."', city='".addslashes($customers["city"])."', state='".addslashes($customers["state"])."', zip='".addslashes($customers["zip"])."', phone='".$customers["phone"]."', sh_prefix='".$addresses["prefix"]."', sh_last_name='".addslashes($addresses["last_name"])."', sh_first_name='".addslashes($addresses["first_name"])."', sh_email='".$addresses["email"]."', sh_country='".$addresses["country"]."', sh_company='".addslashes($addresses["company"])."', sh_position='".addslashes($addresses["position"])."', sh_address1='".addslashes($addresses["address_line1"])."', sh_address2='".addslashes($addresses["address_line2"])."', sh_address3='".addslashes($addresses["address_line3"])."', sh_address4='".addslashes($addresses["address_line4"])."', sh_city_code='".addslashes($addresses["city_code"])."', sh_city='".addslashes($addresses["city"])."', sh_state='".addslashes($addresses["state"])."', sh_zip='".addslashes($addresses["zip"])."', sh_phone='".$addresses["phone"]."', sh_ext='".$addresses["ext"]."', sh_phone2='".$addresses["phone2"]."', sh_ext2='".$addresses["ext2"]."',  destination_type='".$addresses['destination_type']."', gift_text='".addslashes($_SESSION['gift_text'])."',signature='".addslashes($_SESSION['signature'])."', payment_method='8', cctype='".$cctype."', transaction_id='".$_POST['reference_pol']."', ip='".$_POST['ip']."', status='0', status_since=NOW(), discount_code='".$_POST['discount_code']."', discount='".$discount_combo."', subtotal='".$sub_total_combo."', tax='".$tax."', shipping='".$ship."', order_currency='".$_SESSION['currency']."', total_usd='".$total_combo_usd."', ship_method='".$_SESSION['shipopt']."', ups_error='".$_POST['ups_error']."', drop_ship='".$drop_ship."', user_num='".$_SESSION['user']."', referrer_seq='".$_SESSION['referrer_seq']."', currency='".$currency."', deliver_date='".$fechas_combos[$ax]."'");
						$order_num = mysqli_insert_id($conn);
						
						if ( mysqli_affected_rows($conn) < 1 ) {
							mail ("developers@totalcode-software.com", "DAF - Error Inserting Order", "INSERT INTO orders SET ref_num='".$ref_combo_num."', cust_code='".$cust_code."', prefix='".$customers["prefix"]."',last_name='".addslashes($customers["last_name"])."', first_name='".addslashes($customers["first_name"])."', email='".$customers["email"]."', country='".$customers["country"]."', company='".addslashes($customers["company"])."', position='".addslashes($customers["position"])."', address1='".addslashes($customers["address1"])."', address2='".addslashes($customers["address2"])."', city='".addslashes($customers["city"])."', state='".addslashes($customers["state"])."', zip='".addslashes($customers["zip"])."', phone='".$customers["phone"]."', sh_prefix='".$addresses["prefix"]."', sh_last_name='".addslashes($addresses["last_name"])."', sh_first_name='".addslashes($addresses["first_name"])."', sh_email='".$addresses["email"]."', sh_country='".$addresses["country"]."', sh_company='".addslashes($addresses["company"])."', sh_position='".addslashes($addresses["position"])."', sh_address1='".addslashes($addresses["address_line1"])."', sh_address2='".addslashes($addresses["address_line2"])."', sh_address3='".addslashes($addresses["address_line3"])."', sh_address4='".addslashes($addresses["address_line4"])."', sh_city_code='".addslashes($addresses["city_code"])."', sh_city='".addslashes($addresses["city"])."', sh_state='".addslashes($addresses["state"])."', sh_zip='".addslashes($addresses["zip"])."', sh_phone='".$addresses["phone"]."', sh_ext='".$addresses["ext"]."', sh_phone2='".$addresses["phone2"]."', sh_ext2='".$addresses["ext2"]."',  destination_type='".$addresses['destination_type']."', gift_text='".addslashes($_SESSION['gift_text'])."',signature='".addslashes($_SESSION['signature'])."', payment_method='8', cctype='".$cctype."', transaction_id='".$_POST['reference_pol']."', ip='".$_POST['ip']."',  status='0', status_since=NOW(), discount_code='".$_POST['discount_code']."', discount='".$discount_combo."', subtotal='".$sub_total_combo."', tax='".$tax."', shipping='".$ship."', order_currency='".$_SESSION['currency']."', total_usd='".$total_combo_usd."', ship_method='".$_SESSION['shipopt']."',ups_error='".$_POST['ups_error']."', drop_ship='".$drop_ship."', user_num='".$_SESSION['user']."', referrer_seq='".$_SESSION['referrer_seq']."', currency='".$currency."', deliver_date='".$fechas_combos[$ax]."'", "From: <".$conf_admin_mail.">", "-f ".$conf_admin_mail);
						} else {
							// - Orders Status Ctrl - Inicio ----------------------
							$status_user_by = 0; if ( isset($_SESSION['user']) ) $status_user_by = $_SESSION['user'];
							$device_type = ''; if ( isset($_SESSION['device_type']) ) $device_type = $_SESSION['device_type'];

$drop_ship_upd_order_num = $order_num;
$drop_ship_upd = 0;
$sql_drop_ship_upd = 'SELECT drop_ship FROM orders WHERE order_num = "' . $drop_ship_upd_order_num . '"';
$pre_drop_ship_upd = mysqli_query($conn, $sql_drop_ship_upd);
if (mysqli_num_rows($pre_drop_ship_upd)) {
	$reg_drop_ship_upd = mysqli_fetch_assoc($pre_drop_ship_upd);
	$drop_ship_upd = $reg_drop_ship_upd['drop_ship'];
}

							$sql_orders_history = "
								INSERT INTO orders_history SET 
									order_num = $order_num,
									drop_ship = $drop_ship_upd,
									device_type = '$device_type',
									status = '0',
									status_since = NOW(),
									status_user_by = $status_user_by,
									status_cust_code_by = $cust_code";
							mysqli_query($conn, $sql_orders_history);
							// - Orders Status Ctrl - Fin -------------------------	
						}
						mysqli_query($conn, "INSERT INTO orders_products SET order_num='".$order_num."', product_num=".$products_insert['product_num'].", product_price='".$product_total."', product_quantity='1'");
						if($insert_products==1){
							for($x=1;$x<$loc_products;$x++){
								mysqli_query($conn, "INSERT INTO orders_products SET order_num='".$order_num."', product_num='".$id_product[$x]."', product_price='".$price_product[$x]."', product_quantity='1'");
							}
							$insert_products=2;
						}
					}
					// if ( $drop_ship > 0 ) {
						// recalculate_order_cost($order_num,$drop_ship);
						// recalculate_order_charge($order_num,$drop_ship,$currency);
					// }
				}
			}else{
				$shipping = $ship;
				mysqli_query($conn, "INSERT INTO orders SET ref_num='".time()."', cust_code='".$cust_code."', prefix='".$customers["prefix"]."', last_name='".addslashes($customers["last_name"])."', first_name='".addslashes($customers["first_name"])."', email='".$customers["email"]."', country='".$customers["country"]."', company='".addslashes($customers["company"])."', position='".addslashes($customers["position"])."', address1='".addslashes($customers["address1"])."', address2='".addslashes($customers["address2"])."', city='".addslashes($customers["city"])."', state='".addslashes($customers["state"])."', zip='".addslashes($customers["zip"])."', phone='".$customers["phone"]."', sh_prefix='".$addresses["prefix"]."', sh_last_name='".addslashes($addresses["last_name"])."', sh_first_name='".addslashes($addresses["first_name"])."', sh_email='".$addresses["email"]."', sh_country='".$addresses["country"]."', sh_company='".addslashes($addresses["company"])."', sh_position='".addslashes($addresses["position"])."', sh_address1='".addslashes($addresses["address_line1"])."', sh_address2='".addslashes($addresses["address_line2"])."', sh_address3='".addslashes($addresses["address_line3"])."', sh_address4='".addslashes($addresses["address_line4"])."', sh_city_code='".addslashes($addresses["city_code"])."', sh_city='".addslashes($addresses["city"])."', sh_state='".addslashes($addresses["state"])."', sh_zip='".addslashes($addresses["zip"])."', sh_phone='".$addresses["phone"]."', sh_ext='".$addresses["ext"]."', sh_phone2='".$addresses["phone2"]."', sh_ext2='".$addresses["ext2"]."', destination_type='".$addresses['destination_type']."', gift_text='".addslashes($_SESSION['gift_text'])."', signature='".addslashes($_SESSION['signature'])."', payment_method='8', cctype='".$cctype."', transaction_id='".$_POST['reference_pol']."', ip='".$_POST['ip']."', status='0', status_since=NOW(), discount_code='".$_POST['discount_code']."', discount='".$_POST['discount']."', subtotal='".$subtotal."', tax='".$tax."', shipping='".$shipping."', order_currency='".$_SESSION['currency']."', total_usd='".$total_usd."', ship_method='".$_SESSION['shipopt']."', ups_error='".$_POST['ups_error']."', drop_ship='".$drop_ship."', user_num='".$_SESSION['user']."', referrer_seq='".$_SESSION['referrer_seq']."', currency='".$currency."'".$querydeliverydate);
				$order_num = mysqli_insert_id($conn);

				if ( mysqli_affected_rows($conn) < 1 ) {
					mail ("developers@totalcode-software.com", "DAF - Error Inserting Order", "INSERT INTO orders SET ref_num='".time()."', cust_code='".$cust_code."', prefix='".$customers["prefix"]."', last_name='".addslashes($customers["last_name"])."', first_name='".addslashes($customers["first_name"])."', email='".$customers["email"]."', country='".$customers["country"]."', company='".addslashes($customers["company"])."', position='".addslashes($customers["position"])."', address1='".addslashes($customers["address1"])."', address2='".addslashes($customers["address2"])."', city='".addslashes($customers["city"])."', state='".addslashes($customers["state"])."', zip='".addslashes($customers["zip"])."', phone='".$customers["phone"]."', sh_prefix='".$addresses["prefix"]."', sh_last_name='".addslashes($addresses["last_name"])."', sh_first_name='".addslashes($addresses["first_name"])."', sh_email='".$addresses["email"]."', sh_country='".$addresses["country"]."', sh_company='".addslashes($addresses["company"])."', sh_position='".addslashes($addresses["position"])."', sh_address1='".addslashes($addresses["address_line1"])."', sh_address2='".addslashes($addresses["address_line2"])."', sh_address3='".addslashes($addresses["address_line3"])."', sh_address4='".addslashes($addresses["address_line4"])."', sh_city_code='".addslashes($addresses["city_code"])."', sh_city='".addslashes($addresses["city"])."', sh_state='".addslashes($addresses["state"])."', sh_zip='".addslashes($addresses["zip"])."', sh_phone='".$addresses["phone"]."', sh_ext='".$addresses["ext"]."', sh_phone2='".$addresses["phone2"]."', sh_ext2='".$addresses["ext2"]."', destination_type='".$addresses['destination_type']."', gift_text='".addslashes($_SESSION['gift_text'])."', signature='".addslashes($_SESSION['signature'])."', payment_method='8', cctype='".$cctype."', transaction_id='".$_POST['reference_pol']."', ip='".$_POST['ip']."', status='0', status_since=NOW(), discount_code='".$_POST['discount_code']."', discount='".$_POST['discount']."', subtotal='".$subtotal."', tax='".$tax."', shipping='".$shipping."', order_currency='".$_SESSION['currency']."', total_usd='".$total_usd."', ship_method='".$_SESSION['shipopt']."', ups_error='".$_POST['ups_error']."', drop_ship='".$drop_ship."', user_num='".$_SESSION['user']."', referrer_seq='".$_SESSION['referrer_seq']."', currency='".$currency."'".$querydeliverydate, "From: <".$conf_admin_mail.">", "-f ".$conf_admin_mail);
				} else {
					// - Orders Status Ctrl - Inicio ----------------------
					$status_user_by = 0; if ( isset($_SESSION['user']) ) $status_user_by = $_SESSION['user'];
					$device_type = ''; if ( isset($_SESSION['device_type']) ) $device_type = $_SESSION['device_type'];

$drop_ship_upd_order_num = $order_num;
$drop_ship_upd = 0;
$sql_drop_ship_upd = 'SELECT drop_ship FROM orders WHERE order_num = "' . $drop_ship_upd_order_num . '"';
$pre_drop_ship_upd = mysqli_query($conn, $sql_drop_ship_upd);
if (mysqli_num_rows($pre_drop_ship_upd)) {
	$reg_drop_ship_upd = mysqli_fetch_assoc($pre_drop_ship_upd);
	$drop_ship_upd = $reg_drop_ship_upd['drop_ship'];
}

					$sql_orders_history = "
						INSERT INTO orders_history SET 
							order_num = $order_num,
							drop_ship = $drop_ship_upd,
							device_type = '$device_type',
							status = '0',
							status_since = NOW(),
							status_user_by = $status_user_by,
							status_cust_code_by = $cust_code";
					mysqli_query($conn, $sql_orders_history);
					// - Orders Status Ctrl - Fin -------------------------							
				}
				reset ($_SESSION['product_num']);
				do {
					$key = key($_SESSION['product_num']);
					mysqli_query($conn, "INSERT INTO orders_products SET order_num='".$order_num."', product_num='".current($_SESSION['product_num'])."', product_price='".$_SESSION['price'][$key]."', product_quantity='".$_SESSION['product_quantity'][$key]."'");
				} while ( next($_SESSION['product_num']) );
			}
			// if ( $drop_ship > 0 ) {
				// recalculate_order_cost($order_num,$drop_ship);
				// recalculate_order_charge($order_num,$drop_ship,$currency);
			// }
			if ( isset($_SESSION['pre_order_num']) ) {
				mysqli_query($conn, 'UPDATE pre_orders SET placed=NOW() WHERE pre_order_num="'.$_SESSION['pre_order_num'].'"');
			}
		
			$ch_ip = curl_init("http://geoip3.maxmind.com/b?l=29gZpPNXhIMj&i=".$_POST['ip']);
			curl_setopt ($ch_ip, CURLOPT_HEADER, 0);
			curl_setopt ($ch_ip, CURLOPT_RETURNTRANSFER, 1);
			$response = curl_exec ($ch_ip);
			$ip_field = explode(',' , str_replace('(null)','',$response));
			curl_close ($ch_ip);
			mysqli_query($conn, "UPDATE orders SET ip_country='".addslashes(utf8_encode($ip_field[0]))."', ip_state='".addslashes(utf8_encode($ip_field[1]))."', ip_city='".addslashes(utf8_encode($ip_field[2]))."' WHERE order_num='".$order_num."'");

			$ch_cy = curl_init('http://abandonment.saas.seewhy.com:80/abandonment2/WE/seewhy.nogif/1?Event=WebEvent&CustomerCode=9006794118&SessionID='.$customers['email'].'&UserID='.$customers['email'].'&FunnelLevel=7&Section=Order&OrderNumber='.$order_num.'&Value='.$total_usd);
			curl_setopt ($ch_cy, CURLOPT_HEADER, 0);
			curl_setopt ($ch_cy, CURLOPT_RETURNTRANSFER, 1);
			$cy_response = curl_exec($ch_cy);
			curl_close($ch_cy);
		
			if ( $customers["position"] != "" ) {
				$position = $customers["position"]."<br>\n\t\t\t\t\t\t";
			}
			if ( $customers["company"] != "" ) {
				$position .= $customers["company"]."<br>\n\t\t\t\t\t\t";
			}
			if ( $customers["phone"] != "" ) {
				$phones = $customers["phone"]."<br>";
			}
			if ( trim($customers["alt_phone"]) != "" ) {
				$phones .= $customers["alt_phone"]."<br>";
			}
			if ( $addresses["position"] != "" ) {
				$sh_position = $addresses["position"]."<br>\n\t\t\t\t\t\t";
			}
			if ( $addresses["company"] != "" ) {
				$sh_position .= $addresses["company"]."<br>\n\t\t\t\t\t\t";
			}
			$pre_cities = mysqli_query($conn, "SELECT area_code FROM cities WHERE city_code=".$addresses["city_code"]);
			$cities = mysqli_fetch_assoc($pre_cities);
			if ( $addresses["phone"] != "" ) {
				$sh_phones = $cities['area_code'].$addresses["phone"];
				if($addresses["ext"]!=""){
					$sh_phones .= " - ".$addresses["ext"];
				}
				$sh_phones .= "<br>";
			}
			if ( trim($addresses["phone2"]) != "" ) {
				$sh_phones .= $cities['area_code'].$addresses["phone2"];
				if($addresses["ext2"]!=""){
					$sh_phones .= " - ".$addresses["ext2"];
				}
				$sh_phones .= "<br>";
			}
			if ( $customers["address2"] != "" ) {
				$address = $customers["address1"]."<br>\n\t\t\t\t\t\t\t\t\t".$customers["address2"]."<br>\n\t\t\t\t\t\t\t\t\t";
			} else {
				$address = $customers["address1"]."<br>\n\t\t\t\t\t\t\t\t\t";
			}
			if ( $addresses["address_line2"] != "" ) {
				$sh_address = $addresses["address_line1"]."<br>\n\t\t\t\t\t\t\t\t\t".$addresses["address_line2"]."<br>\n\t\t\t\t\t\t\t\t\t";
			} else {
				$sh_address = $addresses["address_line1"]."<br>\n\t\t\t\t\t\t\t\t\t";
			}
			$pre_country = mysqli_query($conn, "SELECT country_".$_SESSION['lang']." FROM countries WHERE country_code='".$customers["country"]."'");
			$country = fx_mysqli_result ($pre_country, 0, 0);
			$pre_country = mysqli_query($conn, "SELECT country_".$_SESSION['lang']." FROM countries WHERE country_code='".$addresses["country"]."'");
			$sh_country = fx_mysqli_result ($pre_country, 0, 0);
			$phones_txt = str_replace("<br>", "\n", $phones);
			$sh_phones_txt = str_replace("<br>","\n", $sh_phones);
			$address_txt = str_replace("<br>\n", "\n", str_replace("\t", "", $address));
			$sh_address_txt = str_replace("<br>\n", "\n", str_replace("\t", "", $sh_address));
			$numrows = 5;
			if ( $_POST['discount'] > 0 ) {
				$discount_box = "
						<td align=right><font face=\"Verdana, Arial, Helvetica\" size=2>".$txt_discount.":</font></td>
						<td align=right><font face=\"Verdana, Arial, Helvetica\" size=2>- \$".number_format($_POST['discount'],$_SESSION['decimal_places'])."</font></td>
					</tr>
					<tr>";
				$numrows = 6;
			}
			$email = $customers["email"];
			$separator = "DAF".time();
			$headers = "From: \"".$conf_store_name."\" <".$txt_sender_email.">
MIME-Version: 1.0
Content-Disposition: inline
Content-Transfer-Encoding: 8bit
Content-Type: multipart/alternative; boundary=\"----Version Separator----".$separator."\"";
			$subject = $txt_order_confirmation." ".$order_num;
			if ( strlen($_SESSION['gift_text']) > 0 ) {
				//		$subject_notify = "** ".$txt_gift." ** ".$subject;
				$subject_notify = $subject;
				$email_gift_text = "\n\t\t\t\t<tr>
							<td colspan=2 bgcolor=\"#eeeeee\">
								<font face=\"Verdana, Arial, Helvetica\" size=2>
								<b>".$txt_gift_card.":</b><br>
								".htmlspecialchars($_SESSION['gift_text'])."<br><br>
								".htmlspecialchars($_SESSION['signature'])."<br><br>
								</font>
							</td>
						</tr>";
			} else {
				$subject_notify = $subject;
			}
			$first_name = $customers["first_name"];
			$last_name  = $customers["last_name"];

			$message = "------Version Separator----".$separator."
Content-Type: text/plain; charset=\"utf-8\"
Content-Transfer-Encoding: 8bit

".$customers["first_name"].",

".$txt_email_intro."

".$txt_order_number.": ".$order_num."

".$txt_order_placed.": ".date("Y-m-d h:i A")."

".substr($emaildeliverydate,0,-4)."

".$txt_bill_to."
".$customers["first_name"]." ".$customers["last_name"]."
".$position.$address_txt.$phones_txt.$customers["city"].", ".$customers["state"]." ".$customers["zip"]."
".$country."

".$txt_ship_to."
".$addresses["first_name"]." ".$addresses["last_name"]."
".$sh_position.$sh_address_txt.$sh_phones_txt.$addresses["city"].", ".$addresses["state"]." ".$addresses["zip"]."
".$sh_country."

".$plainlist."

".$txt_subtotal.": \$".number_format($subtotal,$_SESSION['decimal_places'])."
".$txt_tax.": \$".number_format($tax,$_SESSION['decimal_places'])."
".$txt_shipping.": \$".number_format($shipping,$_SESSION['decimal_places'])."
".$txt_total.": \$".number_format($total,$_SESSION['decimal_places'])."

".$txt_email_cc_note.$txt_email_end."

".$txt_email_note."

".$txt_best_regards."

".$txt_signature."
".$txt_homepage."

------Version Separator----".$separator."
Content-Type: text/html; charset=\"utf-8\"
Content-Transfer-Encoding: 8bit

<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html lang=\"".$_SESSION['lang']."\">
<body bgcolor=\"#ffffff\">
<table border=1 bordercolor=\"#9cb8c6\" width=530 align=\"center\" cellspacing=0 cellpadding=0>
<tr><td>
<a href=\"https://".$txt_homepage."/\"><img src=\"".$conf_image_src."/images/eml_head_thank_".$_SESSION['lang'].".jpg\" border=0 alt=\"".$txt_storename."\"></a>
</td></tr>
</table>
<table border=1 bordercolor=\"#9cb8c6\" width=530 align=\"center\" cellspacing=0 cellpadding=10>
	<tr valign=\"top\">
		<td>
			<font face=\"Verdana, Arial, Helvetica\" size=\"2\">
			<b>".$customers["first_name"].",</b><br><br>
			".$txt_email_intro."<br><br>
			<b>".$txt_order_number.": ".$order_num."</b><br>
			".$txt_order_placed.": ".date("Y-m-d h:i A")."<br>
			".$emaildeliverydate."<br>
			<table width=95% align=center border=0 cellpadding=5 cellspacing=0>
				<tr valign=top>
					<td width=250>
						<table width=95% border=1 align=left cellpadding=5 cellspacing=0>
							<tr>
								<td width=50%>
									<font face=\"Verdana, Arial, Helvetica\" size=\"2\">
									<b>".$txt_bill_to."</b><br>
									".$customers["first_name"]." ".$customers["last_name"]."<br>
									".$position.$address.$phones."
									".$customers["city"].", ".$customers["state"]." ".$customers["zip"]."<br>
									".$country."
									</font>
								</td>
							</tr>
						</table>
					</td>
					<td width=250>
						<table width=95% border=1 align=right cellpadding=5 cellspacing=0>
							<tr>
								<td>
									<font face=\"Verdana, Arial, Helvetica\" size=2>
									<b>".$txt_ship_to."</b><br>
									".$addresses["first_name"]." ".$addresses["last_name"]."<br>
									".$sh_position.$sh_address.$sh_phones."
									".$addresses["city"].", ".$addresses["state"]." ".$addresses["zip"]."<br>
									".$sh_country."
									</font>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan=2>
						<br>
						<table width=500 align=center border=0 cellpadding=5 cellspacing=0>
							<tr bgcolor=\"#bbbbbb\">
								<td width=120><font face=\"Verdana, Arial, Helvetica\" size=2><b>".$txt_reference."</b></font></td>
								<td width=150><font face=\"Verdana, Arial, Helvetica\" size=2><b>".$txt_description."</b></font></td>
								<td align=center><font face=\"Verdana, Arial, Helvetica\" size=2><b>".$txt_quantity."</b></font></td>
								<td align=center><font face=\"Verdana, Arial, Helvetica\" size=2><b>".$txt_price."</b></font></td>
							</tr>
".$listitems."							<tr><td colspan=5><hr></td></tr>
							<tr valign=top>
								<td colspan=2 rowspan=".$numrows.">
									&nbsp;
								</td>".$discount_box."
								<td align=right><font face=\"Verdana, Arial, Helvetica\" size=2><b>".$txt_subtotal.":</b></font></td>
								<td align=right><font face=\"Verdana, Arial, Helvetica\" size=2><b>\$".number_format($subtotal,$_SESSION['decimal_places'])."</b></font></td>
							</tr>
							<tr>
								<td align=right><font face=\"Verdana, Arial, Helvetica\" size=2>".$txt_tax.":</font></td>
								<td align=right><font face=\"Verdana, Arial, Helvetica\" size=2>\$".number_format($tax,$_SESSION['decimal_places'])."</font></td>
							</tr>
							<tr>
								<td align=right><font face=\"Verdana, Arial, Helvetica\" size=2>".$txt_shipping.":</font></td>
								<td align=right><font face=\"Verdana, Arial, Helvetica\" size=2>\$".number_format($shipping,$_SESSION['decimal_places'])."</font></td>
							</tr>
							<tr>
								<td align=right><font face=\"Verdana, Arial, Helvetica\" size=2><b>".$txt_total.":</b></font></td>
								<td align=right><font face=\"Verdana, Arial, Helvetica\" size=2><b>\$".number_format($total,$_SESSION['decimal_places'])."</b></font></td>
							</tr>
						</table>
					</td>
				</tr>".$email_gift_text."
			</table>
			".$txt_email_end."<br><br>".$txt_email_note."
			".$txt_best_regards."<br><br><br>
			".$txt_signature."<br>
			<a href=\"https://".$txt_homepage."/\">".$txt_homepage."</a><br><br></font>
		</td>
	</tr>
</table><br>
<table border=0 width=530 align=\"center\" cellspacing=0 cellpadding=10>
<tr><td>
<font face=\"Verdana, Arial, Helvetica\" size=\"1\" color=\"#888888\">
".str_replace("</h2>","</b>",str_replace("<h2>","<b>",$txt_terms))."
</font>
</td></tr>
</table><br><br>
</body>
</html>


------Version Separator----".$separator."--";

			if ( $opt_devel_local == 1 ) {
				mail ("developers@totalcode-software.com", $subject, $message, $headers, "-f ".$txt_sender_email);
			} else {
				mail ($conf_admin_mail, $subject_notify, $message, $headers, "-f ".$txt_sender_email);
				mail ($email, $subject, $message, $headers, "-f ".$txt_sender_email);
			}

		} else {
			mail ( 'developers@totalcode.com', 'Error PagosOnline - daFlores', 'Pre-orden no existe o ya autorizada - '.$pre_order_num.' - $'.$_POST['value'], 'From: <mailer@'.$_SERVER['SERVER_NAME'].'>', '-f mailer@'.$_SERVER['SERVER_NAME']);
		}
	}
} else {
	mail ('developers@totalcode.com', 'Error PagosOnline - daFlores', 'Falta Total o Firma no cuadra', 'From: <mailer@'.$_SERVER['SERVER_NAME'].'>', '-f mailer@'.$_SERVER['SERVER_NAME']);
}
	
?>