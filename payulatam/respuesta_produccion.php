<!DOCTYPE html>
<html lang="es">
<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
  
		<title>Bittat - Carrito de Compras</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="https://www.bittathome.com/assets/tienda/css/layout.css?v=0.2.1.0.0.3.6" type="text/css" rel="stylesheet">
		<link href="https://www.bittathome.com/assets/tienda/css/layout_fix.css?v=0.2.1.0.0.3.6" type="text/css" rel="stylesheet">
		<link href="https://www.bittathome.com/assets/tienda/css/header.css?v=0.2.1.0.0.3.6" type="text/css" rel="stylesheet">
		<link href="https://www.bittathome.com/assets/tienda/css/footer.css?v=0.2.1.0.0.3.6" type="text/css" rel="stylesheet">
		<link href="https://www.bittathome.com/assets/tienda/css/mobile.css?v=0.2.1.0.0.3.6" type="text/css" rel="stylesheet" media="only screen and (max-width: 1024px)">

		<link rel="apple-touch-icon" sizes="57x57" href="https://www.bittathome.com/assets/tienda/favicon/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="https://www.bittathome.com/assets/tienda/favicon/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="https://www.bittathome.com/assets/tienda/favicon/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="https://www.bittathome.com/assets/tienda/favicon/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="https://www.bittathome.com/assets/tienda/favicon/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="https://www.bittathome.com/assets/tienda/favicon/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="https://www.bittathome.com/assets/tienda/favicon/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="https://www.bittathome.com/assets/tienda/favicon/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="https://www.bittathome.com/assets/tienda/favicon/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="https://www.bittathome.com/assets/tienda/favicon/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="https://www.bittathome.com/assets/tienda/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="https://www.bittathome.com/assets/tienda/favicon/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="https://www.bittathome.com/assets/tienda/favicon/favicon-16x16.png">

		<!-- css fontawesome -->
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css?v=0.2.1.0.0.3.6" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
		<!-- css fontawesome fin -->
</head>
<body>

	<header class="bg-header" id="header">
		<div class="cont-logo">
			<div id="logo">
				<a href="https://www.bittathome.com/">
					<img src="https://www.bittathome.com/assets/tienda/images/home-bittat-group.png" alt="bittat logo">
				</a>
			</div>

			<div class="whatsapp-header">
				<a href="https://api.whatsapp.com/send?phone=573229440083" target="_blank">
					<i class="fab fa-whatsapp"></i>
					<span>322 944 0083</span>
				</a>
			</div>
		</div>

	</header>

	<section id="content">
		<div class="contenido-plano">
			<div class="titulos">
				<h2>RESUMEN TRANSACCIÓN</h2>
			</div>

			<div class="cont-plano">

  
<?php

if ($_SERVER['SERVER_NAME'] == "bittathome.com") {
    //$active_group = 'produccion';
	$servername = "localhost";
	$username = "bittathome_com";
	$password = "tQn18Y3XSecGsaM";
	$dbname = "bittathome_com";    
} else {
    //$active_group = 'local';
	$servername = "localhost";
	$username = "root";
	$password = "root";
	$dbname = "bittat";    
}

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$estado = 0;



$ApiKey = "1C5rPZ7Xx1FCcsK3m03GUyiSzB";
$merchant_id = $_REQUEST['merchantId'];

$referenceCode = $_REQUEST['referenceCode'];


$TX_VALUE = $_REQUEST['TX_VALUE'];
$New_value = number_format($TX_VALUE, 1, '.', '');
$currency = $_REQUEST['currency'];
$transactionState = $_REQUEST['transactionState'];
$firma_cadena = "$ApiKey~$merchant_id~$referenceCode~$New_value~$currency~$transactionState";
$firmacreada = md5($firma_cadena);
$firma = $_REQUEST['signature'];
$reference_pol = $_REQUEST['reference_pol'];
$cus = $_REQUEST['cus'];
$extra1 = $_REQUEST['description'];
$pseBank = $_REQUEST['pseBank'];
$lapPaymentMethod = $_REQUEST['lapPaymentMethod'];
$transactionId = $_REQUEST['transactionId'];

if ($_REQUEST['transactionState'] == 4 ) {
	$estadoTx = "Transacción aprobada";
	$estado = 1007;
}

else if ($_REQUEST['transactionState'] == 6 ) {
	$estadoTx = "Transacción rechazada";
	$estado = 1015;
}

else if ($_REQUEST['transactionState'] == 104 ) {
	$estadoTx = "Error";
	$estado = 0;
}

else if ($_REQUEST['transactionState'] == 7 ) {
	$estadoTx = "Transacción pendiente";
	$estado = 1006;
}

else {
	$estadoTx=$_REQUEST['mensaje'];
	$estado = 0;
}

$sql = "
	UPDATE ordenes SET 
		estado='".$estado."', 
		estado_fecha = NOW(), 
		proceso_referencia = 1 
	WHERE 
		codigo_referencia_pago='".$referenceCode."'";


if ($conn->query($sql) === TRUE) {
    //echo "Record updated successfully";
} else {
    //echo "Error updating record: " . $conn->error;
}


$conn->close();


if (strtoupper($firma) == strtoupper($firmacreada)) {
?>
	<table>
	<tr>
	<td>Estado de la transaccion</td>
	<td><?php echo $estadoTx; ?></td>
	</tr>
	<tr>
	<tr>
	<td>ID de la transaccion</td>
	<td><?php echo $transactionId; ?></td>
	</tr>
	<tr>
	<td>Referencia de la venta</td>
	<td><?php echo $reference_pol; ?></td>
	</tr>
	<tr>
	<td>Referencia de la transaccion</td>
	<td><?php echo $referenceCode; ?></td>
	</tr>
	<tr>
	<?php
	if($pseBank != null) {
	?>
		<tr>
		<td>cus </td>
		<td><?php echo $cus; ?> </td>
		</tr>
		<tr>
		<td>Banco </td>
		<td><?php echo $pseBank; ?> </td>
		</tr>
	<?php
	}
	?>
	<tr>
	<td>Valor total</td>
	<td>$<?php echo number_format($TX_VALUE); ?></td>
	</tr>
	<tr>
	<td>Moneda</td>
	<td><?php echo $currency; ?></td>
	</tr>
	<tr>
	<td>Descripción</td>
	<td><?php echo ($extra1); ?></td>
	</tr>
	<tr>
	<td>Entidad:</td>
	<td><?php echo ($lapPaymentMethod); ?></td>
	</tr>
	</table>
<?php
}
else
{
?>
	<h1>Error validando firma digital.</h1>
<?php
}
?>

			</div>
		</div>
	</section>
  
	<!-- ir al inicio -->
	<a href="#header" id="top">
		<span class="icon icon40 icon-top"></span>
	</a>
	<!-- ir al inicio fin -->

	<footer class="footer-checkout">
		<div class="datos-contacto">
			<ul>
				<li><h6>DATOS DE CONTACTO</h6></li>
				<li><i class="fas fa-phone"></i>(+57 1) 2851914</li>
				<li><i class="fas fa-mobile-alt"></i>(+57) 322 944 0083</li>
				<li><i class="fas fa-envelope-open"></i>servicioalcliente@solucionaticos.com</li>
				<li><i class="fas fa-home"></i>Cra 15 No 78-33 Local 2270 </li>
				<li><i class="fas fa-globe-asia"></i>Bogotá - Colombia</li>
			</ul>
				<ul class="logo-footer">
					<li>
					 	<img src="https://www.bittathome.com/assets/tienda/images/home-bittat-group.png" alt="bittat logo">
					</li>
				</ul>
			</ul>
		</div>

		<div class="copy-footer">
			<p>©2018 Todos los derechos reservados a Domotica</p>
		</div>
	</footer>

</body>
</html>